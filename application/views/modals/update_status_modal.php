<!-- updateTalentStat MODAL -->
<?php
  $att = array(
            'id' => 'updateTalentStatForm',
            'novalidate' => 'novalidate'
        );
  echo form_open('administrator/update_talent_status/', $att);
?>
<div id="updateTalentStat" class="modal modal-fixed-footer">
  <div class="modal-content">
    <h4 class="center-align">Update Talent Status</h4>
    <br><br>
    <div class="row">
      <div class="input-field col s12 m12">
          <select id="status" name="status" required="" aria-required="true">
            <option value="" disabled selected>Choose one</option>
            <option value="active">Active</option>
            <option value="inactive">Inactive</option>
          </select>
          <label>Status</label>
      </div>
      <div class="input-field col s12 m12">
        <textarea id="stat_remarks" name="stat_remarks" class="materialize-textarea" length="150"></textarea>
        <label for="stat_remarks">Remarks</label>
      </div>
    </div>
  </div>
  <div class="modal-footer">
      <a class="modal-action modal-close waves-effect btn-flat">Cancel</a>
      <button type="submit" id="submit-update" disabled="disabled" id="submit" class="modal-action modal-close waves-effect btn-flat">Save</button>
  </div>
</div>
</form>
<!-- /. updateTalentStat MODAL -->

<!-- updateTalentStatModal -->
<script>
$(document).on("click", "#updateTalentStatModal", function () {
      $('#updateTalentStat').openModal();
      var talentId = $(this).data('id');
      $("#talentId").val( talentId );
      $('#updateTalentStatForm').attr('action','<?php echo site_url();?>administrator/update_talent_status/'+talentId);
});
</script>
<!-- ./ updateTalentStatModal -->
