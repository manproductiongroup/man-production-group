<br>

<div class="right-align" id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'en', includedLanguages: 'en,ja,tl', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
<?php if($this->session->flashdata('sales')){?>
  <script type="text/javascript">
    $(document).ready(function(){
        swal({
          title: "Done!",
          text:  '<?php echo $this->session->flashdata('sales');?>',
          type: "success",
          timer: 3000,
          showConfirmButton: false
        });
    });
  </script>
<?php }?>

<?php if($this->session->flashdata('expense')){?>
  <script type="text/javascript">
    $(document).ready(function(){
        swal({
          title: "Done!",
          text:  '<?php echo $this->session->flashdata('expense');?>',
          type: "success",
          timer: 3000,
          showConfirmButton: false
        });
    });
  </script>
<?php }?>

<?php if($this->session->flashdata('reports')){?>
  <script type="text/javascript">
    $(document).ready(function(){
        swal({
          title: "Error in Date!",
          text:  '<?php echo $this->session->flashdata('reports');?>',
          type: "error",
          timer: 3000,
          showConfirmButton: false
        });
    });
  </script>
<?php }?>

<div class="section no-pad-bot" id="index-banner">
  <div class="container">
    <br><br>
    <h3 class="header center indigo-text text-darken-4"><?php echo $prodName ?></h3>
    <br><br>
  </div>
</div>

<div class="container">
  <br><br>

      <h5 class="center">Expenses</h5>
      <br>

      <div class="container">

          <table class="bordered responsive-table" style="margin-bottom:100px">
            <thead>
              <tr>
                  <th data-field="">Expense ID</th>
                  <th data-field="">Entry Name</th>
                  <th data-field="">Price</th>
                  <th data-field="">Date</th>
                  <th data-field="">Option</th>

              </tr>
            </thead>

            <tbody>
            <?php if($expenses):?>
            <?php foreach($expenses as $r){?>
              <tr>
                <td><?php echo $r->expenseId ?></td>
                <td><?php echo $r->entryName ?></td>
                <td><?php echo $r->price?></td>
                <td><?php echo ($r->date == '0000-00-00') ? '' : date_format(date_create($r->date),"m/d/Y") ;?></td>
                <td>
                  <a onclick="delete_expense(<?= $r->expenseId?>)" class="btn-floating btn-small waves-effect waves-light red darken-3 tooltipped" data-position="right" data-delay="50" data-tooltip="Delete Expense"><i class="material-icons">delete</i></a>
                </td>
              </tr>
            <?php }?>
          <?php else:?>
            <td>No record</td><td></td><td></td><td></td><td></td>
          <?php endif?>
            </tbody>
          </table>
      </div>

  <!-- <div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
    <a class="btn-floating btn-large waves-effect waves-light green-dark modal-trigger" href="#addClub"><i class="material-icons">add</i></a>
  </div> -->
</div>

<div class="container">
  <br><br>

      <h5 class="center">Sales</h5>
      <br>

      <div class="container">

          <table class="bordered responsive-table" style="margin-bottom:100px">
            <thead>
              <tr>
                  <th data-field="">Sales ID</th>
                  <th data-field="">Talent</th>
                  <th data-field="">Gross</th>
                  <th data-field="">Talent Fee</th>
                  <th data-field="">Managers Commission</th>
                  <th data-field="">Total</th>
                  <th data-field="">Date</th>
                  <th data-field="">Option</th>
              </tr>
            </thead>

            <tbody>
            <?php if($sales):?>
            <?php foreach($sales as $r){?>
              <tr>
                <td><?php echo $r->salesId ?></td>
                <td><?php echo $r->firstName." ".$r->lastName ?></td>
                <td>¥ <?= number_format($r->gross,2) ?></td>
                <td>¥ <?= number_format($r->talentFee,2) ?></td>
                <td>¥ <?= number_format($r->managerCom,2) ?></td>
                <td>¥ <?= number_format($r->total,2) ?></td>
                <td><?php echo ($r->dateSale == '0000-00-00') ? '' : date_format(date_create($r->dateSale),"m/d/Y") ;?></td>
                <td>
                  <a onclick="delete_expense(<?= $r->salesId?>)" class="btn-floating btn-small waves-effect waves-light red darken-3 tooltipped" data-position="right" data-delay="50" data-tooltip="Delete Sale"><i class="material-icons">delete</i></a>
                </td>
              </tr>
            <?php }?>
          <?php else:?>
            <td>No record</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
          <?php endif?>
            </tbody>
          </table>
      </div>

  <!-- <div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
    <a class="btn-floating btn-large waves-effect waves-light green-dark modal-trigger" href="#addClub"><i class="material-icons">add</i></a>
  </div> -->
</div>

<div class="fixed-action-btn vertical" style="bottom: 45px; right: 24px;">
    <a class="btn-floating btn-large red">
      <i class="material-icons">menu</i>
    </a>
    <ul>
      <li><a href="#viewReport" class="btn-floating blue tooltipped modal-trigger" data-position="left" data-delay="50" data-tooltip="View Report"><i class="fa fa-calendar"></i></a></li>
      <li><a href="#addExpense" class="btn-floating yellow darken-2 tooltipped modal-trigger" data-position="left" data-delay="50" data-tooltip="Add Expenses"><i class="fa fa-bank"></i></a></li>
      <li><a href="#addSales" class="btn-floating green darken-1 tooltipped modal-trigger" data-position="left" data-delay="50" data-tooltip="Add Sales"><i class="fa fa-yen"></i></a></li>
    </ul>
  </div>


<!-- add expense modal -->
<div id="addExpense" class="modal modal-fixed-footer">
<div class="modal-content">
  <h4 class="center-align">新しい経費を追加</h4>
  <h5 class="center-align">Add New Expense</h5>

  <br><br>
  <?php echo form_open('production/add_expense'); ?>

    <div class="input-field col s12">
      <input id="entry" name="entry" type="text" class="validate">
      <label for="entry">Entry Name</label>
    </div>

    <div class="input-field col s12">
      <input id="price" name="price" type="text" class="validate">
      <label for="price">Price</label>
    </div>

    <div class="input-field col s12">
      <input id="date" name="date" type="text" class="datepicker" required/>
      <label for="date">Date</label>
    </div>

</div>
<div class="modal-footer">
  <a  class="modal-action modal-close waves-effect waves-red btn-flat ">cancel</a>
  <button  type="submit" class="modal-action modal-close waves-effect waves-green btn-flat ">Add</button>
</div>
<?php echo form_close() ?>
</div>
<!-- end expense club modal -->

<!-- add sales modal -->
<div id="addSales" class="modal modal-fixed-footer">
<div class="modal-content">
  <h4 class="center-align">新発売を追加</h4>
  <h5 class="center-align">Add New Sale</h5>

  <br><br>
  <?php echo form_open('production/add_sales'); ?>

    <div class="input-field col s12">
      <select name="talentId">
        <option value="" disabled selected>Choose a talent</option>
        <?php foreach ($talents as $row) {?>
          <option value="<?php echo $row->talentId ?>"><?php echo $row->firstName." ".$row->lastName ?></option>
        <?php } ?>
      </select>
      <label>Talent</label>
    </div>
    <br>

    <div class="input-field col s12">
      <input id="gross" name="gross" type="number" class="validate">
      <label for="gross">Gross</label>
    </div>

    <div class="input-field col s12">
      <input id="talentFee" name="talentFee" type="number" class="validate">
      <label for="talentFee">Talent Fee</label>
    </div>

    <div class="input-field col s12">
      <input id="commission" name="commission" type="number" class="validate">
      <label for="commission">Manager Commission</label>
    </div>

    <div class="input-field col s12">
      <input id="date" name="date" type="text" class="datepicker" required/>
      <label for="date">Date</label>
    </div>

</div>
<div class="modal-footer">
  <a  class="modal-action modal-close waves-effect waves-red btn-flat ">cancel</a>
  <button  type="submit" class="modal-action modal-close waves-effect waves-green btn-flat ">Add</button>
</div>
<?php echo form_close() ?>
</div>
<!-- end sales club modal -->

<!-- view report modal -->
<div id="viewReport" class="modal modal-fixed-footer">
<div class="modal-content">
  <h4 class="center-align">レポートを見ます</h4>
  <h5 class="center-align">View Report</h5>

  <br><br>
  <?php echo form_open('production/view_report'); ?>

  <div class="row">
    <div class="input-field col s6">
      <input id="startDate" name="startDate" type="text" class="datepicker" required/>
      <label for="startDate">Start Date [開始日]</label>
    </div>

    <div class="input-field col s6">
      <input id="endDate" name="endDate" type="text" class="datepicker" required/>
      <label for="endDate">End Date [終了日]</label>
    </div>
  </div>
</div>
<div class="modal-footer">
  <a  class="modal-action modal-close waves-effect waves-red btn-flat ">cancel</a>
  <button  type="submit" class="modal-action modal-close waves-effect waves-green btn-flat ">Accept</button>
</div>
<?= form_close()?>
</div>
<!-- end view report modal -->



<script>
$(document).ready(function() {
// the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
$('.modal-trigger').leanModal();
});

// datepicker
$( document ).ready(function() {
  $('.datepicker').pickadate({
       container: 'body',
       selectMonths: true,
       selectYears: 60,
       today: '',
       formatSubmit: 'yyyy/mm/dd',
       format: 'yyyy/mm/dd'
  });
});

$(document).ready(function() {
  $('select').material_select();
});
</script>

<script>
function delete_expense(id){
  swal({    title: "Confirmation",
              text: "Are you sure you want to delete expense entry #"+id+"?",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#f44336",
              confirmButtonText: "Yes, delete it!",
              closeOnConfirm: false },
              function(isConfirm){
                if (isConfirm) {
                    $.ajax({
                        url : "<?php echo site_url('production/delete_expenses')?>/"+id,
                        type: "POST",
                        dataType: "JSON",
                        success: function(data)
                        {
                          swal("Deleted!", "Expense entry #"+id+" successfully deleted!", "success");
                          location.reload(true);
                        },
                        error: function (jqXHR, textStatus, errorThrown)
                        {
                          alert('An error occured while deleting data');
                        }
                    });
                }
              });

}

function delete_sale(id){
  swal({    title: "Confirmation",
              text: "Are you sure you want to delete sale entry #"+id+"?",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#f44336",
              confirmButtonText: "Yes, delete it!",
              closeOnConfirm: false },
              function(isConfirm){
                if (isConfirm) {
                    $.ajax({
                        url : "<?php echo site_url('production/delete_sales')?>/"+id,
                        type: "POST",
                        dataType: "JSON",
                        success: function(data)
                        {
                          swal("Deleted!", "Sale entry  #"+id+" successfully deleted!", "success");
                          location.reload(true);
                        },
                        error: function (jqXHR, textStatus, errorThrown)
                        {
                          alert('An error occured while deleting data');
                        }
                    });
                }
              });

}
</script>
