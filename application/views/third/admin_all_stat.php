<br>

<div class="right-align" id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'en', includedLanguages: 'en,ja,tl', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
<div class="section no-pad-bot" id="index-banner">
  <div class="container">
    <br><br>
    <h3 class="header center cyan-text text-darken-3"><?php echo strtoupper($this->uri->segment('3'))?></h3>
    <br><br>
  </div>
</div>

<div class="container">
  <!-- get talent list -->
    <table class="bordered" style="margin-bottom:100px">
      <thead>
        <tr>
            <th>Talent ID</th>
            <th>Talent Name</th>
            <th>Club</th>
            <th>Production</th>
            <?php if($this->uri->segment('3') == 'injapan' || $this->uri->segment('3') == 'coeapp' ){?>
            <th>COE Number</th>
            <?php }?>
            <?php if($this->uri->segment('3') == 'docsend'){?>
            <th>Remarks</th>
            <?php }?>
            <th>Status</th>
            <th>Date</th>


        </tr>
      </thead>

      <tbody>
        <?php foreach($records as $r){?>
        <tr>
            <td><?php echo $r->talentId ?></td>
            <td><?php echo $r->firstName." ".$r->midName." ".$r->lastName ?></td>
            <td><?php echo $r->clubName ?></td>
            <td><?php echo $r->productionName ?></td>
            <?php if($this->uri->segment('3') == 'injapan' || $this->uri->segment('3') == 'coeapp' ){?>
            <td><?php echo $r->coeNumber ?></td>
            <?php }?>
            <?php if($this->uri->segment('3') == 'docsend'){?>
            <td><?php echo $r->remakrs ?></td>
            <?php }?>
            <td><?php echo strtoupper($r->status) ?></td>
            <td><?php echo $r->date ?></td>
            <!-- <td>
              <a href="<? echo site_url('administrator')?>" class="btn-floating btn-small waves-effect waves-light blue tooltipped edit_club"  data-clubid = "<?php echo $r->clubId ?>" data-position="right" data-delay="50" data-tooltip="Edit Club"><i class="material-icons">mode_edit</i></a>
              <a class="btn-floating btn-small waves-effect waves-light red darken-3 tooltipped" data-position="right" data-delay="50" data-tooltip="Delete Club" onclick="delete_club(<?php echo $r->clubId ?>)"><i class="material-icons">delete</i></a>
            </td> -->
        </tr>
        <?php } ?>
      </tbody>
    </table>
</div>


<script>
$(document).ready(function() {
  // the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
  $('.modal-trigger').leanModal();
});
</script>

<script>
$(document).ready(function() {
   $('select').material_select();
 });
</script>

<script>
$(document).ready(function(){
    $('.tooltipped').tooltip({delay: 50});
  });
</script>
