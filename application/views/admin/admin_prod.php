<br>

<div class="right-align" id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'en', includedLanguages: 'en,ja,tl', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
  <div class="section no-pad-bot" id="index-banner">
      <div class="container">
        <br><br>
        <h3 class="header center indigo-text text-darken-3"><?php echo $prodName ?></h3>
        <br><br>
      </div>
    </div>

    <div class="container">
      <!-- get talent list -->
        <table class="bordered" style="margin-bottom:100px">
          <thead>
            <tr>
                <th data-field="">Club Name</th>
                <th data-field="">Newly Hired</th>
                <th data-field="">Documents Forwarded</th>
                <th data-field="">COE Application</th>
                <th data-field="">COE Release</th>
                <th data-field="">In Japan</th>
            </tr>
          </thead>

          <tbody>
          <?php foreach($clubs as $r){?>
            <tr>
              <td><a href="<?= site_url('administrator/get_all_club/'.$r->clubName.'/'.$r->clubId)?>"><?php echo $r -> clubName; ?></a></td>

              <td><a href="<?php echo site_url('administrator/get_all_stat_club/hired/'.$r->clubId)?>">
              <?php foreach($hired as $jap){?>
              <?php if( $r->clubId == $jap->clubId):?>
              <?php echo $jap->count?>
              <?php endif ?>
              <?php }?>
              </a></td>

              <td><a href="<?php echo site_url('administrator/get_all_stat_club/docsend/'.$r->clubId)?>">
              <?php foreach($docsend as $jap){?>
              <?php if( $r->clubId == $jap->clubId):?>
              <?php echo $jap->count?>
              <?php endif ?>
              <?php }?>
              </a></td>

              <td><a href="<?php echo site_url('administrator/get_all_stat_club/coeapp/'.$r->clubId)?>">
              <?php foreach($coeapp as $jap){?>
              <?php if( $r->clubId == $jap->clubId):?>
              <?php echo $jap->count?>
              <?php endif ?>
              <?php }?>
              </a></td>

              <td><a href="<?php echo site_url('administrator/get_all_stat_club/coerelease/'.$r->clubId)?>">
              <?php foreach($coerelease as $jap){?>
              <?php if( $r->clubId == $jap->clubId):?>
              <?php echo $jap->count?>
              <?php endif ?>
              <?php }?>
              </a></td>

              <td><a href="<?php echo site_url('administrator/get_all_stat_club/injapan/'.$r->clubId)?>">
              <?php foreach($injapan as $jap){?>
              <?php if( $r->clubId == $jap->clubId):?>
              <?php echo $jap->count?>
              <?php endif ?>
              <?php }?>
              </a></td>
            </tr>
          <?php }?>
          </tbody>
        </table>
    </div>
