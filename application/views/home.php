<style>
body {
  display: flex;
  min-height: 100vh;
  flex-direction: column;
}

main {
  flex: 1 0 auto;
}
</style>
<main>
<body class="grey lighten-4">
  <nav class="grey darken-3" role="navigation">
    <div class="nav-wrapper container"><a id="logo-container" href="#" class="brand-logo center">Man Group Production</a>
    </div>
  </nav>
<br><br><br><br><br><br><br><br><br><br>
  <div class="container">
    <div class="section">

      <!--   Icon Section   -->
      <div class="row">

        <div class="col s12 m6">
          <div class="icon-block">
            <h2 class="center indigo-text"><i class="material-icons">group</i></h2>
            <h5 class="center indigo-text">Production Login</h5>
            <center>
              <a href="<?php echo site_url();?>prod_login" class="btn waves-effect waves-light indigo">Go to Production Login
               <i class="material-icons right">send</i>
              </a>
           </center>
          </div>
        </div>

        <div class="col s12 m6">
          <div class="icon-block">
            <h2 class="center teal-text"><i class="material-icons">settings</i></h2>
            <h5 class="center teal-text">Admin Login</h5>
            <center>
              <a href="<?php echo site_url();?>admin_login" class="btn waves-effect waves-light teal">Go to Admin Login
               <i class="material-icons right">send</i>
             </a>
           </center>
          </div>
        </div>
      </div>

    </div>
    <br><br>

    <div class="section">

    </div>
  </div>
</main>
  <footer class="page-footer grey darken-3">
    <div class="footer-copyright center">
      <div class="container">
      Powered by <a class="orange-text" href="#">Tweebs</a>
      </div>
    </div>
  </footer>
