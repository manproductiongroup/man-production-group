<!-- editTalent MODAL -->
<?php
  $att = array(
          'id' => 'editTalentForm',
          'novalidate' => 'novalidate'
        );
  echo form_open('administrator/edit_talent/', $att);
?>
<div id="editTalent" class="modal modal-fixed-footer">
  <div class="modal-content">
    <h4 class="center-align">編集芸能人データ</h4>
    <h5 class="center-align">Edit Talent</h5>
    <br>
      <div class="row">
        <div class="input-field col s8">
          <input placeholder="" id="edit-talent-name" name="edit-talent-name" type="text" required="" aria-required="true"/>
          <label for="edit-talent-name">Talent Name [タレント名]</label>
        </div>
        <div class="input-field col s4">
            <select id="edit-category" name="edit-category" required="" aria-required="true">
              <option value="" disabled selected>Choose one</option>
              <option value="Dancer PRO">Dancer PRO</option>
              <option value="Dancer NON-PRO">Dance NON-PRO</option>
              <option value="Singer PRO">Singer PRO</option>
              <option value="Singer NON-PRO">Singer NON-PRO</option>
            </select>
            <label>Category</label>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s4">
          <input placeholder="" id="edit-first-name" name="edit-first-name" type="text" required="" aria-required="true">
          <label for="edit-first-name">First Name</label>
        </div>
        <div class="input-field col s4">
          <input placeholder="" id="edit-middle-name" name="edit-middle-name" type="text" required="" aria-required="true">
          <label for="edit-middle-name">Middle Name</label>
        </div>
        <div class="input-field col s4">
          <input placeholder="" id="edit-last-name" name="edit-last-name" type="text" required="" aria-required="true">
          <label for="edit-last-name">Last Name</label>
        </div>
      </div>
      <br>
      <div class="row">
        <div class="input-field col s8">
          <select id="edit-club-name" name="edit-club-name" required="" aria-required="true">
            <option value="" disabled selected>Choose one</option>
            <?php if(isset($clubs)) : foreach($clubs as $club):?>
            <option value="<?php echo $club->clubId; ?>"><?php echo $club->clubName; ?></option>
            <?php endforeach; ?>
          </select>
          <?php else: ?>
          <input disabled value="" id="club-name" name="club-name" type="text" class="validate">
          <?php endif; ?>
          <label for="edit-club-name">Club Name [経歴出演店]</label>
        </div>
        <div class="input-field col s4">
          <input placeholder="" id="edit-birthdate" name="edit-birthdate" type="text" class="datepicker" required="" aria-required="true">
          <label for="edit-birthdate">Birthdate [生年月日]</label>
        </div>
      </div>
      <div class="row">
         <div class="input-field col s12">
           <textarea placeholder="" id="edit-birthplace" name="edit-birthplace" class="materialize-textarea"></textarea>
           <label for="edit-birthplace">Birthplace [出生地]</label>
         </div>
       </div>
       <div class="row">
          <div class="input-field col s12">
            <input placeholder="" id="edit-manager" name="edit-manager" type="text">
            <label for="edit-manager">Manager [マネージャ]</label>
          </div>
        </div>
        <br>
        <div class="row">
         <div class="input-field col s4">
           <select id="edit-production" name="edit-production" required="" aria-required="true">
             <option value="" disabled selected>Choose one</option>
             <?php if(isset($production)) : foreach($production as $prod):?>
             <option value="<?php echo $prod->productionId; ?>"><?php echo $prod->productionName; ?></option>
             <?php endforeach; ?>
           </select>
           <?php else: ?>
           <input disabled value="No production" id="production" type="text">
           <?php endif; ?>
           <label for="edit-production">Production [日本プロダクション]</label>
         </div>
         <div class="input-field col s4">
           <input placeholder="" id="edit-processing" name="edit-processing" type="text" class="datepicker">
           <label for="edit-processing">Processing [比国エージェント]</label>
         </div>
         <div class="input-field col s4">
           <input placeholder="" id="edit-doc-send" name="edit-doc-send" type="text" class="datepicker">
           <label for="edit-doc-send">Document Send [ドキュメント入手日]</label>
         </div>
       </div>
       <div class="row">
         <div class="input-field col s6">
           <input placeholder="" id="edit-apply" name="edit-apply" type="text" class="datepicker">
           <label for="edit-apply">Apply [申請日]</label>
         </div>
         <div class="input-field col s6">
             <input placeholder="" id="edit-vis-lic" name="edit-vis-lic" type="text" class="datepicker" required="" aria-required="true">
           <label for="edit-visa-lic">VISA License [VISA交付日]</label>
         </div>
       </div>
       <div class="row">
         <div class="input-field col s4">
           <input placeholder="" id="edit-in-japan" name="edit-in-japan" type="text" class="datepicker">
           <label for="edit-in-japan">In Japan [入国日]</label>
         </div>
         <div class="input-field col s4">
           <input placeholder="" id="edit-extension" name="edit-extension" type="text" class="datepicker">
           <label for="edit-edit-extension">Extension [更新日]</label>
         </div>
         <div class="input-field col s4">
           <input placeholder="" id="edit-fin-visa" name="edit-fin-visa" type="text" class="datepicker" required="" aria-required="true">
           <label for="edit-fin-visa">Finish VISA [帰国日]</label>
         </div>
       </div>
       <div class="row">
          <div class="input-field col s4">
            <input placeholder="" id="edit-pass-number" name="edit-pass-number" type="text">
            <label for="edit-pass-number">Passport Number [パスポートNo]</label>
          </div>
          <div class="input-field col s4">
            <input placeholder="" id="edit-pass-exp" name="edit-pass-exp" type="text" class="datepicker">
            <label for="edit-pass-exp">Expiry Date of Passport [期限]</label>
          </div>
          <div class="input-field col s4">
            <input placeholder="" id="edit-issu" name="edit-issu" type="text">
            <label for="edit-issu">ISSU [発行日]</label>
          </div>
       </div>
       <!-- IMAGES -->
       <!-- IMAGE 1 -->
       <!--<div class="row">
        <div class="file-field input-field">
         <div class="btn">
            <span>File</span>
            <input type="file" name="image1">
          </div>
          <div class="file-path-wrapper">
             <input id="edit-image1" class="file-path validate" type="text">
          </div>
         </div>
       </div>
       <!-- IMAGE 2
       <div class="row">
        <div class="file-field input-field">
         <div class="btn">
            <span>File</span>
            <input type="file" name="image2">
          </div>
          <div class="file-path-wrapper">
             <input id="edit-image2" class="file-path validate" type="text">
          </div>
         </div>
       </div>
       <!-- IMAGE 3
       <div class="row">
        <div class="file-field input-field">
         <div class="btn">
            <span>File</span>
            <input type="file" name="image3">
          </div>
          <div class="file-path-wrapper">
             <input id="edit-image3" class="file-path validate" type="text">
          </div>
         </div>
       </div>
       <!-- IMAGE 4
       <div class="row">
        <div class="file-field input-field">
         <div class="btn">
            <span>File</span>
            <input type="file" name="image4">
          </div>
          <div class="file-path-wrapper">
             <input id="edit-image4" class="file-path validate" type="text">
          </div>
         </div>
       </div>-->
  </div>
  <div class="modal-footer">
      <a class="modal-action modal-close waves-effect btn-flat">Cancel [キャンセル]</a>
      <button type="submit" id="submit-edit" class="modal-action modal-close waves-effect btn-flat">Save [セーブ]</button>
  </div>
</div>
</form>
<!-- /. editTalent MODAL -->

<!-- editTalentModal -->
<script>
$(document).on("click", "#editTalentModal", function () {
      $('#editTalent').openModal();
      var talentId = $(this).data('id');

      $('#editTalentForm').attr('action','<?php echo site_url();?>administrator/edit_talent/'+talentId);

      console.log('EDIT',talentId);
      $.ajax({
        type: "GET",
        url: "<?php echo site_url();?>administrator/view_talent/"+talentId,
        dataType: "JSON",
        success:function(data){
          $('#editTalent').openModal();
          $(".modal-content #edit-talent-name").val(data[0].talentName);
          $(".modal-content #edit-first-name").val(data[0].firstName);
          $(".modal-content #edit-middle-name").val(data[0].midName);
          $(".modal-content #edit-last-name").val(data[0].lastName);
          $(".modal-content #edit-birthdate").val(data[0].bdate);
          $(".modal-content #edit-birthplace").val(data[0].bop);
          $(".modal-content #edit-manager").val(data[0].manager);
          $(".modal-content #edit-processing").val(data[0].processing);
          $(".modal-content #edit-doc-send").val(data[0].docSend);
          $(".modal-content #edit-apply").val(data[0].apply);
          $(".modal-content #edit-vis-lic").val(data[0].visaLicense);
          $(".modal-content #edit-in-japan").val(data[0].inJapana);
          $(".modal-content #edit-extension").val(data[0].extension);
          $(".modal-content #edit-fin-visa").val(data[0].finishVisa);
          $(".modal-content #edit-pass-number").val(data[0].passportNo);
          $(".modal-content #edit-pass-exp").val(data[0].passExp);
          $(".modal-content #edit-issu").val(data[0].issu);
          $(".modal-content #edit-image1").val(data[0].pic1);
          $(".modal-content #edit-image2").val(data[0].pic2);
          $(".modal-content #edit-image3").val(data[0].pic3);
          $(".modal-content #edit-image4").val(data[0].pic4);
        },
      });
});
</script>
<!-- ./ editTalentModal -->
