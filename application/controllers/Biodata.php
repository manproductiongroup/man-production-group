<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Biodata extends CI_Controller {

  public function __construct() {
		parent::__construct();
		$this -> load -> model('production_model');
		$this -> load -> model('Administrator_model');
	}

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/home
	 *	- or -
	 * 		http://example.com/index.php/welcome/home
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

   function talent_biodata()
 	{
 		$id = $this->session->userdata('userId');
 		$key = "1234MaNpr0";
 		$uri_id = $this->uri->segment(3);
 		$talentId = base64_decode($uri_id);
 		$decrypted_id = preg_replace(sprintf('/%s/', $key), '', $talentId);

 		$data['talent'] = $this->Administrator_model->get_talents_where($decrypted_id);
 		$data['salary'] = $this->Administrator_model->get_talents_salary($decrypted_id);
 		$data['production'] = $this->Administrator_model->get_production();
 		$data['clubs'] = $this->Administrator_model->get_clubs();
 		$data['agencies'] = $this->Administrator_model->get_agencies();
 		$data['accountName'] = $this->Administrator_model->get_accountName($id);
    $data['user_type'] = $this->session->userdata('type');
 		$data['main_content'] = 'admin/biodata';
 		$this->load->view('includes/prodtemplate',$data);
 	}

  function edit_talent_bio()
	{
		$talentId = $this->uri->segment(3);
		$this->Administrator_model->edit_talent_bio($talentId);
		$this->Administrator_model->edit_talent_salary($talentId);
		$this->session->set_flashdata('success', 'Talent data have been succesfully updated!');
	}

}
