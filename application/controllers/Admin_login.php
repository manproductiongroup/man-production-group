<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header('Content-Type: text/html;charset=utf-8');

class Admin_login extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/home
	 *	- or -
	 * 		http://example.com/index.php/welcome/home
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function index()
	{
			$data['main_content'] = 'admin/admin_login';
	    $this->load->view('includes/template',$data);
	}

	function validate_account()
	{
  		$this->form_validation->set_rules('username', 'Username', 'required|callback_check_account');
  		$this->form_validation->set_rules('password', 'Password','required');
      if($this->form_validation->run() == false){
         redirect('admin_login');
      }
      else{
					// $data = array(
					// 		'username' => $this->input->post('username'),
					// 		'is_logged_in' => true,
					// );
					// $this->session->set_userdata($data);
          redirect('administrator');
      }
    }

	 function check_account()
   {
			$this->load->model('Administrator_model');
			$user = $this->input->post('username');
			$pass = $this->input->post('password');

      $query = $this->Administrator_model->check_account_db($user,$pass);
      if($query){
      	return true;
      }
      else{
        return false;
      }
   }
}
