<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Third extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->is_logged_in_prod();
		$this -> load -> model('production_model');
		$this -> load -> model('Administrator_model');
	}

	function is_logged_in_prod()
	{
			$is_logged_in_prod = $this->session->userdata('is_logged_in_prod');

			if(!isset($is_logged_in_prod) || $is_logged_in_prod != true)
			{
					redirect('prod_login');
			}
			else{
					return true;
			}
	}

	public function index()
	{
		$user = $this -> session -> userdata('username');
		$id = $this -> session -> userdata('userId');
		$prodid = $this -> session -> userdata('productionId');
		$data = array();

		if ($query = $this -> production_model -> get_clubs($prodid)) {
			$data['clubs'] = $query;
		}

		if ($query = $this -> production_model -> get_talents($prodid)) {
			$data['talents'] = $query;
		}
		$data['accountName'] = $this -> session -> userdata('accountName');
		$data['prodName'] = $this -> production_model -> get_prodName($prodid);
		$data['docsend'] = $this -> production_model -> count_docsend($prodid);
		$data['coeapp'] = $this -> production_model -> count_coeapp($prodid);
		$data['injapan'] = $this -> production_model -> count_injapan($prodid);
		$data['coerelease'] = $this -> production_model -> count_coerelease($prodid);
		$data['hired'] = $this -> production_model -> count_hired($prodid);

		$data['main_content'] = 'third/production';
		$this -> load -> view('includes/thirdtemplate', $data);
	}

	function add_clubs(){
		$this->production_model->add_clubs();
		redirect('production');

	}

	function delete_clubs(){
		$id = $this->uri->segment(3);
		$this->production_model->delete_clubs($id);
		redirect('production');

	}

	function clubs(){
		$data['accountName'] = $this -> session -> userdata('accountName');
		$user = $this -> session -> userdata('username');
		$id = $this -> session -> userdata('userId');
		$prodid = $this -> session -> userdata('productionId');
		$data['clubs'] = $this -> production_model -> get_clubs($prodid);
		$data['talents'] = $this -> production_model -> get_all_talents($prodid);
		$data['prodName'] = $this -> production_model -> get_prodName($prodid);

		$data['main_content'] = 'third/prod_clubs';
		$this -> load -> view('includes/thirdtemplate', $data);

	}

	function get_status(){
		$id = $this->uri->segment(3);
		$prodid = $this -> session -> userdata('productionId');
		$data['accountName'] = $this -> session -> userdata('accountName');
		$data['prodName'] = $this -> production_model -> get_prodName($prodid);
		$data['clubName'] = $this -> production_model -> get_clubName($id);
		$data['main_content'] = 'clubs';
		$data['status'] = $this->production_model->get_status($id);

		$data['main_content'] = 'third/prod_charts';
		$this -> load -> view('includes/thirdtemplate', $data);

	}

	function reports(){

		$data['accountName'] = $this -> session -> userdata('accountName');
		$user = $this -> session -> userdata('username');
		$id = $this -> session -> userdata('userId');
		$prodid = $this -> session -> userdata('productionId');
		$data['prodName'] = $this -> production_model -> get_prodName($prodid);
		$data['talents'] = $this -> production_model -> get_talents($prodid);
		$data['sales'] = $this -> production_model -> get_sales($prodid);
		$data['expenses'] = $this -> production_model -> get_expenses($prodid);

		$data['main_content'] = 'third/prod_reports';
		$this -> load -> view('includes/thirdtemplate', $data);

	}

	function view_report(){
		$start = $this->input->post('startDate');
		$end =  $this->input->post('endDate');
		if($end < $start){
				$this->session->set_flashdata('reports', 'Ending date must be greater than the starting date');
				redirect('production/reports');
		}else{
			$data['start'] = $start;
			$data['end'] = $end;
			$data['accountName'] = $this -> session -> userdata('accountName');
			$user = $this -> session -> userdata('username');
			$id = $this -> session -> userdata('userId');
			$prodid = $this -> session -> userdata('productionId');
			$data['prodName'] = $this -> production_model -> get_prodName($prodid);
			$data['sales'] = $this -> production_model -> get_sales_date($prodid,$start,$end);
			$data['expenses'] = $this -> production_model -> get_expenses_date($prodid,$start,$end);
			$data['income'] = $this-> production_model -> get_income($prodid,$start,$end);

			$data['main_content'] = 'third/prod_income';
			$this -> load -> view('includes/thirdtemplate', $data);
		}

	}

	function add_sales(){
		$prodid = $this -> session -> userdata('productionId');
		$this->production_model->add_sales($prodid);
		$action = 'Added new sale entry';
		$this->production_model->add_logs($action);
		redirect('production/reports');
	}

	function delete_sales(){
		$id = $this->uri->segment('3');
		$this->production_model->delete_sales($id);
		$action = 'Deleted sale entry #'.$id;
		$this->production_model->add_logs($action);
		echo json_encode(array("status" => TRUE));
	}

	function add_expense(){
		$prodid = $this -> session -> userdata('productionId');
		$this->production_model->add_expense($prodid);
		$action = 'Added new expense entry';
		$this->production_model->add_logs($action);
		redirect('production/reports');
	}

	function delete_expenses(){
		$id = $this->uri->segment('3');
		$this->production_model->delete_expenses($id);
		$action = 'Deleted expense entry #'.$id;
		$this->production_model->add_logs($action);
		echo json_encode(array("status" => TRUE));
	}

	function get_all_stat_club(){
			$stat = $this->uri->segment('3');
			$club = $this->uri->segment('4');
			$id = $this-> session->userdata('userId');
			$data['accountName'] = $this->Administrator_model->get_accountName($id);
			$data['records'] = $this->Administrator_model->get_all_stat_club($stat,$club);
			$data['main_content'] = 'admin/admin_all_stat';
			$this->load->view('includes/thirdtemplate',$data);
	}

	function get_all_club(){
			$stat = $this->uri->segment(3);
			$club = $this->uri->segment(4);
			$id = $this-> session->userdata('userId');
			$data['accountName'] = $this->Administrator_model->get_accountName($id);
			$data['records'] = $this->Administrator_model->get_all_club($club);
			$data['main_content'] = 'admin/admin_all_stat';
			$this->load->view('includes/prodtemplate',$data);
	}

	public function admin()
	{
		$data['main_content'] = 'prod_admin';
		$this -> load -> view('includes/template', $data);
	}

	function logout(){
		$action = $this->session->userdata('accountName')." logged out.";
		$this->production_model->add_logs($action);
		$this->session->sess_destroy();
		redirect('prod_login');
	}
}
