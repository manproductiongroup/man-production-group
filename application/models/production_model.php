<?php

class Production_model extends CI_Model{


	function get_clubs($id){
		$this->db->where('productionId',$id);
		$query = $this->db->get('clubs');
		return $query->result();
	}

  function get_talents($id){
		$this->db->where('t.productionId',$id);
		$this->db->join('production p','p.productionId = t.productionId','right');
		$this->db->join('clubs c','c.clubId = t.clubId','right');
		$this->db->order_by('lastName');
		$query = $this->db->get('talents t');
		return $query->result();
	}

	function get_prodName($id){
		$this->db->where('productionId',$id);
		$query = $this->db->get('production');
		return $query->row('productionName');
	}

	function get_clubName($id){
		$this->db->where('clubId',$id);
		$query = $this->db->get('clubs');
		return $query->row('clubName');
	}

  function add_clubs(){
    $data = array(
      'clubName' => $this->input->post('clubName'),
      'capacity' => $this->input->post('capacity'),
      'productionId' => '1'
    );

    $this->db->insert('clubs',$data);

  }

	function count_docsend($id){
		$this->db->where('ts.status','docsend');
		$this->db->where('t.productionId',$id);
		$this->db->join('talents t', 't.talentId = ts.talentId', 'right');
		$this->db->select('count(*) as count, t.clubId as clubId');
		$this->db->group_by('t.clubId');
		$query = $this->db->get('talents_status ts');
		return $query->result();
	}

	function count_coeapp($id){
		$this->db->where('ts.status','coeapp');
		$this->db->where('t.productionId',$id);
		$this->db->join('talents t', 't.talentId = ts.talentId', 'right');
		$this->db->select('count(*) as count, t.clubId as clubId');
		$this->db->group_by('t.clubId');
		$query = $this->db->get('talents_status ts');
		return $query->result();
	}

	function count_coerelease($id){
		$this->db->where('ts.status','coerelease');
		$this->db->where('t.productionId',$id);
		$this->db->join('talents t', 't.talentId = ts.talentId', 'right');
		$this->db->select('count(*) as count, t.clubId as clubId');
		$this->db->group_by('t.clubId');
		$query = $this->db->get('talents_status ts');
		return $query->result();
	}

	function count_injapan($id){
		$this->db->where('ts.status','injapan');
		$this->db->where('t.productionId',$id);
		$this->db->join('talents t', 't.talentId = ts.talentId', 'right');
		$this->db->select('count(*) as count, t.clubId as clubId');
		$this->db->group_by('t.clubId');
		$query = $this->db->get('talents_status ts');
		return $query->result();
	}

	function count_hired($id){
		$this->db->where('ts.status','hired');
		$this->db->where('t.productionId',$id);
		$this->db->join('talents t', 't.talentId = ts.talentId', 'right');
		$this->db->select('count(*) as count, t.clubId as clubId');
		$this->db->group_by('t.clubId');
		$query = $this->db->get('talents_status ts');
		return $query->result();
	}

	function delete_clubs($id){
		$this->db->where('clubId',$id);
    $this->db->delete('clubs');

  }

	function get_status($id){
		$this->db->where('t.clubId',$id);
		$this->db->join('talents t', 't.talentId = ts.talentId', 'right');
		$this->db->select('count(*) as count,ts.status as status, ts.date as date');
		$this->db->group_by('ts.status');
		$this->db->group_by('ts.date');
		$this->db->order_by('date','asc');
		$q = $this->db->get('talents_status ts');
		return $q->result();
	}

	function get_sales($id){
		$this->db->where('s.productionId',$id);
		$this->db->join('talents t','t.talentId = s.talentId','right');
		$q = $this->db->get('sales s');
		return $q->result();
	}

	function get_sales_date($id,$start,$end){
		$this->db->where('s.productionId',$id);
		$this->db->where('s.dateSale >=',$start);
		$this->db->where('s.dateSale <=',$end);
		$this->db->join('talents t','t.talentId = s.talentId','right');
		$q = $this->db->get('sales s');
		return $q->result();
	}

	function get_expenses($id){
		$this->db->where('productionId',$id);
		$q = $this->db->get('expenses');
		return $q->result();
	}

	function get_expenses_date($id,$start,$end){
		$this->db->where('date >=',$start);
		$this->db->where('date <=',$end);
		$this->db->where('productionId',$id);
		$q = $this->db->get('expenses');
		return $q->result();
	}

	function get_income($id,$start,$end){

		$this->db->where('date >=',$start);
		$this->db->where('date <=',$end);
		$this->db->where('productionId',$id);
		$this->db->select('sum(price) as expense');
		$q1 = $this->db->get('expenses');
		$totalExpense = $q1->row('expense');

		$this->db->where('dateSale >=',$start);
		$this->db->where('dateSale <=',$end);
		$this->db->where('productionId',$id);
		$this->db->select('sum(total) as sales');
		$q2 = $this->db->get('sales');
		$totalSales = $q2->row('sales');

		$income = $totalSales - $totalExpense;

		return $income;
	}

	function delete_expenses($id){
		$this->db->where('expenseId',$id);
		$this->db->delete('expenses');
	}

	function delete_sales($id){
		$this->db->where('salesId',$id);
		$this->db->delete('sales');
	}

	function check_account_db($user,$pass){
				$type = array('p','t');
				$this->db->where_in('type', $type);
				// $this->db->or_where('type', 't');
        $this->db->where('username', $user);
        $this->db->where('password',$pass);
        $query = $this->db->get('accounts');

        if($query->num_rows() == 1)
        {
					$row = $query->row();
                $data = array(
                'username' => $row->username,
								'accountName' => $row->accountName,
                'userId' => $row->userId,
                'productionId' => $row->productionId,
								'type' => $row->type,
								'is_logged_in_prod' => true
                );
            $this->session->set_userdata($data);
						$this->session->set_flashdata('login', $row->accountName);
						$action = $this->session->userdata('accountName')." logged in.";
						$this->add_logs($action);
            return $row->type;
        }
        else{
						$this->session->set_flashdata('password', 'Wrong username / password');
            return false;
        }
    }

		function add_sales($id){
			$talcom = (double)$this->input->post('talentFee') + (double)$this->input->post('commission');
			$total = (double)$this->input->post('gross') - $talcom;

			$data = array(
	      'productionId' => $id,
	      'talentId' => $this->input->post('talentId'),
				'gross' => $this->input->post('gross'),
				'talentFee' => $this->input->post('talentFee'),
				'managerCom' => $this->input->post('commission'),
				'total' => $total,
				'dateSale' => $this->input->post('date')
	    );
			$this->session->set_flashdata('sales', 'Sales entry successfully added!');
			$this->db->insert('sales',$data);
		}

		function add_expense($id){
			$data = array(
				'productionId' => $id,
				'entryName' => $this->input->post('entry'),
				'price' => $this->input->post('price'),
				'date' => $this->input->post('date')
			);
			$this->session->set_flashdata('expense', 'Expense entry successfully added!');
			$this->db->insert('expenses',$data);
		}

		function add_logs($action){
	    $data = array(
	      'action' => $action,
	      'accountName' => $this->session->userdata('accountName')
	    );
	    $this->db->insert('system_logs',$data);
	  }

		function get_all_talents($prodid){
			$this->db->where('t.productionId',$prodid);
			$this->db->join('talents t', 't.talentId = ts.talentId', 'right');
			$this->db->join('sales s', 's.talentId = t.talentId','left');
			$this->db->join('agency a', 'a.agencyId = t.agency', 'left');
			$this->db->join('production p', 'p.productionId = t.productionId', 'right');
			$this->db->join('clubs c', 'c.clubId = t.clubId', 'right');
			$this->db->order_by('ts.date','desc');
			$query = $this->db->get('talents_status ts');
			return $query->result();
		}




}
