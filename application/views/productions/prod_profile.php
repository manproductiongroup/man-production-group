<div class="right-align" id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'en', includedLanguages: 'en,ja,tl', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>

<div class="container">
  <div class="row">
     <?php $att = array('class' => 'col s12', 'id' => 'formChangePass');
           echo form_open('production/change_password', $att);
     ?>
     <h1 class="center-align">Account Settings</h1>
       <div class="row">
         <div class="input-field col s12 m4 push-m4">
           <input id="current_pass" type="password" class="validate">
           <label for="current_pass">Current Password</label>
         </div>
       </div>
       <div class="row">
         <div class="input-field col s12 m4 push-m4">
           <input id="old_pass" type="password" class="validate">
           <label for="old_pass">Old Password</label>
         </div>
       </div>
       <div class="row">
         <div class="input-field col s12 m4 push-m4">
           <input id="new_pass" type="password" class="validate">
           <label for="new_pass">New Password</label>
         </div>
       </div>
       <br>
       <div class="row">
          <div class="input-field col s12 m4 push-m4">
           <input type="checkbox" id="show_pass" />
           <label for="show_pass">Show Passwords</label>
          </div>
       </div>
       <div class="row">
         <div class="input-field col s12 m4 push-m4">
            <button class="btn cyan waves-effect waves-light right" type="submit">Save
              <i class="material-icons right">send</i>
            </button>
          </div>
       </div>
     <?php echo form_close(); ?>
   </div>
</div>

<script>
(function() {
	var PasswordToggler = function( element, field ) {
		this.element = element;
		this.field = field;

		this.toggle();
	};

	PasswordToggler.prototype = {
		toggle: function() {
			var self = this;
			self.element.addEventListener( "change", function() {
				if( self.element.checked ) {
					self.field.setAttribute( "type", "text" );
				} else {
					self.field.setAttribute( "type", "password" );
				}
            }, false);
		}
	};

	document.addEventListener( "DOMContentLoaded", function() {
		var checkbox = document.querySelector( "#show_pass" ),
			curr_pwd = document.querySelector( "#current_pass" ),
      old_pwd = document.querySelector( "#old_pass" ),
      new_pwd = document.querySelector( "#new_pass" ),
			form = document.querySelector( "#formChangePass" );

			

			var toggler = new PasswordToggler( checkbox, curr_pwd );
      var toggler = new PasswordToggler( checkbox, old_pwd );
      var toggler = new PasswordToggler( checkbox, new_pwd );

	});

})();
</script>
