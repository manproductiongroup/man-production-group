<?php if($this->session->flashdata('success')){?>
  <script type="text/javascript">
    $(document).ready(function(){
     var $toastContent = $('<span><?php echo $this->session->flashdata('success');?></span>');
     Materialize.toast($toastContent, 5000)
    });
  </script>
<?php }?>
<?php if($this->session->flashdata('error')){?>
  <script type="text/javascript">
    $(document).ready(function(){
     var $toastContent = $('<span><?php echo $this->session->flashdata('error');?></span>');
     Materialize.toast($toastContent, 5000)
    });
  </script>
<?php }?>
<?php if($this->session->flashdata('error_1')){?>
  <script type="text/javascript">
    $(document).ready(function(){
     var $toastContent = $('<span>Image 1 : <?php echo $this->session->flashdata('error_1');?></span>');
     Materialize.toast($toastContent, 5000)
    });
  </script>
<?php }?>
<?php if($this->session->flashdata('error_2')){?>
  <script type="text/javascript">
    $(document).ready(function(){
     var $toastContent = $('<span>Image 2 : <?php echo $this->session->flashdata('error_2');?></span>');
     Materialize.toast($toastContent, 5000)
    });
  </script>
<?php }?>
<?php if($this->session->flashdata('error_3')){?>
  <script type="text/javascript">
    $(document).ready(function(){
     var $toastContent = $('<span>Image 3 : <?php echo $this->session->flashdata('error_3');?></span>');
     Materialize.toast($toastContent, 5000)
    });
  </script>
<?php }?>
<?php if($this->session->flashdata('error_4')){?>
  <script type="text/javascript">
    $(document).ready(function(){
     var $toastContent = $('<span>Image 4 : <?php echo $this->session->flashdata('error_4');?></span>');
     Materialize.toast($toastContent, 5000)
    });
  </script>
<?php }?>
