<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header('Content-Type: text/html;charset=utf-8');

class Administrator extends CI_Controller {
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/home
	 *	- or -
	 * 		http://example.com/index.php/welcome/home
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
 		parent::__construct();
	 	$this->is_logged_in();
 		$this->load->model('Administrator_model');
		$this->load->model('production_model');
 	}

	function is_logged_in()
	{
			$is_logged_in = $this->session->userdata('is_logged_in');

			if(!isset($is_logged_in) || $is_logged_in != true)
			{
					redirect('admin/admin_login');
			}
			else{
					return true;
			}
	}

	// DASHBOARD FUNCTIONS
	public function index()
	{
		$id = $this-> session->userdata('userId');
		$data['accountName'] = $this->Administrator_model->get_accountName($id);
		$data['prod'] = $this->Administrator_model->get_production();
		$data['clubs'] = $this->Administrator_model->get_clubs();
		$data['talentsCount'] = $this->Administrator_model->count_talents();
		$data['outjapan'] = $this -> Administrator_model -> count_outjapan();
		$data['injapan'] = $this -> Administrator_model -> count_injapan();
		$data['coerelease'] = $this -> Administrator_model -> count_coerelease();
		$data['coeapp'] = $this -> Administrator_model -> count_coeapp();
		$data['docsend'] = $this -> Administrator_model -> count_docsend();
		$data['hired'] = $this -> Administrator_model -> count_hired();
		$data['outjapanClub'] = $this -> Administrator_model -> count_outjapan_club();
		$data['injapanClub'] = $this -> Administrator_model -> count_injapan_club();
		$data['coereleaseClub'] = $this -> Administrator_model -> count_coerelease_club();
		$data['coeappClub'] = $this -> Administrator_model -> count_coeapp_club();
		$data['docsendClub'] = $this -> Administrator_model -> count_docsend_club();
		$data['hiredClub'] = $this -> Administrator_model -> count_hired_club();
		$data['cancel'] = $this -> Administrator_model -> count_cancel();

		$data['main_content'] = 'admin/admin';
    $this->load->view('includes/admintemplate',$data);
	}
	// DASHBOARD END FUNCTIONS

	function get_all_stat(){
			$stat = $this->uri->segment(3);
			$id = $this-> session->userdata('userId');
			$data['accountName'] = $this->Administrator_model->get_accountName($id);
			$data['records'] = $this->Administrator_model->get_all_stat($stat);
			$data['main_content'] = 'admin/admin_all_stat';
	    $this->load->view('includes/admintemplate',$data);
	}

	function get_all_stat_club(){
			$stat = $this->uri->segment(3);
			$club = $this->uri->segment(4);
			$id = $this-> session->userdata('userId');
			$data['accountName'] = $this->Administrator_model->get_accountName($id);
			$data['records'] = $this->Administrator_model->get_all_stat_club($stat,$club);
			$data['main_content'] = 'admin/admin_all_stat';
	    $this->load->view('includes/admintemplate',$data);
	}

	function get_all_club(){
			$stat = $this->uri->segment(3);
			$club = $this->uri->segment(4);
			$id = $this-> session->userdata('userId');
			$data['accountName'] = $this->Administrator_model->get_accountName($id);
			$data['records'] = $this->Administrator_model->get_all_club($club);
			$data['main_content'] = 'admin/admin_all_stat';
	    $this->load->view('includes/admintemplate',$data);
	}

	function get_all_prod(){
			$stat = $this->uri->segment(3);
			$prod = $this->uri->segment(4);
			$id = $this-> session->userdata('userId');
			$data['accountName'] = $this->Administrator_model->get_accountName($id);
			$data['records'] = $this->Administrator_model->get_all_prod($prod);
			$data['main_content'] = 'admin/admin_all_stat';
	    $this->load->view('includes/admintemplate',$data);
	}

	function agencies(){
		$id = $this->session->userdata('userId');
		$data['accountName'] = $this->Administrator_model->get_accountName($id);
    $data['agencies'] = $this->Administrator_model->get_agency();

		$data['main_content'] = 'admin/admin_agencies';
		$this->load->view('includes/admintemplate',$data);
	}

	function add_agency(){
		$this->Administrator_model->add_agency();
		redirect('administrator/agencies');
	}


		function delete_agency(){
				$id = $this->uri->segment(3);
				$this->Administrator_model->delete_agency($id);
				echo json_encode(array("status" => TRUE));
		}

	// PRODUCTION FUNCTIONS
	function productions()
  {
		$id = $this->session->userdata('userId');
		$data['accountName'] = $this->Administrator_model->get_accountName($id);
    $data['production'] = $this->Administrator_model->get_production();

		$data['main_content'] = 'admin/admin_production';
		$this->load->view('includes/admintemplate',$data);

  }

	function add_production(){
		$this->Administrator_model->add_production();
		redirect('administrator/productions');
	}

	function update_production(){
		$id = $this->input->post('productionId');
		$this->Administrator_model->update_production($id);
		$this->session->set_flashdata('success', 'Production have been succesfully updated!');
		redirect('administrator/productions');
	}

	function getProdId(){
			$data = $this->Administrator_model->get_prod_where($this->input->post('prodid'));
			echo json_encode($data);
	}

	function delete_production(){
			$id = $this->uri->segment(3);
			$this->Administrator_model->delete_production($id);
			echo json_encode(array("status" => TRUE));
	}
	// PRODUCTION END FUNCTIONS

	// CLUBS FUNCTIONS
	function clubs()
  {
		$id = $this->session->userdata('userId');

		$data['accountName'] = $this->Administrator_model->get_accountName($id);
		$data['production'] = $this->Administrator_model->get_production();
    $data['clubs'] = $this->Administrator_model->get_clubs();

		$data['main_content'] = 'admin/admin_clubs';
		$this->load->view('includes/admintemplate',$data);
  }

	function add_clubs(){
		$this->Administrator_model->add_clubs();
		$this->session->set_flashdata('success', 'Club have been succesfully added!');
		redirect('administrator/clubs');
	}

	function getClubId(){
			$data = $this->Administrator_model->get_club_where($this->input->post('clubid'));
			echo json_encode($data);
	}

	function update_club(){
		$id = $this->input->post('clubId');
		$this->Administrator_model->update_club($id);
		$this->session->set_flashdata('success', 'Club have been succesfully updated!');
		redirect('administrator/clubs');
	}

	function delete_club(){
			$id = $this->uri->segment(3);
			$this->Administrator_model->delete_club($id);
			echo json_encode(array("status" => TRUE));
	}
	// CLUBS END FUNCTIONS

	//TALENTS FUNCTIONS

	function talents()
	{
		$id = $this->session->userdata('userId');
		$data['accountName'] = $this->Administrator_model->get_accountName($id);

    $data['talents'] = $this->Administrator_model->get_talents();
		$data['production'] = $this->Administrator_model->get_production();
		$data['clubs'] = $this->Administrator_model->get_clubs();
		$data['agencies'] = $this->Administrator_model->get_agencies();

		$data['main_content'] = 'admin/admin_talents';
		$this->load->view('includes/admintemplate',$data);
	}

	function get_talents(){
		$talents = $this->Administrator_model->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($talents as $talent) {
			$key = "1234MaNpr0";
			$talent_id = base64_encode($key. $talent->talentId);
			$result = rtrim($talent_id,'=');

			if($talent->statusTalent == 'active'){
				$stat = '<i class="small material-icons green">check_circle</i>';
			}else{
				$stat = '<i class="small material-icons yellow">warning</i>';
			}
			$no++;
			$row = array();
			$row[] = $talent->talentId;
			$row[] = $stat;
			$row[] = '<a href="talent_biodata/'.$result.'" target="_blank">'.$talent->talentName.'</a>';
			$row[] = $talent->lastName;
			$row[] = $talent->firstName;
			$row[] = $talent->productionName;
			$row[] = $talent->clubName;

			//add html for action
			//<a id="editTalentModal" class="btn-floating btn-small waves-effect waves-light cyan darken-3 tooltipped" onclick="edit_talent('."'".$talent->talentId."'".') data-position="right" data-delay="50" data-tooltip="Edit Talent"><i class="material-icons">mode_edit</i></a>
			//onclick="update_talent_stat('."'".$talent->talentId."'".')"
			// <a id="updateTalentStatModal" class="btn-floating btn-small waves-effect waves-light teal darken-3 tooltipped" data-position="right" data-id="'.$talent->talentId.'" data-delay="50" data-tooltip="Update Talent Dates Status"><i class="material-icons">restore</i></a>
			$row[] = '
							      <a id="viewTalent" href="talent_biodata/'.$result.'" target="_blank" class="btn-floating btn-small waves-effect waves-light light-blue darken-3 tooltipped" data-position="right" data-delay="50" data-tooltip="View Talent"><i class="material-icons">search</i></a>
							      <a id="updateTalentStatModal" class="btn-floating btn-small waves-effect waves-light green darken-3 tooltipped" data-position="right" data-id="'.$talent->talentId.'" data-delay="50" data-tooltip="Update Talent Status"><i class="material-icons">restore</i></a>
							      <a class="btn-floating btn-small waves-effect waves-light red darken-3 tooltipped" data-position="right" data-delay="50" data-tooltip="Delete Talent" onclick="delete_talent('."'".$talent->talentId."'".')"><i class="material-icons">delete</i></a>';
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->Administrator_model->count_all(),
			"recordsFiltered" => $this->Administrator_model->count_filtered(),
			"data" => $data,
		);

		echo json_encode($output);
	}

	function talent_biodata()
	{
		$id = $this->session->userdata('userId');
		$key = "1234MaNpr0";
		$uri_id = $this->uri->segment(3);
		$talentId = base64_decode($uri_id);
		$decrypted_id = preg_replace(sprintf('/%s/', $key), '', $talentId);

		$data['talent'] = $this->Administrator_model->get_talents_where($decrypted_id);
		$data['salary'] = $this->Administrator_model->get_talents_salary($decrypted_id);
		$data['production'] = $this->Administrator_model->get_production();
		$data['clubs'] = $this->Administrator_model->get_clubs();
		$data['agencies'] = $this->Administrator_model->get_agencies();
		$data['accountName'] = $this->Administrator_model->get_accountName($id);
		$data['user_type'] = $this->session->userdata('type');

		$data['main_content'] = 'admin/biodata';
		$this->load->view('includes/admintemplate',$data);
	}

	function view_talent()
	{
		$talentId = $this->uri->segment(3);
		$data = $this->Administrator_model->get_talents_where($talentId);
		echo json_encode($data);
	}

	function update_talent_status()
	{
		$talentId = $this->uri->segment(3);
		$this->Administrator_model->update_talent_where($talentId);
		$this->session->set_flashdata('success', 'Talent status have been succesfully updated!');
		redirect('administrator/talents');
	}

	function edit_talent_bio()
	{
		$talentId = $this->uri->segment(3);
		$this->Administrator_model->edit_talent_bio($talentId);
		$this->Administrator_model->edit_talent_salary($talentId);
		$this->session->set_flashdata('success', 'Talent data have been succesfully updated!');
	}

	function edit_talent()
	{
		$talentId = $this->uri->segment(3);
    $talentName = $this->input->post('talent-name');
		$this->Administrator_model->edit_talent_where($talentId);
		$this->session->set_flashdata('success', 'Talent have been succesfully edited!');
		redirect('administrator/talents');
	}

	function delete_talent_where(){
		$talentId = $this->uri->segment(3);
		$this->Administrator_model->delete_talent_where($talentId);
		echo json_encode(array("status" => TRUE));
	}

	function add_talents()
	{
		$talent = $this->Administrator_model->add_talents();
		$this->Administrator_model->addStatus($talent);
		//needs validation
		if($this->input->post('grossSalary') != "" && $this->input->post('netSalary') != "" && $this->input->post('commission') != ""){
			$this->Administrator_model->add_salary($talent);
		}
		$def_img = base_url().'imgs/no_image_available.jpeg';
		$config = array(
        'upload_path'   =>  './uploads/talents/'.$talent,
        'allowed_types' =>  'jpg|jpeg|gif|png'
    );
		$this->load->library('upload', $config);
		$this->upload->initialize($config);

	 if (!is_dir('uploads/talents'))
	 {
			 mkdir('./uploads/talents', 0777, true);
	 }
	 		$dir_exist = true;
	 if (!is_dir('uploads/talents/' . $talent))
	 {
			 mkdir('./uploads/talents/' . $talent, 0777, true);
			 $dir_exist = false;
	 }
	 else{

	 }
	 //IMAGE1
	 if(!empty($_FILES['image1']['name']) && $_FILES['image1']['error'] != 4){
		    if (!$this->upload->do_upload('image1')) {
						 $this->session->set_flashdata('error_1',  $this->upload->display_errors());
						 redirect('administrator/talents', 'error_1');
		    }
		    else {
						$data = $this->upload->data();
						$image_url = base_url().'uploads/talents/'.$talent.'/'.$data['file_name'];
						$image1 = $image_url;
		    }
		}
		else {
				$image1 = $def_img;
		}

		//IMAGE2
		if(!empty($_FILES['image2']['name']) && $_FILES['image2']['error'] != 4){
				 if (!$this->upload->do_upload('image2')) {
							$this->session->set_flashdata('error_2',  $this->upload->display_errors());
							redirect('administrator/talents', 'error_2');
				 }
				 else {
						 $data = $this->upload->data();
						 $image_url = base_url().'uploads/talents/'.$talent.'/'.$data['file_name'];
						 $image2 = $image_url;
				 }
		 }
		 else {
				 $image2 = $def_img;
		 }

		 //IMAGE3
		 if(!empty($_FILES['image3']['name']) && $_FILES['image3']['error'] != 4){
				 if (!$this->upload->do_upload('image3')) {
							$this->session->set_flashdata('error_3',  $this->upload->display_errors());
							redirect('administrator/talents', 'error_3');
				 }
				 else {
						 $data = $this->upload->data();
						 $image_url = base_url().'uploads/talents/'.$talent.'/'.$data['file_name'];
						 $image3 = $image_url;
				 }
		 }
		 else {
				 $image3 = $def_img;
		 }

		 //IMAGE4
		 if(!empty($_FILES['image4']['name']) && $_FILES['image4']['error'] != 4){
				 if (!$this->upload->do_upload('image4')) {
							$this->session->set_flashdata('error_4',  $this->upload->display_errors());
							redirect('administrator/talents', 'error_4');
				 }
				 else {
					 $data = $this->upload->data();
					 $image_url = base_url().'uploads/talents/'.$talent.'/'.$data['file_name'];
					 $image4 = $image_url;
				 }
		 }
		 else {
				 $image4 = $def_img;
		 }

		$this->Administrator_model->upload_talent_images_where($image1, $image2, $image3, $image4, $talent);
		$this->session->set_flashdata('success', 'Talent have been succesfully added!');
		redirect('administrator/talents');
	}

	function upload_talent_images()
	{
		$talentId = $this->uri->segment(3);
		$talent = $this->uri->segment(3);
		// $talent = $this->input->post('talentName');
		$def_img = base_url().'imgs/no_image_available.jpeg';

		$def_img1 = $this->input->post('def_img1_val');
		$def_img2 = $this->input->post('def_img2_val');
		$def_img3 = $this->input->post('def_img3_val');
		$def_img4 = $this->input->post('def_img4_val');

		$config = array(
				'upload_path'   =>  './uploads/talents/'.$talent,
				'allowed_types' =>  'jpg|jpeg|gif|png'
		);
		$this->load->library('upload', $config);
		$this->upload->initialize($config);

	 if (!is_dir('uploads/talents'))
	 {
			 mkdir('./uploads/talents', 0777, true);
	 }
			$dir_exist = true;
	 if (!is_dir('uploads/talents/' . $talent))
	 {
			 mkdir('./uploads/talents/' . $talent, 0777, true);
			 $dir_exist = false;
	 }
	 else{

	 }
	 //IMAGE1
	 if(!empty($_FILES['image1']['name']) && $_FILES['image1']['error'] != 4){
				if (!$this->upload->do_upload('image1')) {
						 $this->session->set_flashdata('error_1',  $this->upload->display_errors());
						 redirect('administrator/talents', 'error_1');
				}
				else {
						$data = $this->upload->data();
						$image_url = base_url().'uploads/talents/'.$talent.'/'.$data['file_name'];
						$image1 = $image_url;
				}
		}
		else {
				$image1 = $def_img1;
		}

		//IMAGE2
		if(!empty($_FILES['image2']['name']) && $_FILES['image2']['error'] != 4){
				 if (!$this->upload->do_upload('image2')) {
							$this->session->set_flashdata('error_2',  $this->upload->display_errors());
							redirect('administrator/talents', 'error_2');
				 }
				 else {
						 $data = $this->upload->data();
						 $image_url = base_url().'uploads/talents/'.$talent.'/'.$data['file_name'];
						 $image2 = $image_url;
				 }
		 }
		 else {
				 $image2 = $def_img2;
		 }

		 //IMAGE3
		 if(!empty($_FILES['image3']['name']) && $_FILES['image3']['error'] != 4){
				 if (!$this->upload->do_upload('image3')) {
							$this->session->set_flashdata('error_3',  $this->upload->display_errors());
							redirect('administrator/talents', 'error_3');
				 }
				 else {
						 $data = $this->upload->data();
						 $image_url = base_url().'uploads/talents/'.$talent.'/'.$data['file_name'];
						 $image3 = $image_url;
				 }
		 }
		 else {
				 $image3 = $def_img3;
		 }

		 //IMAGE4
		 if(!empty($_FILES['image4']['name']) && $_FILES['image4']['error'] != 4){
				 if (!$this->upload->do_upload('image4')) {
							$this->session->set_flashdata('error_4',  $this->upload->display_errors());
							redirect('administrator/talents', 'error_4');
				 }
				 else {
					 $data = $this->upload->data();
					 $image_url = base_url().'uploads/talents/'.$talent.'/'.$data['file_name'];
					 $image4 = $image_url;
				 }
		 }
		 else {
				 $image4 = $def_img4;
		 }
	  $key = "1234MaNpr0";
	  $talent_id = base64_encode($key. $talentId);
	  $result = rtrim($talent_id,'=');
		$this->Administrator_model->upload_talent_images_where($image1, $image2, $image3, $image4, $talentId);
		$this->session->set_flashdata('success', 'Images have been succesfully updated!');
		redirect('administrator/talent_biodata/'.$result);
	}

	//END TALENTS FUNCTIONS

	// SYSTEM LOGS FUNCTIONS
	function system_logs(){
		$logs = $this->Administrator_model->get_logs();
		$data = array();
		$no = $_POST['start'];
		foreach ($logs as $log) {
			$no++;
			$row = array();
			$row[] = $log->logID;
			$row[] = $log->action;
			$row[] = $log->date;
			$row[] = $log->accountName;

			$data[] = $row;
		}

		$logs = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->Administrator_model->count_all_logs(),
			"recordsFiltered" => $this->Administrator_model->count_filtered_logs(),
			"data" => $data,
		);

		echo json_encode($logs);
	}

	function add_logs($action){
		$this->Administrator_model->add_logs($action);
	}
	// SYSTEM LOGS END FUCNTIONS

	// ACCOUNTS FUNCTIONS
	function accounts()
  {
		$id = $this->session->userdata('userId');
		$data['accountName'] = $this->Administrator_model->get_accountName($id);
		$data['production'] = $this->Administrator_model->get_production();
    $data['accounts'] = $this->Administrator_model->get_accounts();

		$data['main_content'] = 'admin/admin_accounts';
		$this->load->view('includes/admintemplate',$data);
  }

	function activate_account(){
		$id = $this->input->post('accountId');
		$status = $this->input->post('active');

		$this->Administrator_model->activate_account($id,$status);
		if($status == 0){
				$this->session->set_flashdata('success', 'Account has been enbled successfully!');
		}else{
				$this->session->set_flashdata('success', 'Account has been disenbled successfully!');
		}

		redirect('administrator/accounts');
	}

	function add_accounts(){
		$this->Administrator_model->add_accounts();
		$this->session->set_flashdata('success', 'Account have been succesfully added!');
		redirect('administrator/accounts');
	}

	function getUserId(){
			$data = $this->Administrator_model->get_user_where($this->input->post('userid'));
			echo json_encode($data);
	}

	function update_account(){
		$id = $this->input->post('userId');
		$this->Administrator_model->update_account($id);
		$this->session->set_flashdata('success', 'Account have been succesfully updated!');
		redirect('administrator/accounts');
	}

	function delete_account(){
			$id = $this->uri->segment(3);
			$this->Administrator_model->delete_account($id);
			echo json_encode(array("status" => TRUE));
	}

	// ACCOUNTS END FUNCTIONS

	// ADMIN TO PROD FUNCTIONS
	function view_prod($id){
		$data = array();

		if ($query = $this -> production_model -> get_clubs($id)) {
			$data['clubs'] = $query;
		}

		if ($query = $this -> production_model -> get_talents($id)) {
			$data['talents'] = $query;
		}
		$data['accountName'] = $this -> session -> userdata('accountName');
		$data['prodName'] = $this -> production_model -> get_prodName($id);
		$data['docsend'] = $this -> production_model -> count_docsend($id);
		$data['coeapp'] = $this -> production_model -> count_coeapp($id);
		$data['coerelease'] = $this -> production_model -> count_coerelease($id);
		$data['injapan'] = $this -> production_model -> count_injapan($id);
		$data['hired'] = $this -> production_model -> count_hired($id);
		$data['main_content'] = 'admin/admin_prod';
		$this -> load -> view('includes/admintemplate', $data);

	}

	function view_clubs(){
		$data['accountName'] = $this -> session -> userdata('accountName');
		$user = $this -> session -> userdata('username');
		$id = $this -> session -> userdata('userId');
		$prodid = $this->uri->segment(3);


		$data['clubs'] = $this -> production_model -> get_clubs($prodid);
		$data['talents'] =$this -> production_model -> get_all_talents($prodid);
		$data['prodName'] = $this -> production_model -> get_prodName($prodid);

		$data['main_content'] = 'admin/admin_prod_clubs';
		$this -> load -> view('includes/admintemplate', $data);
	}

	function view_charts(){
		$id = $this->uri->segment(4);
		$prodid = $this->uri->segment(3);
		$data['accountName'] = $this -> session -> userdata('accountName');
		$data['prodName'] = $this -> production_model -> get_prodName($prodid);
		$data['clubName'] = $this -> production_model -> get_clubName($id);
		$data['main_content'] = 'clubs';
		$data['status'] = $this->production_model->get_status($id);

		$data['main_content'] = 'admin/admin_prod_charts';
		$this -> load -> view('includes/admintemplate', $data);

	}

	function view_accounting(){


	}
	// ADMIN TO PROD FUNCTIONS

	function settings(){
		$data['accountName'] = $this -> session -> userdata('accountName');
		$data['main_content'] = 'admin/admin_settings';
		$this -> load -> view('includes/admintemplate', $data);
	}

	// DATABASE FUNCTIONS
	function backup_database(){
		$user = $this->input->post('dbuser');
		$pass = $this->input->post('dbpass');

		define("BACKUP_PATH", 'C:/xampp/htdocs/manpro/db_backup/');
		$server_name   = $this->db->hostname;
		$username      = $this->db->username;
		$password      = $this->db->password;
		$database_name = $this->db->database;
		$date_string   = date("Ymd");


		// if($user == $username && $pass == $password){
			$cmd = "C:/xampp/mysql/bin/mysqldump --routines -h {$server_name} -u {$username} -p {$password} {$database_name} > " . BACKUP_PATH . "{$date_string}_{$database_name}.sql";
			exec($cmd);
			$this->session->set_flashdata('dbsuccess', 'Database backup success!.');
			redirect('administrator/settings');
		// }
	}

	// DATABASE END FUNCTIONS

	// LOGOUT FUNCTION
	function logout()
	{
		$action = $this->session->userdata('accountName')." logged out.";
		$this->production_model->add_logs($action);
		$this->session->sess_destroy();
		redirect('admin_login');
	}
	// LOGOUT END FUNCTION

	function view_reports(){

		$data['accountName'] = $this -> session -> userdata('accountName');
		$user = $this -> session -> userdata('username');
		$id = $this -> session -> userdata('userId');
		$prodid = $this -> uri -> segment('3');
		$data['prodName'] = $this -> production_model -> get_prodName($prodid);
		$data['talents'] = $this -> production_model -> get_talents($prodid);
		$data['sales'] = $this -> production_model -> get_sales($prodid);
		$data['expenses'] = $this -> production_model -> get_expenses($prodid);

		$data['main_content'] = 'admin/admin_reports';
		$this -> load -> view('includes/admintemplate', $data);
	}

	function view_report(){
		$start = $this->input->post('startDate');
		$end =  $this->input->post('endDate');
		if($end < $start){
				$this->session->set_flashdata('reports', 'Ending date must be greater than the starting date');
				redirect('production/reports');
		}else{
			$data['start'] = $start;
			$data['end'] = $end;
			$data['accountName'] = $this -> session -> userdata('accountName');
			$user = $this -> session -> userdata('username');
			$id = $this -> session -> userdata('userId');
			$prodid = $this -> uri -> segment('3');
			$data['prodName'] = $this -> production_model -> get_prodName($prodid);
			$data['sales'] = $this -> production_model -> get_sales_date($prodid,$start,$end);
			$data['expenses'] = $this -> production_model -> get_expenses_date($prodid,$start,$end);
			$data['income'] = $this-> production_model -> get_income($prodid,$start,$end);

			$data['main_content'] = 'admin/admin_prod_income';
			$this -> load -> view('includes/admintemplate', $data);
		}

	}

	// VIEW ADMIN PROFILE FUNCTION
	function admin_profile(){
		$data['accountName'] = $this -> session -> userdata('accountName');

		$data['main_content'] = 'admin/admin_profile';
		$this->load->view('includes/admintemplate',$data);
	}
	// END VIEW ADMIN PROFILE FUNCTION
}
