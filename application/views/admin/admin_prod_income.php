<br>

<div class="right-align" id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'en', includedLanguages: 'en,ja,tl', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>

<?php if($this->session->flashdata('reports')){?>
  <script type="text/javascript">
    $(document).ready(function(){
        swal({
          title: "Error in date!",
          text:  '<?php echo $this->session->flashdata('reports');?>',
          type: "error",
          timer: 3000,
          showConfirmButton: false
        });
    });
  </script>
<?php }?>


<div class="section no-pad-bot" id="index-banner">
  <div class="container">
    <br><br>
    <h3 class="header center indigo-text text-darken-4"><?php echo $prodName ?></h3>
    <br><br>
  </div>
</div>

<div class="container">
      <br>
      <h5>Start Date: <?= date_format(date_create($start),"m/d/Y") ?></h5>
      <h5>End Date: <?= date_format(date_create($end),"m/d/Y") ?></h5>
      <h5>Total Income: ¥ <?= number_format($income,2) ?></h5>
      <br><br>
      <h5 class="center">Expenses</h5>
      <br>

      <div class="container">

          <table class="bordered responsive-table" style="margin-bottom:100px">
            <thead>
              <tr>
                  <th data-field="">Expense ID</th>
                  <th data-field="">Entry Name</th>
                  <th data-field="">Price</th>
                  <th data-field="">Date</th>
              </tr>
            </thead>

            <tbody>
            <?php if($expenses):?>
            <?php foreach($expenses as $r){?>
              <tr>
                <td><?php echo $r->expenseId ?></td>
                <td><?php echo $r->entryName ?></td>
                <td>¥ <?= number_format($r->price,2) ?></td>
                <td><?php echo $r->date?></td>
              </tr>
            <?php }?>
          <?php else:?>
            <td>No record</td><td></td><td></td><td></td>
          <?php endif?>
            </tbody>
          </table>
      </div>

  <!-- <div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
    <a class="btn-floating btn-large waves-effect waves-light green-dark modal-trigger" href="#addClub"><i class="material-icons">add</i></a>
  </div> -->
</div>

<div class="container">
  <br><br>

      <h5 class="center">Sales</h5>
      <br>

      <div class="container">

          <table class="bordered responsive-table" style="margin-bottom:100px">
            <thead>
              <tr>
                  <th data-field="">Sales ID</th>
                  <th data-field="">Talent</th>
                  <th data-field="">Gross</th>
                  <th data-field="">Talent Fee</th>
                  <th data-field="">Managers Commission</th>
                  <th data-field="">Total</th>
                  <th data-field="">Date</th>
              </tr>
            </thead>

            <tbody>
            <?php if($sales):?>
            <?php foreach($sales as $r){?>
              <tr>
                <td><?php echo $r->salesId ?></td>
                <td><?php echo $r->firstName." ".$r->lastName ?></td>
                <td>¥ <?= number_format($r->gross,2) ?></td>
                <td>¥ <?= number_format($r->talentFee,2) ?></td>
                <td>¥ <?= number_format($r->managerCom,2) ?></td>
                <td>¥ <?= number_format($r->total,2) ?></td>
                <td><?php echo date_format(date_create($r->dateSale),"m/d/Y")?></td>
              </tr>
            <?php }?>
          <?php else:?>
            <td>No record</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
          <?php endif?>
            </tbody>
          </table>
      </div>

  <!-- <div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
    <a class="btn-floating btn-large waves-effect waves-light green-dark modal-trigger" href="#addClub"><i class="material-icons">add</i></a>
  </div> -->
</div>

<div class="fixed-action-btn vertical" style="bottom: 45px; right: 24px;">
    <a class="btn-floating btn-large red">
      <i class="material-icons">menu</i>
    </a>
    <ul>
      <li><a href="#viewReport" class="btn-floating blue tooltipped modal-trigger" data-position="left" data-delay="50" data-tooltip="View Report"><i class="fa fa-calendar"></i></a></li>
    </ul>
  </div>

<!-- view report modal -->
<div id="viewReport" class="modal modal-fixed-footer">
<div class="modal-content">
  <h4 class="center-align">レポートを見ます</h4>
  <h5 class="center-align">View Report</h5>

  <br><br>
  <?php echo form_open('production/view_report'); ?>

  <div class="row">
    <div class="input-field col s6">
      <input id="startDate" name="startDate" type="text" class="datepicker" required/>
      <label for="startDate">Start Date [開始日]</label>
    </div>

    <div class="input-field col s6">
      <input id="endDate" name="endDate" type="text" class="datepicker" required/>
      <label for="endDate">End Date [終了日]</label>
    </div>
  </div>
</div>
<div class="modal-footer">
  <a  class="modal-action modal-close waves-effect waves-red btn-flat ">Cancel</a>
  <button  type="submit" class="modal-action modal-close waves-effect waves-green btn-flat ">Accept</button>
</div>
</div>
<!-- end view report modal -->



<script>
$(document).ready(function() {
// the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
$('.modal-trigger').leanModal();
});

// datepicker
$( document ).ready(function() {
  $('.datepicker').pickadate({
       container: 'body',
       selectMonths: true,
       selectYears: 60,
       today: '',
       formatSubmit: 'yyyy/mm/dd',
       format: 'yyyy/mm/dd'
  });
});

$(document).ready(function() {
  $('select').material_select();
});
</script>
