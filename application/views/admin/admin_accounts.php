<?php if($this->session->flashdata('success')){?>
  <script type="text/javascript">
    $(document).ready(function(){
        swal({
          title: "Done!",
          text:  '<?php echo $this->session->flashdata('success');?>',
          type: "success",
          timer: 3000,
          showConfirmButton: false
        });
    });
  </script>
<?php }?>

<br>

<div class="right-align" id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'en', includedLanguages: 'en,ja,tl', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>

<div class="section no-pad-bot" id="index-banner">
  <div class="container">
    <br><br>
    <h3 class="header center cyan-text text-darken-3">Accounts</h3>
    <br><br>
  </div>
</div>

<div class="container">
  <!-- get talent list -->
    <table class="highlight" style="margin-bottom:100px">
      <thead>
        <tr>
            <th>Account ID</th>
            <th>Account Name</th>
            <th>Username</th>
            <th>Password</th>
            <th>Accout Type</th>
            <th>Status</th>
            <th>Options</th>

        </tr>
      </thead>

      <tbody>
        <?php foreach($accounts as $r){?>
        <tr>
            <td><?php echo $r->userId ?></td>
            <td><?php echo $r->accountName ?></td>
            <td><?php echo $r->username ?></td>
            <td><?php echo $r->password ?></td>
            <td>
              <?php if($r->type == 'a'){
                  echo 'Admin';
                }else if($r->type == 'p'){
                  echo 'Production';
                }else{
                  echo 'Third';
                }
              ?>
            </td>
            <td>
              <div class="switch">
                <label>
                  <?php if($r->active == 1):?>
                    <input id="status_checkbox" type="checkbox" onclick='activate(<?php echo $r->userId?>,<?php echo $r->active?>)' name="status" checked value="<?php echo $r->active ?>">
                  <?php else:?>
                    <input id="status_checkbox" type="checkbox" onclick='activate(<?php echo $r->userId?>,<?php echo $r->active?>)' name="status" value="<?php echo $r->active ?>">
                  <?php endif?>

                  <span class="lever"></span>
                </label>
              </div>
            </td>
            <td>
              <a href="<? echo site_url('administrator')?>" class="btn-floating btn-small waves-effect waves-light blue tooltipped edit_user"  data-userid = "<?php echo $r->userId ?>" data-position="right" data-delay="50" data-tooltip="Edit Account"><i class="material-icons">mode_edit</i></a>
              <a class="btn-floating btn-small waves-effect waves-light red darken-3 tooltipped" data-position="right" data-delay="50" data-tooltip="Delete Account" onclick="delete_account(<?php echo $r->userId ?>)"><i class="material-icons">delete</i></a>
            </td>
        </tr>
        <?php } ?>
      </tbody>
    </table>
</div>

<!-- activate account modal -->

<div id="activate_account" class="modal">
  <div class="modal-content">
    <h4 class="center-align"></h4>
    <h5 class="center-align">Account Status</h5>

    <br><br>
    <?php echo form_open('administrator/activate_account/'); ?>
      <input type="hidden" id="ace" name="accountId">
      <input type="hidden" id="acte" name="active">
      <p>Are you sure you want to activate this account?</p>
  </div>
  <div class="modal-footer">
    <a href="<?php echo base_url('administrator/accounts')?>" class="modal-action modal-close waves-effect waves-red btn-flat ">Cancel</a>
    <button  type="submit" class="modal-action modal-close waves-effect waves-green btn-flat ">Accept</button>
  </div>
  <?php echo form_close(); ?>
</div>

<!-- end activate account modal -->

<!-- deactivate account modal -->

<div id="deactivate_account" class="modal">
  <div class="modal-content">
    <h4 class="center-align"></h4>
    <h5 class="center-align">Account Status</h5>

    <br><br>
    <?php echo form_open('administrator/activate_account/'); ?>
      <input type="hidden" id="acd" name="accountId">
      <input type="hidden" id="actd" name="active">
      <p>Are you sure you want to deactivate this account?</p>
  </div>
  <div class="modal-footer">
    <a href="<?php echo base_url('administrator/accounts')?>" class="modal-action modal-close waves-effect waves-red btn-flat ">Cancel</a>
    <button  type="submit" class="modal-action modal-close waves-effect waves-green btn-flat ">Accept</button>
  </div>
  <?php echo form_close(); ?>
</div>

<!-- end deactivate account modal -->

<!-- Add new account button -->
<div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
  <a class="btn-floating btn-large waves-effect waves-light green-dark modal-trigger" onclick="hideDiv()" href="#addAccount">
    <i class="material-icons">add</i>
  </a>
</div>
<!--  -->

<!-- Add account modal -->
<div id="addAccount" class="modal modal-fixed-footer">
  <div class="modal-content">
    <h4 class="center-align"></h4>
    <h5 class="center-align">Add New Account</h5>
    <br><br>
      <?php echo form_open('administrator/add_accounts'); ?>
        <div class="input-field col s12">
          <input id="accountName" name="accountName" type="text" class="validate">
          <label for="accountName">Account Name</label>
        </div>
        <div class="input-field col s12">
          <input id="username" name="username" type="text" class="validate">
          <label for="username">Username</label>
        </div>
        <div class="input-field col s12">
          <input id="password" name="password" type="text" class="validate">
          <label for="password">Password</label>
        </div>
        <br>
        <div class="input-field col s12">
          <select id="accountType" onchange="selectType(this);" name="type">
            <option value="" disabled selected>Choose one</option>
            <option value="a" >Admin</option>
            <option value="p">Production</option>
            <option value="t">Third</option>
          </select>
          <label for="production">Account Type</label>
        </div>

        <br>
        <div id="selectProd" class="input-field col s12">
          <select id="production" name="production">
            <option value="" disabled selected>Choose one</option>
            <?php if(isset($production)) : foreach($production as $prod):?>
            <option value="<?php echo $prod->productionId; ?>"><?php echo $prod->productionName; ?></option>
            <?php endforeach; ?>
          </select>
          <?php else: ?>
          <input disabled value="No production" id="production" type="text" class="validate">
          <?php endif; ?>
          <label for="production">Production</label>
        </div>

    </div>
    <div class="modal-footer">
      <a  class="modal-action modal-close waves-effect waves-red btn-flat ">Cancel</a>
      <button  type="submit" class="modal-action modal-close waves-effect waves-green btn-flat ">Save</button>
    </div>
    <?php echo form_close()?>
  </div>
<!--  -->

<!-- Add account modal -->
<div id="editAccount" class="modal modal-fixed-footer">
  <div class="modal-content">
    <h4 class="center-align"></h4>
    <h5 class="center-align">Edit Account</h5>
    <br><br>
      <?php echo form_open('administrator/update_account'); ?>
        <div class="input-field col s12">
          <input id="uuserId" name="userId" type="hidden">
          <input id="uaccountName" name="accountName" type="text" class="validate">
          <label for="accountName">Account Name</label>
        </div>
        <div class="input-field col s12">
          <input id="upassword" name="password" type="text" class="validate">
          <label for="password">Password</label>
        </div>
        <br>
        <div class="input-field col s12">
          <select id="uaccountType" onchange="selectType(this);" name="type">
            <option value="" disabled selected>Choose one</option>
            <option value="a" >Admin</option>
            <option value="p">Production</option>
            <option value="t">Third</option>
          </select>
          <label for="production">Account Type [口座の種類]</label>
        </div>

        <br>
        <div id="selectProd" class="input-field col s12">
          <select id="uproduction" name="production">
            <option value="" disabled selected>Choose one</option>
            <?php if(isset($production)) : foreach($production as $prod):?>
            <option value="<?php echo $prod->productionId; ?>"><?php echo $prod->productionName; ?></option>
            <?php endforeach; ?>
          </select>
          <?php else: ?>
          <input disabled value="No production" id="uproduction" type="text" class="validate">
          <?php endif; ?>
          <label for="production">Production</label>
        </div>

    </div>
    <div class="modal-footer">
      <a  class="modal-action modal-close waves-effect waves-red btn-flat ">Cancel</a>
      <button  type="submit" class="modal-action modal-close waves-effect waves-green btn-flat ">Save</button>
    </div>
  </div>
  <?php echo form_close()?>
</div>
<!--  -->

<script>
  function activate(id,status){
    if(status == 1){
        console.log(id);
        $('#acd').val(id);
        $('#actd').val(status);
        $('#deactivate_account').openModal();
    }else{
        console.log(id);
        $('#ace').val(id);
        $('#acte').val(status);
        $('#activate_account').openModal();
    }
  }

  function selectType(type){

      if(type.value == 'p' || type.value == 't' ){
        $('#selectProd').show();
      }else{
        $('#selectProd').hide();
      }
  }

    function hideDiv(){
          $('#selectProd').hide();
    }

</script>

<script>
$(document).ready(function() {
  // the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
  $('.modal-trigger').leanModal();
});
</script>

<script>
$(document).ready(function() {
   $('select').material_select();
 });
</script>

<script>
$(document).ready(function(){
    $('.tooltipped').tooltip({delay: 50});
  });
</script>

<script>
function delete_account(id){
  swal({    title: "Confirmation",
              text: "Are you sure you want to delete account "+id+"?",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#f44336",
              confirmButtonText: "Yes, delete it!",
              closeOnConfirm: false },
              function(isConfirm){
                if (isConfirm) {
                    $.ajax({
                        url : "<?php echo site_url('administrator/delete_account')?>/"+id,
                        type: "POST",
                        dataType: "JSON",
                        success: function(data)
                        {
                          swal("Deleted!", "Account "+id+" successfully deleted!", "success");
                          location.reload(true);
                        },
                        error: function (jqXHR, textStatus, errorThrown)
                        {
                          alert('An error occured while deleting data');
                        }
                    });
                }
              });

}
</script>
