<!-- Dropdown Structure -->
<ul id="navbar_dropdown" class="dropdown-content">
  <li><a href="<?php echo site_url('administrator/admin_profile');?>"><i class="material-icons left">perm_identity</i>My Profile</a></li>
  <li class="divider"></li>
  <li><a href="<?php echo site_url('administrator/settings');?>"><i class="material-icons left">settings</i>Account Settings</a></li>
  <li class="divider"></li>
  <li><a href="<?php echo site_url('administrator/logout');?>"><i class="material-icons left">power_settings_new</i>Logout</a></li>
</ul>
<nav>
  <div class="nav-wrapper cyan darken-4">
    <a href="#!" class="brand-logo" style="margin-left:20px;">Man Production Group</a>
    <a href="#" data-activates="mobile-demo" class="button-collapse" style="margin-left:10px;"><i class="material-icons">menu</i></a>
    <ul class="right hide-on-med-and-down">
      <li class=""><a href="<?php echo site_url('administrator')?>">DASHBOARD</a></li>
      <li class=""><a href="<?php echo site_url('administrator/productions')?>">PRODUCTIONS</a></li>
      <li class=""><a href="<?php echo site_url('administrator/clubs')?>">CLUBS</a></li>
      <li class="active"><a href="<?php echo site_url('administrator/talents')?>">TALENTS</a></li>
      <li class=""><a href="<?php echo site_url('administrator/accounts')?>">ACCOUNTS</a></li>
      <li>
        <!-- Dropdown Trigger -->
        <a class="dropdown-button center-align" href="#" data-activates="navbar_dropdown" style="width: 220px;">
          <?php echo $accountName; ?>
          <i class="material-icons right">keyboard_arrow_down</i>
        </a>
      </li>
    </ul>
    <ul class="side-nav collapsible collapsible-accordion" id="mobile-demo">
      <li class=""><a href="<?php echo site_url('administrator')?>">DASHBOARD</a></li>
      <li class=""><a href="<?php echo site_url('administrator/productions')?>">PRODUCTIONS</a></li>
      <li class=""><a href="<?php echo site_url('administrator/clubs')?>">CLUBS</a></li>
      <li class="active"><a href="<?php echo site_url('administrator/talents')?>">TALENTS</a></li>
      <li class=""><a href="<?php echo site_url('administrator/accounts')?>">ACCOUNTS</a></li>
      <li class="bold active">
        <a class="collapsible-header waves-effect waes-teal">
          <?php echo $accountName; ?>
          <i class="material-icons right">keyboard_arrow_down</i>
        </a>
        <div class="collapsible-body">
          <ul>
            <li><a href="<?php echo site_url('administrator/admin_profile')?>"><i class="material-icons left">perm_identity</i>My Profile</a></li>
            <li><a href="<?php echo site_url('administrator/settings')?>"><i class="material-icons left">settings</i>Account Settings</a></li>
            <li><a href="<?php echo site_url('administrator/logout')?>"><i class="material-icons left">power_settings_new</i>Logout</a></li>
          </ul>
        </div>
      </li>
    </ul>
  </div>
</nav>

<?php if($this->session->flashdata('success')){?>
  <script type="text/javascript">
    $(document).ready(function(){
     var $toastContent = $('<span><?php echo $this->session->flashdata('success');?></span>');
     Materialize.toast($toastContent, 5000)
    });
  </script>
<?php }?>
<?php if($this->session->flashdata('error')){?>
  <script type="text/javascript">
    $(document).ready(function(){
     var $toastContent = $('<span><?php echo $this->session->flashdata('error');?></span>');
     Materialize.toast($toastContent, 5000)
    });
  </script>
<?php }?>
<?php if($this->session->flashdata('error_1')){?>
  <script type="text/javascript">
    $(document).ready(function(){
     var $toastContent = $('<span>Image 1 : <?php echo $this->session->flashdata('error_1');?></span>');
     Materialize.toast($toastContent, 5000)
    });
  </script>
<?php }?>
<?php if($this->session->flashdata('error_2')){?>
  <script type="text/javascript">
    $(document).ready(function(){
     var $toastContent = $('<span>Image 2 : <?php echo $this->session->flashdata('error_2');?></span>');
     Materialize.toast($toastContent, 5000)
    });
  </script>
<?php }?>
<?php if($this->session->flashdata('error_3')){?>
  <script type="text/javascript">
    $(document).ready(function(){
     var $toastContent = $('<span>Image 3 : <?php echo $this->session->flashdata('error_3');?></span>');
     Materialize.toast($toastContent, 5000)
    });
  </script>
<?php }?>
<?php if($this->session->flashdata('error_4')){?>
  <script type="text/javascript">
    $(document).ready(function(){
     var $toastContent = $('<span>Image 4 : <?php echo $this->session->flashdata('error_4');?></span>');
     Materialize.toast($toastContent, 5000)
    });
  </script>
<?php }?>

<div class="section no-pad-bot" id="index-banner">
  <div class="container">
    <br><br>
    <h3 class="header center cyan-text text-darken-3">Talents</h3>
    <br><br>
  </div>
</div>

<div class="container">
    <table class="bordered centered responsive-table">
      <thead>
        <tr>
            <th>Talent ID</th>
            <th>Talent Name</th>
            <th>Name</th>
            <th>Production</th>
            <th>Club</th>
            <th>Document Send</th>
            <th>Apply</th>
            <th>In Japan</th>
            <th>Extension</th>
            <th>VISA License</th>
            <th>Finish VISA</th>
            <th>Options</th>
        </tr>
      </thead>

      <tbody>
        <?php foreach($talents as $talent){?>
        <tr>
            <td><?php echo $talent->talentId; ?></td>
            <td><?php echo $talent->talentName; ?></td>
            <td><?php $last = $talent->lastName;
                      $first = $talent->firstName;
                      $mid = $talent->midName;

                      $fullname = ucfirst($last).', '.ucfirst($first).' ('.ucfirst($mid).')';
                      echo $fullname;
                ?>
            </td>
            <td><?php echo $talent->productionName; ?></td>
            <td><?php echo $talent->clubName; ?></td>
            <td class="right-align"><?php $docSend = strtotime($talent->docSend);
                      echo date('m/d/Y',$docSend);
                ?>
            </td>
            <td class="right-align"><?php $apply = strtotime($talent->apply);
                      echo date('m/d/Y',$apply);
                ?>
            </td>
            <td class="right-align"><?php $inJapan = strtotime($talent->inJapan);
                      echo date('m/d/Y',$inJapan);
                ?>
            </td>
            <td class="right-align"><?php $ext = strtotime($talent->extension);
                      echo date('m/d/Y',$ext);
                ?>
            </td>
            <td class="right-align"><?php $visLic = strtotime($talent->visaLicense);
                      echo date('m/d/Y',$visLic);
                ?>
            </td>
            <td class="right-align"><?php $finVis = strtotime($talent->finishVisa);
                      echo date('m/d/Y',$finVis);
                ?>
            </td>
            <td>
              <a id="viewTalentModal" class="btn-floating btn-small waves-effect waves-light green darken-2 tooltipped" data-id="<?php echo $talent->talentId; ?>" data-position="right" data-delay="50" data-tooltip="View Talent"><i class="material-icons">search</i></a>
              <br><br>
              <a id="editTalentModal" class="btn-floating btn-small waves-effect waves-light green darken-2 tooltipped" data-id="<?php echo $talent->talentId; ?>" data-position="right" data-delay="50" data-tooltip="Edit Talent"><i class="material-icons">mode_edit</i></a>
              <br><br>
              <a id="updateTalentStatModal" class="btn-floating btn-small waves-effect waves-light green darken-2 tooltipped" data-id="<?php echo $talent->talentId; ?>" data-position="right" data-delay="50" data-tooltip="Update Talent Status"><i class="material-icons">restore</i></a>
            </td>
        </tr>
        <?php } ?>
      </tbody>
    </table>

    <div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
      <a class="btn-floating btn-large waves-effect waves-light green-dark modal-trigger" href="#addTalent">
        <i class="material-icons">add</i>
      </a>
    </div>
</div>

<!-- viewTalent MODAL -->
<div id="viewTalent" class="modal modal-fixed-footer">
  <div class="modal-content">
    <h4 class="center-align">ビュー芸能人データ</h4>
    <h5 class="center-align">View Talent</h5>
    <br>
    <div class="row">
      <div class="input-field col s8">
        <input placeholder="" id="talent-name" name="talent-name" type="text" value="" disabled/>
        <label class="teal-text text-darken-4" for="talent-name">Talent Name [タレント名]</label>
      </div>
      <div class="input-field col s4">
        <input placeholder="" id="category" name="category" type="text" value="" disabled/>
        <label class="teal-text text-darken-4" for="category">Category</label>
      </div>
    </div>
    <div class="row">
      <div class="input-field col s4">
        <input placeholder="" id="first-name" name="first-name" type="text" value="" disabled/>
        <label class="teal-text text-darken-4" for="first-name">First Name</label>
      </div>
      <div class="input-field col s4">
        <input placeholder="" id="middle-name" name="middle-name" type="text" value="" disabled/>
        <label class="teal-text text-darken-4" for="middle-name">Middle Name</label>
      </div>
      <div class="input-field col s4">
        <input placeholder="" id="last-name" name="last-name" type="text" value="" disabled/>
        <label class="teal-text text-darken-4" for="last-name">Last Name</label>
      </div>
    </div>
    <br>
    <div class="row">
      <div class="input-field col s8">
        <input placeholder="" id="club-name" name="club-name" type="text" value="" disabled/>
        <label class="teal-text text-darken-4" for="club-name">Club Name [経歴出演店]</label>
      </div>
      <div class="input-field col s4">
        <input placeholder="" id="birthdate" name="birthdate" type="text" value="" disabled/>
        <label class="teal-text text-darken-4" for="birthdate">Birthdate [生年月日]</label>
      </div>
    </div>
    <div class="row">
       <div class="input-field col s12">
         <textarea placeholder="" id="birthplace" name="birthplace" class="materialize-textarea" value="" disabled></textarea>
         <label class="teal-text text-darken-4" for="birthplace">Birthplace [出生地]</label>
       </div>
    </div>
    <div class="row">
        <div class="input-field col s12">
          <input placeholder="" id="manager" name="manager" type="text" value="" disabled/>
          <label class="teal-text text-darken-4" for="manager">Manager [マネージャ]</label>
        </div>
     </div>
     <br>
     <div class="row">
      <div class="input-field col s4">
        <input placeholder="" id="production" name="production" type="text" value="" disabled/>
        <label class="teal-text text-darken-4" for="production">Production [日本プロダクション]</label>
      </div>
      <div class="input-field col s4">
        <input placeholder="" id="processing" name="processing" type="text" value="" disabled/>
        <label class="teal-text text-darken-4" for="processing">Processing [比国エージェント]</label>
      </div>
      <div class="input-field col s4">
        <input placeholder="" id="doc-send" name="doc-send" type="text" value="" disabled/>
        <label class="teal-text text-darken-4" for="doc-send">Document Send [ドキュメント入手日]</label>
      </div>
    </div>
    <div class="row">
      <div class="input-field col s6">
        <input placeholder="" id="apply" name="apply" type="text" value="" disabled/>
        <label class="teal-text text-darken-4" for="apply">Apply [申請日]</label>
      </div>
      <div class="input-field col s6">
          <input placeholder="" id="vis-lic" name="vis-lic" type="text" value="" disabled/>
        <label class="teal-text text-darken-4" for="visa-lic">VISA License [VISA交付日]</label>
      </div>
    </div>
    <div class="row">
      <div class="input-field col s4">
        <input placeholder="" id="in-japan" name="in-japan" type="text" value="" disabled/>
        <label class="teal-text text-darken-4" for="in-japan">In Japan [入国日]</label>
      </div>
      <div class="input-field col s4">
        <input placeholder="" id="extension" name="extension" type="text" value="" disabled/>
        <label class="teal-text text-darken-4" for="extension">Extension [更新日]</label>
      </div>
      <div class="input-field col s4">
        <input placeholder="" id="fin-visa" name="fin-visa" type="text" value="" disabled/>
        <label class="teal-text text-darken-4" for="fin-visa">Finish VISA [帰国日]</label>
      </div>
    </div>
    <div class="row">
       <div class="input-field col s4">
         <input placeholder="" id="pass-number" name="pass-number" type="text" value="" disabled/>
         <label class="teal-text text-darken-4" for="pass-number">Passport Number [パスポートNo]</label>
       </div>
       <div class="input-field col s4">
         <input placeholder="" id="pass-exp" name="pass-exp" type="text" value="" disabled/>
         <label class="teal-text text-darken-4" for="pass-exp">Expiry Date of Passport [期限]</label>
       </div>
       <div class="input-field col s4">
         <input placeholder="" id="issu" name="issu" type="text" value="" disabled/>
         <label class="teal-text text-darken-4" for="issu">ISSU [発行日]</label>
       </div>
    </div>
    <!-- IMAGES -->
    <!-- IMAGE 1 -->
    <div class="row">
      <div class="input-field col s12">
          <input id="image1" name="image1" class="file-path validate" type="text" value="" disabled/>
          <label class="teal-text text-darken-4" for="image1">Image 1</label>
      </div>
    </div>
    <!-- IMAGE 2 -->
    <div class="row">
      <div class="input-field col s12">
          <input id="image2" name="image2" class="file-path validate" type="text" value="" disabled/>
          <label class="teal-text text-darken-4" for="image2">Image 2</label>
      </div>
    </div>
    <!-- IMAGE 3 -->
    <div class="row">
      <div class="input-field col s12">
          <input id="image3" name="image3" class="file-path validate" type="text" value="" disabled/>
          <label class="teal-text text-darken-4" for="image3">Image 3</label>
      </div>
    </div>
    <!-- IMAGE 4 -->
    <div class="row">
      <div class="input-field col s12">
          <input id="image4" name="image4" class="file-path validate" type="text" value="" disabled/>
          <label class="teal-text text-darken-4" for="image4">Image 4</label>
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <a class="modal-action modal-close waves-effect btn-flat">Back [バック]</a>
  </div>
</div>
<!-- /. viewTalent MODAL -->

<!-- editTalent MODAL -->
<?php
  $att = array(
          'id' => 'editTalentForm',
          'novalidate' => 'novalidate'
        );
  echo form_open('administrator/edit_talent/', $att);
?>
<div id="editTalent" class="modal modal-fixed-footer">
  <div class="modal-content">
    <h4 class="center-align">編集芸能人データ</h4>
    <h5 class="center-align">Edit Talent</h5>
    <br>
      <div class="row">
        <div class="input-field col s8">
          <input placeholder="" id="edit-talent-name" name="edit-talent-name" type="text" required="" aria-required="true"/>
          <label for="edit-talent-name">Talent Name [タレント名]</label>
        </div>
        <div class="input-field col s4">
            <select id="edit-category" name="edit-category" required="" aria-required="true">
              <option value="" disabled selected>Choose one</option>
              <option value="Dancer PRO">Dancer PRO</option>
              <option value="Dancer NON-PRO">Dance NON-PRO</option>
              <option value="Singer PRO">Singer PRO</option>
              <option value="Singer NON-PRO">Singer NON-PRO</option>
            </select>
            <label>Category</label>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s4">
          <input placeholder="" id="edit-first-name" name="edit-first-name" type="text" required="" aria-required="true">
          <label for="edit-first-name">First Name</label>
        </div>
        <div class="input-field col s4">
          <input placeholder="" id="edit-middle-name" name="edit-middle-name" type="text" required="" aria-required="true">
          <label for="edit-middle-name">Middle Name</label>
        </div>
        <div class="input-field col s4">
          <input placeholder="" id="edit-last-name" name="edit-last-name" type="text" required="" aria-required="true">
          <label for="edit-last-name">Last Name</label>
        </div>
      </div>
      <br>
      <div class="row">
        <div class="input-field col s8">
          <select id="edit-club-name" name="edit-club-name" required="" aria-required="true">
            <option value="" disabled selected>Choose one</option>
            <?php if(isset($clubs)) : foreach($clubs as $club):?>
            <option value="<?php echo $club->clubId; ?>"><?php echo $club->clubName; ?></option>
            <?php endforeach; ?>
          </select>
          <?php else: ?>
          <input disabled value="" id="club-name" name="club-name" type="text" class="validate">
          <?php endif; ?>
          <label for="edit-club-name">Club Name [経歴出演店]</label>
        </div>
        <div class="input-field col s4">
          <input placeholder="" id="edit-birthdate" name="edit-birthdate" type="text" class="datepicker" required="" aria-required="true">
          <label for="edit-birthdate">Birthdate [生年月日]</label>
        </div>
      </div>
      <div class="row">
         <div class="input-field col s12">
           <textarea placeholder="" id="edit-birthplace" name="edit-birthplace" class="materialize-textarea"></textarea>
           <label for="edit-birthplace">Birthplace [出生地]</label>
         </div>
       </div>
       <div class="row">
          <div class="input-field col s12">
            <input placeholder="" id="edit-manager" name="edit-manager" type="text">
            <label for="edit-manager">Manager [マネージャ]</label>
          </div>
        </div>
        <br>
        <div class="row">
         <div class="input-field col s4">
           <select id="edit-production" name="edit-production" required="" aria-required="true">
             <option value="" disabled selected>Choose one</option>
             <?php if(isset($production)) : foreach($production as $prod):?>
             <option value="<?php echo $prod->productionId; ?>"><?php echo $prod->productionName; ?></option>
             <?php endforeach; ?>
           </select>
           <?php else: ?>
           <input disabled value="No production" id="production" type="text">
           <?php endif; ?>
           <label for="edit-production">Production [日本プロダクション]</label>
         </div>
         <div class="input-field col s4">
           <input placeholder="" id="edit-processing" name="edit-processing" type="text" class="datepicker">
           <label for="edit-processing">Processing [比国エージェント]</label>
         </div>
         <div class="input-field col s4">
           <input placeholder="" id="edit-doc-send" name="edit-doc-send" type="text" class="datepicker">
           <label for="edit-doc-send">Document Send [ドキュメント入手日]</label>
         </div>
       </div>
       <div class="row">
         <div class="input-field col s6">
           <input placeholder="" id="edit-apply" name="edit-apply" type="text" class="datepicker">
           <label for="edit-apply">Apply [申請日]</label>
         </div>
         <div class="input-field col s6">
             <input placeholder="" id="edit-vis-lic" name="edit-vis-lic" type="text" class="datepicker" required="" aria-required="true">
           <label for="edit-visa-lic">VISA License [VISA交付日]</label>
         </div>
       </div>
       <div class="row">
         <div class="input-field col s4">
           <input placeholder="" id="edit-in-japan" name="edit-in-japan" type="text" class="datepicker">
           <label for="edit-in-japan">In Japan [入国日]</label>
         </div>
         <div class="input-field col s4">
           <input placeholder="" id="edit-extension" name="edit-extension" type="text" class="datepicker">
           <label for="edit-edit-extension">Extension [更新日]</label>
         </div>
         <div class="input-field col s4">
           <input placeholder="" id="edit-fin-visa" name="edit-fin-visa" type="text" class="datepicker" required="" aria-required="true">
           <label for="edit-fin-visa">Finish VISA [帰国日]</label>
         </div>
       </div>
       <div class="row">
          <div class="input-field col s4">
            <input placeholder="" id="edit-pass-number" name="edit-pass-number" type="text">
            <label for="edit-pass-number">Passport Number [パスポートNo]</label>
          </div>
          <div class="input-field col s4">
            <input placeholder="" id="edit-pass-exp" name="edit-pass-exp" type="text" class="datepicker">
            <label for="edit-pass-exp">Expiry Date of Passport [期限]</label>
          </div>
          <div class="input-field col s4">
            <input placeholder="" id="edit-issu" name="edit-issu" type="text">
            <label for="edit-issu">ISSU [発行日]</label>
          </div>
       </div>
       <!-- IMAGES -->
       <!-- IMAGE 1 -->
       <!--<div class="row">
        <div class="file-field input-field">
         <div class="btn">
            <span>File</span>
            <input type="file" name="image1">
          </div>
          <div class="file-path-wrapper">
             <input id="edit-image1" class="file-path validate" type="text">
          </div>
         </div>
       </div>
       <!-- IMAGE 2
       <div class="row">
        <div class="file-field input-field">
         <div class="btn">
            <span>File</span>
            <input type="file" name="image2">
          </div>
          <div class="file-path-wrapper">
             <input id="edit-image2" class="file-path validate" type="text">
          </div>
         </div>
       </div>
       <!-- IMAGE 3
       <div class="row">
        <div class="file-field input-field">
         <div class="btn">
            <span>File</span>
            <input type="file" name="image3">
          </div>
          <div class="file-path-wrapper">
             <input id="edit-image3" class="file-path validate" type="text">
          </div>
         </div>
       </div>
       <!-- IMAGE 4
       <div class="row">
        <div class="file-field input-field">
         <div class="btn">
            <span>File</span>
            <input type="file" name="image4">
          </div>
          <div class="file-path-wrapper">
             <input id="edit-image4" class="file-path validate" type="text">
          </div>
         </div>
       </div>-->
  </div>
  <div class="modal-footer">
      <a class="modal-action modal-close waves-effect btn-flat">Cancel [キャンセル]</a>
      <button type="submit" id="submit-edit" class="modal-action modal-close waves-effect btn-flat">Save [セーブ]</button>
  </div>
</div>
</form>
<!-- /. editTalent MODAL -->

<!-- updateTalentStat MODAL -->
<?php
  $att = array(
            'id' => 'updateTalentStatForm',
            'novalidate' => 'novalidate'
        );
  echo form_open('administrator/update_talent_status/', $att);
?>
<div id="updateTalentStat" class="modal modal-fixed-footer">
  <div class="modal-content">
    <h4 class="center-align">更新芸能人データ</h4>
    <h5 class="center-align">Update Talent Status</h5>
    <br><br>
    <div class="row">
      <div class="input-field col s6">
          <select id="status" name="status" required="" aria-required="true">
            <option value="" disabled selected>Choose one</option>
            <option value="applying">Applying</option>
            <option value="processing">Processing</option>
            <option value="preparation">Preparation</option>
            <option value="immigration">Immigration</option>
          </select>
          <label>Status</label>
      </div>
      <div class="input-field col s6">
        <input placeholder="" id="date-status" name="date-status" type="text" class="datepicker" required="" aria-required="true">
        <label for="date-status">Date</label>
      </div>
    </div>
  </div>
  <div class="modal-footer">
      <a class="modal-action modal-close waves-effect btn-flat">Cancel [キャンセル]</a>
      <button type="submit" id="submit-update" disabled="disabled" id="submit" class="modal-action modal-close waves-effect btn-flat">Save [セーブ]</button>
  </div>
</div>
</form>
<!-- /. updateTalentStat MODAL -->

<!-- addTalent MODAL -->
<?php
  $att = array(
            'id' => 'addTalentForm',
            'novalidate' => 'novalidate'
         );
  echo form_open_multipart('administrator/add_talents', $att);
?>
<div id="addTalent" class="modal modal-fixed-footer">
  <div class="modal-content">
    <h4 class="center-align">新しい芸人を追加</h4>
    <h5 class="center-align">Add New Talent</h5>
    <br>
    <h6 class="header6 center-align">PERSONAL INFORMATION</h6>
    <br><br>
      <div class="row">
        <div class="input-field col s8">
          <input id="talent-name" name="talent-name" type="text" required="" aria-required="true">
          <label for="talent-name">Talent Name [タレント名]</label>
        </div>
        <div class="input-field col s4">
            <select id="category" name="category" required="" aria-required="true">
              <option value="" disabled selected>Choose one</option>
              <option value="Dancer PRO">Dancer PRO</option>
              <option value="Dancer NON-PRO">Dance NON-PRO</option>
              <option value="Singer PRO">Singer PRO</option>
              <option value="Singer NON-PRO">Singer NON-PRO</option>
            </select>
            <label>Category</label>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s4">
          <input id="first-name" name="first-name" type="text" required="" aria-required="true">
          <label for="first-name">First Name</label>
        </div>
        <div class="input-field col s4">
          <input id="middle-name" name="middle-name" type="text" required="" aria-required="true">
          <label for="middle-name">Middle Name</label>
        </div>
        <div class="input-field col s4">
          <input id="last-name" name="last-name" type="text" required="" aria-required="true">
          <label for="last-name">Last Name</label>
        </div>
      </div>
      <br>
      <div class="row">
        <div class="input-field col s8">
          <select id="club-name" name="club-name" required="" aria-required="true">
            <option value="" disabled selected>Choose one</option>
            <?php if(isset($clubs)) : foreach($clubs as $club):?>
            <option value="<?php echo $club->clubId; ?>"><?php echo $club->clubName; ?></option>
            <?php endforeach; ?>
          </select>
          <?php else: ?>
          <input disabled value="" id="club-name" name="club-name" type="text">
          <?php endif; ?>
          <label for="club-name">Club Name [経歴出演店]</label>
        </div>
        <div class="input-field col s4">
          <input id="birthdate" name="birthdate" type="text" class="datepicker" required="" aria-required="true">
          <label for="birthdate">Birthdate [生年月日]</label>
        </div>
      </div>
      <div class="row">
         <div class="input-field col s12">
           <textarea id="birthplace" name="birthplace" class="materialize-textarea"></textarea>
           <label for="birthplace">Birthplace [出生地]</label>
         </div>
       </div>
       <div class="row">
          <div class="input-field col s12">
            <input id="manager" name="manager" type="text">
            <label for="manager">Manager [マネージャ]</label>
          </div>
        </div>
        <br>
        <div class="row">
         <div class="input-field col s4">
           <select id="production" name="production" required="" aria-required="true">
             <option value="" disabled selected>Choose one</option>
             <?php if(isset($production)) : foreach($production as $prod):?>
             <option value="<?php echo $prod->productionId; ?>"><?php echo $prod->productionName; ?></option>
             <?php endforeach; ?>
           </select>
           <?php else: ?>
           <input disabled value="No production" id="production" type="text">
           <?php endif; ?>
           <label for="production">Production [日本プロダクション]</label>
         </div>
         <div class="input-field col s4">
           <input id="processing" name="processing" type="text" class="datepicker">
           <label for="processing">Processing [比国エージェント]</label>
         </div>
         <div class="input-field col s4">
           <input id="doc-send" name="doc-send" type="text" class="datepicker">
           <label for="doc-send">Document Send [ドキュメント入手日]</label>
         </div>
       </div>
       <div class="row">
         <div class="input-field col s6">
           <input id="apply" name="apply" type="text" class="datepicker">
           <label for="apply">Apply [申請日]</label>
         </div>
         <div class="input-field col s6">
             <input id="vis-lic" name="vis-lic" type="text" class="datepicker" required="" aria-required="true">
           <label for="visa-lic">VISA License [VISA交付日]</label>
         </div>
       </div>
       <div class="row">
         <div class="input-field col s4">
           <input id="in-japan" name="in-japan" type="text" class="datepicker">
           <label for="in-japan">In Japan [入国日]</label>
         </div>
         <div class="input-field col s4">
           <input id="extension" name="extension" type="text" class="datepicker">
           <label for="extension">Extension [更新日]</label>
         </div>
         <div class="input-field col s4">
           <input id="fin-visa" name="fin-visa" type="text" class="datepicker" required="" aria-required="true">
           <label for="fin-visa">Finish VISA [帰国日]</label>
         </div>
       </div>
       <div class="row">
          <div class="input-field col s4">
            <input id="pass-number" name="pass-number" type="text" class="validate">
            <label for="pass-number">Passport Number [パスポートNo]</label>
          </div>
          <div class="input-field col s4">
            <input id="pass-exp" name="pass-exp" type="text" class="datepicker">
            <label for="pass-exp">Expiry Date of Passport [期限]</label>
          </div>
          <div class="input-field col s4">
            <input id="issu" name="issu" type="text" class="validate">
            <label for="issu">ISSU [発行日]</label>
          </div>
       </div>
       <!-- IMAGES -->
       <!-- IMAGE 1 -->
       <div class="row">
        <div class="file-field input-field">
         <div class="btn">
            <span>File</span>
            <input type="file" name="image1">
          </div>
          <div class="file-path-wrapper">
             <input class="file-path validate" type="text" placeholder="Picture 1">
          </div>
         </div>
       </div>
       <!-- IMAGE 2 -->
       <div class="row">
        <div class="file-field input-field">
         <div class="btn">
            <span>File</span>
            <input type="file" name="image2">
          </div>
          <div class="file-path-wrapper">
             <input class="file-path validate" type="text" placeholder="Picture 2">
          </div>
         </div>
       </div>
       <!-- IMAGE 3 -->
       <div class="row">
        <div class="file-field input-field">
         <div class="btn">
            <span>File</span>
            <input type="file" name="image3">
          </div>
          <div class="file-path-wrapper">
             <input class="file-path validate" type="text" placeholder="Picture 3">
          </div>
         </div>
       </div>
       <!-- IMAGE 4 -->
       <div class="row">
        <div class="file-field input-field">
         <div class="btn">
            <span>File</span>
            <input type="file" name="image4">
          </div>
          <div class="file-path-wrapper">
             <input class="file-path validate" type="text" placeholder="Picture 4">
          </div>
         </div>
       </div>
    </div>
    <div class="modal-footer">
        <button class="modal-action modal-close waves-effect btn-flat">Cancel [キャンセル]</button>
        <button type="submit" id="submit-add" disabled="disabled" class="modal-action modal-close waves-effect btn-flat">Save [セーブ]</button>
    </div>
  </div>
</form>
<!-- /. addTalent MODAL -->

<!-- Modal Trigger -->
<script>
$( document ).ready(function() {
  //MODAL TRIGGER
  // the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
  $('.modal-trigger').leanModal();
});
</script>

<!-- Select -->
<script>
$( document ).ready(function() {
  $('select').material_select();
});
</script>

<!-- Tooltip -->
<script>
$( document ).ready(function() {
  $('.tooltipped').tooltip({delay: 50});
});
</script>

<!-- datepicker -->
<script>
$( document ).ready(function() {
  $('.datepicker').pickadate({
   selectMonths: true,
   selectYears: 60,
   today: '',
   formatSubmit: 'yyyy/mm/dd',
   format: 'yyyy/mm/dd'
  });
});
</script>

<!-- Validation for editTalentForm -->
<script>
$(document).ready(function() {
  // Extension materialize.css
  $.validator.setDefaults({
      errorClass: 'invalid',
      validClass: "valid",
      errorPlacement: function (error, element) {
          $(element)
              .closest("form")
              .find("label[for='" + element.attr("id") + "']")
              .attr('data-error', error.text());
      },
      submitHandler: function (form) {
          console.log('form ok');
      }
  });

  $("#editTalentForm").validate({


  });
});
</script>
<!-- ./ Validation for editTalentForm -->

<!-- Validation for updateTalentStatForm -->
<script>
$(document).ready(function() {
  // Extension materialize.css
  $.validator.setDefaults({
      errorClass: 'invalid',
      validClass: "valid",
      errorPlacement: function (error, element) {
          $(element)
              .closest("form")
              .find("label[for='" + element.attr("id") + "']")
              .attr('data-error', error.text());
      }
  });

  $('input').on('blur', function() {
       if ($("#updateTalentStatForm").valid()) {
          $('#submit-update').prop('disabled', false);
       } else {
           $('#submit-update').prop('disabled', 'disabled');
       }
  });

  $("#updateTalentStatForm").validate();
  
});
</script>
<!-- ./ Validation for updateTalentStatForm -->

<!-- Validation for addTalentForm -->
<script>
$(document).ready(function() {
  // Extension materialize.css
  $.validator.setDefaults({
      errorClass: 'invalid',
      validClass: "valid",
      errorPlacement: function (error, element) {
          $(element)
              .closest("form")
              .find("label[for='" + element.attr("id") + "']")
              .attr('data-error', error.text());
      }
  });

  $('input').on('blur', function() {
       if ($("#addTalentForm").valid()) {
           $('#submit-add').prop('disabled', false);
       } else {
           $('#submit-add').prop('disabled', 'disabled');
       }
  });

  $("#addTalentForm").validate({

  });
});
</script>
<!-- ./ Validation for addTalentForm -->

<!-- viewTalentModal -->
<script>
$(document).on("click", "#viewTalentModal", function () {
      $('#viewTalent').openModal();
      var talentId = $(this).data('id');

      $.ajax({
        type: "GET",
        url: "<?php echo site_url();?>administrator/view_talent/"+talentId,
        dataType: "JSON",
        success:function(data){
          $('#viewTalent').openModal();
          $(".modal-content #talent-name").val(data[0].talentName);
          $(".modal-content #category").val(data[0].category);
          $(".modal-content #first-name").val(data[0].firstName);
          $(".modal-content #middle-name").val(data[0].midName);
          $(".modal-content #last-name").val(data[0].lastName);
          $(".modal-content #club-name").val(data[0].clubName);
          $(".modal-content #birthdate").val(data[0].bdate);
          $(".modal-content #birthplace").val(data[0].bop);
          $(".modal-content #manager").val(data[0].manager);
          $(".modal-content #production").val(data[0].productionName);
          $(".modal-content #processing").val(data[0].processing);
          $(".modal-content #doc-send").val(data[0].docSend);
          $(".modal-content #apply").val(data[0].apply);
          $(".modal-content #vis-lic").val(data[0].visaLicense);
          $(".modal-content #in-japan").val(data[0].inJapana);
          $(".modal-content #extension").val(data[0].extension);
          $(".modal-content #fin-visa").val(data[0].finishVisa);
          $(".modal-content #pass-number").val(data[0].passportNo);
          $(".modal-content #pass-exp").val(data[0].passExp);
          $(".modal-content #issu").val(data[0].issu);
          $(".modal-content #image1").val(data[0].pic1);
          $(".modal-content #image2").val(data[0].pic2);
          $(".modal-content #image3").val(data[0].pic3);
          $(".modal-content #image4").val(data[0].pic4);
        },
      });
});
</script>
<!-- ./ viewTalentModal -->

<!-- editTalentModal -->
<script>
$(document).on("click", "#editTalentModal", function () {
      $('#editTalent').openModal();
      var talentId = $(this).data('id');

      $('#editTalentForm').attr('action','<?php echo site_url();?>administrator/edit_talent/'+talentId);

      console.log('EDIT',talentId);
      $.ajax({
        type: "GET",
        url: "<?php echo site_url();?>administrator/view_talent/"+talentId,
        dataType: "JSON",
        success:function(data){
          $('#editTalent').openModal();
          $(".modal-content #edit-talent-name").val(data[0].talentName);
          $(".modal-content #edit-first-name").val(data[0].firstName);
          $(".modal-content #edit-middle-name").val(data[0].midName);
          $(".modal-content #edit-last-name").val(data[0].lastName);
          $(".modal-content #edit-birthdate").val(data[0].bdate);
          $(".modal-content #edit-birthplace").val(data[0].bop);
          $(".modal-content #edit-manager").val(data[0].manager);
          $(".modal-content #edit-processing").val(data[0].processing);
          $(".modal-content #edit-doc-send").val(data[0].docSend);
          $(".modal-content #edit-apply").val(data[0].apply);
          $(".modal-content #edit-vis-lic").val(data[0].visaLicense);
          $(".modal-content #edit-in-japan").val(data[0].inJapana);
          $(".modal-content #edit-extension").val(data[0].extension);
          $(".modal-content #edit-fin-visa").val(data[0].finishVisa);
          $(".modal-content #edit-pass-number").val(data[0].passportNo);
          $(".modal-content #edit-pass-exp").val(data[0].passExp);
          $(".modal-content #edit-issu").val(data[0].issu);
          $(".modal-content #edit-image1").val(data[0].pic1);
          $(".modal-content #edit-image2").val(data[0].pic2);
          $(".modal-content #edit-image3").val(data[0].pic3);
          $(".modal-content #edit-image4").val(data[0].pic4);
        },
      });
});
</script>
<!-- ./ editTalentModal -->

<!-- updateTalentStatModal -->
<script>
$(document).on("click", "#updateTalentStatModal", function () {
      $('#updateTalentStat').openModal();
      var talentId = $(this).data('id');
      $("#talentId").val( talentId );
      $('#updateTalentStatForm').attr('action','<?php echo site_url();?>administrator/update_talent_status/'+talentId);
});
</script>
<!-- ./ updateTalentStatModal -->
