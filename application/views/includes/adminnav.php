<?php $head2 = $this->uri->segment('2'); if( $head2 != 'view_prod' && $head2 != 'view_clubs' && $head2 != 'view_reports' && $head2 != 'view_report' ){?>
<!-- Dropdown Structure -->
<ul id="navbar_dropdown" class="dropdown-content">
  <li><a href="<?php echo site_url('administrator/admin_profile');?>"><i class="material-icons left">perm_identity</i>My Profile</a></li>
  <li class="divider"></li>
  <li><a href="<?php echo site_url('administrator/settings');?>"><i class="material-icons left">settings</i>Account Settings</a></li>
  <li class="divider"></li>
  <li><a href="<?php echo site_url('administrator/logout');?>"><i class="material-icons left">power_settings_new</i>Logout</a></li>
</ul>
<nav>
  <div class="nav-wrapper cyan darken-4">
    <a href="#!" class="brand-logo" style="margin-left:20px;">Man Group Production</a>
    <a href="#" data-activates="mobile-demo" class="button-collapse" style="margin-left:10px;"><i class="material-icons">menu</i></a>
    <ul class="right hide-on-med-and-down">
      <!--home tab  -->
      <li
      <?php
      	$head = $this->uri->segment('1');
        $head2 = $this->uri->segment('2');
      	if($head == 'administrator' && $head2 == '')
      	{
      ?>
      class="active"
      <?php }?>
      >
        <a href="<?php echo base_url('administrator')?>">DASHBOARD</a>
      </li>
      <!--productions tab  -->
      <li
      <?php
      	$head = $this->uri->segment('1');
        $head2 = $this->uri->segment('2');
      	if($head == 'administrator' && $head2 == 'productions')
      	{
      ?>
      class="active"
      <?php }?>
      >
      <a href="<?php echo base_url('administrator/productions')?>">PRODUCTIONS</a>
    </li>
      <!--clubs tab  -->
      <li
      <?php
      	$head = $this->uri->segment('1');
        $head2 = $this->uri->segment('2');
      	if($head == 'administrator' && $head2 == 'clubs')
      	{
      ?>
      class="active"
      <?php }?>
      >
      <a href="<?php echo base_url('administrator/clubs')?>">CLUBS</a>
    </li>
    <!--agencies tab  -->
    <li
    <?php
      $head = $this->uri->segment('1');
      $head2 = $this->uri->segment('2');
      if($head == 'administrator' && $head2 == 'agencies')
      {
    ?>
    class="active"
    <?php }?>
    >
    <a href="<?php echo base_url('administrator/agencies')?>">AGENCIES</a>
  </li>
      <!--talents tab  -->
      <li
      <?php
      	$head = $this->uri->segment('1');
        $head2 = $this->uri->segment('2');
      	if($head == 'administrator' && $head2 == 'talents')
      	{
      ?>
      class="active"
      <?php }?>
      >
      <a href="<?php echo base_url('administrator/talents')?>">TALENTS</a>
    </li>
    <!--accounts tab  -->
      <li
      <?php
       $head = $this->uri->segment('1');
        $head2 = $this->uri->segment('2');
       if($head == 'administrator' && $head2 == 'accounts')
       {
      ?>
      class="active"
      <?php }?>
      >
      <a href="<?php echo base_url('administrator/accounts')?>">ACCOUNTS</a>
    </li>
      <li>
        <!-- Dropdown Trigger -->
        <a class="dropdown-button center-align" href="#" data-activates="navbar_dropdown" style="width: 220px;">
          <?php echo $accountName; ?>
          <i class="material-icons right">keyboard_arrow_down</i>
        </a>
      </li>
    </ul>
    <ul class="side-nav collapsible collapsible-accordion" id="mobile-demo">
      <!--home tab  -->
      <li
      <?php
        $head = $this->uri->segment('1');
        $head2 = $this->uri->segment('2');
        if($head == 'administrator' && $head2 == '')
        {
      ?>
      class="active"
      <?php }?>
      >
        <a href="<?php echo base_url('administrator')?>">DASHBOARD</a>
      </li>
      <!--productions tab  -->
      <li
      <?php
        $head = $this->uri->segment('1');
        $head2 = $this->uri->segment('2');
        if($head == 'administrator' && $head2 == 'productions')
        {
      ?>
      class="active"
      <?php }?>
      >
      <a href="<?php echo base_url('administrator/productions')?>">PRODUCTIONS</a>
    </li>
      <!--clubs tab  -->
      <li
      <?php
        $head = $this->uri->segment('1');
        $head2 = $this->uri->segment('2');
        if($head == 'administrator' && $head2 == 'clubs')
        {
      ?>
      class="active"
      <?php }?>
      >
      <a href="<?php echo base_url('administrator/clubs')?>">CLUBS</a>
    </li>
    <!--productions tab  -->
    <li
    <?php
      $head = $this->uri->segment('1');
      $head2 = $this->uri->segment('2');
      if($head == 'administrator' && $head2 == 'agencies')
      {
    ?>
    class="active"
    <?php }?>
    >
    <a href="<?php echo base_url('administrator/agencies')?>">AGENCIES</a>
  </li>
      <!--talents tab  -->
      <li
      <?php
        $head = $this->uri->segment('1');
        $head2 = $this->uri->segment('2');
        if($head == 'administrator' && $head2 == 'talents')
        {
      ?>
      class="active"
      <?php }?>
      >
      <a href="<?php echo base_url('administrator/talents')?>">TALENTS</a>
    </li>
    <!--accounts tab  -->
      <li
      <?php
       $head = $this->uri->segment('1');
        $head2 = $this->uri->segment('2');
       if($head == 'administrator' && $head2 == 'accounts')
       {
      ?>
      class="active"
      <?php }?>
      >
      <a href="<?php echo base_url('administrator/accounts')?>">ACCOUNTS</a>
    </li>
      <li class="bold active">
        <a class="collapsible-header waves-effect waes-teal">
          <?php echo $accountName; ?>
          <i class="material-icons right">keyboard_arrow_down</i>
        </a>
        <div class="collapsible-body">
          <ul>
            <li><a href="<?php echo site_url('administrator/admin_profile')?>"><i class="material-icons left">perm_identity</i>My Profile</a></li>
            <li><a href="<?php echo site_url('administrator/settings')?>"><i class="material-icons left">settings</i>Account Settings</a></li>
            <li><a href="<?php echo site_url('administrator/logout')?>"><i class="material-icons left">power_settings_new</i>Logout</a></li>
          </ul>
        </div>
      </li>
    </ul>
  </div>
</nav>
<?php }?>

<?php
	$head = $this->uri->segment('1');
  $head2 = $this->uri->segment('2');
	if($head == 'administrator' && $head2 == 'view_prod')
	{
?>
<!-- Dropdown Structure -->
<ul id="navbar_dropdown" class="dropdown-content">
  <li><a href="<?php echo site_url('administrator/admin_profile');?>"><i class="material-icons left">perm_identity</i>My Profile</a></li>
  <li class="divider"></li>
  <li><a href="<?php echo site_url('administrator/settings');?>"><i class="material-icons left">settings</i>Account Settings</a></li>
  <li class="divider"></li>
  <li><a href="<?php echo site_url('administrator/logout');?>"><i class="material-icons left">power_settings_new</i>Logout</a></li>
</ul>
<nav>
  <div class="nav-wrapper indigo darken-4">
    <a href="#!" class="brand-logo" style="margin-left:20px;">Man Group Production</a>
    <a href="#" data-activates="mobile-demo" class="button-collapse" style="margin-left:10px;"><i class="material-icons">menu</i></a>
    <ul class="right hide-on-med-and-down">
      <li class=""><a href="<?php echo site_url('administrator')?>">HOME</a></li>
      <li class="active"><a href="<?php echo site_url('administrator/view_prod/'.$this->uri->segment(3))?>">DASHBOARD</a></li>
      <li class=""><a href="<?php echo site_url('administrator/view_clubs/'.$this->uri->segment(3))?>">CLUBS</a></li>
      <li class=""><a href="<?php echo site_url('administrator/view_reports/'.$this->uri->segment(3))?>">REPORTS</a></li>
      <li>
        <!-- Dropdown Trigger -->
        <a class="dropdown-button center-align" href="#" data-activates="navbar_dropdown" style="width: 220px;">
          <?php echo $accountName; ?>
          <i class="material-icons right">keyboard_arrow_down</i>
        </a>
      </li>
    </ul>
    <ul class="side-nav collapsible collapsible-accordion" id="mobile-demo">
      <li class=""><a href="<?php echo site_url('production')?>">HOME</a></li>
      <li class="active"><a href="<?php echo site_url('administrator/view_prod/'.$this->uri->segment(3))?>">DASHBOARD</a></li>
      <li class=""><a href="<?php echo site_url('administrator/view_clubs/'.$this->uri->segment(3))?>">CLUBS</a></li>
      <li class=""><a href="<?php echo site_url('administrator/view_reports/'.$this->uri->segment(3))?>">REPORTS</a></li>
      <li class="bold active">
        <a class="collapsible-header waves-effect waes-teal">
          <?php echo $accountName; ?>
          <i class="material-icons right">keyboard_arrow_down</i>
        </a>
        <div class="collapsible-body">
          <ul>
            <li><a href="<?php echo site_url('administrator/admin_profile')?>"><i class="material-icons left">perm_identity</i>My Profile</a></li>
            <li><a href="<?php echo site_url('administrator/settings')?>"><i class="material-icons left">settings</i>Account Settings</a></li>
            <li><a href="<?php echo site_url('administrator/logout')?>"><i class="material-icons left">power_settings_new</i>Logout</a></li>
          </ul>
        </div>
      </li>
    </ul>
  </div>
</nav>
<?php }?>

<?php
	$head = $this->uri->segment('1');
  $head2 = $this->uri->segment('2');
	if($head == 'administrator' && $head2 == 'view_clubs')
	{
?>
<!-- Dropdown Structure -->
<ul id="navbar_dropdown" class="dropdown-content">
  <li><a href="<?php echo site_url('administrator/admin_profile');?>"><i class="material-icons left">perm_identity</i>My Profile</a></li>
  <li class="divider"></li>
  <li><a href="<?php echo site_url('administrator/settings');?>"><i class="material-icons left">settings</i>Account Settings</a></li>
  <li class="divider"></li>
  <li><a href="<?php echo site_url('administrator/logout');?>"><i class="material-icons left">power_settings_new</i>Logout</a></li>
</ul>
<nav>
  <div class="nav-wrapper indigo darken-4">
    <a href="#!" class="brand-logo" style="margin-left:20px;">Man Group Production</a>
    <a href="#" data-activates="mobile-demo" class="button-collapse" style="margin-left:10px;"><i class="material-icons">menu</i></a>
    <ul class="right hide-on-med-and-down">
      <li class=""><a href="<?php echo site_url('administrator')?>">HOME</a></li>
      <li class=""><a href="<?php echo site_url('administrator/view_prod/'.$this->uri->segment(3))?>">DASHBOARD</a></li>
      <li class="active"><a href="<?php echo site_url('administrator/view_clubs/'.$this->uri->segment(3))?>">CLUBS</a></li>
      <li class=""><a href="<?php echo site_url('administrator/view_reports/'.$this->uri->segment(3))?>">REPORTS</a></li>
      <li>
        <!-- Dropdown Trigger -->
        <a class="dropdown-button center-align" href="#" data-activates="navbar_dropdown" style="width: 220px;">
          <?php echo $accountName; ?>
          <i class="material-icons right">keyboard_arrow_down</i>
        </a>
      </li>
    </ul>
    <ul class="side-nav collapsible collapsible-accordion" id="mobile-demo">
      <li class=""><a href="<?php echo site_url('production')?>">HOME</a></li>
      <li class=""><a href="<?php echo site_url('administrator/view_prod/'.$this->uri->segment(3))?>">DASHBOARD</a></li>
      <li class="active"><a href="<?php echo site_url('administrator/view_clubs/'.$this->uri->segment(3))?>">CLUBS</a></li>
      <li class=""><a href="<?php echo site_url('administrator/view_reports/'.$this->uri->segment(3))?>">REPORTS</a></li>
      <li class="bold active">
        <a class="collapsible-header waves-effect waes-teal">
          <?php echo $accountName; ?>
          <i class="material-icons right">keyboard_arrow_down</i>
        </a>
        <div class="collapsible-body">
          <ul>
            <li><a href="<?php echo site_url('administrator/admin_profile')?>"><i class="material-icons left">perm_identity</i>My Profile</a></li>
            <li><a href="<?php echo site_url('administrator/settings')?>"><i class="material-icons left">settings</i>Account Settings</a></li>
            <li><a href="<?php echo site_url('administrator/logout')?>"><i class="material-icons left">power_settings_new</i>Logout</a></li>
          </ul>
        </div>
      </li>
    </ul>
  </div>
</nav>
<?php }?>


<?php
	$head = $this->uri->segment('1');
  $head2 = $this->uri->segment('2');
	if($head == 'administrator' && $head2 == 'view_reports')
	{
?>
<!-- Dropdown Structure -->
<ul id="navbar_dropdown" class="dropdown-content">
  <li><a href="<?php echo site_url('administrator/admin_profile');?>"><i class="material-icons left">perm_identity</i>My Profile</a></li>
  <li class="divider"></li>
  <li><a href="<?php echo site_url('administrator/settings');?>"><i class="material-icons left">settings</i>Account Settings</a></li>
  <li class="divider"></li>
  <li><a href="<?php echo site_url('administrator/logout');?>"><i class="material-icons left">power_settings_new</i>Logout</a></li>
</ul>
<nav>
  <div class="nav-wrapper indigo darken-4">
    <a href="#!" class="brand-logo" style="margin-left:20px;">Man Group Production</a>
    <a href="#" data-activates="mobile-demo" class="button-collapse" style="margin-left:10px;"><i class="material-icons">menu</i></a>
    <ul class="right hide-on-med-and-down">
      <li class=""><a href="<?php echo site_url('administrator')?>">HOME</a></li>
      <li class=""><a href="<?php echo site_url('administrator/view_prod/'.$this->uri->segment(3))?>">DASHBOARD</a></li>
      <li class=""><a href="<?php echo site_url('administrator/view_clubs/'.$this->uri->segment(3))?>">CLUBS</a></li>
      <li class="active"><a href="<?php echo site_url('administrator/view_reports/'.$this->uri->segment(3))?>">REPORTS</a></li>
      <li>
        <!-- Dropdown Trigger -->
        <a class="dropdown-button center-align" href="#" data-activates="navbar_dropdown" style="width: 220px;">
          <?php echo $accountName; ?>
          <i class="material-icons right">keyboard_arrow_down</i>
        </a>
      </li>
    </ul>
    <ul class="side-nav collapsible collapsible-accordion" id="mobile-demo">
      <li class=""><a href="<?php echo site_url('production')?>">HOME</a></li>
      <li class=""><a href="<?php echo site_url('administrator/view_prod/'.$this->uri->segment(3))?>">DASHBOARD</a></li>
      <li class=""><a href="<?php echo site_url('administrator/view_clubs/'.$this->uri->segment(3))?>">CLUBS</a></li>
      <li class="active"><a href="<?php echo site_url('administrator/view_reports/'.$this->uri->segment(3))?>">REPORTS</a></li>
      <li class="bold active">
        <a class="collapsible-header waves-effect waes-teal">
          <?php echo $accountName; ?>
          <i class="material-icons right">keyboard_arrow_down</i>
        </a>
        <div class="collapsible-body">
          <ul>
            <li><a href="<?php echo site_url('administrator/admin_profile')?>"><i class="material-icons left">perm_identity</i>My Profile</a></li>
            <li><a href="<?php echo site_url('administrator/settings')?>"><i class="material-icons left">settings</i>Account Settings</a></li>
            <li><a href="<?php echo site_url('administrator/logout')?>"><i class="material-icons left">power_settings_new</i>Logout</a></li>
          </ul>
        </div>
      </li>
    </ul>
  </div>
</nav>
<?php }?>

<?php
	$head = $this->uri->segment('1');
  $head2 = $this->uri->segment('2');
	if($head == 'administrator' && $head2 == 'view_report')
	{
?>
<!-- Dropdown Structure -->
<ul id="navbar_dropdown" class="dropdown-content">
  <li><a href="<?php echo site_url('administrator/admin_profile');?>"><i class="material-icons left">perm_identity</i>My Profile</a></li>
  <li class="divider"></li>
  <li><a href="<?php echo site_url('administrator/settings');?>"><i class="material-icons left">settings</i>Account Settings</a></li>
  <li class="divider"></li>
  <li><a href="<?php echo site_url('administrator/logout');?>"><i class="material-icons left">power_settings_new</i>Logout</a></li>
</ul>
<nav>
  <div class="nav-wrapper indigo darken-4">
    <a href="#!" class="brand-logo" style="margin-left:20px;">Man Group Production</a>
    <a href="#" data-activates="mobile-demo" class="button-collapse" style="margin-left:10px;"><i class="material-icons">menu</i></a>
    <ul class="right hide-on-med-and-down">
      <li class=""><a href="<?php echo site_url('administrator')?>">HOME</a></li>
      <li class=""><a href="<?php echo site_url('administrator/view_prod/'.$this->uri->segment(3))?>">DASHBOARD</a></li>
      <li class=""><a href="<?php echo site_url('administrator/view_clubs/'.$this->uri->segment(3))?>">CLUBS</a></li>
      <li class="active"><a href="<?php echo site_url('administrator/view_reports/'.$this->uri->segment(3))?>">REPORTS</a></li>
      <li>
        <!-- Dropdown Trigger -->
        <a class="dropdown-button center-align" href="#" data-activates="navbar_dropdown" style="width: 220px;">
          <?php echo $accountName; ?>
          <i class="material-icons right">keyboard_arrow_down</i>
        </a>
      </li>
    </ul>
    <ul class="side-nav collapsible collapsible-accordion" id="mobile-demo">
      <li class=""><a href="<?php echo site_url('production')?>">HOME</a></li>
      <li class=""><a href="<?php echo site_url('administrator/view_prod/'.$this->uri->segment(3))?>">DASHBOARD</a></li>
      <li class=""><a href="<?php echo site_url('administrator/view_clubs/'.$this->uri->segment(3))?>">CLUBS</a></li>
      <li class="active"><a href="<?php echo site_url('administrator/view_reports/'.$this->uri->segment(3))?>">REPORTS</a></li>
      <li class="bold active">
        <a class="collapsible-header waves-effect waes-teal">
          <?php echo $accountName; ?>
          <i class="material-icons right">keyboard_arrow_down</i>
        </a>
        <div class="collapsible-body">
          <ul>
            <li><a href="<?php echo site_url('administrator/admin_profile')?>"><i class="material-icons left">perm_identity</i>My Profile</a></li>
            <li><a href="<?php echo site_url('administrator/settings')?>"><i class="material-icons left">settings</i>Account Settings</a></li>
            <li><a href="<?php echo site_url('administrator/logout')?>"><i class="material-icons left">power_settings_new</i>Logout</a></li>
          </ul>
        </div>
      </li>
    </ul>
  </div>
</nav>
<?php }?>
