$(function editProd() {
		$("a.edit_prod").on("click", function(event){
			event.preventDefault();
            $('#editProduction').openModal();
		$.ajax({
		url:'getProdId',
			  type: 'post',
			  data: {'prodid': $(this).data('prodid')},
			  dataType: "json",
			  success: function(data, status) {
				  $('#editProdId').val(data.productionId);
				  $('#editProdName').val(data.productionName);
			  },
			  error: function(xhr, desc, err) {
				console.log(xhr);
				console.log("Details: " + desc + "\nError:" + err);
			  }
      });
	});
});

$(function editClub() {
		$("a.edit_club").on("click", function(event){
			event.preventDefault();
            $('#editClub').openModal();
		$.ajax({
		url:'getClubId',
			  type: 'post',
			  data: {'clubid': $(this).data('clubid')},
			  dataType: "json",
			  success: function(data, status) {
				  $('#uclubId').val(data.clubId);
				  $('#uclubName').val(data.clubName);
          $('#uclubColor').val(data.color);
				  $('#uproduction').val(data.productionId);

			  },
			  error: function(xhr, desc, err) {
				console.log(xhr);
				console.log("Details: " + desc + "\nError:" + err);
			  }
      });
	});
});

$(function editClub() {
		$("a.edit_user").on("click", function(event){
			event.preventDefault();
            $('#editAccount').openModal();
		$.ajax({
		url:'getUserId',
			  type: 'post',
			  data: {'userid': $(this).data('userid')},
			  dataType: "json",
			  success: function(data, status) {
				  $('#uuserId').val(data.userId);
				  $('#uaccountName').val(data.accountName);
          $('#upassword').val(data.password);
				  $('#uproduction').val(data.productionId);
			  },
			  error: function(xhr, desc, err) {
				console.log(xhr);
				console.log("Details: " + desc + "\nError:" + err);
			  }
      });
	});
});
