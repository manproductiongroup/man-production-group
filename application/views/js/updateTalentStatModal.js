$(document).on("click", "#updateTalentStatModal", function () {
      $('#updateTalentStat').openModal();
      var talentId = $(this).data('id');
      $("#talentId").val( talentId );
      $('#updateTalentStatForm').attr('action','<?php echo site_url();?>administrator/update_talent_status/'+talentId);
});
