-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 05, 2016 at 08:05 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `manpro_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE `accounts` (
  `userId` int(11) NOT NULL,
  `accountName` varchar(50) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `productionId` int(11) NOT NULL DEFAULT '0',
  `type` varchar(1) NOT NULL,
  `active` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`userId`, `accountName`, `username`, `password`, `productionId`, `type`, `active`) VALUES
(1, 'Admin', 'admin', 'admin', 0, 'a', 1),
(2, 'Prod1', 'prod1', '1234', 1, 'p', 1);

-- --------------------------------------------------------

--
-- Table structure for table `clubs`
--

CREATE TABLE `clubs` (
  `clubId` int(11) NOT NULL,
  `clubName` varchar(50) NOT NULL,
  `capacity` int(11) NOT NULL,
  `productionId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `clubs`
--

INSERT INTO `clubs` (`clubId`, `clubName`, `capacity`, `productionId`) VALUES
(1, 'Club1', 10, 1),
(2, 'Club2', 10, 2);

-- --------------------------------------------------------

--
-- Table structure for table `pricing`
--

CREATE TABLE `pricing` (
  `priceId` int(11) NOT NULL,
  `talentId` int(11) NOT NULL,
  `origPrice` double NOT NULL,
  `sellingPrice` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `production`
--

CREATE TABLE `production` (
  `productionId` int(11) NOT NULL,
  `productionName` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `production`
--

INSERT INTO `production` (`productionId`, `productionName`) VALUES
(1, 'Prod1'),
(2, 'Prod2');

-- --------------------------------------------------------

--
-- Table structure for table `status_logs`
--

CREATE TABLE `status_logs` (
  `statusId` int(11) NOT NULL,
  `talentId` int(11) NOT NULL,
  `status` enum('applying','processing','preparation','immigaration') NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `status_logs`
--

INSERT INTO `status_logs` (`statusId`, `talentId`, `status`, `date`) VALUES
(1, 1, 'applying', '2017-02-04');

-- --------------------------------------------------------

--
-- Table structure for table `system_logs`
--

CREATE TABLE `system_logs` (
  `logID` int(11) NOT NULL,
  `action` varchar(50) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `accountName` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `system_logs`
--

INSERT INTO `system_logs` (`logID`, `action`, `date`, `accountName`) VALUES
(1, 'Herb logged out.', '2016-09-05 18:01:56', 'Herb'),
(2, 'Admin logged in.', '2016-09-05 18:02:02', 'Admin'),
(3, 'Added new talent TN1', '2016-09-05 18:04:01', 'Admin'),
(4, 'Status update on talent ID 1', '2016-09-05 18:04:38', 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `talents`
--

CREATE TABLE `talents` (
  `talentId` int(11) NOT NULL,
  `lastName` varchar(30) NOT NULL,
  `firstName` varchar(50) NOT NULL,
  `midName` varchar(30) NOT NULL,
  `bdate` date NOT NULL,
  `productionId` int(11) NOT NULL,
  `clubId` int(11) NOT NULL,
  `processing` date NOT NULL,
  `manager` varchar(100) NOT NULL,
  `docSend` date NOT NULL,
  `apply` date NOT NULL,
  `visaLicense` date NOT NULL,
  `inJapan` date NOT NULL,
  `extension` date NOT NULL,
  `finishVisa` date NOT NULL,
  `talentName` varchar(50) NOT NULL,
  `category` varchar(20) NOT NULL,
  `pob` varchar(50) NOT NULL,
  `passportNo` varchar(30) NOT NULL,
  `issu` varchar(30) NOT NULL,
  `passExp` date NOT NULL,
  `pic1` varchar(150) NOT NULL,
  `pic2` varchar(150) NOT NULL,
  `pic3` varchar(150) NOT NULL,
  `pic4` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `talents`
--

INSERT INTO `talents` (`talentId`, `lastName`, `firstName`, `midName`, `bdate`, `productionId`, `clubId`, `processing`, `manager`, `docSend`, `apply`, `visaLicense`, `inJapan`, `extension`, `finishVisa`, `talentName`, `category`, `pob`, `passportNo`, `issu`, `passExp`, `pic1`, `pic2`, `pic3`, `pic4`) VALUES
(1, 'LN1', 'FN1', 'MN1', '1992-02-29', 1, 1, '0000-00-00', 'MAN1', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'TN1', 'Dancer PRO', 'BP1', '12345', '12345', '2016-09-30', 'http://localhost/manpro/imgs/no_image_available.jpeg', 'http://localhost/manpro/imgs/no_image_available.jpeg', 'http://localhost/manpro/imgs/no_image_available.jpeg', 'http://localhost/manpro/imgs/no_image_available.jpeg');

-- --------------------------------------------------------

--
-- Table structure for table `talents_status`
--

CREATE TABLE `talents_status` (
  `statusId` int(11) NOT NULL,
  `talentId` int(11) NOT NULL,
  `status` enum('applying','processing','preparation','immigration') NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `talents_status`
--

INSERT INTO `talents_status` (`statusId`, `talentId`, `status`, `date`) VALUES
(1, 1, 'applying', '2017-02-04');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`userId`);

--
-- Indexes for table `clubs`
--
ALTER TABLE `clubs`
  ADD PRIMARY KEY (`clubId`);

--
-- Indexes for table `pricing`
--
ALTER TABLE `pricing`
  ADD PRIMARY KEY (`priceId`);

--
-- Indexes for table `production`
--
ALTER TABLE `production`
  ADD PRIMARY KEY (`productionId`);

--
-- Indexes for table `status_logs`
--
ALTER TABLE `status_logs`
  ADD PRIMARY KEY (`statusId`);

--
-- Indexes for table `system_logs`
--
ALTER TABLE `system_logs`
  ADD PRIMARY KEY (`logID`);

--
-- Indexes for table `talents`
--
ALTER TABLE `talents`
  ADD PRIMARY KEY (`talentId`);

--
-- Indexes for table `talents_status`
--
ALTER TABLE `talents_status`
  ADD PRIMARY KEY (`statusId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accounts`
--
ALTER TABLE `accounts`
  MODIFY `userId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `clubs`
--
ALTER TABLE `clubs`
  MODIFY `clubId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `pricing`
--
ALTER TABLE `pricing`
  MODIFY `priceId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `production`
--
ALTER TABLE `production`
  MODIFY `productionId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `status_logs`
--
ALTER TABLE `status_logs`
  MODIFY `statusId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `system_logs`
--
ALTER TABLE `system_logs`
  MODIFY `logID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `talents`
--
ALTER TABLE `talents`
  MODIFY `talentId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `talents_status`
--
ALTER TABLE `talents_status`
  MODIFY `statusId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
