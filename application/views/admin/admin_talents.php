<?php header('Content-Type: text/html;charset=utf-8'); ?>
<?php if($this->session->flashdata('success')){?>
  <script type="text/javascript">
    $(document).ready(function(){
        swal({
          title: "Done!",
          text:  '<?php echo $this->session->flashdata('success');?>',
          type: "success",
          timer: 5000,
          showConfirmButton: false
        });
    });
  </script>
<?php }?>
<?php if($this->session->flashdata('error')){?>
  <script type="text/javascript">
    $(document).ready(function(){
      swal({
        title: "Oops! Something went wrong!",
        text:  '<?php echo $this->session->flashdata('error');?>',
        type: "error",
        timer: 5000,
        showConfirmButton: false
      });
    });
  </script>
<?php }?>
<?php if($this->session->flashdata('error_1')){?>
  <script type="text/javascript">
    $(document).ready(function(){
     var $toastContent = $('<span>Image 1 : <?php echo $this->session->flashdata('error_1');?></span>');
     Materialize.toast($toastContent, 5000)
    });
  </script>
<?php }?>
<?php if($this->session->flashdata('error_2')){?>
  <script type="text/javascript">
    $(document).ready(function(){
     var $toastContent = $('<span>Image 2 : <?php echo $this->session->flashdata('error_2');?></span>');
     Materialize.toast($toastContent, 5000)
    });
  </script>
<?php }?>
<?php if($this->session->flashdata('error_3')){?>
  <script type="text/javascript">
    $(document).ready(function(){
     var $toastContent = $('<span>Image 3 : <?php echo $this->session->flashdata('error_3');?></span>');
     Materialize.toast($toastContent, 5000)
    });
  </script>
<?php }?>
<?php if($this->session->flashdata('error_4')){?>
  <script type="text/javascript">
    $(document).ready(function(){
     var $toastContent = $('<span>Image 4 : <?php echo $this->session->flashdata('error_4');?></span>');
     Materialize.toast($toastContent, 5000)
    });
  </script>
<?php }?>
<body>

  <br>

  <div class="right-align" id="google_translate_element"></div><script type="text/javascript">
  function googleTranslateElementInit() {
    new google.translate.TranslateElement({pageLanguage: 'en', includedLanguages: 'en,ja,tl', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
  }
  </script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>

<div class="section no-pad-bot" id="index-banner">
  <div class="container">
    <br><br>
    <h3 class="header center cyan-text text-darken-3">Talents</h3>
    <br><br>
  </div>
</div>

<div class="container">
    <table id="admin_talents_table" class="bordered centered responsive-table" cellspacing="0" width="100%">
      <thead>
        <tr>
            <th>Talent ID</th>
            <th>Status</th>
            <th>Talent Name</th>
            <th>Last Name</th>
            <th>First Name</th>
            <th>Production</th>
            <th>Club</th>
            <th class="right-align">Options</th>
        </tr>
      </thead>
      <tbody>
      </tbody>
    </table>

    <br/><br/><br/><br/><br/>
    <div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
      <a class="btn-floating btn-large waves-effect waves-light green-dark modal-trigger" href="#addTalent">
        <i class="material-icons">add</i>
      </a>
    </div>
</div>

<?php $this->load->view('modals/edit_talent_modal');?>
<?php $this->load->view('modals/update_status_modal');?>
<?php $this->load->view('modals/add_talent_modal');?>

<script type="text/javascript">
var method; //for save method string
var table;

$( document ).ready(function() {
  table = $('#admin_talents_table').DataTable({
        "processing": true,
        "serverSide": true,
        "order": [],
        "ajax": {
            "url": "<?php echo site_url('administrator/get_talents')?>",
            "type": "POST"
        },
        "columnDefs": [
          // {
          //     "targets": [ -1 ],
          //     "orderable": false,
          //     "class": "right-align"
          // },
          { "width": "5%", "targets": 0 },
          { "width": "10%", "targets": 0 },
          { "width": "5%", "targets": 0 },
          { "width": "5%", "targets": 0 },
          { "width": "5%", "targets": 0 },
          { "width": "20%", "targets": 0 },
          { "width": "20%", "targets": 0 },
          { "width": "40%", "targets": 0 },
        ]
    });

    $('.modal-trigger').leanModal();

    $('.tooltipped').tooltip({delay: 50});

    $('select').material_select();

    $('.materialboxed').materialbox();

    //UPDATE TALENT STAT
    $.validator.setDefaults({
        errorClass: 'invalid',
        validClass: "valid",
        errorPlacement: function (error, element) {
            $(element)
                .closest("form")
                .find("label[for='" + element.attr("id") + "']")
                .attr('data-error', error.text());
        }
    });

    $('input').on('blur', function() {
         if ($("#updateTalentStatForm").valid()) {
            $('#submit-update').prop('disabled', false);
         } else {
             $('#submit-update').prop('disabled', 'disabled');
         }
    });

    $("#updateTalentStatForm").validate({

    });
});

function view_talent_bio(id){
  console.log('hi');
}

function delete_talent(id){
  swal({    title: "Confirmation",
              text: "Are you sure you want to delete TALENT "+id+"?",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#f44336",
              confirmButtonText: "Yes, delete it!",
              closeOnConfirm: false },
              function(isConfirm){
                if (isConfirm) {
                    $.ajax({
                        url : "<?php echo site_url('administrator/delete_talent_where')?>/"+id,
                        type: "POST",
                        dataType: "JSON",
                        success: function(data)
                        {
                          swal({   title: "Deleted",
                                   text: "TALENT "+id+" successfully deleted!",
                                   type: "success",
                                   timer: 1000,
                                   showConfirmButton: false
                          },
                          function (isConfirm){
                              location.reload(true);
                          }
                          );
                        },
                        error: function (jqXHR, textStatus, errorThrown)
                        {
                          alert('An error occured while deleting data');
                        }
                    });
                }
              });

}
</script>
