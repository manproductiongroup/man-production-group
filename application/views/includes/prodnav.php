<?php
	$head = $this->uri->segment('1');
  $head2 = $this->uri->segment('2');
	if($head == 'production' && $head2 == '')
	{
?>

<!-- Dropdown Structure -->
<ul id="navbar_dropdown" class="dropdown-content">
  <li><a href="<?php echo site_url('production/prod_profile');?>"><i class="material-icons left">settings</i>Account Settings</a></li>
  <li class="divider"></li>
  <li><a href="<?php echo site_url('production/settings');?>"><i class="material-icons left">book</i>Logs</a></li>
  <li class="divider"></li>
  <li><a href="<?php echo site_url('production/logout');?>"><i class="material-icons left">power_settings_new</i>Logout</a></li>
</ul>
<nav>
  <div class="nav-wrapper indigo darken-4">
    <a href="#!" class="brand-logo" style="margin-left:20px;">Man Group Production</a>
    <a href="#" data-activates="mobile-demo" class="button-collapse" style="margin-left:10px;"><i class="material-icons">menu</i></a>
    <ul class="right hide-on-med-and-down">
      <li class="active"><a href="<?php echo site_url('production')?>">DASHBOARD</a></li>
      <li class=""><a href="<?php echo site_url('production/clubs')?>">CLUBS</a></li>
      <li class=""><a href="<?php echo site_url('production/reports')?>">REPORTS</a></li>
      <li>
        <!-- Dropdown Trigger -->
        <a class="dropdown-button center-align" href="#" data-activates="navbar_dropdown" style="width: 220px;">
          <?php echo $accountName; ?>
          <i class="material-icons right">keyboard_arrow_down</i>
        </a>
      </li>
    </ul>
    <ul class="side-nav collapsible collapsible-accordion" id="mobile-demo">
      <li class="active"><a href="<?php echo site_url('production')?>">DASHBOARD</a></li>
      <li class=""><a href="<?php echo site_url('production/clubs')?>">CLUBS</a></li>
      <li class=""><a href="<?php echo site_url('production/reports')?>">REPORTS</a></li>
      <li class="bold active">
        <a class="collapsible-header waves-effect waes-teal">
          <?php echo $accountName; ?>
          <i class="material-icons right">keyboard_arrow_down</i>
        </a>
        <div class="collapsible-body">
          <ul>
            <li><a href="<?php echo site_url('production/prod_profile')?>"><i class="material-icons left">settings</i>Settings</a></li>
            <li><a href="<?php echo site_url('production/settings')?>"><i class="material-icons left">book</i>Logs</a></li>
            <li><a href="<?php echo site_url('production/logout')?>"><i class="material-icons left">power_settings_new</i>Logout</a></li>
          </ul>
        </div>
      </li>
    </ul>
  </div>
</nav>
<?php } ?>

<?php
	$head = $this->uri->segment('1');
  $clubs = $this->uri->segment('2');
	if($head == 'production' && $clubs == 'clubs')
	{
?>

<!-- Dropdown Structure -->
<ul id="navbar_dropdown" class="dropdown-content">
  <li><a href="<?php echo site_url('production/prod_profile');?>"><i class="material-icons left">settings</i>Account Settings</a></li>
  <li class="divider"></li>
  <li><a href="<?php echo site_url('production/settings');?>"><i class="material-icons left">book</i>Logs</a></li>
  <li class="divider"></li>
  <li><a href="<?php echo site_url('production/logout');?>"><i class="material-icons left">power_settings_new</i>Logout</a></li>
</ul>
<nav>
  <div class="nav-wrapper indigo darken-4">
    <a href="#!" class="brand-logo" style="margin-left:20px;">Man Group Production</a>
    <a href="#" data-activates="mobile-demo" class="button-collapse" style="margin-left:10px;"><i class="material-icons">menu</i></a>
    <ul class="right hide-on-med-and-down">
      <li class=""><a href="<?php echo site_url('production')?>">DASHBOARD</a></li>
      <li class="active"><a href="<?php echo site_url('production/clubs')?>">CLUBS</a></li>
      <li class=""><a href="<?php echo site_url('production/reports')?>">REPORTS</a></li>
      <li>
        <!-- Dropdown Trigger -->
        <a class="dropdown-button center-align" href="#" data-activates="navbar_dropdown" style="width: 220px;">
          <?php echo $accountName; ?>
          <i class="material-icons right">keyboard_arrow_down</i>
        </a>
      </li>
    </ul>
    <ul class="side-nav collapsible collapsible-accordion" id="mobile-demo">
      <li class=""><a href="<?php echo site_url('production')?>">DASHBOARD</a></li>
      <li class="active"><a href="<?php echo site_url('production/clubs')?>">CLUBS</a></li>
      <li class=""><a href="<?php echo site_url('production/reports')?>">REPORTS</a></li>
      <li class="bold active">
        <a class="collapsible-header waves-effect waes-teal">
          <?php echo $accountName; ?>
          <i class="material-icons right">keyboard_arrow_down</i>
        </a>
        <div class="collapsible-body">
          <ul>
            <li><a href="<?php echo site_url('production/prod_profile')?>"><i class="material-icons left">settings</i>Account Settings</a></li>
            <li><a href="<?php echo site_url('production/settings')?>"><i class="material-icons left">book</i>Logs</a></li>
            <li><a href="<?php echo site_url('production/logout')?>"><i class="material-icons left">power_settings_new</i>Logout</a></li>
          </ul>
        </div>
      </li>
    </ul>
  </div>
</nav>

<?php }?>

<?php
	$head = $this->uri->segment('1');
  $clubs = $this->uri->segment('2');
	if($head == 'production' && $clubs == 'reports')
	{
?>

<!-- Dropdown Structure -->
<ul id="navbar_dropdown" class="dropdown-content">
  <li><a href="<?php echo site_url('production/prod_profile');?>"><i class="material-icons left">settings</i>Account Settings</a></li>
  <li class="divider"></li>
  <li><a href="<?php echo site_url('production/settings');?>"><i class="material-icons left">book</i>Logs</a></li>
  <li class="divider"></li>
  <li><a href="<?php echo site_url('production/logout');?>"><i class="material-icons left">power_settings_new</i>Logout</a></li>
</ul>
<nav>
  <div class="nav-wrapper indigo darken-4">
    <a href="#!" class="brand-logo" style="margin-left:20px;">Man Group Production</a>
    <a href="#" data-activates="mobile-demo" class="button-collapse" style="margin-left:10px;"><i class="material-icons">menu</i></a>
    <ul class="right hide-on-med-and-down">
      <li class=""><a href="<?php echo site_url('production')?>">DASHBOARD</a></li>
      <li class=""><a href="<?php echo site_url('production/clubs')?>">CLUBS</a></li>
      <li class="active"><a href="<?php echo site_url('production/reports')?>">REPORTS</a></li>
      <li>
        <!-- Dropdown Trigger -->
        <a class="dropdown-button center-align" href="#" data-activates="navbar_dropdown" style="width: 220px;">
          <?php echo $accountName; ?>
          <i class="material-icons right">keyboard_arrow_down</i>
        </a>
      </li>
    </ul>
    <ul class="side-nav collapsible collapsible-accordion" id="mobile-demo">
      <li class=""><a href="<?php echo site_url('production')?>">DASHBOARD</a></li>
      <li class=""><a href="<?php echo site_url('production/clubs')?>">CLUBS</a></li>
      <li class="active"><a href="<?php echo site_url('production/reports')?>">REPORTS</a></li>
      <li class="bold active">
        <a class="collapsible-header waves-effect waes-teal">
          <?php echo $accountName; ?>
          <i class="material-icons right">keyboard_arrow_down</i>
        </a>
        <div class="collapsible-body">
          <ul>
            <li><a href="<?php echo site_url('production/prod_profile')?>"><i class="material-icons left">settings</i>Account Settings</a></li>
            <li><a href="<?php echo site_url('production/settings')?>"><i class="material-icons left">book</i>Logs</a></li>
            <li><a href="<?php echo site_url('production/logout')?>"><i class="material-icons left">power_settings_new</i>Logout</a></li>
          </ul>
        </div>
      </li>
    </ul>
  </div>
</nav>

<?php }?>

<?php
	$head = $this->uri->segment('1');
  $clubs = $this->uri->segment('2');
	if($head == 'production' && $clubs == 'view_report')
	{
?>

<!-- Dropdown Structure -->
<ul id="navbar_dropdown" class="dropdown-content">
  <li><a href="<?php echo site_url('production/prod_profile');?>"><i class="material-icons left">settings</i>Account Settings</a></li>
  <li class="divider"></li>
  <li><a href="<?php echo site_url('production/settings');?>"><i class="material-icons left">book</i>Logs</a></li>
  <li class="divider"></li>
  <li><a href="<?php echo site_url('production/logout');?>"><i class="material-icons left">power_settings_new</i>Logout</a></li>
</ul>
<nav>
  <div class="nav-wrapper indigo darken-4">
    <a href="#!" class="brand-logo" style="margin-left:20px;">Man Group Production</a>
    <a href="#" data-activates="mobile-demo" class="button-collapse" style="margin-left:10px;"><i class="material-icons">menu</i></a>
    <ul class="right hide-on-med-and-down">
      <li class=""><a href="<?php echo site_url('production')?>">DASHBOARD</a></li>
      <li class=""><a href="<?php echo site_url('production/clubs')?>">CLUBS</a></li>
      <li class="active"><a href="<?php echo site_url('production/reports')?>">REPORTS</a></li>
      <li>
        <!-- Dropdown Trigger -->
        <a class="dropdown-button center-align" href="#" data-activates="navbar_dropdown" style="width: 220px;">
          <?php echo $accountName; ?>
          <i class="material-icons right">keyboard_arrow_down</i>
        </a>
      </li>
    </ul>
    <ul class="side-nav collapsible collapsible-accordion" id="mobile-demo">
      <li class=""><a href="<?php echo site_url('production')?>">DASHBOARD</a></li>
      <li class=""><a href="<?php echo site_url('production/clubs')?>">CLUBS</a></li>
      <li class="active"><a href="<?php echo site_url('production/reports')?>">REPORTS</a></li>
      <li class="bold active">
        <a class="collapsible-header waves-effect waes-teal">
          <?php echo $accountName; ?>
          <i class="material-icons right">keyboard_arrow_down</i>
        </a>
        <div class="collapsible-body">
          <ul>
            <li><a href="<?php echo site_url('production/prod_profile')?>"><i class="material-icons left">settings</i>Account Settings</a></li>
            <li><a href="<?php echo site_url('production/settings')?>"><i class="material-icons left">book</i>Logs</a></li>
            <li><a href="<?php echo site_url('production/logout')?>"><i class="material-icons left">power_settings_new</i>Logout</a></li>
          </ul>
        </div>
      </li>
    </ul>
  </div>
</nav>

<?php }?>


<?php
	$head = $this->uri->segment('1');
  $charts = $this->uri->segment('2');
	if($head == 'production' && $charts == 'get_status')
	{
?>

<!-- Dropdown Structure -->
<ul id="navbar_dropdown" class="dropdown-content">
  <li><a href="<?php echo site_url('production/prod_profile');?>"><i class="material-icons left">settings</i>Account Settings</a></li>
  <li class="divider"></li>
  <li><a href="<?php echo site_url('production/settings');?>"><i class="material-icons left">book</i>Logs</a></li>
  <li class="divider"></li>
  <li><a href="<?php echo site_url('production/logout');?>"><i class="material-icons left">power_settings_new</i>Logout</a></li>
</ul>
<nav>
  <div class="nav-wrapper indigo darken-4">
    <a href="#!" class="brand-logo" style="margin-left:20px;">Man Group Production</a>
    <a href="#" data-activates="mobile-demo" class="button-collapse" style="margin-left:10px;"><i class="material-icons">menu</i></a>
    <ul class="right hide-on-med-and-down">
      <li class=""><a href="<?php echo site_url('production')?>">DASHBOARD</a></li>
      <li class="active"><a href="<?php echo site_url('production/clubs')?>">CLUBS</a></li>
      <li class=""><a href="<?php echo site_url('production/reports')?>">REPORTS</a></li>
      <li>
        <!-- Dropdown Trigger -->
        <a class="dropdown-button center-align" href="#" data-activates="navbar_dropdown" style="width: 220px;">
          <?php echo $accountName; ?>
          <i class="material-icons right">keyboard_arrow_down</i>
        </a>
      </li>
    </ul>
    <ul class="side-nav collapsible collapsible-accordion" id="mobile-demo">
      <li class=""><a href="<?php echo site_url('production')?>">DASHBOARD</a></li>
      <li class="active"><a href="<?php echo site_url('production/clubs')?>">CLUBS</a></li>
      <li class=""><a href="<?php echo site_url('production/reports')?>">REPORTS</a></li>
      <li class="bold active">
        <a class="collapsible-header waves-effect waes-teal">
          <?php echo $accountName; ?>
          <i class="material-icons right">keyboard_arrow_down</i>
        </a>
        <div class="collapsible-body">
          <ul>
            <li><a href="<?php echo site_url('production/prod_profile')?>"><i class="material-icons left">settings</i>Account Settings</a></li>
            <li><a href="<?php echo site_url('production/settings')?>"><i class="material-icons left">book</i>Logs</a></li>
            <li><a href="<?php echo site_url('production/logout')?>"><i class="material-icons left">power_settings_new</i>Logout</a></li>
          </ul>
        </div>
      </li>
    </ul>
  </div>
</nav>

<?php }?>

<?php
	$head = $this->uri->segment('1');
  $charts = $this->uri->segment('2');
	if($head == 'production' && $charts == 'get_all_stat_club')
	{
?>

<!-- Dropdown Structure -->
<ul id="navbar_dropdown" class="dropdown-content">
  <li><a href="<?php echo site_url('production/prod_profile');?>"><i class="material-icons left">settings</i>Account Settings</a></li>
  <li class="divider"></li>
  <li><a href="<?php echo site_url('production/settings');?>"><i class="material-icons left">book</i>Logs</a></li>
  <li class="divider"></li>
  <li><a href="<?php echo site_url('production/logout');?>"><i class="material-icons left">power_settings_new</i>Logout</a></li>
</ul>
<nav>
  <div class="nav-wrapper indigo darken-4">
    <a href="#!" class="brand-logo" style="margin-left:20px;">Man Group Production</a>
    <a href="#" data-activates="mobile-demo" class="button-collapse" style="margin-left:10px;"><i class="material-icons">menu</i></a>
    <ul class="right hide-on-med-and-down">
      <li class="active"><a href="<?php echo site_url('production')?>">DASHBOARD</a></li>
      <li class=""><a href="<?php echo site_url('production/clubs')?>">CLUBS</a></li>
      <li class=""><a href="<?php echo site_url('production/reports')?>">REPORTS</a></li>
      <li>
        <!-- Dropdown Trigger -->
        <a class="dropdown-button center-align" href="#" data-activates="navbar_dropdown" style="width: 220px;">
          <?php echo $accountName; ?>
          <i class="material-icons right">keyboard_arrow_down</i>
        </a>
      </li>
    </ul>
    <ul class="side-nav collapsible collapsible-accordion" id="mobile-demo">
      <li class="active"><a href="<?php echo site_url('production')?>">DASHBOARD</a></li>
      <li class=""><a href="<?php echo site_url('production/clubs')?>">CLUBS</a></li>
      <li class=""><a href="<?php echo site_url('production/reports')?>">REPORTS</a></li>
      <li class="bold active">
        <a class="collapsible-header waves-effect waes-teal">
          <?php echo $accountName; ?>
          <i class="material-icons right">keyboard_arrow_down</i>
        </a>
        <div class="collapsible-body">
          <ul>
            <li><a href="<?php echo site_url('production/prod_profile')?>"><i class="material-icons left">settings</i>Account Settings</a></li>
            <li><a href="<?php echo site_url('production/settings')?>"><i class="material-icons left">book</i>Logs</a></li>
            <li><a href="<?php echo site_url('production/logout')?>"><i class="material-icons left">power_settings_new</i>Logout</a></li>
          </ul>
        </div>
      </li>
    </ul>
  </div>
</nav>

<?php }?>

<?php
	$head = $this->uri->segment('1');
  $charts = $this->uri->segment('2');
	if($head == 'production' && $charts == 'get_all_club')
	{
?>

<!-- Dropdown Structure -->
<ul id="navbar_dropdown" class="dropdown-content">
  <li><a href="<?php echo site_url('production/prod_profile');?>"><i class="material-icons left">settings</i>Account Settings</a></li>
  <li class="divider"></li>
  <li><a href="<?php echo site_url('production/settings');?>"><i class="material-icons left">book</i>Logs</a></li>
  <li class="divider"></li>
  <li><a href="<?php echo site_url('production/logout');?>"><i class="material-icons left">power_settings_new</i>Logout</a></li>
</ul>
<nav>
  <div class="nav-wrapper indigo darken-4">
    <a href="#!" class="brand-logo" style="margin-left:20px;">Man Group Production</a>
    <a href="#" data-activates="mobile-demo" class="button-collapse" style="margin-left:10px;"><i class="material-icons">menu</i></a>
    <ul class="right hide-on-med-and-down">
      <li class="active"><a href="<?php echo site_url('production')?>">DASHBOARD</a></li>
      <li class=""><a href="<?php echo site_url('production/clubs')?>">CLUBS</a></li>
      <li class=""><a href="<?php echo site_url('production/reports')?>">REPORTS</a></li>
      <li>
        <!-- Dropdown Trigger -->
        <a class="dropdown-button center-align" href="#" data-activates="navbar_dropdown" style="width: 220px;">
          <?php echo $accountName; ?>
          <i class="material-icons right">keyboard_arrow_down</i>
        </a>
      </li>
    </ul>
    <ul class="side-nav collapsible collapsible-accordion" id="mobile-demo">
      <li class="active"><a href="<?php echo site_url('production')?>">DASHBOARD</a></li>
      <li class=""><a href="<?php echo site_url('production/clubs')?>">CLUBS</a></li>
      <li class=""><a href="<?php echo site_url('production/reports')?>">REPORTS</a></li>
      <li class="bold active">
        <a class="collapsible-header waves-effect waes-teal">
          <?php echo $accountName; ?>
          <i class="material-icons right">keyboard_arrow_down</i>
        </a>
        <div class="collapsible-body">
          <ul>
            <li><a href="<?php echo site_url('production/prod_profile')?>"><i class="material-icons left">settings</i>Account Settings</a></li>
            <li><a href="<?php echo site_url('production/settings')?>"><i class="material-icons left">book</i>Logs</a></li>
            <li><a href="<?php echo site_url('production/logout')?>"><i class="material-icons left">power_settings_new</i>Logout</a></li>
          </ul>
        </div>
      </li>
    </ul>
  </div>
</nav>

<?php }?>

<?php
	$head = $this->uri->segment('1');
  $head2 = $this->uri->segment('2');
	if($head == 'production' && $head2 == 'prod_profile')
	{
?>

<!-- Dropdown Structure -->
<ul id="navbar_dropdown" class="dropdown-content">
  <li><a href="<?php echo site_url('production/prod_profile');?>"><i class="material-icons left">settings</i>Account Settings</a></li>
  <li class="divider"></li>
  <li><a href="<?php echo site_url('production/settings');?>"><i class="material-icons left">book</i>Logs</a></li>
  <li class="divider"></li>
  <li><a href="<?php echo site_url('production/logout');?>"><i class="material-icons left">power_settings_new</i>Logout</a></li>
</ul>
<nav>
  <div class="nav-wrapper indigo darken-4">
    <a href="#!" class="brand-logo" style="margin-left:20px;">Man Group Production</a>
    <a href="#" data-activates="mobile-demo" class="button-collapse" style="margin-left:10px;"><i class="material-icons">menu</i></a>
    <ul class="right hide-on-med-and-down">
      <li class="active"><a href="<?php echo site_url('production')?>">DASHBOARD</a></li>
      <li class=""><a href="<?php echo site_url('production/clubs')?>">CLUBS</a></li>
      <li class=""><a href="<?php echo site_url('production/reports')?>">REPORTS</a></li>
      <li>
        <!-- Dropdown Trigger -->
        <a class="dropdown-button center-align" href="#" data-activates="navbar_dropdown" style="width: 220px;">
          <?php echo $accountName; ?>
          <i class="material-icons right">keyboard_arrow_down</i>
        </a>
      </li>
    </ul>
    <ul class="side-nav collapsible collapsible-accordion" id="mobile-demo">
      <li class="active"><a href="<?php echo site_url('production')?>">DASHBOARD</a></li>
      <li class=""><a href="<?php echo site_url('production/clubs')?>">CLUBS</a></li>
      <li class=""><a href="<?php echo site_url('production/reports')?>">REPORTS</a></li>
      <li class="bold active">
        <a class="collapsible-header waves-effect waes-teal">
          <?php echo $accountName; ?>
          <i class="material-icons right">keyboard_arrow_down</i>
        </a>
        <div class="collapsible-body">
          <ul>
            <li><a href="<?php echo site_url('production/prod_profile')?>"><i class="material-icons left">settings</i>Account Settings</a></li>
            <li><a href="<?php echo site_url('production/settings')?>"><i class="material-icons left">book</i>Logs</a></li>
            <li><a href="<?php echo site_url('production/logout')?>"><i class="material-icons left">power_settings_new</i>Logout</a></li>
          </ul>
        </div>
      </li>
    </ul>
  </div>
</nav>
<?php } ?>
