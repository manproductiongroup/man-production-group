-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 15, 2016 at 06:19 PM
-- Server version: 5.6.21
-- PHP Version: 5.5.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `manpro_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE IF NOT EXISTS `accounts` (
`userId` int(11) NOT NULL,
  `accountName` varchar(50) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `productionId` int(11) NOT NULL DEFAULT '0',
  `type` varchar(1) NOT NULL,
  `active` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`userId`, `accountName`, `username`, `password`, `productionId`, `type`, `active`) VALUES
(1, 'MGP ADMIN', 'mgp_admin', 'mgp2016', 0, 'a', 1),
(2, 'mgp_production', 'mgp_production', 'mgp2016', 0, 'p', 1),
(3, 'JP Promotion', 'JP Promotion', 'manpro2016', 2, 'p', 1),
(4, 'Eastern Trade', 'Eastern Trade', 'manpro2016', 3, 'p', 1),
(5, 'Star Field', 'Star Field', 'manpro2016', 4, 'p', 1),
(6, 'Herb', 'herb', 'herb123', 0, 'a', 1);

-- --------------------------------------------------------

--
-- Table structure for table `agency`
--

CREATE TABLE IF NOT EXISTS `agency` (
`agencyId` int(11) NOT NULL,
  `agencyName` varchar(50) NOT NULL,
  `address` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `agency`
--

INSERT INTO `agency` (`agencyId`, `agencyName`, `address`) VALUES
(1, 'JCJ', ''),
(2, 'Aqium/Sunrise', ''),
(3, 'AML', ''),
(4, 'Mayon', ''),
(5, 'Twin Star', ''),
(6, 'Victoria', '');

-- --------------------------------------------------------

--
-- Table structure for table `clubs`
--

CREATE TABLE IF NOT EXISTS `clubs` (
`clubId` int(11) NOT NULL,
  `clubName` varchar(50) NOT NULL,
  `color` varchar(20) NOT NULL DEFAULT '#000000',
  `productionId` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `clubs`
--

INSERT INTO `clubs` (`clubId`, `clubName`, `color`, `productionId`) VALUES
(1, 'Kai', '#000000', 2),
(2, 'Precious', '#000000', 2),
(3, 'Romantiko', '#000000', 2),
(5, 'New Marie', '#000000', 4),
(6, 'Vision', '#000000', 4),
(8, 'Not Yet Assigned - SF', '#000000', 4),
(9, 'Not Yet Assigned - ET', '#000000', 3),
(10, 'Not Yet Assigned - JP', '#000000', 2);

-- --------------------------------------------------------

--
-- Table structure for table `expenses`
--

CREATE TABLE IF NOT EXISTS `expenses` (
`expenseId` int(11) NOT NULL,
  `productionId` int(11) NOT NULL,
  `entryName` varchar(50) NOT NULL,
  `price` double NOT NULL,
  `date` date NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=cp932;

-- --------------------------------------------------------

--
-- Table structure for table `production`
--

CREATE TABLE IF NOT EXISTS `production` (
`productionId` int(11) NOT NULL,
  `productionName` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `production`
--

INSERT INTO `production` (`productionId`, `productionName`) VALUES
(2, 'JP Promotion'),
(3, 'Eastern Trade'),
(4, 'Star Field');

-- --------------------------------------------------------

--
-- Table structure for table `sales`
--

CREATE TABLE IF NOT EXISTS `sales` (
`salesId` int(11) NOT NULL,
  `productionId` int(11) NOT NULL,
  `talentId` int(11) NOT NULL,
  `gross` double NOT NULL,
  `talentFee` double NOT NULL,
  `managerCom` double NOT NULL,
  `total` double NOT NULL,
  `dateSale` date NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=111 DEFAULT CHARSET=cp932;

--
-- Dumping data for table `sales`
--

INSERT INTO `sales` (`salesId`, `productionId`, `talentId`, `gross`, `talentFee`, `managerCom`, `total`, `dateSale`, `timestamp`) VALUES
(1, 4, 111, 100000, 60000, 40000, 0, '0000-00-00', '2016-11-02 09:35:23'),
(2, 4, 99, 80000, 40000, 40000, 0, '0000-00-00', '2016-11-02 09:36:07'),
(3, 4, 100, 70000, 50000, 20000, 0, '0000-00-00', '2016-11-02 09:36:44'),
(4, 4, 106, 100000, 60000, 40000, 0, '0000-00-00', '2016-11-02 09:37:01'),
(5, 2, 98, 110000, 70000, 40000, 0, '0000-00-00', '2016-11-02 09:37:35'),
(6, 2, 92, 110000, 70000, 40000, 0, '0000-00-00', '2016-11-02 09:38:00'),
(7, 2, 93, 120000, 80000, 40000, 0, '0000-00-00', '2016-11-02 09:38:20'),
(8, 2, 94, 100000, 60000, 40000, 0, '0000-00-00', '2016-11-02 09:38:44'),
(9, 2, 95, 90000, 50000, 40000, 0, '0000-00-00', '2016-11-02 09:39:04'),
(10, 4, 101, 80000, 50000, 30000, 0, '0000-00-00', '2016-11-02 09:39:40'),
(11, 4, 102, 100000, 60000, 40000, 0, '0000-00-00', '2016-11-02 09:39:58'),
(12, 4, 103, 90000, 50000, 40000, 0, '0000-00-00', '2016-11-02 09:40:20'),
(13, 4, 104, 100000, 70000, 30000, 0, '0000-00-00', '2016-11-02 09:40:54'),
(14, 4, 105, 80000, 40000, 40000, 0, '0000-00-00', '2016-11-02 09:41:15'),
(15, 4, 107, 120000, 80000, 40000, 0, '0000-00-00', '2016-11-02 09:41:39'),
(16, 4, 108, 100000, 60000, 40000, 0, '0000-00-00', '2016-11-02 10:21:03'),
(17, 2, 96, 100000, 60000, 40000, 0, '0000-00-00', '2016-11-02 10:22:42'),
(18, 2, 97, 100000, 60000, 40000, 0, '0000-00-00', '2016-11-02 10:23:57'),
(19, 4, 109, 80000, 40000, 40000, 0, '0000-00-00', '2016-11-02 10:24:20'),
(20, 4, 110, 120000, 80000, 40000, 0, '0000-00-00', '2016-11-02 10:24:31'),
(21, 2, 134, 90000, 50000, 40000, 0, '0000-00-00', '2016-11-03 03:36:14'),
(22, 2, 87, 80000, 40000, 40000, 0, '0000-00-00', '2016-11-03 04:14:34'),
(23, 2, 88, 80000, 40000, 40000, 0, '0000-00-00', '2016-11-03 04:14:57'),
(24, 2, 89, 80000, 40000, 40000, 0, '0000-00-00', '2016-11-03 04:15:20'),
(25, 2, 90, 80000, 40000, 40000, 0, '0000-00-00', '2016-11-03 04:15:46'),
(26, 2, 32, 80000, 40000, 40000, 0, '0000-00-00', '2016-11-03 04:16:11'),
(27, 2, 33, 80000, 40000, 40000, 0, '0000-00-00', '2016-11-03 04:16:39'),
(28, 2, 30, 90000, 50000, 40000, 0, '0000-00-00', '2016-11-03 04:18:04'),
(29, 2, 29, 90000, 50000, 40000, 0, '0000-00-00', '2016-11-03 04:20:10'),
(30, 2, 28, 90000, 50000, 40000, 0, '0000-00-00', '2016-11-03 04:18:29'),
(31, 2, 31, 90000, 50000, 40000, 0, '0000-00-00', '2016-11-03 04:18:39'),
(32, 2, 131, 100000, 60000, 40000, 0, '0000-00-00', '2016-11-03 05:52:39'),
(33, 2, 132, 100000, 60000, 40000, 0, '0000-00-00', '2016-11-03 05:53:33'),
(34, 2, 133, 110000, 70000, 40000, 0, '0000-00-00', '2016-11-03 05:54:21'),
(35, 2, 130, 80000, 40000, 40000, 0, '0000-00-00', '2016-11-03 06:35:51'),
(36, 2, 124, 80000, 40000, 40000, 0, '0000-00-00', '2016-11-03 06:42:20'),
(37, 2, 125, 80000, 40000, 40000, 0, '0000-00-00', '2016-11-03 06:42:43'),
(38, 2, 126, 80000, 40000, 40000, 0, '0000-00-00', '2016-11-03 06:43:10'),
(39, 2, 127, 80000, 40000, 40000, 0, '0000-00-00', '2016-11-03 06:43:52'),
(40, 2, 128, 80000, 40000, 40000, 0, '0000-00-00', '2016-11-03 06:44:23'),
(41, 2, 129, 80000, 40000, 40000, 0, '0000-00-00', '2016-11-03 06:44:56'),
(42, 2, 21, 80000, 40000, 40000, 0, '0000-00-00', '2016-11-04 03:18:17'),
(43, 2, 20, 80000, 40000, 40000, 0, '0000-00-00', '2016-11-04 03:17:46'),
(44, 2, 22, 80000, 40000, 40000, 0, '0000-00-00', '2016-11-04 03:18:42'),
(45, 2, 27, 80000, 40000, 40000, 0, '0000-00-00', '2016-11-04 03:19:00'),
(46, 4, 44, 80000, 40000, 40000, 0, '0000-00-00', '2016-11-04 03:19:46'),
(47, 4, 45, 80000, 40000, 40000, 0, '0000-00-00', '2016-11-04 03:20:17'),
(48, 4, 47, 80000, 40000, 40000, 0, '0000-00-00', '2016-11-04 03:20:48'),
(49, 4, 52, 80000, 40000, 40000, 0, '0000-00-00', '2016-11-04 03:21:09'),
(50, 2, 40, 80000, 40000, 40000, 0, '0000-00-00', '2016-11-04 03:22:26'),
(51, 2, 51, 80000, 40000, 40000, 0, '0000-00-00', '2016-11-04 09:39:53'),
(52, 2, 41, 80000, 40000, 40000, 0, '0000-00-00', '2016-11-04 03:25:03'),
(53, 2, 42, 80000, 40000, 40000, 0, '0000-00-00', '2016-11-04 03:25:14'),
(54, 2, 43, 80000, 40000, 40000, 0, '0000-00-00', '2016-11-04 03:25:24'),
(55, 2, 46, 80000, 40000, 40000, 0, '0000-00-00', '2016-11-04 03:25:35'),
(56, 2, 91, 80000, 40000, 40000, 0, '0000-00-00', '2016-11-04 03:25:45'),
(57, 2, 53, 80000, 40000, 40000, 0, '0000-00-00', '2016-11-04 03:25:55'),
(58, 3, 66, 80000, 40000, 40000, 0, '0000-00-00', '2016-11-04 03:26:05'),
(59, 2, 112, 80000, 40000, 40000, 0, '0000-00-00', '2016-11-04 03:26:13'),
(60, 3, 67, 80000, 40000, 40000, 0, '0000-00-00', '2016-11-04 03:26:23'),
(61, 3, 68, 80000, 40000, 40000, 0, '0000-00-00', '2016-11-04 03:26:34'),
(62, 3, 63, 75000, 40000, 35000, 0, '0000-00-00', '2016-11-04 03:28:39'),
(63, 3, 60, 75000, 40000, 35000, 0, '0000-00-00', '2016-11-04 03:28:49'),
(64, 3, 61, 75000, 40000, 35000, 0, '0000-00-00', '2016-11-04 03:29:01'),
(65, 3, 62, 75000, 40000, 35000, 0, '0000-00-00', '2016-11-04 03:29:13'),
(66, 3, 64, 75000, 40000, 35000, 0, '0000-00-00', '2016-11-04 03:29:23'),
(67, 3, 65, 75000, 40000, 35000, 0, '0000-00-00', '2016-11-04 03:29:34'),
(68, 2, 18, 90000, 50000, 40000, 0, '0000-00-00', '2016-11-04 06:44:18'),
(69, 2, 57, 120000, 80000, 40000, 0, '0000-00-00', '2016-11-04 10:12:29'),
(70, 3, 84, 0, 0, 0, 0, '0000-00-00', '2016-11-04 08:42:05'),
(71, 3, 83, 0, 0, 0, 0, '0000-00-00', '2016-11-04 08:42:12'),
(72, 3, 82, 0, 0, 0, 0, '0000-00-00', '2016-11-04 08:42:19'),
(73, 3, 81, 0, 0, 0, 0, '0000-00-00', '2016-11-04 08:45:11'),
(74, 3, 80, 0, 0, 0, 0, '0000-00-00', '2016-11-04 08:45:16'),
(75, 3, 79, 0, 0, 0, 0, '0000-00-00', '2016-11-04 08:45:31'),
(76, 3, 78, 0, 0, 0, 0, '0000-00-00', '2016-11-04 08:45:37'),
(77, 3, 77, 0, 0, 0, 0, '0000-00-00', '2016-11-04 08:45:43'),
(78, 3, 76, 0, 0, 0, 0, '0000-00-00', '2016-11-04 08:45:50'),
(79, 2, 54, 90000, 50000, 40000, 0, '0000-00-00', '2016-11-04 10:13:20'),
(80, 2, 55, 90000, 50000, 40000, 0, '0000-00-00', '2016-11-04 10:13:28'),
(81, 2, 86, 0, 0, 0, 0, '0000-00-00', '2016-11-04 09:16:14'),
(82, 4, 38, 0, 0, 0, 0, '0000-00-00', '2016-11-04 09:24:26'),
(83, 4, 37, 0, 0, 0, 0, '0000-00-00', '2016-11-04 09:24:32'),
(84, 4, 36, 0, 0, 0, 0, '0000-00-00', '2016-11-04 09:24:38'),
(85, 2, 56, 100000, 60000, 40000, 0, '0000-00-00', '2016-11-04 10:14:43'),
(86, 2, 50, 90000, 50000, 40000, 0, '0000-00-00', '2016-11-04 10:14:01'),
(87, 2, 49, 90000, 50000, 40000, 0, '0000-00-00', '2016-11-04 10:13:49'),
(88, 2, 48, 90000, 50000, 40000, 0, '0000-00-00', '2016-11-04 10:13:42'),
(89, 2, 14, 0, 0, 0, 0, '0000-00-00', '2016-11-04 09:25:38'),
(90, 2, 15, 110000, 70000, 40000, 0, '0000-00-00', '2016-11-04 10:15:45'),
(91, 2, 17, 100000, 60000, 40000, 0, '0000-00-00', '2016-11-04 10:15:04'),
(92, 2, 16, 100000, 60000, 40000, 0, '0000-00-00', '2016-11-04 10:14:56'),
(93, 4, 58, 0, 0, 0, 0, '0000-00-00', '2016-11-04 09:26:39'),
(94, 4, 59, 0, 0, 0, 0, '0000-00-00', '2016-11-04 09:27:04'),
(95, 4, 85, 0, 0, 0, 0, '0000-00-00', '2016-11-04 09:27:35'),
(96, 4, 121, 100000, 80000, 20000, 0, '0000-00-00', '2016-11-04 15:07:45'),
(97, 4, 118, 90000, 70000, 20000, 0, '0000-00-00', '2016-11-04 15:10:41'),
(98, 4, 122, 90000, 70000, 20000, 0, '0000-00-00', '2016-11-04 15:11:03'),
(99, 4, 119, 90000, 70000, 20000, 0, '0000-00-00', '2016-11-04 15:11:23'),
(100, 4, 123, 90000, 70000, 20000, 0, '0000-00-00', '2016-11-04 15:11:54'),
(101, 4, 120, 90000, 70000, 20000, 0, '0000-00-00', '2016-11-04 15:12:16'),
(102, 4, 116, 90000, 70000, 20000, 0, '0000-00-00', '2016-11-04 15:12:38'),
(103, 4, 117, 90000, 70000, 20000, 0, '0000-00-00', '2016-11-04 15:13:13'),
(104, 4, 69, 90000, 70000, 20000, 0, '0000-00-00', '2016-11-07 02:33:00'),
(105, 4, 70, 90000, 70000, 20000, 0, '0000-00-00', '2016-11-07 02:33:09'),
(106, 4, 71, 90000, 70000, 20000, 0, '0000-00-00', '2016-11-07 02:33:19'),
(107, 4, 72, 90000, 70000, 20000, 0, '0000-00-00', '2016-11-07 02:33:27'),
(108, 4, 75, 90000, 70000, 20000, 0, '0000-00-00', '2016-11-07 02:33:46'),
(109, 4, 74, 100000, 80000, 20000, 0, '0000-00-00', '2016-11-07 02:34:14'),
(110, 4, 73, 90000, 70000, 20000, 0, '0000-00-00', '2016-11-07 02:35:40');

-- --------------------------------------------------------

--
-- Table structure for table `status_logs`
--

CREATE TABLE IF NOT EXISTS `status_logs` (
`statusId` int(11) NOT NULL,
  `talentId` int(11) NOT NULL,
  `status` enum('applying','processing','preparation','immigaration') NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `system_logs`
--

CREATE TABLE IF NOT EXISTS `system_logs` (
`logID` int(11) NOT NULL,
  `action` varchar(50) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `accountName` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=779 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `system_logs`
--

INSERT INTO `system_logs` (`logID`, `action`, `date`, `accountName`) VALUES
(1, 'MGP ADMIN logged in.', '2016-09-25 16:41:29', 'MGP ADMIN'),
(2, 'MGP ADMIN logged out.', '2016-09-25 16:42:08', 'MGP ADMIN'),
(3, 'MGP ADMIN logged in.', '2016-09-25 16:47:58', 'MGP ADMIN'),
(4, 'MGP ADMIN logged out.', '2016-09-25 16:55:04', 'MGP ADMIN'),
(5, 'MGP ADMIN logged in.', '2016-09-26 02:38:49', 'MGP ADMIN'),
(6, 'Added production and account for mgp_production', '2016-09-26 02:40:30', 'MGP ADMIN'),
(7, 'MGP ADMIN logged out.', '2016-09-26 02:40:39', 'MGP ADMIN'),
(8, 'MGP ADMIN logged in.', '2016-09-26 02:41:54', 'MGP ADMIN'),
(9, 'Deleted production id 1', '2016-09-26 02:42:18', 'MGP ADMIN'),
(10, 'Deleted production id 1', '2016-09-26 02:42:22', 'MGP ADMIN'),
(11, 'Updated account mgp_production', '2016-09-26 02:42:51', 'MGP ADMIN'),
(12, 'MGP ADMIN logged out.', '2016-09-26 02:43:04', 'MGP ADMIN'),
(13, 'mgp_production logged in.', '2016-09-26 02:43:49', 'mgp_production'),
(14, 'mgp_production logged out.', '2016-09-26 02:44:05', 'mgp_production'),
(15, 'MGP ADMIN logged in.', '2016-09-26 02:44:29', 'MGP ADMIN'),
(16, 'Added production and account for JP Promotion', '2016-09-26 02:45:04', 'MGP ADMIN'),
(17, 'Added club Kai', '2016-09-26 02:45:55', 'MGP ADMIN'),
(18, 'Added club Precious', '2016-09-26 02:46:14', 'MGP ADMIN'),
(19, 'Added club Romantiko', '2016-09-26 02:46:27', 'MGP ADMIN'),
(20, 'Paulene Alcazar Sarmiento biodata updated.', '2016-09-26 02:53:16', 'MGP ADMIN'),
(21, 'MGP ADMIN logged out.', '2016-09-26 03:02:40', 'MGP ADMIN'),
(22, 'MGP ADMIN logged in.', '2016-09-26 03:56:13', 'MGP ADMIN'),
(23, 'MGP ADMIN logged in.', '2016-09-26 06:46:57', 'MGP ADMIN'),
(24, 'MGP ADMIN logged in.', '2016-09-27 03:57:52', 'MGP ADMIN'),
(25, 'Paulene Alcazar Sarmiento biodata updated.', '2016-09-27 04:00:30', 'MGP ADMIN'),
(26, 'Paulene Alcazar Sarmiento biodata updated.', '2016-09-27 04:03:52', 'MGP ADMIN'),
(27, 'Paulene Alcazar Sarmiento biodata updated.', '2016-09-27 04:04:22', 'MGP ADMIN'),
(28, 'Allaine Marie Marqueses Rico biodata updated.', '2016-09-27 04:04:53', 'MGP ADMIN'),
(29, 'Allaine Marie Marqueses Rico biodata updated.', '2016-09-27 04:05:07', 'MGP ADMIN'),
(30, 'Allaine Marie Marqueses Rico biodata updated.', '2016-09-27 04:05:52', 'MGP ADMIN'),
(31, 'Allaine Marie Marqueses Rico biodata updated.', '2016-09-27 04:05:56', 'MGP ADMIN'),
(32, 'Joyce Fernando Gonzales biodata updated.', '2016-09-27 04:06:05', 'MGP ADMIN'),
(33, 'Allaine Marie Marqueses Rico biodata updated.', '2016-09-27 04:06:36', 'MGP ADMIN'),
(34, 'Joyce Fernando Gonzales biodata updated.', '2016-09-27 04:07:07', 'MGP ADMIN'),
(35, 'MGP ADMIN logged in.', '2016-09-27 05:16:28', 'MGP ADMIN'),
(36, 'MGP ADMIN logged in.', '2016-09-27 08:49:50', 'MGP ADMIN'),
(37, 'Joyce Fernando Gonzales biodata updated.', '2016-09-27 08:52:25', 'MGP ADMIN'),
(38, 'MGP ADMIN logged in.', '2016-09-27 09:55:48', 'MGP ADMIN'),
(39, 'Joyce Fernando Gonzales biodata updated.', '2016-09-27 09:56:33', 'MGP ADMIN'),
(40, 'MGP ADMIN logged in.', '2016-09-29 03:11:39', 'MGP ADMIN'),
(41, 'Added production and account for Eastern Trade', '2016-09-29 03:12:37', 'MGP ADMIN'),
(42, 'Added production and account for Star Field', '2016-09-29 03:12:53', 'MGP ADMIN'),
(43, 'Paulene Alcazar Sarmiento biodata updated.', '2016-09-29 03:13:47', 'MGP ADMIN'),
(44, 'Added production and account for NONE', '2016-09-29 03:14:25', 'MGP ADMIN'),
(45, 'Added club NONE', '2016-09-29 03:14:36', 'MGP ADMIN'),
(46, 'Added club New Marie', '2016-09-29 03:14:54', 'MGP ADMIN'),
(47, 'Paulene Alcazar Sarmiento biodata updated.', '2016-09-29 03:15:24', 'MGP ADMIN'),
(48, 'Allaine Marie Marqueses Rico biodata updated.', '2016-09-29 03:15:44', 'MGP ADMIN'),
(49, 'Soleta Lazaga Sarona biodata updated.', '2016-09-29 03:17:03', 'MGP ADMIN'),
(50, 'Joyce Fernando Gonzales biodata updated.', '2016-09-29 03:21:09', 'MGP ADMIN'),
(51, 'Joyce Fernando Gonzales biodata updated.', '2016-09-29 03:22:46', 'MGP ADMIN'),
(52, 'Joyce Fernando Gonzales biodata updated.', '2016-09-29 03:23:00', 'MGP ADMIN'),
(53, 'Updated club Precious', '2016-09-29 03:25:56', 'MGP ADMIN'),
(54, 'Updated club Kai', '2016-09-29 03:26:12', 'MGP ADMIN'),
(55, 'Updated club Precious', '2016-09-29 03:26:18', 'MGP ADMIN'),
(56, 'Updated club Romantiko', '2016-09-29 03:26:24', 'MGP ADMIN'),
(57, 'Updated club New Marie', '2016-09-29 03:26:30', 'MGP ADMIN'),
(58, 'Deleted production id 5', '2016-09-29 03:29:25', 'MGP ADMIN'),
(59, 'MGP ADMIN logged in.', '2016-09-29 03:34:27', 'MGP ADMIN'),
(60, 'Updated club Kai', '2016-09-29 03:34:41', 'MGP ADMIN'),
(61, 'Updated club Precious', '2016-09-29 03:34:50', 'MGP ADMIN'),
(62, 'Updated club Romantiko', '2016-09-29 03:34:58', 'MGP ADMIN'),
(63, 'Updated club New Marie', '2016-09-29 03:35:05', 'MGP ADMIN'),
(64, 'MGP ADMIN logged in.', '2016-09-30 03:05:49', 'MGP ADMIN'),
(65, 'Updated club Kai', '2016-09-30 03:32:28', 'MGP ADMIN'),
(66, 'Updated club Precious', '2016-09-30 03:32:44', 'MGP ADMIN'),
(67, 'Updated club New Marie', '2016-09-30 03:33:08', 'MGP ADMIN'),
(68, 'Updated club Romantiko', '2016-09-30 03:33:15', 'MGP ADMIN'),
(69, 'Jonna Gersalia Gelilio biodata updated.', '2016-09-30 03:33:47', 'MGP ADMIN'),
(70, 'Jonna Gersalia Gelilio biodata updated.', '2016-09-30 03:33:50', 'MGP ADMIN'),
(71, 'MGP ADMIN logged in.', '2016-09-30 04:04:34', 'MGP ADMIN'),
(72, 'MGP ADMIN logged in.', '2016-09-30 05:47:09', 'MGP ADMIN'),
(73, 'MGP ADMIN logged in.', '2016-09-30 07:55:33', 'MGP ADMIN'),
(74, 'MGP ADMIN logged in.', '2016-09-30 09:24:25', 'MGP ADMIN'),
(75, 'MGP ADMIN logged in.', '2016-09-30 09:57:26', 'MGP ADMIN'),
(76, 'try biodata updated.', '2016-09-30 10:00:00', 'MGP ADMIN'),
(77, 'try biodata updated.', '2016-09-30 10:00:57', 'MGP ADMIN'),
(78, 'Jonna Gersalia Gelilio biodata updated.', '2016-09-30 11:22:01', 'MGP ADMIN'),
(79, 'Janica Serrano Mariano biodata updated.', '2016-09-30 11:22:26', 'MGP ADMIN'),
(80, 'Yandra Lowella Cabillan biodata updated.', '2016-09-30 11:22:36', 'MGP ADMIN'),
(81, 'Cyrille Herrera De leon biodata updated.', '2016-09-30 11:22:50', 'MGP ADMIN'),
(82, 'Roxanne Javier Tolentino biodata updated.', '2016-09-30 11:23:15', 'MGP ADMIN'),
(83, 'Jenyviv Gagarin Riño biodata updated.', '2016-09-30 11:23:39', 'MGP ADMIN'),
(84, 'Iriskasy Lago Bulan biodata updated.', '2016-09-30 11:23:57', 'MGP ADMIN'),
(85, 'Rhea Yumol Santiago biodata updated.', '2016-09-30 11:24:36', 'MGP ADMIN'),
(86, 'Ernalyn Lompero Lantin biodata updated.', '2016-09-30 11:25:08', 'MGP ADMIN'),
(87, 'Rose Anne Conception Cruzada biodata updated.', '2016-09-30 11:25:26', 'MGP ADMIN'),
(88, 'Angielyn Placa Celajes biodata updated.', '2016-09-30 11:25:51', 'MGP ADMIN'),
(89, 'Amberly Balansag Quitilen biodata updated.', '2016-09-30 11:26:22', 'MGP ADMIN'),
(90, 'Quennie Saladio Lamos biodata updated.', '2016-09-30 11:26:33', 'MGP ADMIN'),
(91, 'Charmine Aradillos Largo biodata updated.', '2016-09-30 11:26:43', 'MGP ADMIN'),
(92, 'Amapula Paubsanon Rodriguez biodata updated.', '2016-09-30 11:26:52', 'MGP ADMIN'),
(93, 'Ma. Cristina Llaneta Gasmin biodata updated.', '2016-09-30 11:27:03', 'MGP ADMIN'),
(94, 'Rica Mercurio Villa biodata updated.', '2016-09-30 11:27:17', 'MGP ADMIN'),
(95, 'Angelica Guno Hernandez biodata updated.', '2016-09-30 11:27:30', 'MGP ADMIN'),
(96, 'Kathleen Nario Gipson biodata updated.', '2016-09-30 11:27:47', 'MGP ADMIN'),
(97, 'Joyce Fernando Gonzales biodata updated.', '2016-09-30 11:28:04', 'MGP ADMIN'),
(98, 'MGP ADMIN logged in.', '2016-10-01 09:13:51', 'MGP ADMIN'),
(99, 'Updated club New Marie', '2016-10-01 09:14:23', 'MGP ADMIN'),
(100, 'Added club Vision', '2016-10-01 09:14:50', 'MGP ADMIN'),
(101, 'MGP ADMIN logged in.', '2016-10-01 09:20:00', 'MGP ADMIN'),
(102, 'Added production and account for None', '2016-10-01 09:20:31', 'MGP ADMIN'),
(103, 'Added club None.', '2016-10-01 09:22:25', 'MGP ADMIN'),
(104, 'MGP ADMIN logged in.', '2016-10-01 16:59:41', 'MGP ADMIN'),
(105, 'MGP ADMIN logged in.', '2016-10-01 17:46:53', 'MGP ADMIN'),
(106, 'Richelle Joy Armogenia Baltazar biodata updated.', '2016-10-01 17:48:03', 'MGP ADMIN'),
(107, 'Richelle Joy Armogenia Baltazar biodata updated.', '2016-10-01 17:48:15', 'MGP ADMIN'),
(108, 'Richelle Joy Armogenia Baltazar biodata updated.', '2016-10-01 17:48:26', 'MGP ADMIN'),
(109, 'MGP ADMIN logged in.', '2016-10-02 01:18:57', 'MGP ADMIN'),
(110, 'MGP ADMIN logged in.', '2016-10-02 02:01:08', 'MGP ADMIN'),
(111, 'try2 biodata updated.', '2016-10-02 02:06:22', 'MGP ADMIN'),
(112, 'MGP ADMIN logged in.', '2016-10-02 02:52:40', 'MGP ADMIN'),
(113, 'MGP ADMIN logged in.', '2016-10-02 02:54:09', 'MGP ADMIN'),
(114, 'MGP ADMIN logged in.', '2016-10-02 07:44:09', 'MGP ADMIN'),
(115, 'MGP ADMIN logged in.', '2016-10-02 18:03:35', 'MGP ADMIN'),
(116, 'Joyce Fernando Gonzales biodata updated.', '2016-10-02 18:03:58', 'MGP ADMIN'),
(117, 'Joyce Fernando Gonzales biodata updated.', '2016-10-02 18:07:04', 'MGP ADMIN'),
(118, 'Kathleen Nario Gipson biodata updated.', '2016-10-02 18:07:21', 'MGP ADMIN'),
(119, 'Angelica Guno Hernandez biodata updated.', '2016-10-02 18:07:31', 'MGP ADMIN'),
(120, 'Rica Mercurio Villa biodata updated.', '2016-10-02 18:07:41', 'MGP ADMIN'),
(121, 'Ma. Cristina Llaneta Gasmin biodata updated.', '2016-10-02 18:07:54', 'MGP ADMIN'),
(122, 'Amapula Paubsanon Rodriguez biodata updated.', '2016-10-02 18:08:02', 'MGP ADMIN'),
(123, 'Charmine Aradillos Largo biodata updated.', '2016-10-02 18:08:19', 'MGP ADMIN'),
(124, 'Quennie Saladio Lamos biodata updated.', '2016-10-02 18:08:31', 'MGP ADMIN'),
(125, 'Amberly Balansag Quitilen biodata updated.', '2016-10-02 18:08:39', 'MGP ADMIN'),
(126, 'Angielyn Placa Celajes biodata updated.', '2016-10-02 18:08:52', 'MGP ADMIN'),
(127, 'Rose Anne Conception Cruzada biodata updated.', '2016-10-02 18:08:59', 'MGP ADMIN'),
(128, 'Ernalyn Lompero Lantin biodata updated.', '2016-10-02 18:09:06', 'MGP ADMIN'),
(129, 'Rhea Yumol Santiago biodata updated.', '2016-10-02 18:09:15', 'MGP ADMIN'),
(130, 'Iriskasy Lago Bulan biodata updated.', '2016-10-02 18:09:24', 'MGP ADMIN'),
(131, 'Jenyviv Gagarin Riño biodata updated.', '2016-10-02 18:18:23', 'MGP ADMIN'),
(132, 'Roxanne Javier Tolentino biodata updated.', '2016-10-02 18:18:39', 'MGP ADMIN'),
(133, 'Cyrille Herrera De leon biodata updated.', '2016-10-02 18:18:53', 'MGP ADMIN'),
(134, 'Yandra Lowella Cabillan biodata updated.', '2016-10-02 18:19:03', 'MGP ADMIN'),
(135, 'Janica Serrano Mariano biodata updated.', '2016-10-02 18:19:12', 'MGP ADMIN'),
(136, 'Jonna Gersalia Gelilio biodata updated.', '2016-10-02 18:19:27', 'MGP ADMIN'),
(137, 'Paulene Alcazar Sarmiento biodata updated.', '2016-10-02 18:19:36', 'MGP ADMIN'),
(138, 'Allaine Marie Marqueses Rico biodata updated.', '2016-10-02 18:19:46', 'MGP ADMIN'),
(139, 'Soleta Lazaga Sarona biodata updated.', '2016-10-02 18:19:56', 'MGP ADMIN'),
(140, 'Catherine Comeros Lehao biodata updated.', '2016-10-02 18:21:59', 'MGP ADMIN'),
(141, 'Danna Daien Alimento Lopez biodata updated.', '2016-10-02 18:22:09', 'MGP ADMIN'),
(142, 'Maria Criselle Desipeda Sanglay biodata updated.', '2016-10-02 18:22:20', 'MGP ADMIN'),
(143, 'Julianne Cajara David biodata updated.', '2016-10-02 18:22:30', 'MGP ADMIN'),
(144, 'Mary Ann Marzan Montaño biodata updated.', '2016-10-02 18:22:40', 'MGP ADMIN'),
(145, 'Jemelline Juico Carpio biodata updated.', '2016-10-02 18:22:49', 'MGP ADMIN'),
(146, 'Raquel Santos Sunga biodata updated.', '2016-10-02 18:23:03', 'MGP ADMIN'),
(147, 'Lovely Charmaine Fernandez Bonifacio biodata updat', '2016-10-02 18:23:12', 'MGP ADMIN'),
(148, 'Queenie Averilla Espiritu biodata updated.', '2016-10-02 18:23:20', 'MGP ADMIN'),
(149, 'Anna Rose Marchan Salavante biodata updated.', '2016-10-02 18:23:30', 'MGP ADMIN'),
(150, 'Ma. Gellin Santos Razo biodata updated.', '2016-10-02 18:23:42', 'MGP ADMIN'),
(151, 'Ma. Gellin Santos Razo biodata updated.', '2016-10-02 18:23:52', 'MGP ADMIN'),
(152, 'Leslie May Acmac biodata updated.', '2016-10-02 18:24:08', 'MGP ADMIN'),
(153, 'Maricae Verbara biodata updated.', '2016-10-02 18:24:25', 'MGP ADMIN'),
(154, 'Maria Lilibeth Policarpio biodata updated.', '2016-10-02 18:24:40', 'MGP ADMIN'),
(155, 'Santio Cundangan biodata updated.', '2016-10-02 18:24:53', 'MGP ADMIN'),
(156, 'Christine May Pangahin biodata updated.', '2016-10-02 18:25:09', 'MGP ADMIN'),
(157, 'Abigael Agbon biodata updated.', '2016-10-02 18:25:38', 'MGP ADMIN'),
(158, 'Maryrose Bayonito biodata updated.', '2016-10-02 18:25:46', 'MGP ADMIN'),
(159, 'Jennifer Urquico biodata updated.', '2016-10-02 18:25:55', 'MGP ADMIN'),
(160, 'Sheila Manzano Elaurza biodata updated.', '2016-10-02 18:26:02', 'MGP ADMIN'),
(161, 'Jennifer Prado biodata updated.', '2016-10-02 18:26:12', 'MGP ADMIN'),
(162, 'Nikkie Rose Montes biodata updated.', '2016-10-02 18:26:23', 'MGP ADMIN'),
(163, 'Rose Diane Calungsud biodata updated.', '2016-10-02 18:26:36', 'MGP ADMIN'),
(164, 'Kc-lyn Serran biodata updated.', '2016-10-02 18:26:48', 'MGP ADMIN'),
(165, 'Jourwel Bautista biodata updated.', '2016-10-02 18:27:05', 'MGP ADMIN'),
(166, 'Jedda Apolinario biodata updated.', '2016-10-02 18:27:14', 'MGP ADMIN'),
(167, 'Jocelyn Caborloy biodata updated.', '2016-10-02 18:27:27', 'MGP ADMIN'),
(168, 'Jaziel Cruz biodata updated.', '2016-10-02 18:27:35', 'MGP ADMIN'),
(169, 'Amy Tatad biodata updated.', '2016-10-02 18:27:43', 'MGP ADMIN'),
(170, 'Giovanni Realino Sauro biodata updated.', '2016-10-02 18:27:50', 'MGP ADMIN'),
(171, 'Carlito Balagtas Mata biodata updated.', '2016-10-02 18:27:57', 'MGP ADMIN'),
(172, 'Maricar San Antonio Dellomas biodata updated.', '2016-10-02 18:28:07', 'MGP ADMIN'),
(173, 'Joyce Ann Garcia Songcuan biodata updated.', '2016-10-02 18:28:18', 'MGP ADMIN'),
(174, 'Jessel Dela Cruz Francisco biodata updated.', '2016-10-02 18:28:28', 'MGP ADMIN'),
(175, 'Sheilla Marie Piad biodata updated.', '2016-10-02 18:28:41', 'MGP ADMIN'),
(176, 'Richelle Joy Armogenia Baltazar biodata updated.', '2016-10-02 18:28:49', 'MGP ADMIN'),
(177, 'MGP ADMIN logged out.', '2016-10-02 18:37:55', 'MGP ADMIN'),
(178, 'JP Promotion logged in.', '2016-10-02 18:38:06', 'JP Promotion'),
(179, 'MGP ADMIN logged in.', '2016-10-03 03:50:54', 'MGP ADMIN'),
(180, 'MGP ADMIN logged in.', '2016-10-03 03:51:08', 'MGP ADMIN'),
(181, 'Jenyviv Gagarin Riño biodata updated.', '2016-10-03 03:55:38', 'MGP ADMIN'),
(182, 'Jonna Gersalia Gelilio biodata updated.', '2016-10-03 03:55:58', 'MGP ADMIN'),
(183, 'Jonna Gersalia Gelilio biodata updated.', '2016-10-03 03:56:00', 'MGP ADMIN'),
(184, 'Jonna Gersalia Gelilio biodata updated.', '2016-10-03 03:56:34', 'MGP ADMIN'),
(185, 'Jonna Gersalia Gelilio biodata updated.', '2016-10-03 04:01:47', 'MGP ADMIN'),
(186, 'MGP ADMIN logged in.', '2016-10-03 04:02:34', 'MGP ADMIN'),
(187, 'MGP ADMIN logged out.', '2016-10-03 04:06:12', 'MGP ADMIN'),
(188, 'mgp_production logged in.', '2016-10-03 04:07:47', 'mgp_production'),
(189, 'mgp_production logged out.', '2016-10-03 04:08:35', 'mgp_production'),
(190, 'MGP ADMIN logged in.', '2016-10-03 04:32:09', 'MGP ADMIN'),
(191, 'MGP ADMIN logged in.', '2016-10-03 05:00:11', 'MGP ADMIN'),
(192, 'Added new talent test test test', '2016-10-03 05:00:46', 'MGP ADMIN'),
(193, 'Added new talent test teste test', '2016-10-03 05:01:54', 'MGP ADMIN'),
(194, 'test biodata updated.', '2016-10-03 05:15:05', 'MGP ADMIN'),
(195, 'MGP ADMIN logged out.', '2016-10-03 05:15:35', 'MGP ADMIN'),
(196, 'MGP ADMIN logged in.', '2016-10-03 09:31:55', 'MGP ADMIN'),
(197, 'MGP ADMIN logged in.', '2016-10-03 10:21:57', 'MGP ADMIN'),
(198, 'MGP ADMIN logged out.', '2016-10-03 10:25:24', 'MGP ADMIN'),
(199, 'JP Promotion logged in.', '2016-10-03 10:25:53', 'JP Promotion'),
(200, 'MGP ADMIN logged in.', '2016-10-03 11:40:50', 'MGP ADMIN'),
(201, 'MGP ADMIN logged in.', '2016-10-04 02:42:39', 'MGP ADMIN'),
(202, 'MGP ADMIN logged in.', '2016-10-04 03:47:03', 'MGP ADMIN'),
(203, 'MGP ADMIN logged in.', '2016-10-05 05:21:36', 'MGP ADMIN'),
(204, 'MGP ADMIN logged in.', '2016-10-06 04:21:53', 'MGP ADMIN'),
(205, 'MGP ADMIN logged out.', '2016-10-06 04:23:06', 'MGP ADMIN'),
(206, 'MGP ADMIN logged in.', '2016-10-06 05:22:29', 'MGP ADMIN'),
(207, 'Added new talent   ', '2016-10-06 05:24:53', 'MGP ADMIN'),
(208, 'Added new talent #76', '2016-10-06 05:24:53', 'MGP ADMIN'),
(209, 'Added new talent   ', '2016-10-06 05:25:57', 'MGP ADMIN'),
(210, 'Added new talent #77', '2016-10-06 05:25:57', 'MGP ADMIN'),
(211, 'Added new talent   ', '2016-10-06 05:26:42', 'MGP ADMIN'),
(212, 'Added new talent #78', '2016-10-06 05:26:42', 'MGP ADMIN'),
(213, 'Added new talent   ', '2016-10-06 05:27:26', 'MGP ADMIN'),
(214, 'Added new talent #79', '2016-10-06 05:27:26', 'MGP ADMIN'),
(215, 'Added new talent   ', '2016-10-06 05:28:15', 'MGP ADMIN'),
(216, 'Added new talent #80', '2016-10-06 05:28:15', 'MGP ADMIN'),
(217, 'Added new talent   ', '2016-10-06 05:28:58', 'MGP ADMIN'),
(218, 'Added new talent #81', '2016-10-06 05:28:58', 'MGP ADMIN'),
(219, 'Added new talent   ', '2016-10-06 05:29:39', 'MGP ADMIN'),
(220, 'Added new talent #82', '2016-10-06 05:29:39', 'MGP ADMIN'),
(221, 'Added new talent   ', '2016-10-06 05:30:18', 'MGP ADMIN'),
(222, 'Added new talent #83', '2016-10-06 05:30:18', 'MGP ADMIN'),
(223, 'Added new talent   ', '2016-10-06 05:30:56', 'MGP ADMIN'),
(224, 'Added new talent #84', '2016-10-06 05:30:56', 'MGP ADMIN'),
(225, 'Added new talent   ', '2016-10-06 05:34:58', 'MGP ADMIN'),
(226, 'MGP ADMIN logged in.', '2016-10-06 05:35:24', 'MGP ADMIN'),
(227, 'Added new talent   ', '2016-10-06 05:37:28', 'MGP ADMIN'),
(228, 'Added new talent #85', '2016-10-06 05:37:28', 'MGP ADMIN'),
(229, 'MGP ADMIN logged in.', '2016-10-06 08:21:12', 'MGP ADMIN'),
(230, 'MGP ADMIN logged in.', '2016-10-10 03:11:16', 'MGP ADMIN'),
(231, 'Added new talent   ', '2016-10-10 03:13:14', 'MGP ADMIN'),
(232, 'Added new talent #86', '2016-10-10 03:13:14', 'MGP ADMIN'),
(233, 'Added new talent   ', '2016-10-10 03:16:22', 'MGP ADMIN'),
(234, 'MGP ADMIN logged in.', '2016-10-10 03:16:37', 'MGP ADMIN'),
(235, 'Added new talent   ', '2016-10-10 03:17:12', 'MGP ADMIN'),
(236, 'Added new talent #87', '2016-10-10 03:17:12', 'MGP ADMIN'),
(237, 'Added new talent   ', '2016-10-10 03:17:34', 'MGP ADMIN'),
(238, 'Added new talent #88', '2016-10-10 03:17:34', 'MGP ADMIN'),
(239, 'Added new talent   ', '2016-10-10 03:17:57', 'MGP ADMIN'),
(240, 'Added new talent #89', '2016-10-10 03:17:57', 'MGP ADMIN'),
(241, 'Added new talent   ', '2016-10-10 03:18:26', 'MGP ADMIN'),
(242, 'Added new talent #90', '2016-10-10 03:18:26', 'MGP ADMIN'),
(243, 'MGP ADMIN logged in.', '2016-10-10 03:48:32', 'MGP ADMIN'),
(244, 'MGP ADMIN logged in.', '2016-10-10 08:11:57', 'MGP ADMIN'),
(245, 'MGP ADMIN logged out.', '2016-10-10 08:14:20', 'MGP ADMIN'),
(246, 'MGP ADMIN logged in.', '2016-10-12 03:09:28', 'MGP ADMIN'),
(247, 'Added new talent   ', '2016-10-12 03:14:19', 'MGP ADMIN'),
(248, 'MGP ADMIN logged in.', '2016-10-12 03:19:47', 'MGP ADMIN'),
(249, 'MGP ADMIN logged in.', '2016-10-12 03:30:14', 'MGP ADMIN'),
(250, 'Added new talent   ', '2016-10-12 03:34:23', 'MGP ADMIN'),
(251, 'Added new talent   ', '2016-10-12 03:35:11', 'MGP ADMIN'),
(252, 'Added new talent   ', '2016-10-12 03:36:03', 'MGP ADMIN'),
(253, 'Added new talent   ', '2016-10-12 03:37:17', 'MGP ADMIN'),
(254, 'Nami biodata updated.', '2016-10-12 03:38:18', 'MGP ADMIN'),
(255, 'Heart biodata updated.', '2016-10-12 03:38:43', 'MGP ADMIN'),
(256, 'Yuna biodata updated.', '2016-10-12 03:38:57', 'MGP ADMIN'),
(257, 'Aki biodata updated.', '2016-10-12 03:39:14', 'MGP ADMIN'),
(258, 'Added new talent   ', '2016-10-12 03:40:15', 'MGP ADMIN'),
(259, 'Added new talent   ', '2016-10-12 03:41:10', 'MGP ADMIN'),
(260, 'Added new talent   ', '2016-10-12 03:42:00', 'MGP ADMIN'),
(261, 'Added new talent   ', '2016-10-12 03:44:55', 'MGP ADMIN'),
(262, 'Added new talent   ', '2016-10-12 03:45:52', 'MGP ADMIN'),
(263, 'Added new talent   ', '2016-10-12 03:46:36', 'MGP ADMIN'),
(264, 'Added new talent   ', '2016-10-12 03:47:26', 'MGP ADMIN'),
(265, 'Added new talent   ', '2016-10-12 03:48:00', 'MGP ADMIN'),
(266, 'Added new talent   ', '2016-10-12 03:48:42', 'MGP ADMIN'),
(267, 'Added new talent   ', '2016-10-12 03:49:25', 'MGP ADMIN'),
(268, 'Added new talent   ', '2016-10-12 03:50:22', 'MGP ADMIN'),
(269, 'Added new talent   ', '2016-10-12 03:51:03', 'MGP ADMIN'),
(270, 'Added new talent   ', '2016-10-12 03:51:40', 'MGP ADMIN'),
(271, 'Added new talent   ', '2016-10-12 03:52:16', 'MGP ADMIN'),
(272, 'Added new talent   ', '2016-10-12 03:52:48', 'MGP ADMIN'),
(273, 'Added new talent   ', '2016-10-12 03:53:59', 'MGP ADMIN'),
(274, 'Ara biodata updated.', '2016-10-12 03:56:09', 'MGP ADMIN'),
(275, 'MGP ADMIN logged in.', '2016-10-12 03:56:37', 'MGP ADMIN'),
(276, 'MGP ADMIN logged in.', '2016-10-12 04:09:19', 'MGP ADMIN'),
(277, 'MGP ADMIN logged in.', '2016-10-12 08:24:48', 'MGP ADMIN'),
(278, 'MGP ADMIN logged in.', '2016-10-13 03:36:18', 'MGP ADMIN'),
(279, 'Added new talent   ', '2016-10-13 03:38:11', 'MGP ADMIN'),
(280, 'Marie Joyze Radam Amanca biodata updated.', '2016-10-13 03:39:53', 'MGP ADMIN'),
(281, 'Maria Joanna Chiyuto Merez biodata updated.', '2016-10-13 03:40:42', 'MGP ADMIN'),
(282, 'Sheila Manzano Elaurza biodata updated.', '2016-10-13 03:42:50', 'MGP ADMIN'),
(283, 'Sophia Joahnna Lacuesta Abad biodata updated.', '2016-10-13 03:43:50', 'MGP ADMIN'),
(284, 'Maria Lilibeth Lina Policarpio biodata updated.', '2016-10-13 03:45:03', 'MGP ADMIN'),
(285, 'Christine Marry Jugarap Pangahin biodata updated.', '2016-10-13 03:45:54', 'MGP ADMIN'),
(286, 'Jennifer Modesto Prado biodata updated.', '2016-10-13 03:46:51', 'MGP ADMIN'),
(287, 'MGP ADMIN logged in.', '2016-10-13 05:19:00', 'MGP ADMIN'),
(288, 'Nikkie Rose Miguel Montes biodata updated.', '2016-10-13 05:21:45', 'MGP ADMIN'),
(289, 'Rose Diane Lamboso Calungsod biodata updated.', '2016-10-13 05:23:51', 'MGP ADMIN'),
(290, 'Jocelyn Taguinod Cabornay biodata updated.', '2016-10-13 05:24:49', 'MGP ADMIN'),
(291, 'Jierminresi Jourwel Galiga Bautista biodata update', '2016-10-13 05:25:24', 'MGP ADMIN'),
(292, 'Jedda Rubi Apolinario biodata updated.', '2016-10-13 05:25:54', 'MGP ADMIN'),
(293, 'Maricar Pingad Vergara biodata updated.', '2016-10-13 05:26:34', 'MGP ADMIN'),
(294, 'Sanrio Garcia Cundangan biodata updated.', '2016-10-13 05:27:08', 'MGP ADMIN'),
(295, 'Abigael Anselmo Agbon biodata updated.', '2016-10-13 05:27:45', 'MGP ADMIN'),
(296, 'MGP ADMIN logged in.', '2016-10-13 08:37:50', 'MGP ADMIN'),
(297, 'MGP ADMIN logged in.', '2016-10-14 03:57:59', 'MGP ADMIN'),
(298, 'Added new talent   ', '2016-10-14 03:59:51', 'MGP ADMIN'),
(299, 'Added new talent   ', '2016-10-14 04:00:30', 'MGP ADMIN'),
(300, 'Added new talent   ', '2016-10-14 04:01:08', 'MGP ADMIN'),
(301, 'MGP ADMIN logged in.', '2016-10-19 05:39:54', 'MGP ADMIN'),
(302, 'MGP ADMIN logged in.', '2016-10-21 06:54:19', 'MGP ADMIN'),
(303, 'MGP ADMIN logged in.', '2016-10-26 03:27:17', 'MGP ADMIN'),
(304, 'MGP ADMIN logged in.', '2016-10-26 05:13:41', 'MGP ADMIN'),
(305, 'Mary Ann Marzan Montaño biodata updated.', '2016-10-26 05:14:29', 'MGP ADMIN'),
(306, 'Jemelline Juico Carpio biodata updated.', '2016-10-26 05:15:48', 'MGP ADMIN'),
(307, 'Lovely Charmaine Fernandez Bonifacio biodata updat', '2016-10-26 05:16:14', 'MGP ADMIN'),
(308, 'Maricar Pingad Vergara biodata updated.', '2016-10-26 05:16:49', 'MGP ADMIN'),
(309, 'Queenie Averilla Espiritu biodata updated.', '2016-10-26 05:17:31', 'MGP ADMIN'),
(310, 'Anna Rose Marchan Salavante biodata updated.', '2016-10-26 05:17:55', 'MGP ADMIN'),
(311, 'Ma. Gellin Santos Razo biodata updated.', '2016-10-26 05:18:22', 'MGP ADMIN'),
(312, 'Abigael Anselmo Agbon biodata updated.', '2016-10-26 05:18:54', 'MGP ADMIN'),
(313, 'Paulene Alcazar Sarmiento biodata updated.', '2016-10-26 05:20:33', 'MGP ADMIN'),
(314, 'Allaine Marie Marqueses Rico biodata updated.', '2016-10-26 05:21:01', 'MGP ADMIN'),
(315, 'Soleta Lazaga Sarona biodata updated.', '2016-10-26 05:21:23', 'MGP ADMIN'),
(316, 'Jennifer De Guzman Urquico biodata updated.', '2016-10-26 05:22:47', 'MGP ADMIN'),
(317, 'Sheila Manzano Elaurza biodata updated.', '2016-10-26 05:23:14', 'MGP ADMIN'),
(318, 'Sophia Joahnna Lacuesta Abad biodata updated.', '2016-10-26 05:23:35', 'MGP ADMIN'),
(319, 'MGP ADMIN logged in.', '2016-10-26 07:25:37', 'MGP ADMIN'),
(320, 'MGP ADMIN logged in.', '2016-10-26 07:27:00', 'MGP ADMIN'),
(321, 'MGP ADMIN logged in.', '2016-10-26 08:27:19', 'MGP ADMIN'),
(322, 'Ara biodata updated.', '2016-10-26 08:39:44', 'MGP ADMIN'),
(323, 'Heart biodata updated.', '2016-10-26 08:41:10', 'MGP ADMIN'),
(324, 'Nami biodata updated.', '2016-10-26 08:41:44', 'MGP ADMIN'),
(325, 'Yuna biodata updated.', '2016-10-26 08:42:48', 'MGP ADMIN'),
(326, 'Aki biodata updated.', '2016-10-26 08:43:19', 'MGP ADMIN'),
(327, 'Lin biodata updated.', '2016-10-26 08:44:26', 'MGP ADMIN'),
(328, 'Nicole biodata updated.', '2016-10-26 08:44:44', 'MGP ADMIN'),
(329, 'Sofia biodata updated.', '2016-10-26 08:45:24', 'MGP ADMIN'),
(330, 'MGP ADMIN logged in.', '2016-10-26 08:46:29', 'MGP ADMIN'),
(331, 'MGP ADMIN logged in.', '2016-10-26 08:52:24', 'MGP ADMIN'),
(332, 'MGP ADMIN logged in.', '2016-10-26 08:52:48', 'MGP ADMIN'),
(333, 'MGP ADMIN logged in.', '2016-10-26 08:53:04', 'MGP ADMIN'),
(334, 'Jeane biodata updated.', '2016-10-26 08:58:19', 'MGP ADMIN'),
(335, 'Melody biodata updated.', '2016-10-26 08:59:57', 'MGP ADMIN'),
(336, 'Nana biodata updated.', '2016-10-26 09:01:45', 'MGP ADMIN'),
(337, 'Karen biodata updated.', '2016-10-26 09:02:07', 'MGP ADMIN'),
(338, 'Angel biodata updated.', '2016-10-26 09:02:49', 'MGP ADMIN'),
(339, 'Rose biodata updated.', '2016-10-26 09:03:27', 'MGP ADMIN'),
(340, 'Yuri biodata updated.', '2016-10-26 09:03:47', 'MGP ADMIN'),
(341, 'Ai biodata updated.', '2016-10-26 09:04:26', 'MGP ADMIN'),
(342, 'Anna biodata updated.', '2016-10-26 09:04:49', 'MGP ADMIN'),
(343, 'Erica biodata updated.', '2016-10-26 09:05:50', 'MGP ADMIN'),
(344, 'Chery biodata updated.', '2016-10-26 09:06:07', 'MGP ADMIN'),
(345, 'MGP ADMIN logged in.', '2016-10-26 09:06:46', 'MGP ADMIN'),
(346, 'Nicole biodata updated.', '2016-10-26 09:07:17', 'MGP ADMIN'),
(347, 'MGP ADMIN logged in.', '2016-10-26 11:00:33', 'MGP ADMIN'),
(348, 'Deleted agency id 1', '2016-10-26 11:03:33', 'MGP ADMIN'),
(349, 'Deleted agency id 2', '2016-10-26 11:03:57', 'MGP ADMIN'),
(350, 'Deleted agency id 2', '2016-10-26 11:03:57', 'MGP ADMIN'),
(351, 'MGP ADMIN logged in.', '2016-10-27 05:36:29', 'MGP ADMIN'),
(352, 'MGP ADMIN logged in.', '2016-10-28 08:58:03', 'MGP ADMIN'),
(353, 'MGP ADMIN logged in.', '2016-10-28 08:59:05', 'MGP ADMIN'),
(354, 'Added new talent   ', '2016-10-28 09:08:59', 'MGP ADMIN'),
(355, 'Ara biodata updated.', '2016-10-28 09:10:29', 'MGP ADMIN'),
(356, 'Added new talent   ', '2016-10-28 09:13:20', 'MGP ADMIN'),
(357, 'Added new talent   ', '2016-10-28 09:14:06', 'MGP ADMIN'),
(358, 'Added new talent   ', '2016-10-28 09:14:41', 'MGP ADMIN'),
(359, 'Shiena Aquino biodata updated.', '2016-10-28 09:15:06', 'MGP ADMIN'),
(360, 'Rufa Tingson biodata updated.', '2016-10-28 09:15:20', 'MGP ADMIN'),
(361, 'Tracy Anne Solon biodata updated.', '2016-10-28 09:15:48', 'MGP ADMIN'),
(362, 'Mary Rose Esguerra biodata updated.', '2016-10-28 09:16:00', 'MGP ADMIN'),
(363, 'Added new talent   ', '2016-10-28 09:16:46', 'MGP ADMIN'),
(364, 'Ailyn Nazarene Taway biodata updated.', '2016-10-28 09:16:57', 'MGP ADMIN'),
(365, 'Added new talent   ', '2016-10-28 09:17:33', 'MGP ADMIN'),
(366, 'Added new talent   ', '2016-10-28 09:18:22', 'MGP ADMIN'),
(367, 'Added new talent   ', '2016-10-28 09:18:57', 'MGP ADMIN'),
(368, 'MGP ADMIN logged in.', '2016-10-28 10:14:00', 'MGP ADMIN'),
(369, 'Joan Marie Rapanan Mangonlay biodata updated.', '2016-10-28 10:14:24', 'MGP ADMIN'),
(370, 'Maybelle L. De Guia biodata updated.', '2016-10-28 10:23:04', 'MGP ADMIN'),
(371, 'Added new talent   ', '2016-10-28 10:30:16', 'MGP ADMIN'),
(372, 'Added new talent   ', '2016-10-28 10:30:57', 'MGP ADMIN'),
(373, 'Added new talent   ', '2016-10-28 10:31:52', 'MGP ADMIN'),
(374, 'Added new talent   ', '2016-10-28 10:32:28', 'MGP ADMIN'),
(375, 'Added new talent   ', '2016-10-28 10:33:00', 'MGP ADMIN'),
(376, 'Added new talent   ', '2016-10-28 10:33:34', 'MGP ADMIN'),
(377, 'Added new talent   ', '2016-10-28 10:34:06', 'MGP ADMIN'),
(378, 'Added new talent   ', '2016-10-28 10:34:48', 'MGP ADMIN'),
(379, 'Added new talent   ', '2016-10-28 10:35:22', 'MGP ADMIN'),
(380, 'Added new talent   ', '2016-10-28 10:35:55', 'MGP ADMIN'),
(381, 'Mary Rose Esguerra biodata updated.', '2016-10-28 10:38:46', 'MGP ADMIN'),
(382, 'Tracy Anne Solon biodata updated.', '2016-10-28 10:39:18', 'MGP ADMIN'),
(383, 'Tracy Anne Solon biodata updated.', '2016-10-28 10:39:18', 'MGP ADMIN'),
(384, 'Rufa Tingson biodata updated.', '2016-10-28 10:39:47', 'MGP ADMIN'),
(385, 'Shiena Aquino biodata updated.', '2016-10-28 10:40:20', 'MGP ADMIN'),
(386, 'Ailyn Nazarene Taway biodata updated.', '2016-10-28 10:40:46', 'MGP ADMIN'),
(387, 'Ailyn Nazarene Taway biodata updated.', '2016-10-28 10:40:47', 'MGP ADMIN'),
(388, 'Rhea Realino biodata updated.', '2016-10-28 10:41:14', 'MGP ADMIN'),
(389, 'Joan Delos Santos biodata updated.', '2016-10-28 10:41:43', 'MGP ADMIN'),
(390, 'Ruzzel Realino biodata updated.', '2016-10-28 10:42:07', 'MGP ADMIN'),
(391, 'Irah Mae Panoncillon biodata updated.', '2016-10-28 10:43:06', 'MGP ADMIN'),
(392, 'Quennie Rose Buco biodata updated.', '2016-10-28 10:43:35', 'MGP ADMIN'),
(393, 'Ma. Arian Cruz biodata updated.', '2016-10-28 10:44:15', 'MGP ADMIN'),
(394, 'Alamari Labsan biodata updated.', '2016-10-28 10:44:40', 'MGP ADMIN'),
(395, 'Janica Serrano Mariano biodata updated.', '2016-10-28 10:45:16', 'MGP ADMIN'),
(396, 'Yandra Lowella Cabillan biodata updated.', '2016-10-28 10:46:44', 'MGP ADMIN'),
(397, 'Jonna Gersalia Gelilio biodata updated.', '2016-10-28 10:46:57', 'MGP ADMIN'),
(398, 'Jenyviv Gagarin Riño biodata updated.', '2016-10-28 10:47:08', 'MGP ADMIN'),
(399, 'Roxanne Javier Tolentino biodata updated.', '2016-10-28 10:47:20', 'MGP ADMIN'),
(400, 'Cyrille Herrera De leon biodata updated.', '2016-10-28 10:47:32', 'MGP ADMIN'),
(401, 'Jennevy P. Del Rosario biodata updated.', '2016-10-28 10:48:47', 'MGP ADMIN'),
(402, 'Maybelle L. De Guia biodata updated.', '2016-10-28 10:49:02', 'MGP ADMIN'),
(403, 'Maybelle L. De Guia biodata updated.', '2016-10-28 10:49:21', 'MGP ADMIN'),
(404, 'Richelle Joy Armogenia Baltazar biodata updated.', '2016-10-28 10:54:27', 'MGP ADMIN'),
(405, 'Sheilla Marie Piad biodata updated.', '2016-10-28 10:54:37', 'MGP ADMIN'),
(406, 'Jessel Dela Cruz Francisco biodata updated.', '2016-10-28 10:54:46', 'MGP ADMIN'),
(407, 'Joyce Ann Garcia Songcuan biodata updated.', '2016-10-28 10:54:55', 'MGP ADMIN'),
(408, 'Maricar San Antonio Dellomas biodata updated.', '2016-10-28 10:55:05', 'MGP ADMIN'),
(409, 'Carlito Balagtas Mata biodata updated.', '2016-10-28 10:55:17', 'MGP ADMIN'),
(410, 'Giovanni Realino Sauro biodata updated.', '2016-10-28 10:55:27', 'MGP ADMIN'),
(411, 'MGP ADMIN logged in.', '2016-10-31 04:42:07', 'MGP ADMIN'),
(412, 'Added new talent   ', '2016-10-31 04:42:44', 'MGP ADMIN'),
(413, 'MGP ADMIN logged in.', '2016-10-31 04:45:21', 'MGP ADMIN'),
(414, 'MGP ADMIN logged out.', '2016-10-31 04:45:41', 'MGP ADMIN'),
(415, 'MGP ADMIN logged in.', '2016-10-31 07:44:26', 'MGP ADMIN'),
(416, 'MGP ADMIN logged in.', '2016-10-31 07:58:22', 'MGP ADMIN'),
(417, 'MGP ADMIN logged out.', '2016-10-31 08:06:41', 'MGP ADMIN'),
(418, 'MGP ADMIN logged in.', '2016-10-31 12:43:09', 'MGP ADMIN'),
(419, 'MGP ADMIN logged in.', '2016-11-01 08:11:14', 'MGP ADMIN'),
(420, 'MGP ADMIN logged out.', '2016-11-01 08:14:33', 'MGP ADMIN'),
(421, 'MGP ADMIN logged in.', '2016-11-01 11:13:45', 'MGP ADMIN'),
(422, 'MGP ADMIN logged in.', '2016-11-02 01:32:24', 'MGP ADMIN'),
(423, 'Jeane biodata updated.', '2016-11-02 01:34:49', 'MGP ADMIN'),
(424, 'Erica biodata updated.', '2016-11-02 01:35:20', 'MGP ADMIN'),
(425, 'Chery biodata updated.', '2016-11-02 01:35:49', 'MGP ADMIN'),
(426, 'Hazzel Talimpusan biodata updated.', '2016-11-02 01:36:31', 'MGP ADMIN'),
(427, 'Cristine Santos biodata updated.', '2016-11-02 01:36:51', 'MGP ADMIN'),
(428, 'Rheoda Ipil biodata updated.', '2016-11-02 01:37:00', 'MGP ADMIN'),
(429, 'Jean Tagalog biodata updated.', '2016-11-02 01:37:09', 'MGP ADMIN'),
(430, 'Irah Mae Panoncillon biodata updated.', '2016-11-02 01:39:42', 'MGP ADMIN'),
(431, 'Irah Mae Panoncillon biodata updated.', '2016-11-02 01:40:40', 'MGP ADMIN'),
(432, 'Irah Mae Senarosa Panoncillon biodata updated.', '2016-11-02 01:49:56', 'MGP ADMIN'),
(433, 'Queennie Rose Reyes Buco biodata updated.', '2016-11-02 01:51:10', 'MGP ADMIN'),
(434, 'Irah Mae Senarosa Panoncillon biodata updated.', '2016-11-02 01:51:34', 'MGP ADMIN'),
(435, 'Ma. Arian Cruz biodata updated.', '2016-11-02 01:53:52', 'MGP ADMIN'),
(436, 'Alamari Labsan biodata updated.', '2016-11-02 01:54:35', 'MGP ADMIN'),
(437, 'Ma. Arian Franco Cruz biodata updated.', '2016-11-02 01:55:42', 'MGP ADMIN'),
(438, 'Almarie Bonifacio Labsan biodata updated.', '2016-11-02 01:56:06', 'MGP ADMIN'),
(439, 'Janica Serrano Mariano biodata updated.', '2016-11-02 01:56:48', 'MGP ADMIN'),
(440, 'Jonna Gersalia Gelilio biodata updated.', '2016-11-02 01:57:56', 'MGP ADMIN'),
(441, 'Janica Serrano Mariano biodata updated.', '2016-11-02 01:58:10', 'MGP ADMIN'),
(442, 'Jonna Gersalia Gelilio biodata updated.', '2016-11-02 01:59:11', 'MGP ADMIN'),
(443, 'Yandra Lowella Cabillan biodata updated.', '2016-11-02 01:59:37', 'MGP ADMIN'),
(444, 'Jenyviv Gagarin Riño biodata updated.', '2016-11-02 02:01:22', 'MGP ADMIN'),
(445, 'Roxanne Javier Tolentino biodata updated.', '2016-11-02 02:03:00', 'MGP ADMIN'),
(446, 'Cyrille Herrera De leon biodata updated.', '2016-11-02 02:03:53', 'MGP ADMIN'),
(447, 'Maybelle Lipata De Guia biodata updated.', '2016-11-02 02:06:20', 'MGP ADMIN'),
(448, 'Jennevy P. Del Rosario biodata updated.', '2016-11-02 02:08:54', 'MGP ADMIN'),
(449, 'Jennevy Pingol Del Rosario biodata updated.', '2016-11-02 02:10:00', 'MGP ADMIN'),
(450, 'Rozel Ann Magpayo Kasilag biodata updated.', '2016-11-02 02:10:58', 'MGP ADMIN'),
(451, 'Almarie Bonifacio Labsan biodata updated.', '2016-11-02 02:12:04', 'MGP ADMIN'),
(452, 'Rozel Ann Magpayo Kasilag biodata updated.', '2016-11-02 02:12:58', 'MGP ADMIN'),
(453, 'Rozel Ann Magpayo Kasilag biodata updated.', '2016-11-02 02:13:20', 'MGP ADMIN'),
(454, 'Rozel Ann Magpayo Kasilag biodata updated.', '2016-11-02 02:13:34', 'MGP ADMIN'),
(455, 'Rozel Ann Magpayo Kasilag biodata updated.', '2016-11-02 02:13:54', 'MGP ADMIN'),
(456, 'Rozel Ann Magpayo Kasilag biodata updated.', '2016-11-02 02:14:19', 'MGP ADMIN'),
(457, 'Rozel Ann Magpayo Kasilag biodata updated.', '2016-11-02 02:14:37', 'MGP ADMIN'),
(458, 'Joan Marie Rapanan Mangonlay biodata updated.', '2016-11-02 02:21:13', 'MGP ADMIN'),
(459, 'Joan Marie Rapanan Mangonlay biodata updated.', '2016-11-02 02:21:49', 'MGP ADMIN'),
(460, 'Nami biodata updated.', '2016-11-02 02:22:16', 'MGP ADMIN'),
(461, 'Heart biodata updated.', '2016-11-02 02:22:44', 'MGP ADMIN'),
(462, 'Yuna biodata updated.', '2016-11-02 02:23:10', 'MGP ADMIN'),
(463, 'Aki biodata updated.', '2016-11-02 02:23:27', 'MGP ADMIN'),
(464, 'Anna biodata updated.', '2016-11-02 02:23:44', 'MGP ADMIN'),
(465, 'Ai biodata updated.', '2016-11-02 02:24:02', 'MGP ADMIN'),
(466, 'Yuri biodata updated.', '2016-11-02 02:24:19', 'MGP ADMIN'),
(467, 'Rose biodata updated.', '2016-11-02 02:24:37', 'MGP ADMIN'),
(468, 'Angel biodata updated.', '2016-11-02 02:24:54', 'MGP ADMIN'),
(469, 'Melody biodata updated.', '2016-11-02 02:25:09', 'MGP ADMIN'),
(470, 'Nana biodata updated.', '2016-11-02 02:25:28', 'MGP ADMIN'),
(471, 'Lin biodata updated.', '2016-11-02 02:25:54', 'MGP ADMIN'),
(472, 'Salve Miaros biodata updated.', '2016-11-02 02:27:02', 'MGP ADMIN'),
(473, 'Charlotte Alano biodata updated.', '2016-11-02 02:27:16', 'MGP ADMIN'),
(474, 'Marra Tibayan biodata updated.', '2016-11-02 02:27:36', 'MGP ADMIN'),
(475, 'Nicole biodata updated.', '2016-11-02 02:28:00', 'MGP ADMIN'),
(476, 'Karen biodata updated.', '2016-11-02 02:28:15', 'MGP ADMIN'),
(477, 'Ara biodata updated.', '2016-11-02 02:28:32', 'MGP ADMIN'),
(478, 'Brigette Bolagdan biodata updated.', '2016-11-02 02:29:50', 'MGP ADMIN'),
(479, 'Lovely Jane Gonzales biodata updated.', '2016-11-02 02:30:11', 'MGP ADMIN'),
(480, 'Lady Dianne Laderas biodata updated.', '2016-11-02 02:30:29', 'MGP ADMIN'),
(481, 'Sofia biodata updated.', '2016-11-02 02:30:51', 'MGP ADMIN'),
(482, 'Mary Rose Esguerra biodata updated.', '2016-11-02 02:31:16', 'MGP ADMIN'),
(483, 'Tracy Anne Solon biodata updated.', '2016-11-02 02:31:32', 'MGP ADMIN'),
(484, 'Rufa Tingson biodata updated.', '2016-11-02 02:31:45', 'MGP ADMIN'),
(485, 'Shiena Aquino biodata updated.', '2016-11-02 02:32:00', 'MGP ADMIN'),
(486, 'Ailyn Nazarene Taway biodata updated.', '2016-11-02 02:32:16', 'MGP ADMIN'),
(487, 'Rhea Realino biodata updated.', '2016-11-02 02:32:54', 'MGP ADMIN'),
(488, 'Ruzzel Realino biodata updated.', '2016-11-02 02:33:03', 'MGP ADMIN'),
(489, 'Joan Delos Santos biodata updated.', '2016-11-02 02:33:17', 'MGP ADMIN'),
(490, 'Updated club None', '2016-11-02 02:35:21', 'MGP ADMIN'),
(491, 'Deleted account id 6', '2016-11-02 02:35:41', 'MGP ADMIN'),
(492, 'Deleted account id 7', '2016-11-02 02:35:52', 'MGP ADMIN'),
(493, 'Mary Rose Esguerra biodata updated.', '2016-11-02 02:41:11', 'MGP ADMIN'),
(494, 'Mary Rose Esguerra biodata updated.', '2016-11-02 02:42:05', 'MGP ADMIN'),
(495, 'Mary Rose Esguerra biodata updated.', '2016-11-02 02:42:55', 'MGP ADMIN'),
(496, 'MGP ADMIN logged in.', '2016-11-02 02:52:13', 'MGP ADMIN'),
(497, 'Joan Marie Rapanan Mangonlay biodata updated.', '2016-11-02 02:54:18', 'MGP ADMIN'),
(498, 'MGP ADMIN logged in.', '2016-11-02 04:37:19', 'MGP ADMIN'),
(499, 'MGP ADMIN logged in.', '2016-11-02 05:06:58', 'MGP ADMIN'),
(500, 'MGP ADMIN logged in.', '2016-11-02 05:37:56', 'MGP ADMIN'),
(501, 'Irah Mae Senarosa Panoncillon biodata updated.', '2016-11-02 06:12:19', 'MGP ADMIN'),
(502, 'Queennie Rose Reyes Buco biodata updated.', '2016-11-02 06:12:39', 'MGP ADMIN'),
(503, 'Ma. Arian Franco Cruz biodata updated.', '2016-11-02 06:13:00', 'MGP ADMIN'),
(504, 'Almarie Bonifacio Labsan biodata updated.', '2016-11-02 06:13:19', 'MGP ADMIN'),
(505, 'Janica Serrano Mariano biodata updated.', '2016-11-02 06:13:47', 'MGP ADMIN'),
(506, 'Jonna Gersalia Gelilio biodata updated.', '2016-11-02 06:14:13', 'MGP ADMIN'),
(507, 'Yandra Lowella Cabillan biodata updated.', '2016-11-02 06:14:45', 'MGP ADMIN'),
(508, 'Jenyviv Gagarin Riño biodata updated.', '2016-11-02 06:15:03', 'MGP ADMIN'),
(509, 'Roxanne Javier Tolentino biodata updated.', '2016-11-02 06:15:24', 'MGP ADMIN'),
(510, 'Maybelle Lipata De Guia biodata updated.', '2016-11-02 06:15:44', 'MGP ADMIN'),
(511, 'Jennevy Pingol Del Rosario biodata updated.', '2016-11-02 06:16:09', 'MGP ADMIN'),
(512, 'Rozel Ann Magpayo Kasilag biodata updated.', '2016-11-02 06:16:30', 'MGP ADMIN'),
(513, 'Cyrille Herrera De leon biodata updated.', '2016-11-02 06:17:27', 'MGP ADMIN'),
(514, 'Updated club Not Yet Assigned - JP', '2016-11-02 06:33:20', 'MGP ADMIN'),
(515, 'Added club Not Yet Assigned - SF', '2016-11-02 06:33:45', 'MGP ADMIN'),
(516, 'Added club Not Yet Assigned - ET', '2016-11-02 06:34:00', 'MGP ADMIN'),
(517, 'Deleted production id 6', '2016-11-02 06:34:10', 'MGP ADMIN'),
(518, 'MGP ADMIN logged out.', '2016-11-02 06:35:58', 'MGP ADMIN'),
(519, 'MGP ADMIN logged in.', '2016-11-02 06:36:03', 'MGP ADMIN'),
(520, 'Added production and account for None', '2016-11-02 06:36:18', 'MGP ADMIN'),
(521, 'Updated club None', '2016-11-02 06:37:05', 'MGP ADMIN'),
(522, 'Added club Not Yet Assigned - JP', '2016-11-02 06:37:49', 'MGP ADMIN'),
(523, 'Deleted club id 7', '2016-11-02 06:40:44', 'MGP ADMIN'),
(524, 'MGP ADMIN logged in.', '2016-11-02 06:40:46', 'MGP ADMIN'),
(525, 'Deleted production id 7', '2016-11-02 06:40:51', 'MGP ADMIN'),
(526, 'Updated production None', '2016-11-02 06:47:10', 'MGP ADMIN'),
(527, 'MGP ADMIN logged in.', '2016-11-02 06:48:06', 'MGP ADMIN'),
(528, 'MGP ADMIN logged in.', '2016-11-02 06:48:11', 'MGP ADMIN'),
(529, 'Deleted club id 4', '2016-11-02 06:52:43', 'MGP ADMIN'),
(530, 'Deleted production id 6', '2016-11-02 06:52:57', 'MGP ADMIN'),
(531, 'Deleted account id 8', '2016-11-02 06:56:30', 'MGP ADMIN'),
(532, 'Added production and account for None', '2016-11-02 06:56:43', 'MGP ADMIN'),
(533, 'Added club None', '2016-11-02 06:56:54', 'MGP ADMIN'),
(534, 'MGP ADMIN logged in.', '2016-11-02 06:59:37', 'MGP ADMIN'),
(535, 'Catherine Comeros Lehao biodata updated.', '2016-11-02 07:07:26', 'MGP ADMIN'),
(536, 'Danna Daien Alimento Lopez biodata updated.', '2016-11-02 07:07:45', 'MGP ADMIN'),
(537, 'Maria Criselle Desipeda Sanglay biodata updated.', '2016-11-02 07:08:02', 'MGP ADMIN'),
(538, 'Julianne Cajara David biodata updated.', '2016-11-02 07:08:25', 'MGP ADMIN'),
(539, 'Raquel Santos Sunga biodata updated.', '2016-11-02 07:08:43', 'MGP ADMIN'),
(540, 'Marie Joyze Radam Amanca biodata updated.', '2016-11-02 07:13:22', 'MGP ADMIN'),
(541, 'Maria Joanna Chiyuto Merez biodata updated.', '2016-11-02 07:13:40', 'MGP ADMIN'),
(542, 'Maria Lilibeth Lina Policarpio biodata updated.', '2016-11-02 07:14:46', 'MGP ADMIN'),
(543, 'Sanrio Garcia Cundangan biodata updated.', '2016-11-02 07:15:06', 'MGP ADMIN'),
(544, 'Christine Marry Jugarap Pangahin biodata updated.', '2016-11-02 07:15:48', 'MGP ADMIN'),
(545, 'Jedda Rubi Apolinario biodata updated.', '2016-11-02 07:17:03', 'MGP ADMIN'),
(546, 'Jierminresi Jourwel Galiga Bautista biodata update', '2016-11-02 07:17:11', 'MGP ADMIN'),
(547, 'Jocelyn Taguinod Cabornay biodata updated.', '2016-11-02 07:17:16', 'MGP ADMIN'),
(548, 'Rose Diane Lamboso Calungsod biodata updated.', '2016-11-02 07:17:23', 'MGP ADMIN'),
(549, 'Nikkie Rose Miguel Montes biodata updated.', '2016-11-02 07:17:29', 'MGP ADMIN'),
(550, 'Jennifer Modesto Prado biodata updated.', '2016-11-02 07:17:35', 'MGP ADMIN'),
(551, 'Maryrose Bayonito biodata updated.', '2016-11-02 07:19:04', 'MGP ADMIN'),
(552, 'Robilyn Guinto biodata updated.', '2016-11-02 07:19:19', 'MGP ADMIN'),
(553, 'Mary Ann Bumanlag biodata updated.', '2016-11-02 07:47:42', 'MGP ADMIN'),
(554, 'Klarene Pauline biodata updated.', '2016-11-02 07:47:47', 'MGP ADMIN'),
(555, 'Kristel Cadag biodata updated.', '2016-11-02 07:47:55', 'MGP ADMIN'),
(556, 'Arlyn Tabanyag biodata updated.', '2016-11-02 07:48:00', 'MGP ADMIN'),
(557, 'Rubistar Florendo biodata updated.', '2016-11-02 07:48:07', 'MGP ADMIN'),
(558, 'Shela Mae Gallero biodata updated.', '2016-11-02 07:50:35', 'MGP ADMIN'),
(559, 'Victoria Daet biodata updated.', '2016-11-02 07:50:49', 'MGP ADMIN'),
(560, 'Mary Joy Malig-on biodata updated.', '2016-11-02 07:50:54', 'MGP ADMIN'),
(561, 'Joan Ozaraga biodata updated.', '2016-11-02 07:51:02', 'MGP ADMIN'),
(562, 'Kc-lyn Serran biodata updated.', '2016-11-02 07:51:08', 'MGP ADMIN'),
(563, 'Jaziel Cruz biodata updated.', '2016-11-02 07:51:31', 'MGP ADMIN'),
(564, 'Amy Tatad biodata updated.', '2016-11-02 07:51:31', 'MGP ADMIN'),
(565, 'Leslie May Acmac biodata updated.', '2016-11-02 07:52:07', 'MGP ADMIN'),
(566, 'Deleted club id 7', '2016-11-02 07:52:32', 'MGP ADMIN'),
(567, 'Deleted production id 6', '2016-11-02 07:52:39', 'MGP ADMIN'),
(568, 'Deleted account id 9', '2016-11-02 08:28:51', 'MGP ADMIN'),
(569, 'MGP ADMIN logged in.', '2016-11-02 09:09:41', 'MGP ADMIN'),
(570, 'Jean Tagalog biodata updated.', '2016-11-02 09:22:41', 'MGP ADMIN'),
(571, 'Rheoda Ipil biodata updated.', '2016-11-02 09:22:54', 'MGP ADMIN'),
(572, 'Cristine Santos biodata updated.', '2016-11-02 09:23:02', 'MGP ADMIN'),
(573, 'Hazzel Talimpusan biodata updated.', '2016-11-02 09:23:12', 'MGP ADMIN'),
(574, 'Chery biodata updated.', '2016-11-02 09:23:34', 'MGP ADMIN'),
(575, 'Erica biodata updated.', '2016-11-02 09:23:47', 'MGP ADMIN'),
(576, 'Jeane biodata updated.', '2016-11-02 09:23:57', 'MGP ADMIN'),
(577, 'MGP ADMIN logged out.', '2016-11-02 09:30:31', 'MGP ADMIN'),
(578, 'MGP ADMIN logged in.', '2016-11-02 09:34:41', 'MGP ADMIN'),
(579, 'Joan Marie Rapanan Mangonlay biodata updated.', '2016-11-02 09:35:23', 'MGP ADMIN'),
(580, 'Chery biodata updated.', '2016-11-02 09:36:07', 'MGP ADMIN'),
(581, 'Erica biodata updated.', '2016-11-02 09:36:44', 'MGP ADMIN'),
(582, 'Jeane biodata updated.', '2016-11-02 09:37:01', 'MGP ADMIN'),
(583, 'Sofia biodata updated.', '2016-11-02 09:37:35', 'MGP ADMIN'),
(584, 'Nami biodata updated.', '2016-11-02 09:38:00', 'MGP ADMIN'),
(585, 'Heart biodata updated.', '2016-11-02 09:38:20', 'MGP ADMIN'),
(586, 'Yuna biodata updated.', '2016-11-02 09:38:44', 'MGP ADMIN'),
(587, 'Aki biodata updated.', '2016-11-02 09:39:04', 'MGP ADMIN'),
(588, 'Anna biodata updated.', '2016-11-02 09:39:40', 'MGP ADMIN'),
(589, 'Ai biodata updated.', '2016-11-02 09:39:58', 'MGP ADMIN'),
(590, 'Yuri biodata updated.', '2016-11-02 09:40:20', 'MGP ADMIN'),
(591, 'Rose biodata updated.', '2016-11-02 09:40:54', 'MGP ADMIN'),
(592, 'Angel biodata updated.', '2016-11-02 09:41:15', 'MGP ADMIN'),
(593, 'Melody biodata updated.', '2016-11-02 09:41:39', 'MGP ADMIN'),
(594, 'Nana biodata updated.', '2016-11-02 10:21:03', 'MGP ADMIN'),
(595, 'Lin biodata updated.', '2016-11-02 10:22:42', 'MGP ADMIN'),
(596, 'Nicole biodata updated.', '2016-11-02 10:23:57', 'MGP ADMIN'),
(597, 'Karen biodata updated.', '2016-11-02 10:24:20', 'MGP ADMIN'),
(598, 'Ara biodata updated.', '2016-11-02 10:24:31', 'MGP ADMIN'),
(599, 'Karen biodata updated.', '2016-11-02 10:24:32', 'MGP ADMIN'),
(600, 'Ara biodata updated.', '2016-11-02 10:24:38', 'MGP ADMIN'),
(601, 'MGP ADMIN logged in.', '2016-11-03 01:13:06', 'MGP ADMIN'),
(602, 'MGP ADMIN logged in.', '2016-11-03 02:23:32', 'MGP ADMIN'),
(603, 'MGP ADMIN logged in.', '2016-11-03 02:25:08', 'MGP ADMIN'),
(604, 'Added new talent Apple N Marbida', '2016-11-03 03:02:37', 'MGP ADMIN'),
(605, 'Apple N. Marbida biodata updated.', '2016-11-03 03:03:08', 'MGP ADMIN'),
(606, 'Apple N. Marbida biodata updated.', '2016-11-03 03:03:55', 'MGP ADMIN'),
(607, 'Apple N. Marbida biodata updated.', '2016-11-03 03:07:23', 'MGP ADMIN'),
(608, 'MGP ADMIN logged in.', '2016-11-03 03:33:04', 'MGP ADMIN'),
(609, 'Apple N. Marbida biodata updated.', '2016-11-03 03:36:14', 'MGP ADMIN'),
(610, 'Apple N. Marbida biodata updated.', '2016-11-03 03:42:03', 'MGP ADMIN'),
(611, 'MGP ADMIN logged out.', '2016-11-03 03:46:24', 'MGP ADMIN'),
(612, 'MGP ADMIN logged in.', '2016-11-03 03:50:39', 'MGP ADMIN'),
(613, 'Apple N. Marbida biodata updated.', '2016-11-03 03:51:18', 'MGP ADMIN'),
(614, 'MGP ADMIN logged in.', '2016-11-03 03:52:51', 'MGP ADMIN'),
(615, 'Apple N. Marbida biodata updated.', '2016-11-03 03:53:09', 'MGP ADMIN'),
(616, 'MGP ADMIN logged in.', '2016-11-03 04:14:14', 'MGP ADMIN'),
(617, 'Irah Mae Senarosa Panoncillon biodata updated.', '2016-11-03 04:14:34', 'MGP ADMIN'),
(618, 'Queennie Rose Reyes Buco biodata updated.', '2016-11-03 04:14:57', 'MGP ADMIN'),
(619, 'Ma. Arian Franco Cruz biodata updated.', '2016-11-03 04:15:20', 'MGP ADMIN'),
(620, 'Almarie Bonifacio Labsan biodata updated.', '2016-11-03 04:15:46', 'MGP ADMIN'),
(621, 'Janica Serrano Mariano biodata updated.', '2016-11-03 04:16:11', 'MGP ADMIN'),
(622, 'Jonna Gersalia Gelilio biodata updated.', '2016-11-03 04:16:39', 'MGP ADMIN'),
(623, 'Cyrille Herrera De leon biodata updated.', '2016-11-03 04:18:04', 'MGP ADMIN'),
(624, 'Roxanne Javier Tolentino biodata updated.', '2016-11-03 04:18:15', 'MGP ADMIN'),
(625, 'Jenyviv Gagarin Riño biodata updated.', '2016-11-03 04:18:29', 'MGP ADMIN'),
(626, 'Yandra Lowella Cabillan biodata updated.', '2016-11-03 04:18:39', 'MGP ADMIN'),
(627, 'Roxanne Javier Tolentino biodata updated.', '2016-11-03 04:20:10', 'MGP ADMIN'),
(628, 'Rhoda Ypil biodata updated.', '2016-11-03 05:52:39', 'MGP ADMIN'),
(629, 'Kristine Santos biodata updated.', '2016-11-03 05:53:33', 'MGP ADMIN'),
(630, 'Hazel Gay Calipusan biodata updated.', '2016-11-03 05:54:21', 'MGP ADMIN'),
(631, 'Jean Tagalog biodata updated.', '2016-11-03 06:35:51', 'MGP ADMIN'),
(632, 'Salve Miaros biodata updated.', '2016-11-03 06:42:20', 'MGP ADMIN'),
(633, 'Charlotte  Ethel Aquino Alano biodata updated.', '2016-11-03 06:42:43', 'MGP ADMIN'),
(634, 'Marra Elano Tibayan biodata updated.', '2016-11-03 06:43:10', 'MGP ADMIN'),
(635, 'Brigette Canicia Borlagdan biodata updated.', '2016-11-03 06:43:52', 'MGP ADMIN'),
(636, 'Lovely Jane Bonita Boncales biodata updated.', '2016-11-03 06:44:23', 'MGP ADMIN'),
(637, 'Lady Dianne Magbanna Laderas biodata updated.', '2016-11-03 06:44:56', 'MGP ADMIN'),
(638, 'Salve Miaros biodata updated.', '2016-11-03 07:05:18', 'MGP ADMIN'),
(639, 'MGP ADMIN logged in.', '2016-11-03 08:50:05', 'MGP ADMIN'),
(640, 'MGP ADMIN logged out.', '2016-11-03 08:57:05', 'MGP ADMIN'),
(641, 'MGP ADMIN logged out.', '2016-11-03 08:57:18', 'MGP ADMIN'),
(642, 'mgp_production logged in.', '2016-11-03 08:57:22', 'mgp_production'),
(643, 'MGP ADMIN logged in.', '2016-11-03 08:58:50', 'MGP ADMIN'),
(644, 'MGP ADMIN logged in.', '2016-11-03 08:59:58', 'MGP ADMIN'),
(645, 'MGP ADMIN logged in.', '2016-11-03 09:43:52', 'MGP ADMIN'),
(646, 'MGP ADMIN logged in.', '2016-11-03 20:33:14', 'MGP ADMIN'),
(647, 'MGP ADMIN logged in.', '2016-11-04 01:13:29', 'MGP ADMIN'),
(648, 'Quennie Saladio Ramos biodata updated.', '2016-11-04 02:42:53', 'MGP ADMIN'),
(649, 'Charmine Aradillos Largo biodata updated.', '2016-11-04 03:17:46', 'MGP ADMIN'),
(650, 'Quennie Saladio Ramos biodata updated.', '2016-11-04 03:18:17', 'MGP ADMIN'),
(651, 'Amberly Balansag Quitilen biodata updated.', '2016-11-04 03:18:42', 'MGP ADMIN'),
(652, 'Iriskasy Lago Bulan biodata updated.', '2016-11-04 03:19:00', 'MGP ADMIN'),
(653, 'Mary Ann Marzan Montaño biodata updated.', '2016-11-04 03:19:46', 'MGP ADMIN'),
(654, 'Jemelline Juico Carpio biodata updated.', '2016-11-04 03:20:17', 'MGP ADMIN'),
(655, 'Lovely Charmaine Fernandez Bonifacio biodata updat', '2016-11-04 03:20:48', 'MGP ADMIN'),
(656, 'Maricar Pingad Vergara biodata updated.', '2016-11-04 03:21:09', 'MGP ADMIN'),
(657, 'Catherine Comeros Lehao biodata updated.', '2016-11-04 03:22:26', 'MGP ADMIN'),
(658, 'Leslie May Acmac biodata updated.', '2016-11-04 03:24:50', 'MGP ADMIN'),
(659, 'Danna Daien Alimento Lopez biodata updated.', '2016-11-04 03:25:03', 'MGP ADMIN'),
(660, 'Maria Criselle Desipeda Sanglay biodata updated.', '2016-11-04 03:25:14', 'MGP ADMIN'),
(661, 'Julianne Cajara David biodata updated.', '2016-11-04 03:25:24', 'MGP ADMIN'),
(662, 'Raquel Santos Sunga biodata updated.', '2016-11-04 03:25:35', 'MGP ADMIN'),
(663, 'Marie Joyze Radam Amanca biodata updated.', '2016-11-04 03:25:45', 'MGP ADMIN'),
(664, 'Maria Lilibeth Lina Policarpio biodata updated.', '2016-11-04 03:25:55', 'MGP ADMIN'),
(665, 'Jocelyn Taguinod Cabornay biodata updated.', '2016-11-04 03:26:05', 'MGP ADMIN'),
(666, 'Robilyn Guinto biodata updated.', '2016-11-04 03:26:13', 'MGP ADMIN'),
(667, 'Jaziel Cruz biodata updated.', '2016-11-04 03:26:23', 'MGP ADMIN'),
(668, 'Amy Tatad biodata updated.', '2016-11-04 03:26:34', 'MGP ADMIN'),
(669, 'Kc-lyn Serran biodata updated.', '2016-11-04 03:28:39', 'MGP ADMIN'),
(670, 'Jennifer Modesto Prado biodata updated.', '2016-11-04 03:28:49', 'MGP ADMIN');
INSERT INTO `system_logs` (`logID`, `action`, `date`, `accountName`) VALUES
(671, 'Nikkie Rose Miguel Montes biodata updated.', '2016-11-04 03:29:01', 'MGP ADMIN'),
(672, 'Rose Diane Lamboso Calungsod biodata updated.', '2016-11-04 03:29:13', 'MGP ADMIN'),
(673, 'Jierminresi Jourwel Galiga Bautista biodata update', '2016-11-04 03:29:23', 'MGP ADMIN'),
(674, 'Jedda Rubi Apolinario biodata updated.', '2016-11-04 03:29:34', 'MGP ADMIN'),
(675, 'Ma. Cristina Llaneta Gasmin biodata updated.', '2016-11-04 03:32:59', 'MGP ADMIN'),
(676, 'Robilyn Guinto biodata updated.', '2016-11-04 03:57:46', 'MGP ADMIN'),
(677, 'Ma. Cristina Llaneta Gasmin biodata updated.', '2016-11-04 06:44:18', 'MGP ADMIN'),
(678, 'MGP ADMIN logged in.', '2016-11-04 08:01:51', 'MGP ADMIN'),
(679, 'Maryrose Bayonito biodata updated.', '2016-11-04 08:36:24', 'MGP ADMIN'),
(680, 'Herb logged in.', '2016-11-04 08:38:09', 'Herb'),
(681, 'Amy Tatad biodata updated.', '2016-11-04 08:40:54', 'MGP ADMIN'),
(682, 'Jaziel Cruz biodata updated.', '2016-11-04 08:41:03', 'MGP ADMIN'),
(683, 'Kc-lyn Serran biodata updated.', '2016-11-04 08:41:10', 'MGP ADMIN'),
(684, 'Added new talent test test test', '2016-11-04 08:41:33', 'Herb'),
(685, 'Joan Ozaraga biodata updated.', '2016-11-04 08:42:05', 'MGP ADMIN'),
(686, 'Mary Joy Malig-on biodata updated.', '2016-11-04 08:42:12', 'MGP ADMIN'),
(687, 'Victoria Daet biodata updated.', '2016-11-04 08:42:19', 'MGP ADMIN'),
(688, 'Shela Mae Gallero biodata updated.', '2016-11-04 08:45:11', 'MGP ADMIN'),
(689, 'Rubistar Florendo biodata updated.', '2016-11-04 08:45:16', 'MGP ADMIN'),
(690, 'Arlyn Tabanyag biodata updated.', '2016-11-04 08:45:31', 'MGP ADMIN'),
(691, 'Kristel Cadag biodata updated.', '2016-11-04 08:45:37', 'MGP ADMIN'),
(692, 'Klarene Pauline biodata updated.', '2016-11-04 08:45:43', 'MGP ADMIN'),
(693, 'Mary Ann Bumanlag biodata updated.', '2016-11-04 08:45:50', 'MGP ADMIN'),
(694, 'Herb logged out.', '2016-11-04 08:47:11', 'Herb'),
(695, 'Leslie May Acmac biodata updated.', '2016-11-04 08:47:42', 'MGP ADMIN'),
(696, 'Cyrille Herrera De leon biodata updated.', '2016-11-04 09:06:45', 'MGP ADMIN'),
(697, 'Maria Lilibeth Lina Policarpio biodata updated.', '2016-11-04 09:12:29', 'MGP ADMIN'),
(698, 'Herb logged in.', '2016-11-04 09:12:38', 'Herb'),
(699, 'Herb logged out.', '2016-11-04 09:12:46', 'Herb'),
(700, 'Raquel Santos Sunga biodata updated.', '2016-11-04 09:12:51', 'MGP ADMIN'),
(701, 'Julianne Cajara David biodata updated.', '2016-11-04 09:12:56', 'MGP ADMIN'),
(702, 'Maria Criselle Desipeda Sanglay biodata updated.', '2016-11-04 09:12:58', 'MGP ADMIN'),
(703, 'Danna Daien Alimento Lopez biodata updated.', '2016-11-04 09:14:02', 'MGP ADMIN'),
(704, 'Catherine Comeros Lehao biodata updated.', '2016-11-04 09:14:09', 'MGP ADMIN'),
(705, 'Sanrio Garcia Cundangan biodata updated.', '2016-11-04 09:15:07', 'MGP ADMIN'),
(706, 'Christine Marry Jugarap Pangahin biodata updated.', '2016-11-04 09:15:27', 'MGP ADMIN'),
(707, 'Marie Joyze Radam Amanca biodata updated.', '2016-11-04 09:16:07', 'MGP ADMIN'),
(708, 'Maria Joanna Chiyuto Merez biodata updated.', '2016-11-04 09:16:14', 'MGP ADMIN'),
(709, 'Jennifer Modesto Prado biodata updated.', '2016-11-04 09:18:17', 'MGP ADMIN'),
(710, 'Nikkie Rose Miguel Montes biodata updated.', '2016-11-04 09:18:25', 'MGP ADMIN'),
(711, 'Rose Diane Lamboso Calungsod biodata updated.', '2016-11-04 09:18:31', 'MGP ADMIN'),
(712, 'Jierminresi Jourwel Galiga Bautista biodata update', '2016-11-04 09:18:37', 'MGP ADMIN'),
(713, 'Jedda Rubi Apolinario biodata updated.', '2016-11-04 09:18:45', 'MGP ADMIN'),
(714, 'Jocelyn Taguinod Cabornay biodata updated.', '2016-11-04 09:18:52', 'MGP ADMIN'),
(715, 'Mary Ann Marzan Montaño biodata updated.', '2016-11-04 09:21:37', 'MGP ADMIN'),
(716, 'Jemelline Juico Carpio biodata updated.', '2016-11-04 09:21:44', 'MGP ADMIN'),
(717, 'Lovely Charmaine Fernandez Bonifacio biodata updat', '2016-11-04 09:21:50', 'MGP ADMIN'),
(718, 'Maricar Pingad Vergara biodata updated.', '2016-11-04 09:21:56', 'MGP ADMIN'),
(719, 'Soleta Lazaga Sarona biodata updated.', '2016-11-04 09:24:26', 'MGP ADMIN'),
(720, 'Allaine Marie Marqueses Rico biodata updated.', '2016-11-04 09:24:32', 'MGP ADMIN'),
(721, 'Paulene Alcazar Sarmiento biodata updated.', '2016-11-04 09:24:38', 'MGP ADMIN'),
(722, 'Abigael Anselmo Agbon biodata updated.', '2016-11-04 09:24:46', 'MGP ADMIN'),
(723, 'Ma. Gellin Santos Razo biodata updated.', '2016-11-04 09:24:52', 'MGP ADMIN'),
(724, 'Anna Rose Marchan Salavante biodata updated.', '2016-11-04 09:24:58', 'MGP ADMIN'),
(725, 'Queenie Averilla Espiritu biodata updated.', '2016-11-04 09:25:32', 'MGP ADMIN'),
(726, 'Joyce Fernando Gonzales biodata updated.', '2016-11-04 09:25:38', 'MGP ADMIN'),
(727, 'Kathleen Nario Gipson biodata updated.', '2016-11-04 09:25:46', 'MGP ADMIN'),
(728, 'Rica Mercurio Villa biodata updated.', '2016-11-04 09:25:58', 'MGP ADMIN'),
(729, 'Angelica Guno Hernandez biodata updated.', '2016-11-04 09:26:05', 'MGP ADMIN'),
(730, 'Jennifer De Guzman Urquico biodata updated.', '2016-11-04 09:26:39', 'MGP ADMIN'),
(731, 'Sheila Manzano Elaurza biodata updated.', '2016-11-04 09:27:04', 'MGP ADMIN'),
(732, 'Sophia Joahnna Lacuesta Abad biodata updated.', '2016-11-04 09:27:35', 'MGP ADMIN'),
(733, 'Roxanne Javier Tolentino biodata updated.', '2016-11-04 09:34:50', 'MGP ADMIN'),
(734, 'Jenyviv Gagarin Riño biodata updated.', '2016-11-04 09:34:59', 'MGP ADMIN'),
(735, 'Yandra Lowella Cabillan biodata updated.', '2016-11-04 09:35:06', 'MGP ADMIN'),
(736, 'Jonna Gersalia Gelilio biodata updated.', '2016-11-04 09:35:20', 'MGP ADMIN'),
(737, 'Janica Serrano Mariano biodata updated.', '2016-11-04 09:35:30', 'MGP ADMIN'),
(738, 'Leslie May Acmac biodata updated.', '2016-11-04 09:39:53', 'MGP ADMIN'),
(739, 'Maryrose Bayonito biodata updated.', '2016-11-04 10:12:29', 'MGP ADMIN'),
(740, 'Sanrio Garcia Cundangan biodata updated.', '2016-11-04 10:13:20', 'MGP ADMIN'),
(741, 'Christine Marry Jugarap Pangahin biodata updated.', '2016-11-04 10:13:28', 'MGP ADMIN'),
(742, 'Queenie Averilla Espiritu biodata updated.', '2016-11-04 10:13:42', 'MGP ADMIN'),
(743, 'Anna Rose Marchan Salavante biodata updated.', '2016-11-04 10:13:49', 'MGP ADMIN'),
(744, 'Ma. Gellin Santos Razo biodata updated.', '2016-11-04 10:14:01', 'MGP ADMIN'),
(745, 'Abigael Anselmo Agbon biodata updated.', '2016-11-04 10:14:43', 'MGP ADMIN'),
(746, 'Angelica Guno Hernandez biodata updated.', '2016-11-04 10:14:56', 'MGP ADMIN'),
(747, 'Rica Mercurio Villa biodata updated.', '2016-11-04 10:15:04', 'MGP ADMIN'),
(748, 'Kathleen Nario Gipson biodata updated.', '2016-11-04 10:15:45', 'MGP ADMIN'),
(749, 'Herb logged in.', '2016-11-04 12:56:24', 'Herb'),
(750, 'MGP ADMIN logged in.', '2016-11-04 15:03:50', 'MGP ADMIN'),
(751, 'Rhea Realino biodata updated.', '2016-11-04 15:07:45', 'MGP ADMIN'),
(752, 'Rufa Tingson biodata updated.', '2016-11-04 15:10:41', 'MGP ADMIN'),
(753, 'Joan Delos Santos biodata updated.', '2016-11-04 15:11:03', 'MGP ADMIN'),
(754, 'Shiena Aquino biodata updated.', '2016-11-04 15:11:23', 'MGP ADMIN'),
(755, 'Ruzzel Realino biodata updated.', '2016-11-04 15:11:54', 'MGP ADMIN'),
(756, 'Ruzzel Realino biodata updated.', '2016-11-04 15:11:54', 'MGP ADMIN'),
(757, 'Ailyn Nazarene Taway biodata updated.', '2016-11-04 15:12:16', 'MGP ADMIN'),
(758, 'Mary Rose Esguerra biodata updated.', '2016-11-04 15:12:38', 'MGP ADMIN'),
(759, 'Tracy Anne Solon biodata updated.', '2016-11-04 15:13:13', 'MGP ADMIN'),
(760, 'MGP ADMIN logged out.', '2016-11-04 15:15:36', 'MGP ADMIN'),
(761, 'MGP ADMIN logged in.', '2016-11-06 17:52:02', 'MGP ADMIN'),
(762, 'MGP ADMIN logged in.', '2016-11-07 01:35:03', 'MGP ADMIN'),
(763, 'MGP ADMIN logged in.', '2016-11-07 02:02:47', 'MGP ADMIN'),
(764, 'Giovanni Realino Sauro biodata updated.', '2016-11-07 02:33:00', 'MGP ADMIN'),
(765, 'Carlito Balagtas Mata biodata updated.', '2016-11-07 02:33:09', 'MGP ADMIN'),
(766, 'Maricar San Antonio Dellomas biodata updated.', '2016-11-07 02:33:19', 'MGP ADMIN'),
(767, 'Joyce Ann Garcia Songcuan biodata updated.', '2016-11-07 02:33:27', 'MGP ADMIN'),
(768, 'Joyce Ann Garcia Songcuan biodata updated.', '2016-11-07 02:33:37', 'MGP ADMIN'),
(769, 'Richelle Joy Armogenia Baltazar biodata updated.', '2016-11-07 02:33:46', 'MGP ADMIN'),
(770, 'Sheilla Marie Piad biodata updated.', '2016-11-07 02:34:14', 'MGP ADMIN'),
(771, 'Jessel Dela Cruz Francisco biodata updated.', '2016-11-07 02:35:40', 'MGP ADMIN'),
(772, 'Herb logged in.', '2016-11-07 05:03:05', 'Herb'),
(773, 'MGP ADMIN logged in.', '2016-11-08 01:37:31', 'MGP ADMIN'),
(774, 'MGP ADMIN logged in.', '2016-11-08 04:27:58', 'MGP ADMIN'),
(775, 'MGP ADMIN logged in.', '2016-11-08 05:53:44', 'MGP ADMIN'),
(776, 'Herb logged in.', '2016-11-08 09:47:10', 'Herb'),
(777, 'Herb logged in.', '2016-11-15 15:42:40', 'Herb'),
(778, 'Added new talent test test tet', '2016-11-15 16:47:05', 'Herb');

-- --------------------------------------------------------

--
-- Table structure for table `talents`
--

CREATE TABLE IF NOT EXISTS `talents` (
`talentId` int(11) NOT NULL,
  `status` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `stat_remarks` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `talentName` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `firstName` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `midName` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `lastName` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `bdate` date NOT NULL,
  `pob` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `category` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `productionId` int(11) NOT NULL,
  `clubId` int(11) NOT NULL,
  `agency` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `agencyDate` date NOT NULL,
  `passportNo` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `visaNo` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `finishVisa` date NOT NULL,
  `departSched` date NOT NULL,
  `arrivalSched` date NOT NULL,
  `grossPrice` int(11) NOT NULL,
  `pdosStat` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `pdspStat` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `docSend` date NOT NULL,
  `remarks` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `coeNum` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `coeAppPh` date NOT NULL,
  `coeReleasePh` date NOT NULL,
  `coeTranslationJp` date NOT NULL,
  `coeReleaseJp` date NOT NULL,
  `coeForward` date NOT NULL,
  `coeReceive` date NOT NULL,
  `poeaApp` date NOT NULL,
  `poeaRelease` date NOT NULL,
  `pic1` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `pic2` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `pic3` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `pic4` varchar(150) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=137 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `talents`
--

INSERT INTO `talents` (`talentId`, `status`, `stat_remarks`, `talentName`, `firstName`, `midName`, `lastName`, `bdate`, `pob`, `category`, `productionId`, `clubId`, `agency`, `agencyDate`, `passportNo`, `visaNo`, `finishVisa`, `departSched`, `arrivalSched`, `grossPrice`, `pdosStat`, `pdspStat`, `docSend`, `remarks`, `coeNum`, `coeAppPh`, `coeReleasePh`, `coeTranslationJp`, `coeReleaseJp`, `coeForward`, `coeReceive`, `poeaApp`, `poeaRelease`, `pic1`, `pic2`, `pic3`, `pic4`) VALUES
(14, 'inactive', '', 'Joyce Fernando Gonzales', 'Joyce', 'Fernando', 'Gonzales', '1980-12-19', 'Plaridel Bulacan', 'Singer NON-PRO', 2, 3, '', '2016-09-06', 'No data provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '2016-09-09', 'No remarks provided.', 'No data provided.', '2016-09-13', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/uploads/talents/14/Joyce_Gonzales.jpg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(15, 'active', '', 'Kathleen Nario Gipson', 'Kathleen', 'Nario', 'Gipson', '0000-00-00', 'No data provided.', 'Singer PRO', 2, 3, '', '2016-09-06', 'No data provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '0000-00-00', 'No remarks provided.', 'No data provided.', '2016-09-13', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/uploads/talents/15/Kathleen_Gipson.jpg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(16, 'active', '', 'Angelica Guno Hernandez', 'Angelica', 'Guno', 'Hernandez', '0000-00-00', 'No data provided.', 'Dancer PRO', 2, 3, '', '2016-09-06', 'No data provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '0000-00-00', 'No remarks provided.', 'No data provided.', '2016-09-13', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/uploads/talents/16/Angelica_Hernandez.jpg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(17, 'active', '', 'Rica Mercurio Villa', 'Rica', 'Mercurlo', 'Villa', '0000-00-00', 'No data provided.', 'Dancer PRO', 2, 3, '', '2016-09-06', 'No data provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '0000-00-00', 'No remarks provided.', 'No data provided.', '2016-09-13', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/uploads/talents/17/Rica_Villa.jpg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(18, 'active', '', 'Ma. Cristina Llaneta Gasmin', 'Ma. Cristina', 'Llaneta', 'Gasmin', '0000-00-00', 'No data provided.', 'Dancer PRO', 2, 3, '', '0000-00-00', 'No data provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '0000-00-00', 'No remarks provided.', 'No data provided.', '2016-09-13', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(19, 'active', '', 'Amapula Paubsanon Rodriguez', 'Amapula', 'Paubsanon', 'Rodriguez', '0000-00-00', 'No data provided.', 'Dancer PRO', 2, 1, '', '0000-00-00', 'No data provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '0000-00-00', 'No remarks provided.', '', '2016-09-13', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(20, 'active', '', 'Charmine Aradillos Largo', 'Charmine', 'Aradillos', 'Largo', '0000-00-00', 'No data provided.', 'Dancer PRO', 2, 1, '', '0000-00-00', 'No data provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '0000-00-00', 'No remarks provided.', 'No data provided.', '2016-09-13', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(21, 'active', '', 'Quennie Saladio Ramos', 'Quennie', 'Saladio', 'Ramos', '0000-00-00', 'No data provided.', 'Dancer NON-PRO', 2, 1, '', '0000-00-00', 'No data provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '0000-00-00', 'No remarks provided.', 'No data provided.', '2016-09-13', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(22, 'active', '', 'Amberly Balansag Quitilen', 'Amberly', 'Balansag', 'Quitilen', '0000-00-00', 'No data provided.', 'Dancer PRO', 2, 1, '', '0000-00-00', 'No data provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '0000-00-00', 'No remarks provided.', 'No data provided.', '2016-09-13', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(23, 'active', '', 'Angielyn Placa Celajes', 'Angielyn', 'Placa', 'Celajes', '0000-00-00', 'No data provided.', 'Dancer PRO', 2, 2, '', '0000-00-00', 'No data provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '0000-00-00', 'No remarks provided.', '', '2016-09-13', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(24, 'active', '', 'Rose Anne Conception Cruzada', 'Rose Anne', 'Conception', 'Cruzada', '0000-00-00', 'No data provided.', 'Dancer PRO', 2, 2, '', '0000-00-00', 'No data provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '0000-00-00', 'No remarks provided.', '', '2016-09-13', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(25, 'active', '', 'Ernalyn Lompero Lantin', 'Ernalyn', 'Lompero', 'Lantin', '0000-00-00', 'No data provided.', 'Dancer PRO', 2, 2, '', '0000-00-00', 'No data provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '0000-00-00', 'No remarks provided.', '', '2016-09-13', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(26, 'active', '', 'Rhea Yumol Santiago', 'Rhea', 'Yumol', 'Santiago', '0000-00-00', 'No data provided.', 'Dancer PRO', 2, 2, '', '0000-00-00', 'No data provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '0000-00-00', 'No remarks provided.', '', '2016-09-13', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(27, 'active', '', 'Iriskasy Lago Bulan', 'Iriskasy', 'Lago', 'Bulan', '0000-00-00', 'No data provided.', 'Dancer PRO', 2, 2, '', '0000-00-00', 'No data provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '0000-00-00', 'No remarks provided.', 'No data provided.', '2016-09-13', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(28, 'active', '', 'Jenyviv Gagarin Riño', 'Jenyviv', 'Gagarin', 'Riño', '1989-01-14', 'No data provided.', 'Singer NON-PRO', 2, 1, '', '2016-05-26', 'EC2700947', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '0000-00-00', 'No remarks provided.', '16-001735', '0000-00-00', '2016-09-16', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/uploads/talents/28/Jenyviv_Riño.jpg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(29, 'active', '', 'Roxanne Javier Tolentino', 'Roxanne', 'Javier', 'Tolentino', '1985-05-02', 'No data provided.', 'Singer NON-PRO', 2, 1, '', '2016-05-26', 'EB5875453', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '0000-00-00', 'No remarks provided.', '16-001737', '0000-00-00', '2016-09-16', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/uploads/talents/29/Roxanne_Tolentino.jpg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(30, 'active', '', 'Cyrille Herrera De leon', 'Cyrille', 'Herrera', 'De leon', '1995-05-20', 'No data provided.', 'Dancer PRO', 2, 1, '', '2016-05-26', 'No data provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '0000-00-00', 'No remarks provided.', '16-001733', '0000-00-00', '2016-09-16', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/uploads/talents/30/Cyrille_De_Leon.jpg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(31, 'active', '', 'Yandra Lowella Cabillan', 'Yandra Lowella', '', 'Cabillan', '1994-11-28', 'No data provided.', 'Dancer PRO', 2, 1, '', '2016-05-26', 'No data provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '0000-00-00', 'No remarks provided.', '16-001734', '0000-00-00', '2016-09-16', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/uploads/talents/31/Yandra_Cabillan.jpg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(32, 'active', '', 'Janica Serrano Mariano', 'Janica', 'Serrano', 'Mariano', '1993-09-09', 'No data provided.', 'Dancer PRO', 2, 1, '', '2016-05-26', 'No data provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '0000-00-00', 'No remarks provided.', '16-001732', '0000-00-00', '2016-09-16', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '', 'http://mangrouppro.com/uploads/talents/32/Janica_Mariano.jpg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(33, 'active', '', 'Jonna Gersalia Gelilio', 'Jonna', 'Gersalia', 'Gelilio', '1992-05-19', 'No data provided.', 'Dancer PRO', 2, 1, '', '2016-05-26', 'No data provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '0000-00-00', 'No remarks provided.', '16-001736', '0000-00-00', '2016-09-16', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/uploads/talents/33/Jonna_Gelilio.jpg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(36, 'active', '', 'Paulene Alcazar Sarmiento', 'Paulene', 'Alcazar', 'Sarmiento', '1994-09-16', 'Manila', 'Dancer NON-PRO', 4, 6, '', '2016-09-06', 'No data provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '2016-09-09', 'No remarks provided.', 'No data provided.', '2016-10-18', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/uploads/talents/36/Paulene_Sarmiento.jpg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(37, 'active', '', 'Allaine Marie Marqueses Rico', 'Allaine Marie', 'Marqueses', 'Rico', '1994-05-15', 'Manila', 'Dancer NON-PRO', 4, 6, '', '2016-09-06', 'No data provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '2016-09-09', 'No remarks provided.', 'No data provided.', '2016-10-18', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/uploads/talents/37/Allaine_Marie_Rico.jpg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(38, 'active', '', 'Soleta Lazaga Sarona', 'Soleta', 'Lazaga', 'Sorena', '1995-03-05', 'No data provided.', 'Dancer PRO', 4, 6, '', '2016-09-06', 'No data provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '2016-09-09', 'No remarks provided.', 'No data provided.', '2016-10-18', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/uploads/talents/38/Soleta_Sarona.jpg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(40, 'active', '', 'Catherine Comeros Lehao', 'Catherine', 'Comeros', 'Lehao', '1989-03-26', 'Manila', 'Dancer NON-PRO', 2, 10, '', '2016-09-05', 'No data provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '2016-09-30', 'No remarks provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/uploads/talents/40/Catherine_Lehao.jpg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(41, 'active', '', 'Danna Daien Alimento Lopez', 'Danna Daien', 'Alimento', 'Lopez', '1995-02-13', 'Quezon City Metro Manila', 'Dancer NON-PRO', 2, 10, '', '2016-09-05', 'No data provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '2016-09-30', 'No remarks provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/uploads/talents/41/Danna_Daien_Lopez.jpg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(42, 'active', '', 'Maria Criselle Desipeda Sanglay', 'Maria Criselle', 'Desipeda', 'Sanglay', '1990-12-26', 'Manila', 'Dancer NON-PRO', 2, 10, '', '2016-09-05', 'No data provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '2016-09-30', 'No remarks provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/uploads/talents/42/Maria_Criselle_Sanglay.jpg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(43, 'active', '', 'Julianne Cajara David', 'Julianne', 'Cajara', 'David', '1990-03-08', 'Manila', 'Dancer NON-PRO', 2, 10, '', '2016-09-05', 'No data provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '2016-09-30', 'No remarks provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/uploads/talents/43/Julianne_David.jpg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(44, 'active', '', 'Mary Ann Marzan Montaño', 'Mary Ann', 'Marzan', 'Montaño', '1979-12-26', 'Caloocan City', 'Dancer NON-PRO', 4, 6, '', '2016-09-05', 'No data provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '2016-09-30', 'No remarks provided.', 'No data provided.', '2016-10-18', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/uploads/talents/44/Mary_Ann_Montano.jpg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(45, 'active', '', 'Jemelline Juico Carpio', 'Jemelline', 'Juico', 'Carpio', '1990-04-01', 'Las Piñas Metro Manila', 'Dancer NON-PRO', 4, 6, '', '2016-09-05', 'No data provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '2016-09-30', 'No remarks provided.', 'No data provided.', '2016-10-18', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/uploads/talents/45/Jemelline_Carpio.jpg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(46, 'active', '', 'Raquel Santos Sunga', 'Raquel', 'Santos', 'Sunga', '1994-02-15', 'Masantol Pampanga', 'Singer NON-PRO', 2, 10, '', '2016-09-05', 'No data provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '2016-09-30', 'No remarks provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/uploads/talents/46/Raquel_Sunga.jpg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(47, 'active', '', 'Lovely Charmaine Fernandez Bonifacio', 'Lovely Charmaine', 'Fernandez', 'Bonifacio', '1987-10-07', 'Camiling Tarlac', 'Singer NON-PRO', 4, 6, '', '2016-09-05', 'No data provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '2016-09-30', 'No remarks provided.', 'No data provided.', '2016-10-18', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/uploads/talents/47/Lovely_Bonifacio.jpg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(48, 'active', '', 'Queenie Averilla Espiritu', 'Queenie', 'Averilla', 'Espiritu', '1995-10-07', 'Pasig City Metro Manila', 'Dancer NON-PRO', 2, 3, '', '2016-09-06', 'No data provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '2016-09-30', 'No remarks provided.', 'No data provided.', '2016-10-18', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/uploads/talents/48/Queenie_Espiritu.jpg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(49, 'active', '', 'Anna Rose Marchan Salavante', 'Anna Rose', 'Marchan', 'Salavante', '1993-01-04', 'Angeles City Pampanga', 'Singer NON-PRO', 2, 3, '', '2016-09-06', 'No data provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '2016-09-30', 'No remarks provided.', 'No data provided.', '2016-10-18', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/uploads/talents/49/Anna_Rose_Salavante.jpg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(50, 'active', '', 'Ma. Gellin Santos Razo', 'Ma. Gellin', 'Sants', 'Razo', '1994-10-27', 'Pateros Metro Manila', 'Singer NON-PRO', 2, 3, '', '2016-09-06', 'No data provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '2016-09-30', 'No remarks provided.', 'No data provided.', '2016-10-18', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/uploads/talents/50/Ma__Gellin_Razo.jpg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(51, 'active', '', 'Leslie May Acmac', 'Leslie May', '', 'Acmac', '0000-00-00', 'No data provided.', 'Dancer NON-PRO', 2, 10, '', '2016-09-05', 'No data provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '0000-00-00', 'No remarks provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/uploads/talents/51/Leslie_May_Acmac.jpg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(52, 'active', '', 'Maricar Pingad Vergara', 'Maricar', 'Pingad', 'Vergara', '0000-00-00', 'No data provided.', 'Dancer NON-PRO', 4, 6, '', '2016-09-05', 'No data provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '2016-10-14', 'No remarks provided.', 'No data provided.', '2016-10-18', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/uploads/talents/52/Maricae_Verbara.jpg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(53, 'active', '', 'Maria Lilibeth Lina Policarpio', 'Maria Lilibeth', 'Lina', 'Policarpio', '0000-00-00', 'No data provided.', 'Dancer NON-PRO', 2, 10, '', '2016-09-05', 'No data provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '2016-10-14', 'No remarks provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/uploads/talents/53/Maria_Lilibeth_Policarpio.jpg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(54, 'active', '', 'Sanrio Garcia Cundangan', 'Sanrio', 'Garcia', 'Cundangan', '0000-00-00', 'No data provided.', 'Dancer NON-PRO', 2, 10, '', '2016-09-06', 'No data provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '2016-10-14', 'No remarks provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/uploads/talents/54/Santio_Cundangan.jpg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(55, 'active', '', 'Christine Marry Jugarap Pangahin', 'Christine Marry', 'Jugarap', 'Pangahin', '0000-00-00', 'No data provided.', 'Dancer NON-PRO', 2, 10, '', '2016-09-06', 'No data provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '2016-10-14', 'No remarks provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/uploads/talents/55/Christine_May_Pangahin.jpg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(56, 'active', '', 'Abigael Anselmo Agbon', 'Abigael', 'Anselmo', 'Agbon', '0000-00-00', 'No data provided.', 'Dancer NON-PRO', 2, 3, '', '2016-09-06', 'No data provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '2016-10-14', 'No remarks provided.', 'No data provided.', '2016-10-18', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/uploads/talents/56/Abigael_Agbon.jpg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(57, 'active', '', 'Maryrose Bayonito', 'Maryrose', '', 'Bayonito', '0000-00-00', 'No data provided.', 'Singer NON-PRO', 2, 10, '', '2016-09-06', 'No data provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '0000-00-00', 'No remarks provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/uploads/talents/57/Maryrose_Bayonito.jpg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(58, 'active', '', 'Jennifer De Guzman Urquico', 'Jennifer', 'De Guzman', 'Urquico', '0000-00-00', 'Manila', 'Singer NON-PRO', 4, 6, '', '2016-09-07', 'No data provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '2016-10-14', 'No remarks provided.', 'No data provided.', '2016-10-18', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/uploads/talents/58/Jennifer_Urquico.jpg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(59, 'active', '', 'Sheila Manzano Elaurza', 'Sheila', 'Manzano', 'Elaurza', '1981-08-27', 'Navotas Metro Manila', 'Dancer NON-PRO', 4, 6, '', '2016-09-22', 'No data provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '2016-10-14', 'No remarks provided.', 'No data provided.', '2016-10-18', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(60, 'active', '', 'Jennifer Modesto Prado', 'Jennifer', 'Modesto', 'Prado', '0000-00-00', 'No data provided.', 'Dancer NON-PRO', 3, 9, '', '2016-09-28', 'No data provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '2016-10-14', 'No remarks provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/uploads/talents/60/Jennifer_Prado.jpg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(61, 'active', '', 'Nikkie Rose Miguel Montes', 'Nikkie Rose', 'Miguel', 'Montes', '0000-00-00', 'No data provided.', 'Dancer NON-PRO', 3, 9, '', '2016-09-28', 'No data provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '2016-10-14', 'No remarks provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/uploads/talents/61/Nikkie_Rose_Montes.jpg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(62, 'active', '', 'Rose Diane Lamboso Calungsod', 'Rose Diane', 'Lamboso', 'Calungsod', '0000-00-00', 'No data provided.', 'Dancer NON-PRO', 3, 9, '', '2016-09-28', 'No data provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '2016-10-14', 'No remarks provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/uploads/talents/62/Rose_Diane_Calungsud.jpg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(63, 'active', '', 'Kc-lyn Serran', 'Kc-lyn', '', 'Serran', '0000-00-00', 'No data provided.', 'Dancer NON-PRO', 3, 9, '', '2016-09-28', 'No data provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '0000-00-00', 'No remarks provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/uploads/talents/63/Kc-lyn_Serran.jpg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(64, 'active', '', 'Jierminresi Jourwel Galiga Bautista', 'Jierminresi Jourwel', 'Galiga', 'Bautista', '0000-00-00', 'No data provided.', 'Dancer NON-PRO', 3, 9, '', '2016-09-28', 'No data provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '2016-10-14', 'No remarks provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/uploads/talents/64/Jourwel_Bautista.jpg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(65, 'active', '', 'Jedda Rubi Apolinario', 'Jedda', 'Rubi', 'Apolinario', '0000-00-00', 'No data provided.', 'Dancer NON-PRO', 3, 9, '', '2016-09-28', 'No data provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '2016-10-14', 'No remarks provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/uploads/talents/65/Jedda_Apolinario.jpg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(66, 'active', '', 'Jocelyn Taguinod Cabornay', 'Jocelyn', 'Taguinod', 'Cabornay', '0000-00-00', 'No data provided.', 'Dancer NON-PRO', 3, 9, '', '2016-09-28', 'No data provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '2016-10-14', 'No remarks provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/uploads/talents/66/Jocelyn_Caborloy.jpg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(67, 'active', '', 'Jaziel Cruz', 'Jaziel', '', 'Cruz', '0000-00-00', 'No data provided.', 'Dancer NON-PRO', 3, 9, '', '2016-09-28', 'No data provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '0000-00-00', 'No remarks provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/uploads/talents/67/Jaziel_Cruz.jpg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(68, 'active', '', 'Amy Tatad', 'Amy', '', 'Tatad', '0000-00-00', 'No data provided.', 'Singer NON-PRO', 3, 9, '', '2016-09-28', 'No data provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '0000-00-00', 'No remarks provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/uploads/talents/68/Amy_Tatad.jpg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(69, 'active', '', 'Giovanni Realino Sauro', 'Giovanni', 'Realino', 'Sauro', '0000-00-00', 'No data provided.', 'Dancer NON-PRO', 4, 5, '', '0000-00-00', 'No data provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '2016-10-29', 'No remarks provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(70, 'active', '', 'Carlito Balagtas Mata', 'Carlito', 'Balagtas', 'Mata', '0000-00-00', 'No data provided.', 'Dancer NON-PRO', 4, 5, '', '0000-00-00', 'No data provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '2016-10-29', 'No remarks provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(71, 'active', '', 'Maricar San Antonio Dellomas', 'Maricar', 'San Antonio', 'Dellomas', '0000-00-00', 'No data provided.', 'Dancer NON-PRO', 4, 5, '', '0000-00-00', 'No data provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '2016-10-29', 'No remarks provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(72, 'active', '', 'Joyce Ann Garcia Songcuan', 'Joyce Ann', 'Garcia', 'Songcuan', '0000-00-00', 'No data provided.', 'Dancer NON-PRO', 4, 5, '', '0000-00-00', 'No data provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '2016-10-29', 'No remarks provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(73, 'active', '', 'Jessel Dela Cruz Francisco', 'Jessel', 'Dela Cruz', 'Francisco', '0000-00-00', 'No data provided.', 'Dancer NON-PRO', 4, 5, '', '0000-00-00', 'No data provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '2016-10-29', 'No remarks provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(74, 'active', '', 'Sheilla Marie Piad', 'Sheilla Marie', '', 'Piad', '0000-00-00', 'No data provided.', 'Dancer NON-PRO', 4, 5, '', '0000-00-00', 'No data provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '2016-10-29', 'No remarks provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(75, 'active', '', 'Richelle Joy Armogenia Baltazar', 'Richelle Joy', 'Armogenia', 'Baltazar', '0000-00-00', 'No data provided.', 'Dancer PRO', 4, 5, '', '0000-00-00', 'No data provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '2016-10-29', 'No remarks provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(76, 'active', '', 'Mary Ann Bumanlag', 'Mary Ann', '', 'Bumanlag', '0000-00-00', 'No data provided.', 'Dancer NON-PRO', 3, 9, '', '2016-09-26', 'No data provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '0000-00-00', 'No remarks provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/uploads/talents/76/Mary_Ann_Bumanlag.jpg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(77, 'active', '', 'Klarene Pauline', 'Klarene Pauline', '', '', '0000-00-00', 'No data provided.', 'Dancer NON-PRO', 3, 9, '', '2016-09-26', 'No data provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '0000-00-00', 'No remarks provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/uploads/talents/77/Klarene_Pauline.jpg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(78, 'active', '', 'Kristel Cadag', 'Kristel', '', 'Cadag', '0000-00-00', 'No data provided.', 'Dancer NON-PRO', 3, 9, '', '2016-09-26', 'No data provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '0000-00-00', 'No remarks provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/uploads/talents/78/Kristel_Cadag.jpg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(79, 'active', '', 'Arlyn Tabanyag', 'Arlyn', '', 'Tabanyag', '0000-00-00', 'No data provided.', 'Dancer NON-PRO', 3, 9, '', '2016-09-26', 'No data provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '0000-00-00', 'No remarks provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/uploads/talents/79/Arlyn_Tabanyag.jpg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(80, 'active', '', 'Rubistar Florendo', 'Rubistar', '', 'Florendo', '0000-00-00', 'No data provided.', 'Dancer NON-PRO', 3, 9, '', '2016-09-26', 'No data provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '0000-00-00', 'No remarks provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/uploads/talents/80/Rubistar_Florendo.jpg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(81, 'active', '', 'Shela Mae Gallero', 'Shela Mae', '', 'Gallero', '0000-00-00', 'No data provided.', 'Dancer NON-PRO', 3, 9, '', '2016-09-26', 'No data provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '0000-00-00', 'No remarks provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/uploads/talents/81/Shela_Mae_Gallero.jpg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(82, 'active', '', 'Victoria Daet', 'Victoria', '', 'Daet', '0000-00-00', 'No data provided.', 'Dancer NON-PRO', 3, 9, '', '2016-09-26', 'No data provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '0000-00-00', 'No remarks provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/uploads/talents/82/Victoria_Daet.jpg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(83, 'active', '', 'Mary Joy Malig-on', 'Mary Joy', '', 'Malig-on', '0000-00-00', 'No data provided.', 'Dancer NON-PRO', 3, 9, '', '2016-09-26', 'No data provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '0000-00-00', 'No remarks provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/uploads/talents/83/Mary_Joy_Malig-on.jpg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(84, 'active', '', 'Joan Ozaraga', 'Joan', '', 'Ozaraga', '0000-00-00', 'No data provided.', 'Dancer NON-PRO', 3, 9, '', '2016-09-26', 'No data provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '0000-00-00', 'No remarks provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/uploads/talents/84/Joan_Ozaraga.jpg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(85, 'active', '', 'Sophia Joahnna Lacuesta Abad', 'Sophia Joahnna', 'Lacuesta', 'Abad', '1985-07-27', 'Ternate Cavite', 'Dancer NON-PRO', 4, 6, '', '2016-10-05', 'No data provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '2016-10-14', 'No remarks provided.', 'No data provided.', '2016-10-18', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/uploads/talents/85/Sophia_Joahnna_Lacuesta_Abad.jpg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(86, 'active', '', 'Maria Joanna Chiyuto Merez', 'Maria Joanna', 'Chiyuto', 'Merez', '1990-10-04', 'Pasig Metro Manila', 'Dancer NON-PRO', 2, 1, '', '2016-07-25', 'No data provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '2016-10-14', 'No remarks provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(87, 'active', '', 'Irah Mae Senarosa Panoncillon', 'Irah Mae', 'Senarosa', 'Panoncillon', '0000-00-00', 'No data provided.', 'Dancer NON-PRO', 2, 1, '', '0000-00-00', 'No data provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '0000-00-00', 'No remarks provided.', '16-001534', '0000-00-00', '2016-08-24', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/uploads/talents/87/Irah_Mae_Senarosa_Panoncillon.jpg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(88, 'active', '', 'Queennie Rose Reyes Buco', 'Queennie Rose', 'Reyes', 'Buco', '0000-00-00', 'No data provided.', 'Dancer NON-PRO', 2, 1, '', '0000-00-00', 'No data provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '0000-00-00', 'No remarks provided.', '16-001535', '0000-00-00', '2016-08-24', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/uploads/talents/88/Queennie_Rose_Reyes_Buco1.jpg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(89, 'active', '', 'Ma. Arian Franco Cruz', 'Ma. Arian', 'Franco', 'Cruz', '0000-00-00', 'No data provided.', 'Dancer NON-PRO', 2, 1, '', '0000-00-00', 'No data provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '0000-00-00', 'No remarks provided.', '16-001536', '0000-00-00', '2016-08-24', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/uploads/talents/89/Ma__Arian_Franco_Cruz.jpg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(90, 'active', '', 'Almarie Bonifacio Labsan', 'Almarie', 'Bonifacio', 'Labsan', '0000-00-00', 'No data provided.', 'Dancer NON-PRO', 2, 1, '', '0000-00-00', 'No data provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '0000-00-00', 'No remarks provided.', '16-001533', '0000-00-00', '2016-08-24', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/uploads/talents/90/Almarie_Bonifacio_Labsan.jpg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(91, 'active', '', 'Marie Joyze Radam Amanca', 'Marie Joyze', 'Radam', 'Amanca', '1990-10-24', 'Pateros Metro Manila', 'Dancer NON-PRO', 2, 1, '', '2016-07-25', 'No data provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '2016-10-14', 'No remarks provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/uploads/talents/91/1111.JPG', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg');
INSERT INTO `talents` (`talentId`, `status`, `stat_remarks`, `talentName`, `firstName`, `midName`, `lastName`, `bdate`, `pob`, `category`, `productionId`, `clubId`, `agency`, `agencyDate`, `passportNo`, `visaNo`, `finishVisa`, `departSched`, `arrivalSched`, `grossPrice`, `pdosStat`, `pdspStat`, `docSend`, `remarks`, `coeNum`, `coeAppPh`, `coeReleasePh`, `coeTranslationJp`, `coeReleaseJp`, `coeForward`, `coeReceive`, `poeaApp`, `poeaRelease`, `pic1`, `pic2`, `pic3`, `pic4`) VALUES
(92, 'active', '', 'Nami', 'Rose Ann', 'Marquez', 'Bantog', '0000-00-00', 'No data provided.', 'Dancer NON-PRO', 2, 3, '', '0000-00-00', 'No data provided.', 'No data provided.', '0000-00-00', '2016-07-06', '0000-00-00', 0, 'PENDING', 'PENDING', '0000-00-00', 'No remarks provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(93, 'active', '', 'Heart', 'Arlene', 'Solibet', 'Bautista', '0000-00-00', 'No data provided.', 'Singer PRO', 2, 3, '', '0000-00-00', 'No data provided.', 'No data provided.', '0000-00-00', '2016-07-06', '0000-00-00', 0, 'PENDING', 'PENDING', '0000-00-00', 'No remarks provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(94, 'active', '', 'Yuna', 'Joanne', 'Santos', 'Uybungco', '0000-00-00', 'No data provided.', 'Dancer NON-PRO', 2, 3, '', '0000-00-00', 'No data provided.', 'No data provided.', '0000-00-00', '2016-07-06', '0000-00-00', 0, 'PENDING', 'PENDING', '0000-00-00', 'No remarks provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(95, 'active', '', 'Aki', 'Luckies', 'Ocampo', 'Teopaco', '0000-00-00', 'No data provided.', 'Dancer NON-PRO', 2, 3, '', '0000-00-00', 'No data provided.', 'No data provided.', '0000-00-00', '2016-07-06', '0000-00-00', 0, 'PENDING', 'PENDING', '0000-00-00', 'No remarks provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(96, 'active', '', 'Lin', 'Edlenia', 'Monoy', 'Enriquez', '0000-00-00', 'No data provided.', 'Dancer NON-PRO', 2, 3, '', '0000-00-00', 'No data provided.', 'No data provided.', '0000-00-00', '2016-08-11', '0000-00-00', 0, 'PENDING', 'PENDING', '0000-00-00', 'No remarks provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(97, 'active', '', 'Nicole', 'Kimberly Ross', 'Pesebre', 'Aquino', '0000-00-00', 'No data provided.', 'Singer NON-PRO', 2, 3, '', '0000-00-00', 'No data provided.', 'No data provided.', '0000-00-00', '2016-08-11', '0000-00-00', 0, 'PENDING', 'PENDING', '0000-00-00', 'No remarks provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(98, 'active', '', 'Sofia', 'Shery Rose', 'David', 'Yamazaki', '0000-00-00', 'No data provided.', 'Singer NON-PRO', 2, 3, '', '0000-00-00', 'No data provided.', 'No data provided.', '0000-00-00', '2016-09-09', '0000-00-00', 0, 'PENDING', 'PENDING', '0000-00-00', 'No remarks provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(99, 'active', '', 'Chery', 'Bernadeth', 'Galocia', 'Cabug', '0000-00-00', 'No data provided.', 'Dancer NON-PRO', 4, 6, '', '0000-00-00', 'No data provided.', 'No data provided.', '0000-00-00', '2016-07-17', '2016-10-16', 0, 'PENDING', 'PENDING', '0000-00-00', 'No remarks provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(100, 'active', '', 'Erica', 'Gieleen Erica', 'Dela Cruz', 'Sy', '0000-00-00', 'No data provided.', 'Dancer NON-PRO', 4, 6, '', '0000-00-00', 'No data provided.', 'No data provided.', '0000-00-00', '2016-07-17', '2016-10-16', 0, 'PENDING', 'PENDING', '0000-00-00', 'No remarks provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(101, 'active', '', 'Anna', 'Ana Fatime', 'Teneza', 'Magno', '0000-00-00', 'No data provided.', 'Dancer NON-PRO', 4, 6, '', '0000-00-00', 'No data provided.', 'No data provided.', '0000-00-00', '2016-07-17', '0000-00-00', 0, 'PENDING', 'PENDING', '0000-00-00', 'No remarks provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(102, 'active', '', 'Ai', 'Remelyn', 'Caralde', 'Panopio', '0000-00-00', 'No data provided.', 'Dancer NON-PRO', 4, 6, '', '0000-00-00', 'No data provided.', 'No data provided.', '0000-00-00', '2016-07-17', '0000-00-00', 0, 'PENDING', 'PENDING', '0000-00-00', 'No remarks provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(103, 'active', '', 'Yuri', 'Camille', '', 'Yamat', '0000-00-00', 'No data provided.', 'Dancer NON-PRO', 4, 6, '', '0000-00-00', 'No data provided.', 'No data provided.', '0000-00-00', '2016-07-17', '0000-00-00', 0, 'PENDING', 'PENDING', '0000-00-00', 'No remarks provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(104, 'active', '', 'Rose', 'Rosemaries', 'Dela Rosa', 'Garcia', '0000-00-00', 'No data provided.', 'Dancer NON-PRO', 4, 6, '', '0000-00-00', 'No data provided.', 'No data provided.', '0000-00-00', '2016-07-17', '0000-00-00', 0, 'PENDING', 'PENDING', '0000-00-00', 'No remarks provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(105, 'active', '', 'Angel', 'Jamaica', 'Manguray', 'Esteban', '0000-00-00', 'No data provided.', 'Singer PRO', 4, 6, '', '0000-00-00', 'No data provided.', 'No data provided.', '0000-00-00', '2016-07-20', '0000-00-00', 0, 'PENDING', 'PENDING', '0000-00-00', 'No remarks provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(106, 'active', '', 'Jeane', 'Jane', 'Estorba', 'Albarracin', '0000-00-00', 'No data provided.', 'Dancer NON-PRO', 4, 6, '', '0000-00-00', 'No data provided.', 'No data provided.', '0000-00-00', '2016-07-20', '2016-10-16', 0, 'PENDING', 'PENDING', '0000-00-00', 'No remarks provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(107, 'active', '', 'Melody', 'Ma. Meldy', 'Marino', 'Tayas', '0000-00-00', 'No data provided.', 'Dancer NON-PRO', 4, 6, '', '0000-00-00', 'No data provided.', 'No data provided.', '0000-00-00', '2016-07-20', '0000-00-00', 0, 'PENDING', 'PENDING', '0000-00-00', 'No remarks provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(108, 'active', '', 'Nana', 'Analee', 'Gabrillo', 'Samera', '0000-00-00', 'No data provided.', 'Dancer NON-PRO', 4, 6, '', '0000-00-00', 'No data provided.', 'No data provided.', '0000-00-00', '2016-07-20', '0000-00-00', 0, 'PENDING', 'PENDING', '0000-00-00', 'No remarks provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(109, 'active', '', 'Karen', 'Mary Rose', 'Sardo', 'Santa Romana', '0000-00-00', 'No data provided.', 'Dancer NON-PRO', 4, 6, '', '0000-00-00', 'No data provided.', 'No data provided.', '0000-00-00', '2016-08-11', '0000-00-00', 0, 'PENDING', 'PENDING', '0000-00-00', 'No remarks provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(110, 'active', '', 'Ara', 'Anabelle', 'Buizon', 'Jose', '0000-00-00', 'No data provided.', 'Dancer NON-PRO', 4, 6, '', '0000-00-00', 'No data provided.', 'No data provided.', '0000-00-00', '2016-08-11', '0000-00-00', 0, 'PENDING', 'PENDING', '0000-00-00', 'No remarks provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(111, 'active', '', 'Joan Marie Rapanan Mangonlay', 'Joan Marie', 'Rapanan', 'Mangonlay', '0000-00-00', 'No data provided.', 'Dancer NON-PRO', 4, 6, '', '0000-00-00', 'No data provided.', 'No data provided.', '0000-00-00', '2016-07-17', '2016-09-23', 0, 'PENDING', 'PENDING', '0000-00-00', 'No remarks provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(112, 'active', '', 'Robilyn Guinto', 'Robilyn', '', 'Guinto', '2004-01-09', 'No data provided.', 'Dancer NON-PRO', 2, 10, '', '2016-09-05', 'No data provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '0000-00-00', 'No remarks provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(113, 'active', '', 'Maybelle Lipata De Guia', 'Maybelle', 'Lipata', 'De Guia', '0000-00-00', 'No data provided.', 'Dancer NON-PRO', 4, 6, '', '0000-00-00', 'No data provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '0000-00-00', 'No remarks provided.', '16-002014', '0000-00-00', '2016-10-11', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/uploads/talents/113/Maybelle_Lipata_De_Guia.jpg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(114, 'active', '', 'Jennevy Pingol Del Rosario', 'Jennevy', 'Pingol', 'Del Rosario', '0000-00-00', 'No data provided.', 'Singer NON-PRO', 4, 6, '', '0000-00-00', 'No data provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '0000-00-00', 'No remarks provided.', '16-002015', '0000-00-00', '2016-10-11', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/uploads/talents/114/Jennevy_Pingol_Del_Rosario.jpg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(115, 'active', '', 'Rozel Ann Magpayo Kasilag', 'Rozel Ann', 'Magpayo', 'Kasilag', '0000-00-00', 'No data provided.', 'Dancer NON-PRO', 4, 6, '', '0000-00-00', 'No data provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '0000-00-00', 'No remarks provided.', '16-002017', '0000-00-00', '2016-10-11', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/uploads/talents/115/Rozel_Ann_Magpayo_Kasilag.jpg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(116, 'active', '', 'Mary Rose Esguerra', 'Mary Rose', '', 'Esguerra', '0000-00-00', 'No data provided.', 'Dancer NON-PRO', 4, 5, '', '0000-00-00', 'No data provided.', 'No data provided.', '0000-00-00', '2016-10-29', '0000-00-00', 0, 'PENDING', 'PENDING', '0000-00-00', 'No remarks provided.', 'No data provided.', '0000-00-00', '2016-07-25', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(117, 'active', '', 'Tracy Anne Solon', 'Tracy Anne', '', 'Solon', '0000-00-00', 'No data provided.', 'Dancer NON-PRO', 4, 5, '', '0000-00-00', 'No data provided.', 'No data provided.', '0000-00-00', '2016-10-29', '0000-00-00', 0, 'PENDING', 'PENDING', '0000-00-00', 'No remarks provided.', 'No data provided.', '0000-00-00', '2016-07-25', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(118, 'active', '', 'Rufa Tingson', 'Rufa', '', 'Tingson', '0000-00-00', 'No data provided.', 'Dancer NON-PRO', 4, 5, '', '0000-00-00', 'No data provided.', 'No data provided.', '0000-00-00', '2016-10-29', '0000-00-00', 0, 'PENDING', 'PENDING', '0000-00-00', 'No remarks provided.', 'No data provided.', '0000-00-00', '2016-07-25', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(119, 'active', '', 'Shiena Aquino', 'Shiena', '', 'Aquino', '0000-00-00', 'No data provided.', 'Dancer NON-PRO', 4, 5, '', '0000-00-00', 'No data provided.', 'No data provided.', '0000-00-00', '2016-10-29', '0000-00-00', 0, 'PENDING', 'PENDING', '0000-00-00', 'No remarks provided.', 'No data provided.', '0000-00-00', '2016-07-25', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(120, 'active', '', 'Ailyn Nazarene Taway', 'Ailyn Nazarene', '', 'Taway', '0000-00-00', 'No data provided.', 'Dancer NON-PRO', 4, 5, '', '0000-00-00', 'No data provided.', 'No data provided.', '0000-00-00', '2016-10-29', '0000-00-00', 0, 'PENDING', 'PENDING', '0000-00-00', 'No remarks provided.', 'No data provided.', '0000-00-00', '2016-07-25', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(121, 'active', '', 'Rhea Realino', 'Rhea', '', 'Realino', '0000-00-00', 'No data provided.', 'Dancer NON-PRO', 4, 5, '', '0000-00-00', 'No data provided.', 'No data provided.', '0000-00-00', '2016-10-29', '0000-00-00', 0, 'PENDING', 'PENDING', '0000-00-00', 'No remarks provided.', 'No data provided.', '0000-00-00', '2016-07-25', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(122, 'active', '', 'Joan Delos Santos', 'Joan', '', 'Delos Santos', '0000-00-00', 'No data provided.', 'Dancer NON-PRO', 4, 5, '', '0000-00-00', 'No data provided.', 'No data provided.', '0000-00-00', '2016-10-29', '0000-00-00', 0, 'PENDING', 'PENDING', '0000-00-00', 'No remarks provided.', 'No data provided.', '0000-00-00', '2016-07-25', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(123, 'active', '', 'Ruzzel Realino', 'Ruzzel', '', 'Realino', '0000-00-00', 'No data provided.', 'Dancer NON-PRO', 4, 5, '', '0000-00-00', 'No data provided.', 'No data provided.', '0000-00-00', '2016-10-29', '0000-00-00', 0, 'PENDING', 'PENDING', '0000-00-00', 'No remarks provided.', 'No data provided.', '0000-00-00', '2016-07-25', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(124, 'active', '', 'Salve Miaros', 'Salve', '', 'Miaros', '0000-00-00', 'No data provided.', 'Dancer NON-PRO', 2, 2, '', '0000-00-00', 'No data provided.', 'No data provided.', '0000-00-00', '2016-05-13', '2016-08-13', 0, 'PENDING', 'PENDING', '0000-00-00', 'No remarks provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(125, 'active', '', 'Charlotte  Ethel Aquino Alano', 'Charlotte Ethel', 'Aquino', 'Alano', '0000-00-00', 'No data provided.', 'Dancer NON-PRO', 2, 2, '', '0000-00-00', 'No data provided.', 'No data provided.', '0000-00-00', '2016-05-13', '0000-00-00', 0, 'PENDING', 'PENDING', '0000-00-00', 'No remarks provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(126, 'active', '', 'Marra Elano Tibayan', 'Marra', 'Elano', 'Tibayan', '0000-00-00', 'No data provided.', 'Dancer NON-PRO', 2, 2, '', '0000-00-00', 'No data provided.', 'No data provided.', '0000-00-00', '2016-05-13', '0000-00-00', 0, 'PENDING', 'PENDING', '0000-00-00', 'No remarks provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(127, 'active', '', 'Brigette Canicia Borlagdan', 'Brigette', 'Canicia', 'Borlagdan', '0000-00-00', 'No data provided.', 'Dancer NON-PRO', 2, 2, '', '0000-00-00', 'No data provided.', 'No data provided.', '0000-00-00', '2016-08-13', '0000-00-00', 0, 'PENDING', 'PENDING', '0000-00-00', 'No remarks provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(128, 'active', 'hehehe', 'Lovely Jane Bonita Boncales', 'Lovely Jane', 'Bonita', 'Boncales', '0000-00-00', 'No data provided.', 'Dancer NON-PRO', 2, 2, '', '0000-00-00', 'No data provided.', 'No data provided.', '0000-00-00', '2016-08-13', '0000-00-00', 0, 'PENDING', 'PENDING', '0000-00-00', 'No remarks provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(129, 'inactive', '', 'Lady Dianne Magbanna Laderas', 'Lady Dianne', 'Magbanna', 'Laderas', '0000-00-00', 'No data provided.', 'Dancer NON-PRO', 2, 2, '', '0000-00-00', 'No data provided.', 'No data provided.', '0000-00-00', '2016-08-31', '0000-00-00', 0, 'PENDING', 'PENDING', '0000-00-00', 'No remarks provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(130, 'active', '', 'Jean Tagalog', 'Jean', '', 'Tagalog', '0000-00-00', 'No data provided.', 'Dancer NON-PRO', 2, 2, '', '0000-00-00', 'No data provided.', 'No data provided.', '0000-00-00', '2016-05-13', '2016-09-18', 0, 'PENDING', 'PENDING', '0000-00-00', 'No remarks provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(132, 'active', '', 'Kristine Santos', 'Kristine', '', 'Santos', '0000-00-00', 'No data provided.', 'Dancer NON-PRO', 2, 2, '', '0000-00-00', 'No data provided.', 'No data provided.', '0000-00-00', '2016-06-18', '2016-09-18', 0, 'PENDING', 'PENDING', '0000-00-00', 'No remarks provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(133, 'inactive', '', 'Hazel Gay Calipusan', 'Hazel Gay', '', 'Calipusan', '0000-00-00', 'No data provided.', 'Dancer NON-PRO', 2, 2, '', '0000-00-00', 'No data provided.', 'No data provided.', '0000-00-00', '2016-06-18', '2016-09-18', 0, 'PENDING', 'PENDING', '0000-00-00', 'No remarks provided.', 'No data provided.', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg', 'http://mangrouppro.com/imgs/no_image_available.jpeg'),
(136, 'inactive', '', 'test', 'test', 'test', 'tet', '0000-00-00', '', 'Dancer PRO', 2, 1, '2:2', '2016-11-04', '', '', '0000-00-00', '0000-00-00', '0000-00-00', 0, 'PENDING', 'PENDING', '0000-00-00', '', '', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'http://localhost/manpro/imgs/no_image_available.jpeg', 'http://localhost/manpro/imgs/no_image_available.jpeg', 'http://localhost/manpro/imgs/no_image_available.jpeg', 'http://localhost/manpro/imgs/no_image_available.jpeg');

-- --------------------------------------------------------

--
-- Table structure for table `talents_status`
--

CREATE TABLE IF NOT EXISTS `talents_status` (
`statusId` int(11) NOT NULL,
  `talentId` int(11) NOT NULL,
  `status` varchar(20) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=121 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `talents_status`
--

INSERT INTO `talents_status` (`statusId`, `talentId`, `status`, `date`) VALUES
(1, 14, 'coeapp', '2016-09-13'),
(2, 15, 'coeapp', '2016-09-13'),
(3, 16, 'coeapp', '2016-09-13'),
(4, 17, 'coeapp', '2016-09-13'),
(5, 18, 'coeapp', '2016-09-13'),
(6, 19, 'coeapp', '2016-09-13'),
(7, 20, 'coeapp', '2016-09-13'),
(8, 21, 'coeapp', '2016-09-13'),
(9, 22, 'coeapp', '2016-09-13'),
(10, 23, 'coeapp', '2016-09-13'),
(11, 24, 'coeapp', '2016-09-13'),
(12, 25, 'coeapp', '2016-09-13'),
(13, 26, 'coeapp', '2016-09-13'),
(14, 27, 'coeapp', '2016-09-13'),
(15, 36, 'coeapp', '2016-10-18'),
(16, 37, 'coeapp', '2016-10-18'),
(17, 38, 'coeapp', '2016-10-18'),
(18, 40, 'docsend', '2016-09-30'),
(19, 41, 'docsend', '2016-09-30'),
(20, 42, 'docsend', '2016-09-30'),
(21, 43, 'docsend', '2016-09-30'),
(22, 44, 'coeapp', '2016-10-18'),
(23, 45, 'coeapp', '2016-10-18'),
(24, 46, 'docsend', '2016-09-30'),
(25, 47, 'coeapp', '2016-10-18'),
(26, 48, 'coeapp', '2016-10-18'),
(27, 49, 'coeapp', '2016-10-18'),
(28, 50, 'coeapp', '2016-10-18'),
(29, 91, 'docsend', '2016-10-14'),
(30, 86, 'docsend', '2016-10-14'),
(31, 59, 'coeapp', '2016-10-18'),
(32, 85, 'coeapp', '2016-10-18'),
(33, 53, 'docsend', '2016-10-14'),
(34, 55, 'docsend', '2016-10-14'),
(35, 60, 'docsend', '2016-10-14'),
(36, 61, 'docsend', '2016-10-14'),
(37, 62, 'docsend', '2016-10-14'),
(38, 66, 'docsend', '2016-10-14'),
(39, 64, 'docsend', '2016-10-14'),
(40, 65, 'docsend', '2016-10-14'),
(41, 52, 'coeapp', '2016-10-18'),
(42, 54, 'docsend', '2016-10-14'),
(43, 56, 'coeapp', '2016-10-18'),
(44, 58, 'coeapp', '2016-10-18'),
(45, 110, 'injapan', '2016-08-11'),
(46, 93, 'injapan', '2016-07-06'),
(47, 92, 'injapan', '2016-07-06'),
(48, 94, 'injapan', '2016-07-06'),
(49, 95, 'injapan', '2016-07-06'),
(50, 96, 'injapan', '2016-08-11'),
(51, 98, 'injapan', '2016-09-09'),
(52, 106, 'outjapan', '2016-10-16'),
(53, 107, 'injapan', '2016-07-20'),
(54, 108, 'injapan', '2016-07-20'),
(55, 109, 'injapan', '2016-08-11'),
(56, 105, 'injapan', '2016-07-20'),
(57, 104, 'injapan', '2016-07-17'),
(58, 103, 'injapan', '2016-07-17'),
(59, 102, 'injapan', '2016-07-17'),
(60, 101, 'injapan', '2016-07-17'),
(61, 100, 'outjapan', '2016-10-16'),
(62, 99, 'outjapan', '2016-10-16'),
(63, 97, 'injapan', '2016-08-11'),
(64, 116, 'injapan', '2016-10-29'),
(65, 117, 'injapan', '2016-10-29'),
(66, 118, 'injapan', '2016-10-29'),
(67, 119, 'injapan', '2016-10-29'),
(68, 120, 'injapan', '2016-10-29'),
(69, 121, 'injapan', '2016-10-29'),
(70, 122, 'injapan', '2016-10-29'),
(71, 123, 'injapan', '2016-10-29'),
(72, 111, 'outjapan', '2016-09-23'),
(73, 124, 'outjapan', '2016-08-13'),
(74, 125, 'injapan', '2016-05-13'),
(75, 126, 'injapan', '2016-05-13'),
(76, 127, 'injapan', '2016-08-13'),
(77, 128, 'injapan', '2016-08-13'),
(78, 129, 'injapan', '2016-08-31'),
(79, 130, 'outjapan', '2016-09-18'),
(80, 131, 'outjapan', '2016-09-18'),
(81, 132, 'outjapan', '2016-09-18'),
(82, 133, 'outjapan', '2016-09-18'),
(83, 87, 'coerelease', '2016-08-24'),
(84, 88, 'coerelease', '2016-08-24'),
(85, 89, 'coerelease', '2016-08-24'),
(86, 90, 'coerelease', '2016-08-24'),
(87, 32, 'coerelease', '2016-09-16'),
(88, 31, 'coerelease', '2016-09-16'),
(89, 33, 'coerelease', '2016-09-16'),
(90, 28, 'coerelease', '2016-09-16'),
(91, 29, 'coerelease', '2016-09-16'),
(92, 30, 'coerelease', '2016-09-16'),
(93, 114, 'coerelease', '2016-10-11'),
(94, 113, 'coerelease', '2016-10-11'),
(95, 75, 'docsend', '2016-10-29'),
(96, 74, 'docsend', '2016-10-29'),
(97, 73, 'docsend', '2016-10-29'),
(98, 72, 'docsend', '2016-10-29'),
(99, 71, 'docsend', '2016-10-29'),
(100, 70, 'docsend', '2016-10-29'),
(101, 69, 'docsend', '2016-10-29'),
(102, 115, 'coerelease', '2016-10-11'),
(103, 134, 'injapan', '2016-11-05'),
(104, 112, 'hired', '2016-09-05'),
(105, 57, 'hired', '2016-09-06'),
(106, 68, 'hired', '2016-09-28'),
(107, 67, 'hired', '2016-09-28'),
(108, 63, 'hired', '2016-09-28'),
(110, 84, 'hired', '2016-09-26'),
(111, 83, 'hired', '2016-09-26'),
(112, 82, 'hired', '2016-09-26'),
(113, 81, 'hired', '2016-09-26'),
(114, 80, 'hired', '2016-09-26'),
(115, 79, 'hired', '2016-09-26'),
(116, 78, 'hired', '2016-09-26'),
(117, 77, 'hired', '2016-09-26'),
(118, 76, 'hired', '2016-09-26'),
(119, 51, 'hired', '2016-09-05'),
(120, 136, 'hired', '2016-11-04');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
 ADD PRIMARY KEY (`userId`);

--
-- Indexes for table `agency`
--
ALTER TABLE `agency`
 ADD PRIMARY KEY (`agencyId`);

--
-- Indexes for table `clubs`
--
ALTER TABLE `clubs`
 ADD PRIMARY KEY (`clubId`);

--
-- Indexes for table `expenses`
--
ALTER TABLE `expenses`
 ADD PRIMARY KEY (`expenseId`);

--
-- Indexes for table `production`
--
ALTER TABLE `production`
 ADD PRIMARY KEY (`productionId`);

--
-- Indexes for table `sales`
--
ALTER TABLE `sales`
 ADD PRIMARY KEY (`salesId`);

--
-- Indexes for table `status_logs`
--
ALTER TABLE `status_logs`
 ADD PRIMARY KEY (`statusId`);

--
-- Indexes for table `system_logs`
--
ALTER TABLE `system_logs`
 ADD PRIMARY KEY (`logID`);

--
-- Indexes for table `talents`
--
ALTER TABLE `talents`
 ADD PRIMARY KEY (`talentId`);

--
-- Indexes for table `talents_status`
--
ALTER TABLE `talents_status`
 ADD PRIMARY KEY (`statusId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accounts`
--
ALTER TABLE `accounts`
MODIFY `userId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `agency`
--
ALTER TABLE `agency`
MODIFY `agencyId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `clubs`
--
ALTER TABLE `clubs`
MODIFY `clubId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `expenses`
--
ALTER TABLE `expenses`
MODIFY `expenseId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `production`
--
ALTER TABLE `production`
MODIFY `productionId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `sales`
--
ALTER TABLE `sales`
MODIFY `salesId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=111;
--
-- AUTO_INCREMENT for table `status_logs`
--
ALTER TABLE `status_logs`
MODIFY `statusId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `system_logs`
--
ALTER TABLE `system_logs`
MODIFY `logID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=779;
--
-- AUTO_INCREMENT for table `talents`
--
ALTER TABLE `talents`
MODIFY `talentId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=137;
--
-- AUTO_INCREMENT for table `talents_status`
--
ALTER TABLE `talents_status`
MODIFY `statusId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=121;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
