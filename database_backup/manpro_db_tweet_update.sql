-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 15, 2016 at 07:55 PM
-- Server version: 5.6.21
-- PHP Version: 5.5.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `manpro_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE IF NOT EXISTS `accounts` (
`userId` int(11) NOT NULL,
  `accountName` varchar(50) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `productionId` int(11) NOT NULL DEFAULT '0',
  `type` varchar(1) NOT NULL,
  `active` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`userId`, `accountName`, `username`, `password`, `productionId`, `type`, `active`) VALUES
(1, 'Rom', 'try', '5431', 1, 'p', 1),
(2, 'Admin', 'admin', 'admin', 0, 'a', 1),
(3, 'Herb', 'hebi', '5431', 2, 'p', 1),
(4, 'Green', 'green', '1234', 3, 'p', 1),
(5, 'test', 'test', 'test', 0, 'a', 1);

-- --------------------------------------------------------

--
-- Table structure for table `clubs`
--

CREATE TABLE IF NOT EXISTS `clubs` (
`clubId` int(11) NOT NULL,
  `clubName` varchar(50) NOT NULL,
  `capacity` int(11) NOT NULL,
  `productionId` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `clubs`
--

INSERT INTO `clubs` (`clubId`, `clubName`, `capacity`, `productionId`) VALUES
(1, 'romantiko', 10, 1),
(2, 'stella', 10, 2),
(3, 'porno graffiti', 10, 1),
(4, 'Rezero', 15, 2),
(6, 'Kristal', 20, 1),
(7, 'Green', 10, 3),
(8, 'New Club', 10, 3);

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE IF NOT EXISTS `logs` (
`logId` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `action` varchar(100) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pricing`
--

CREATE TABLE IF NOT EXISTS `pricing` (
`priceId` int(11) NOT NULL,
  `talentId` int(11) NOT NULL,
  `origPrice` double NOT NULL,
  `sellingPrice` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `production`
--

CREATE TABLE IF NOT EXISTS `production` (
`productionId` int(11) NOT NULL,
  `productionName` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `production`
--

INSERT INTO `production` (`productionId`, `productionName`) VALUES
(1, 'JP'),
(2, 'Hebi'),
(3, 'Green Cross');

-- --------------------------------------------------------

--
-- Table structure for table `status_logs`
--

CREATE TABLE IF NOT EXISTS `status_logs` (
`statusId` int(11) NOT NULL,
  `talentId` int(11) NOT NULL,
  `status` enum('applying','processing','preparation','immigaration') NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `status_logs`
--

INSERT INTO `status_logs` (`statusId`, `talentId`, `status`, `date`) VALUES
(1, 1, 'applying', '2016-07-03'),
(2, 2, 'applying', '2016-07-06'),
(3, 3, 'applying', '2016-07-19'),
(4, 1, 'processing', '2016-08-10'),
(5, 3, '', '2016-08-31'),
(6, 0, 'processing', '2016-09-02'),
(7, 15, 'processing', '2016-09-02'),
(8, 15, 'applying', '2016-09-01');

-- --------------------------------------------------------

--
-- Table structure for table `system_logs`
--

CREATE TABLE IF NOT EXISTS `system_logs` (
`logID` int(11) NOT NULL,
  `action` varchar(50) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `accountName` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `system_logs`
--

INSERT INTO `system_logs` (`logID`, `action`, `date`, `accountName`) VALUES
(1, 'Admin logged in.', '2016-09-01 12:43:57', 'Admin'),
(2, 'Rom logged in.', '2016-09-01 12:46:29', 'Rom'),
(3, 'Rom logged out.', '2016-09-01 12:48:13', 'Rom'),
(4, 'Admin logged in.', '2016-09-01 12:49:18', 'Admin'),
(5, 'Admin logged out.', '2016-09-01 12:49:22', 'Admin'),
(6, 'Admin logged in.', '2016-09-01 13:11:28', 'Admin'),
(7, 'Admin logged out.', '2016-09-01 14:39:24', 'Admin'),
(8, 'Admin logged in.', '2016-09-01 14:39:27', 'Admin'),
(9, 'Admin logged in.', '2016-09-01 14:41:58', 'Admin'),
(10, 'Admin logged in.', '2016-09-04 01:09:21', 'Admin'),
(11, 'Added new talent Harley Quinn', '2016-09-04 01:36:48', 'Admin'),
(12, 'Added new talent Harley Quinn', '2016-09-04 01:40:07', 'Admin'),
(13, 'Harley Quinn biodata updated.', '2016-09-04 01:59:31', 'Admin'),
(14, 'Harley Quinn EDIT biodata updated.', '2016-09-04 01:59:43', 'Admin'),
(15, 'Admin logged in.', '2016-09-04 18:36:46', 'Admin'),
(16, 'Admin logged in.', '2016-09-04 20:13:40', 'Admin'),
(17, 'Added new talent GAGA', '2016-09-04 20:17:58', 'Admin'),
(18, 'Admin logged in.', '2016-09-05 16:05:19', 'Admin'),
(19, 'Status update on talent ID undefined', '2016-09-05 16:27:50', 'Admin'),
(20, 'Status update on talent ID undefined', '2016-09-05 16:29:21', 'Admin'),
(21, 'Status update on talent ID 15', '2016-09-05 16:33:53', 'Admin'),
(22, 'Status update on talent ID 15', '2016-09-05 16:35:17', 'Admin'),
(23, 'Status update on talent ID 15', '2016-09-05 17:18:32', 'Admin'),
(24, 'Admin logged in.', '2016-09-15 16:04:29', 'Admin'),
(25, 'Added new talent ???', '2016-09-15 16:22:20', 'Admin'),
(26, 'Added new talent  ???????', '2016-09-15 16:29:47', 'Admin'),
(27, 'Added new talent ?????', '2016-09-15 16:43:57', 'Admin'),
(28, 'Added new talent ???', '2016-09-15 16:46:20', 'Admin'),
(29, 'Added new talent ???', '2016-09-15 16:53:07', 'Admin'),
(30, 'Added new talent ???', '2016-09-15 16:57:19', 'Admin'),
(31, 'Added new talent ????', '2016-09-15 17:04:45', 'Admin'),
(32, 'Added new talent ????', '2016-09-15 17:07:50', 'Admin'),
(33, 'Added new talent ????', '2016-09-15 17:12:50', 'Admin'),
(34, 'Added new talent ???', '2016-09-15 17:23:22', 'Admin'),
(35, 'Added new talent ???', '2016-09-15 17:27:25', 'Admin'),
(36, 'Added new talent ???', '2016-09-15 17:32:46', 'Admin'),
(37, 'Added new talent ???', '2016-09-15 17:34:19', 'Admin'),
(38, 'Added new talent ???', '2016-09-15 17:36:56', 'Admin'),
(39, 'Added new talent ???', '2016-09-15 17:38:54', 'Admin'),
(40, 'Added new talent ?????', '2016-09-15 17:43:19', 'Admin'),
(41, 'Added new talent ?????', '2016-09-15 17:44:07', 'Admin'),
(42, 'Added new talent ?????', '2016-09-15 17:45:11', 'Admin'),
(43, 'Added new talent ???', '2016-09-15 17:51:39', 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `talents`
--

CREATE TABLE IF NOT EXISTS `talents` (
`talentId` int(11) NOT NULL,
  `lastName` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `firstName` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `midName` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `bdate` date NOT NULL,
  `productionId` int(11) NOT NULL,
  `clubId` int(11) NOT NULL,
  `processing` date NOT NULL,
  `manager` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `docSend` date NOT NULL,
  `apply` date NOT NULL,
  `visaLicense` date NOT NULL,
  `inJapan` date NOT NULL,
  `extension` date NOT NULL,
  `finishVisa` date NOT NULL,
  `talentName` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `category` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `pob` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `passportNo` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `issu` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `passExp` date NOT NULL,
  `pic1` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `pic2` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `pic3` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `pic4` varchar(150) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `talents`
--

INSERT INTO `talents` (`talentId`, `lastName`, `firstName`, `midName`, `bdate`, `productionId`, `clubId`, `processing`, `manager`, `docSend`, `apply`, `visaLicense`, `inJapan`, `extension`, `finishVisa`, `talentName`, `category`, `pob`, `passportNo`, `issu`, `passExp`, `pic1`, `pic2`, `pic3`, `pic4`) VALUES
(45, '最高でした', '最高でした', '最高でした', '0000-00-00', 2, 2, '0000-00-00', 'None', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '最高でした', 'Dancer NON-PRO', '', '', '', '0000-00-00', 'http://localhost/manpro/imgs/no_image_available.jpeg', 'http://localhost/manpro/imgs/no_image_available.jpeg', 'http://localhost/manpro/imgs/no_image_available.jpeg', 'http://localhost/manpro/imgs/no_image_available.jpeg'),
(46, 'でした', 'でした', 'でした', '0000-00-00', 1, 1, '0000-00-00', 'None', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'でした', 'Dancer NON-PRO', '', '', '', '0000-00-00', 'http://localhost/manpro/imgs/no_image_available.jpeg', 'http://localhost/manpro/imgs/no_image_available.jpeg', 'http://localhost/manpro/imgs/no_image_available.jpeg', 'http://localhost/manpro/imgs/no_image_available.jpeg');

-- --------------------------------------------------------

--
-- Table structure for table `talents_status`
--

CREATE TABLE IF NOT EXISTS `talents_status` (
`statusId` int(11) NOT NULL,
  `talentId` int(11) NOT NULL,
  `status` enum('applying','processing','preparation','immigration') NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `talents_status`
--

INSERT INTO `talents_status` (`statusId`, `talentId`, `status`, `date`) VALUES
(1, 1, 'processing', '2016-08-10'),
(2, 2, 'applying', '2016-07-06'),
(3, 3, 'immigration', '2016-08-31'),
(4, 0, 'processing', '2016-09-02'),
(5, 15, 'applying', '2016-09-01');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
 ADD PRIMARY KEY (`userId`);

--
-- Indexes for table `clubs`
--
ALTER TABLE `clubs`
 ADD PRIMARY KEY (`clubId`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
 ADD PRIMARY KEY (`logId`);

--
-- Indexes for table `pricing`
--
ALTER TABLE `pricing`
 ADD PRIMARY KEY (`priceId`);

--
-- Indexes for table `production`
--
ALTER TABLE `production`
 ADD PRIMARY KEY (`productionId`);

--
-- Indexes for table `status_logs`
--
ALTER TABLE `status_logs`
 ADD PRIMARY KEY (`statusId`);

--
-- Indexes for table `system_logs`
--
ALTER TABLE `system_logs`
 ADD PRIMARY KEY (`logID`);

--
-- Indexes for table `talents`
--
ALTER TABLE `talents`
 ADD PRIMARY KEY (`talentId`);

--
-- Indexes for table `talents_status`
--
ALTER TABLE `talents_status`
 ADD PRIMARY KEY (`statusId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accounts`
--
ALTER TABLE `accounts`
MODIFY `userId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `clubs`
--
ALTER TABLE `clubs`
MODIFY `clubId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
MODIFY `logId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pricing`
--
ALTER TABLE `pricing`
MODIFY `priceId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `production`
--
ALTER TABLE `production`
MODIFY `productionId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `status_logs`
--
ALTER TABLE `status_logs`
MODIFY `statusId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `system_logs`
--
ALTER TABLE `system_logs`
MODIFY `logID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=44;
--
-- AUTO_INCREMENT for table `talents`
--
ALTER TABLE `talents`
MODIFY `talentId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT for table `talents_status`
--
ALTER TABLE `talents_status`
MODIFY `statusId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
