$( document ).ready(function() {
    //DATEPICKER
    $('.datepicker').pickadate({
     formatSubmit: 'yyyy/mm/dd',
     hiddenName: true,
     format: 'dddd, dd mmm, yyyy',
     selectMonths: true,
     today: '',
     selectYears: 60
    });

    //TOOLTIP
    $('.tooltipped').tooltip({delay: 50});

    //SELECT
     $('select').material_select();

     //MODAL TRIGGER
     // the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
     $('.modal-trigger').leanModal();
});
