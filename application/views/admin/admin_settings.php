<?php if($this->session->flashdata('dbsuccess')){?>
  <script type="text/javascript">
    $(document).ready(function(){
        swal({
          title: "Done!",
          text:  '<?php echo $this->session->flashdata('dbsuccess');?>',
          type: "success",
          timer: 5000,
          showConfirmButton: false
        });
    });
  </script>
<?php }?>
<br>

<div class="right-align" id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'en', includedLanguages: 'en,ja,tl', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
<div class="section no-pad-bot" id="index-banner">
  <div class="container">
    <br><br>
    <h3 class="header center cyan-text text-darken-3">Settings</h3>
    <br><br>
  </div>
</div>

<div class="container">

  <!-- <div class="section no-pad-bot" id="index-banner">
    <div class="container">
      <br><br>
      <h4 class="header center cyan-text text-darken-3">Database Backup History</h4>
      <br><br>
    </div>
  </div> -->

  <!-- <div>
    <h6 class="right"><a class="waves-effect waves-light btn modal-trigger" href="#backupDatabase"><i class="material-icons left">settings_backup_restore</i>Backup Database</a></h6>
  </div> -->
 <!-- <?php echo site_url('db_backup')?>
  <div class="container"> -->
    <!-- get db backup list -->
      <!-- <table class="highlight" style="margin-bottom:100px">
        <thead>
          <tr>
              <th data-field="name">Backup ID</th>
              <th data-field="status">Database File Name</th>
              <th data-field="status">Date</th>
              <th data-field="status">Username</th>
          </tr>
        </thead>

          <tbody>
        </tbody>
      </table>
  </div> -->

  <div class="section no-pad-bot" id="index-banner">
    <div class="container">
      <br><br>
      <h4 class="header center cyan-text text-darken-3">System Logs</h4>
      <br><br>
    </div>
  </div>

  <div>
    <!-- get db backup list -->
      <table id="system_logs" class="highlight" style="margin-bottom:100px">
        <thead>
          <tr>
              <th data-field="name">Log ID</th>
              <th data-field="status">Action</th>
              <th data-field="status">Date</th>
              <th data-field="status">Account Name</th>
          </tr>
        </thead>

          <tbody>
        </tbody>
      </table>
  </div>

</div>
<br><br><br>
<!-- database backup modal -->
<!-- <div id="backupDatabase" class="modal modal-fixed-footer">
  <div class="modal-content">
    <h4 class="center-align">Backup Database</h4>
    <br><br>
      <?php echo form_open('administrator/backup_database'); ?>

        <div class="input-field col s6">
          <input id="dbuser" name="dbuser" type="text" class="validate">
          <label for="dbuser">MySQL Username</label>
        </div>

        <div class="input-field col s6">
          <input id="dbpass" name="dbpass" type="text" class="validate">
          <label for="dbpass">MySQL Password</label>
        </div>

    </div>
    <div class="modal-footer">
      <button  class="modal-action modal-close waves-effect waves-red btn-flat ">Cancel [キャンセル]</button>
      <button  type="submit" class="modal-action modal-close waves-effect waves-green btn-flat ">Accept [セーブ]</button>
    </div>
  </div>
</div> -->
<!--  -->

<script type="text/javascript">
var method; //for save method string
var table;

$( document ).ready(function() {
  table = $('#system_logs').DataTable({
        "processing": true,
        "serverSide": true,
        "order": [],
        "ajax": {
            "url": "<?php echo site_url('administrator/system_logs')?>",
            "type": "POST"
        },
        "columnDefs": [
          {
              "targets": [ -1 ],
              "orderable": false,
              "class": "right-align"
          },
        ]
    });

    $('.modal-trigger').leanModal();

    $('.tooltipped').tooltip({delay: 50});

    $('select').material_select();

    $('.materialboxed').materialbox();
});

function view_talent_bio(id){
  console.log('hi');
}
</script>

<script>
$(document).ready(function() {
  // the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
  $('.modal-trigger').leanModal();
});
</script>
