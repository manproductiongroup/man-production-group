<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Prod_login extends CI_Controller {

	public function __construct() {
		parent::__construct();

		$this -> load -> model('production_model');

	}

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/home
	 *	- or -
	 * 		http://example.com/index.php/welcome/home
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$data['main_content'] = 'prod_login';
    $this->load->view('includes/template',$data);
	}


	function validate_account()
	{

        $this->form_validation->set_rules('username', 'Username', 'required|callback_check_account');
        $this->form_validation->set_rules('password', 'Password','required');
            if($this->form_validation->run() == false){
               redirect('prod_login');
            }
            else{
								if($this->session->userdata('type') == 'p'){
									redirect('production');
								}else{
									redirect('third');
								}
            }
    }

			function check_account()
    {
						$user = $this->input->post('username');
						$pass = $this->input->post('password');

            $query = $this->production_model->check_account_db($user,$pass);

            if($query){
                return true;
            }
            else{
                return false;
            }
    }

}
