<?php if($this->session->flashdata('success')){?>
  <script type="text/javascript">
    $(document).ready(function(){
        swal({
          title: "Done!",
          text:  '<?php echo $this->session->flashdata('success');?>',
          type: "success",
          timer: 3000,
          showConfirmButton: false
        });
    });
  </script>
<?php }?>

<?php if($this->session->flashdata('username')){?>
  <script type="text/javascript">
    $(document).ready(function(){
        swal({
          title: "Error!",
          text:  '<?php echo $this->session->flashdata('username');?>',
          type: "error",
          timer: 3000,
          showConfirmButton: false
        });
    });
  </script>
<?php }?>

<?php if($this->session->flashdata('prodname')){?>
  <script type="text/javascript">
    $(document).ready(function(){
        swal({
          title: "Error!",
          text:  '<?php echo $this->session->flashdata('prodname');?>',
          type: "error",
          timer: 3000,
          showConfirmButton: false
        });
    });
  </script>
<?php }?>

<br>

<div class="right-align" id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'en', includedLanguages: 'en,ja,tl', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>

<div class="section no-pad-bot" id="index-banner">
  <div class="container">
    <br><br>
    <h3 class="header center cyan-text text-darken-3">Agencies</h3>
    <br><br>
  </div>
</div>

<div class="container">
  <!-- get talent list -->
    <table class="highlight" style="margin-bottom:100px">
      <thead>
        <tr>
            <th data-field="name">Agency ID</th>
            <th data-field="status">Agency Name</th>
            <th data-field="status">Location</th>
            <th data-field="status">Options</th>
        </tr>
      </thead>

      <tbody>
        <?php foreach($agencies as $r){?>
        <tr>
            <td><?php echo $r->agencyId ?></td>
            <td><?php echo $r->agencyName ?></td>
              <td><?php echo $r->address ?></td>
            <td>
              <!-- <a href="<? echo site_url('administrator')?>" class="btn-floating btn-small waves-effect waves-light blue tooltipped edit_prod"  data-prodid = "<?php echo $r->productionId ?>" data-position="right" data-delay="50" data-tooltip="Edit Production"><i class="material-icons">mode_edit</i></a> -->
              <a class="btn-floating btn-small waves-effect waves-light red darken-3 tooltipped" data-position="right" data-delay="50" data-tooltip="Delete Agency" onclick="delete_agency(<?php echo $r->agencyId ?>)"><i class="material-icons">delete</i></a>
            </td>
        </tr>
        <?php } ?>
      </tbody>
    </table>
</div>


<!-- Add new production button -->
<div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
  <a class="btn-floating btn-large waves-effect waves-light green-dark modal-trigger" href="#addProduction">
    <i class="material-icons">add</i>
  </a>
</div>
<!--  -->

<!-- Add production modal -->
<div id="addProduction" class="modal modal-fixed-footer">
  <div class="modal-content">
    <!-- <h4 class="center-align">新しい生産を追加</h4> -->
    <h5 class="center-align">Add New Agency</h5>
    <br><br>
    <form action="<?php echo site_url('administrator/add_agency'); ?>" method="post">
        <div class="input-field col s12">
          <input id="agencyName" name="agencyName" type="text" class="validate" required>
          <label for="agencyName">Agency Name</label>
        </div>

        <div class="input-field col s12">
          <input id="address" name="address" type="text" class="validate">
          <label for="address">Address</label>
        </div>
    </div>
    <div class="modal-footer">
      <a  class="modal-action modal-close waves-effect waves-red btn-flat ">Cancel [キャンセル]</a>
      <button  type="submit" class="modal-action modal-close waves-effect waves-green btn-flat ">Save [セーブ]</button>
    </div>
  </form>
  </div>
<!--  -->

<!-- edit production modal -->
<div id="editProduction" class="modal modal-fixed-footer">
  <div class="modal-content">
    <h4 class="center-align">新しい生産を追加</h4>
    <h5 class="center-align">Edit Production</h5>
    <br><br>
      <?php echo form_open('administrator/update_production'); ?>
        <div class="input-field col s12">
          <input id="editProdId" name="productionId" type="hidden">
          <input id="editProdName" name="productionName" type="text" class="validate">
          <label for="editProdName">Production Name [生産名 ]</label>
        </div>
    </div>
    <div class="modal-footer">
      <a  class="modal-action modal-close waves-effect waves-red btn-flat ">Cancel [キャンセル]</a>
      <button  type="submit" class="modal-action modal-close waves-effect waves-green btn-flat ">Save [セーブ]</button>
    </div>
  </div>
  <?php echo form_close()?>
<!--  -->

<script>
$(document).ready(function() {
  // the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
  $('.modal-trigger').leanModal();
});

$(document).ready(function(){
    $('.tooltipped').tooltip({delay: 50});
  });
</script>

<script>
function delete_agency(id){
  swal({    title: "Confirmation",
              text: "Are you sure you want to delete agency "+id+"?",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#f44336",
              confirmButtonText: "Yes, delete it!",
              closeOnConfirm: false },
              function(isConfirm){
                if (isConfirm) {
                    $.ajax({
                        url : "<?php echo site_url('administrator/delete_agency')?>/"+id,
                        type: "POST",
                        dataType: "JSON",
                        success: function(data)
                        {
                          swal("Deleted!", "Agency "+id+" successfully deleted!", "success");
                          location.reload(true);
                        },
                        error: function (jqXHR, textStatus, errorThrown)
                        {
                          alert('An error occured while deleting data');
                        }
                    });
                }
              });

}
</script>
