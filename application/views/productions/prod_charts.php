<br>

<div class="right-align" id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'en', includedLanguages: 'en,ja,tl', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
<div class="section no-pad-bot" id="index-banner">
  <div class="container">
    <br><br>
    <h3 class="header center indigo-text text-darken-4"><?php echo $prodName ?></h3>
    <h5 class="header center indigo-text text-darken-4"><?php echo $clubName ?></h5>
    <br><br>
  </div>
</div>

<div class="container">
  <h6 class="right"><a href="<?php echo site_url('production/clubs')?>" class="waves-effect waves-light btn"><i class="material-icons left">replay</i>back</a></h6>
</div>

<!-- status chart modal -->
  <div class="container">
    <div class="modal-content">
      <div id="chart" style="margin: 0 auto"></div>
    </div>
  </div>
<!-- end status chart modal -->

<script type="text/javascript">
  $(function () {
    $('#chart').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Monthly Status Count'

        },
        subtitle: {
            text: ''

        },
        credits: {
             enabled: false
         },

        xAxis: {
            categories: [
                <?php foreach($status as $r){?>
            			'<?php echo date('F Y',strtotime($r->date))?>',
            			<?php }?>
                ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            tickInterval: 1,
            title: {
                text: 'Talents Count'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: '	D保留',
            data: [
                    <?php foreach($status as $r){?>
                      <?php if($r->status == 'processing'){?>
                        <?php echo $r->count?>,
                      <?php }else{?>
                        '0',
                        <?php } ?>
              			<?php }?>

                  ]

        },{
            name: '	申請中',
            data: [
              <?php foreach($status as $r){?>
                <?php if($r->status == 'applying'){?>
                  <?php echo $r->count?>,
                <?php }else{?>
                  '0',
                  <?php } ?>
              <?php }?>

                  ]

        },{
            name: '	入国準備',
            data: [
              <?php foreach($status as $r){?>
                <?php if($r->status == 'preparation'){?>
                  <?php echo $r->count?>,
                <?php }else{?>
                  '0',
                  <?php } ?>
              <?php }?>
                  ]

        },{
            name: '	入国',
            data: [
              <?php foreach($status as $r){?>
                <?php if($r->status == 'immigration'){?>
                  <?php echo $r->count?>,
                <?php }else{?>
                  '0',
                  <?php } ?>
              <?php }?>

                  ]

        }]
    });
  });
  </script>
