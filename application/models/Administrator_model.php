<?php

class Administrator_model extends CI_Model{
  // Talents Datatables Initialization
  var $table = 'talents t';
  var $column_order = array('talentId','talentName','lastName','firstName','productionName','clubName',NULL);
  var $column_search = array('talentName','lastName','firstName','productionName','clubName');
  var $order = array('talentId' => 'desc');

  // Logs Datatables Initialization
  var $logs = 'system_logs l';
  var $column_order_logs = array('logID','action','date','accountName');
  var $column_search_logs = array('logID','action','date','accountName');
  var $order_logs = array('logID' => 'desc');

  function check_account_db($user,$pass)
  {
    $this->db->where('type', 'a');
    $this->db->where('username', $user);
    $this->db->where('password',$pass);
    $query = $this->db->get('accounts');

    if($query->num_rows() == 1)
    {
      $row = $query->row();
      $data = array(
        'username' => $row->username,
        'userId' => $row->userId,
        'type'  => $row->type,
        'accountName' => $row->accountName,
        'is_logged_in' => true
      );
      $this->session->set_userdata($data);
      $this->session->set_flashdata('login', $row->accountName);

      $this->load->model('Administrator_model');
      $action = $this->session->userdata('accountName')." logged in.";
      $this->Administrator_model->add_logs($action);
      return true;
    }
    else{
      $this->session->set_flashdata('password', 'Wrong username/password.');
      return false;
    }
    }

  function get_production()
  {
    $query = $this->db->get('production');
    return $query->result();
  }

  function get_accountName($id){
    $this->db->where('userId',$id);
    $q = $this->db->get('accounts');
    return $q->row('accountName');

  }

  function get_accounts(){
    $this->db->where('userId !=',1);
    $q = $this->db->get('accounts');
    return $q->result();
  }

  function get_clubs()
  {
    $query =  $this->db->query('select * from clubs c inner join production p on p.productionId = c.productionId');
    return $query->result();
  }

  function get_agencies(){
    $query =  $this->db->query('select * from agency');
    return $query->result();
  }

  //TALENTS
  private function _get_datatables_query()
  {
    $this->db->from($this->table);

    $i = 0;

    foreach ($this->column_search as $item) // loop column
    {
      if($_POST['search']['value']) // if datatable send POST for search
      {

        if($i===0) // first loop
        {
          $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
          $this->db->like($item, $_POST['search']['value']);
        }
        else
        {
          $this->db->or_like($item, $_POST['search']['value']);
        }

        if(count($this->column_search) - 1 == $i) //last loop
          $this->db->group_end(); //close bracket
      }
      $i++;
    }

    if(isset($_POST['order'])) // here order processing
    {
      $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }
    else if(isset($this->order))
    {
      $order = $this->order;
      $this->db->order_by(key($order), $order[key($order)]);
    }
  }

  function get_datatables()
  {
    $this->_get_datatables_query();
    if($_POST['length'] != -1)
    $this->db->limit($_POST['length'], $_POST['start']);
    $this->db->join('production p', 't.productionId = p.productionId');
    $this->db->join('clubs c', 't.clubId = c.clubId');
    $query = $this->db->get();
    return $query->result();
  }

  public function count_all()
  {
    $this->db->from($this->table);
    return $this->db->count_all_results();
  }

  function count_filtered()
  {
    $this->_get_datatables_query();
    $this->db->join('production p', 't.productionId = p.productionId');
    $this->db->join('clubs c', 't.clubId = c.clubId');
    $query = $this->db->get();
    return $query->num_rows();
  }

  function get_talents()
  {
    $this->db->join('production p', 't.productionId = p.productionId');
    $this->db->join('clubs c', 't.clubId = c.clubId');
    $query = $this->db->get('talents t');
    return $query->result();
  }

  function get_talents_where($talentId)
  {
    $this->db->where('talentId', $talentId);
    $this->db->join('production p', 't.productionId = p.productionId');
    $this->db->join('clubs c', 't.clubId = c.clubId');
    $this->db->join('agency a', 't.agency = a.agencyId','left');
    $query = $this->db->get('talents t');
    return $query->result();
  }

  function get_talents_salary($talentId)
  {
    $this->db->where('talentId', $talentId);
    $query = $this->db->get('sales');
    return $query->result();
  }

  function delete_talent_where($talentId)
  {
    $this->db->where('talentId', $talentId);
    $this->db->delete('talents');
  }

  function edit_talent_bio($talentId){
    $this->db->where('clubId',$this->input->post('booking'));
    $q = $this->db->get('clubs');
    $prodid = $q->row('productionId');

    $data = array(
      'talentName' => $this->input->post('talentName'),
      'firstName' => $this->input->post('firstName'),
      'midName' => $this->input->post('midName'),
      'lastName' => $this->input->post('lastName'),
      'bdate' => $this->input->post('birthdate'),
      'pob' => $this->input->post('birthplace'),
      'category' => $this->input->post('category'),
      'productionId' => $prodid,
      'clubId' => $this->input->post('booking'),
      'agency' => $this->input->post('agency'),
      'agencyDate' => $this->input->post('agencyDate'),
      'passportNo' => $this->input->post('passNumber'),
      'visaNo' => $this->input->post('visaNumber'),
      'finishVisa' => $this->input->post('visaFin'),
      'coeNum'  => $this->input->post('coeNum'),
      'grossPrice' => $this->input->post('grossPrice'),
      'arrivalSched' => $this->input->post('arrivalSched'),
      'departSched' => $this->input->post('departSched'),
      'pdosStat' => $this->input->post('pdosStat'),
      'pdspStat' => $this->input->post('pdspStat'),
      'docSend' => $this->input->post('docSend'),
      'remarks' => $this->input->post('remarks'),
      'coeAppPh' => $this->input->post('coeAppPh'),
      'coeReleasePh' => $this->input->post('coeReleasePh'),
      'coeTranslationJp' => $this->input->post('coeTranslationJp'),
      'coeReleaseJp' => $this->input->post('coeReleaseJp'),
      'coeForward' => $this->input->post('coeForward'),
      'coeReceive' => $this->input->post('coeReceive'),
      'poeaApp' => $this->input->post('poeaApp'),
      'poeaRelease' => $this->input->post('poeaRelease')
    );
    $this->db->where('talentId', $talentId);
    $query = $this->db->update('talents', $data);

    $action = $this->input->post('talentName')." biodata updated.";
    $this->add_logs($action);
    // For status allocation
    $this->addStatus($talentId);
    //


  }

  function update_talent_where($talentId)
  {
    $this->db->where('talentId',$talentId);
    $data = array(
      'statusTalent' => $this->input->post('status'),
      'stat_remarks' => $this->input->post('stat_remarks'),
    );
    $this->db->update('talents', $data);
    $date = new DateTime();
    if($this->input->post('status') == 'inactive'){

      $this->db->where('talentId',$talentId);
      $status = $this->db->get('talents_status');

      if($status->num_rows() != 0){
        $data2 = array(
          'status' => 'inactive',
          'date' => $date->format('U = Y-m-d H:i:s'),
        );
        $this->db->where('talentId',$talentId);
        $this->db->update('talents_status',$data2);
      }else{
        $data2 = array(
          'talentId' => $talentId,
          'status' => 'inactive',
          'date' => $date->format('U = Y-m-d H:i:s'),
        );
        $this->db->insert('talents_status',$data2);
      }
    }else{
        $this->addStatus($talentId);
    }


    // $this->updateDates($talentId);
    //
    // if($q->num_rows() != 0){
    //   $this->db->where('talentId', $talentId);
    //   $query = $this->db->update('talents_status', $data);
    //
    // }else{
    //   $query = $this->db->insert('talents_status', $data);
    // }
    //
    // $this->db->where('talentId',$talentId);
    // $this->db->where('status',$this->input->post('status'));
    //
    // $q2 = $this->db->get('status_logs');
    // if($q2->num_rows() != 0){
    //   $this->db->where('talentId',$talentId);
    //   $this->db->where('status',$this->input->post('status'));
    //   $query = $this->db->update('status_logs', $data);
    // }else{
    //   $query = $this->db->insert('status_logs', $data);
    // }
    //
    // $action = "Status update on talent ID ".$talentId;
		// $this->add_logs($action);
  }

  function updateDates($talentId)
  {
    $status = $this->input->post('status');
    $date = $this->input->post('date-status');
    if($status == 'applying'){
      $data = array(
               'apply' => $date,
             );
      $this->db->where('talentId',$talentId);
      $this->db->update('talents', $data);
    }else if($status == 'processing'){
      $data = array(
               'processing' => $date,
             );
      $this->db->where('talentId',$talentId);
      $this->db->update('talents', $data);
    }else if($status == 'preparation'){
      $data = array(
               'docSend' => $date,
             );
      $this->db->where('talentId',$talentId);
      $this->db->update('talents', $data);
    }else if($status == 'immigration'){
      $data = array(
               'inJapan' => $date,
             );
      $this->db->where('talentId',$talentId);
      $this->db->update('talents', $data);
    }
  }

  function edit_talent_where($talentId)
  {
    $data = array(
      'talentName' => $this->input->post('edit-talent-name'),
      'firstName' => $this->input->post('edit-first-name'),
      'midName' => $this->input->post('edit-middle-name'),
      'lastName' => $this->input->post('edit-last-name'),
      'bdate' => $this->input->post('edit-birthdate'),
      'category' => $this->input->post('edit-category'),
      'productionId' => $this->input->post('edit-production'),
      'clubId' => $this->input->post('edit-club-name'),
      'processing' => $this->input->post('edit-processing'),
      'manager' => $this->input->post('edit-manager'),
      'docSend' => $this->input->post('edit-doc-send'),
      'apply' => $this->input->post('edit-apply'),
      'visaLicense' => $this->input->post('edit-vis-lic'),
      'inJapan' => $this->input->post('edit-in-japan'),
      'extension' => $this->input->post('edit-extension'),
      'finishVisa' => $this->input->post('edit-fin-visa'),
      'pob' => $this->input->post('edit-birthplace'),
      'passportNo' => $this->input->post('edit-pass-number'),
      'issu' => $this->input->post('edit-issu'),
      'passExp' => $this->input->post('edit-pass-exp')
      // 'pic1' => $image1['full_path'],
      // 'pic2' => $image2['full_path'],
      // 'pic3' => $image3['full_path'],
      // 'pic4' => $image4['full_path']
    );
    $this->db->where('talentId', $talentId);
    $query = $this->db->update('talents', $data);

    $action = "Edited biodata of ".$this->input->post('edit-talent-name');
		$this->add_logs($action);
  }

  function count_injapan(){
		$this->db->where('ts.status','injapan');
		$this->db->join('talents t', 't.talentId = ts.talentId', 'right');
		$this->db->select('count(*) as count, t.clubId as clubId');
		$query = $this->db->get('talents_status ts');
		return $query->row();
	}

	function count_coeapp(){
		$this->db->where('ts.status','coeapp');
		$this->db->join('talents t', 't.talentId = ts.talentId', 'right');
		$this->db->select('count(*) as count, t.clubId as clubId');
		$query = $this->db->get('talents_status ts');
		return $query->row();
	}

  function count_coerelease(){
		$this->db->where('ts.status','coerelease');
		$this->db->join('talents t', 't.talentId = ts.talentId', 'right');
		$this->db->select('count(*) as count, t.clubId as clubId');
		$query = $this->db->get('talents_status ts');
		return $query->row();
	}

	function count_docsend(){
		$this->db->where('ts.status','docsend');
		$this->db->join('talents t', 't.talentId = ts.talentId', 'right');
		$this->db->select('count(*) as count, t.clubId as clubId');
		$query = $this->db->get('talents_status ts');
		return $query->row();
	}

  function count_outjapan(){
		$this->db->where('ts.status','outjapan');
		$this->db->join('talents t', 't.talentId = ts.talentId', 'right');
		$this->db->select('count(*) as count, t.clubId as clubId');
		$query = $this->db->get('talents_status ts');
		return $query->row();
	}

  function count_hired(){
		$this->db->where('ts.status','hired');
		$this->db->join('talents t', 't.talentId = ts.talentId', 'right');
		$this->db->select('count(*) as count, t.clubId as clubId');
		$query = $this->db->get('talents_status ts');
		return $query->row();
	}

  function count_injapan_club(){
    $this->db->where('ts.status','injapan');
    $this->db->join('talents t', 't.talentId = ts.talentId', 'left');
    $this->db->select('count(*) as count, t.clubId as clubId');
    $this->db->group_by('t.clubId');
    $query = $this->db->get('talents_status ts');
    return $query->result();
  }

  function count_coeapp_club(){
    $this->db->where('ts.status','coeapp');
    $this->db->join('talents t', 't.talentId = ts.talentId', 'right');
    $this->db->select('count(*) as count, t.clubId as clubId');
    $this->db->group_by('t.clubId');
    $query = $this->db->get('talents_status ts');
    return $query->result();
  }

  function count_coerelease_club(){
    $this->db->where('ts.status','coerelease');
    $this->db->join('talents t', 't.talentId = ts.talentId', 'right');
    $this->db->select('count(*) as count, t.clubId as clubId');
    $this->db->group_by('t.clubId');
    $query = $this->db->get('talents_status ts');
    return $query->result();
  }

  function count_docsend_club(){
    $this->db->where('ts.status','docsend');
    $this->db->join('talents t', 't.talentId = ts.talentId', 'right');
    $this->db->select('count(*) as count, t.clubId as clubId');
    $this->db->group_by('t.clubId');
    $query = $this->db->get('talents_status ts');
    return $query->result();
  }

  function count_outjapan_club(){
    $this->db->where('ts.status','outjapan');
    $this->db->join('talents t', 't.talentId = ts.talentId', 'right');
    $this->db->select('count(*) as count, t.clubId as clubId');
    $this->db->group_by('t.clubId');
    $query = $this->db->get('talents_status ts');
    return $query->result();
  }

  function count_hired_club(){
    $this->db->where('ts.status','hired');
    $this->db->join('talents t', 't.talentId = ts.talentId', 'right');
    $this->db->select('count(*) as count, t.clubId as clubId');
    $this->db->group_by('t.clubId');
    $query = $this->db->get('talents_status ts');
    return $query->result();
  }


  function get_all_stat($stat){
    switch($stat){
      case 'outjapan':
            $this->db->where('ts.status','outjapan');
            break;

      case 'injapan':
            $this->db->where('ts.status','injapan');
            break;

      case 'coerelease':
            $this->db->where('ts.status','coerelease');
            break;

      case 'coeapp':
            $this->db->where('ts.status','coeapp');
            break;

      case 'docsend':
            $this->db->where('ts.status','docsend');
            break;

      case 'hired':
            $this->db->where('ts.status','hired');
            break;

      case 'cancelled':
            $this->db->where('t.statusTalent','inactive');
            break;
    }
    $this->db->join('talents t', 't.talentId = ts.talentId', 'right');
    $this->db->join('sales s', 's.talentId = t.talentId', 'left');
    $this->db->join('agency a', 'a.agencyId = t.agency', 'left');
    $this->db->join('production p', 'p.productionId = t.productionId', 'right');
    $this->db->join('clubs c', 'c.clubId = t.clubId', 'right');
    $this->db->order_by('ts.date','desc');
    $query = $this->db->get('talents_status ts');
    return $query->result();
  }

  function get_all_stat_club($stat,$club){
    switch($stat){
      case 'outjapan':
            $this->db->where('ts.status','outjapan');
            break;

      case 'injapan':
            $this->db->where('ts.status','injapan');
            break;

      case 'coeapp':
            $this->db->where('ts.status','coeapp');
            break;

      case 'coerelease':
            $this->db->where('ts.status','coerelease');
            break;

      case 'docsend':
            $this->db->where('ts.status','docsend');
            break;

      case 'hired':
            $this->db->where('ts.status','hired');
            break;
    }
    $this->db->where('t.clubId',$club);
    $this->db->join('talents t', 't.talentId = ts.talentId', 'right');
    $this->db->join('sales s', 's.talentId = t.talentId', 'left');
    $this->db->join('agency a', 'a.agencyId = t.agency', 'left');
    $this->db->join('production p', 'p.productionId = t.productionId', 'right');
    $this->db->join('clubs c', 'c.clubId = t.clubId', 'right');
    $this->db->order_by('ts.date','desc');
    $query = $this->db->get('talents_status ts');
    return $query->result();
  }

  function get_all_club($club){
    $this->db->where('t.clubId',$club);
    $this->db->join('talents t', 't.talentId = ts.talentId', 'right');
    $this->db->join('sales s', 's.talentId = t.talentId', 'left');
    $this->db->join('agency a', 'a.agencyId = t.agency', 'left');
    $this->db->join('production p', 'p.productionId = t.productionId', 'right');
    $this->db->join('clubs c', 'c.clubId = t.clubId', 'right');
    $this->db->order_by('docSend','desc');
    $query = $this->db->get('talents_status ts');
    return $query->result();
  }

  function get_all_prod($prod){
    $this->db->where('t.productionId',$prod);
    $this->db->join('talents t', 't.talentId = ts.talentId', 'right');
    $this->db->join('sales s', 's.talentId = t.talentId', 'left');
    $this->db->join('agency a', 'a.agencyId = t.agency', 'left');
    $this->db->join('production p', 'p.productionId = t.productionId', 'right');
    $this->db->join('clubs c', 'c.clubId = t.clubId', 'right');
    $this->db->order_by('docSend','desc');
    $query = $this->db->get('talents_status ts');
    return $query->result();
  }

  function activate_account($id,$status){
    if($status == 1){
      $status = 0;
    }else{
      $status = 1;
    }
    $data = array(
      'active' => $status,
    );

    $this->db->where('userId',$id);
    $query = $this->db->update('accounts', $data);

    $action = "Activate/Deactivate account of account ID ".$id;
		$this->add_logs($action);

  }

  function add_production(){
    $this->db->where('accountName',$this->input->post('username'));
    $this->db->select('count(*) as username');
    $found = $this->db->get('accounts');

    $this->db->where('productionName',$this->input->post('productionName'));
    $this->db->select('count(*) as prodName');
    $prodName = $this->db->get('production');

    if($found->row('username') != 0){
      $this->session->set_flashdata('username', 'Username already exists!');
    }else if($prodName->row('prodName') != 0){
        $this->session->set_flashdata('prodname', 'Production name already exists!');
    }else{
      $data = array(
        'productionName' => $this->input->post('productionName'),
      );
      $this->db->insert('production',$data);

      $this->db->where('productionName',$this->input->post('productionName'));
      $q = $this->db->get('production');
      $prodid = $q->row('productionId');

      $data2 = array(
        'accountName' => $this->input->post('username'),
        'username' => $this->input->post('username'),
        'password' => 'manpro2016',
        'productionId' => $prodid,
        'type' => 'p',

      );
      $this->db->insert('accounts',$data2);

      $action = "Added production and account for ".$this->input->post('productionName');
      $this->add_logs($action);
      $this->session->set_flashdata('success', 'Production have been succesfully added!');
    }


  }

  function update_production($prodid){
    $data = array(
      'productionName' => $this->input->post('productionName'),
    );
    $this->db->where('productionId',$prodid);
    $this->db->update('production',$data);

    $action = "Updated production ".$this->input->post('productionName');
    $this->add_logs($action);
  }

  function delete_production($prodid){
    $action = "Deleted production id ".$prodid;
    $this->add_logs($action);
    $this->db->where('productionId',$prodid);
    $this->db->delete('production');
  }

  function add_clubs(){

    $this->db->where('clubName',$this->input->post('clubName'));
    $this->db->select('count(*) as club');
    $club = $this->db->get('clubs');

    if($club->row('club') != 0){
      $this->session->set_flashdata('club', 'Club already exists!');
    }else{
      $data = array(
        'clubName' => $this->input->post('clubName'),
        'color' => $this->input->post('clubColor'),
        'productionId' => $this->input->post('production')
      );

      $this->db->insert('clubs',$data);

      $action = "Added club ".$this->input->post('clubName');
      $this->add_logs($action);
    }

  }

  function update_club($clubid){
    $data = array(
      'clubName' => $this->input->post('clubName'),
      'color' => $this->input->post('clubColor'),
      'productionId' => $this->input->post('production')
    );

    $this->db->where('clubId',$clubid);
    $this->db->update('clubs',$data);

    $action = "Updated club ".$this->input->post('clubName');
    $this->add_logs($action);
  }

  function delete_club($clubid){
    $action = "Deleted club id ".$clubid;
    $this->add_logs($action);
    $this->db->where('clubId',$clubid);
    $this->db->delete('clubs');
  }

  function add_accounts(){
    if($this->input->post('type') == 'p' || $this->input->post('type') == 't' ){
      $this->db->set('productionId', $this->input->post('production'));
    }
    $this->db->set('type', $this->input->post('type'));
    $this->db->set('accountName', $this->input->post('accountName'));
    $this->db->set('username', $this->input->post('username'));
    $this->db->set('password', $this->input->post('password'));
    $this->db->set('type', $this->input->post('type'));
    $this->db->insert('accounts');

    $action = "Added account for ".$this->input->post('accountName');
    $this->add_logs($action);
  }

  function update_account($userid){
    $data = array(
      'accountName' => $this->input->post('accountName'),
      'password' => $this->input->post('password'),
      'productionId' => $this->input->post('production')
    );

    $this->db->where('userId',$userid);
    $this->db->update('accounts',$data);

    $action = "Updated account ".$this->input->post('accountName');
    $this->add_logs($action);
  }

  function delete_account($userid){
    $action = "Deleted account id ".$userid;
    $this->add_logs($action);
    $this->db->where('userId',$userid);
    $this->db->delete('accounts');
  }

  function add_talents(){
    if(empty($this->input->post('manager'))){
      $manager = 'None';
    }else{
      $manager = $this->input->post('manager');
    }

    // productionID fix
    $this->db->where('clubId',$this->input->post('booking'));
    $q = $this->db->get('clubs');
    $prodid = $q->row('productionId');
    //

    // // For adding logs
    $action = "Added new talent ".$this->input->post('firstName')." ".$this->input->post('middleName')." ".$this->input->post('lastName');
    $this->add_logs($action);
    // //

    $data = array(
      'statusTalent'  => 'active',
      'stat_remarks'  => '',
      'talentName' => $this->input->post('talentName'),
      'firstName' => $this->input->post('firstName'),
      'midName' => $this->input->post('middleName'),
      'lastName' => $this->input->post('lastName'),
      'bdate' => $this->input->post('birthdate'),
      'pob' => $this->input->post('birthplace'),
      'category' => $this->input->post('category'),
      'productionId' => $prodid,
      'clubId' => $this->input->post('booking'),
      'agency' => $this->input->post('agency'),
      'agencyDate' => $this->input->post('agencyDate'),
      'passportNo' => $this->input->post('passNumber'),
      'visaNo' => $this->input->post('visaNumber'),
      'finishVisa' => $this->input->post('visaFin'),
      'coeNum'  => $this->input->post('coeNum'),
      'grossPrice' => $this->input->post('grossPrice'),
      'arrivalSched' => $this->input->post('arrivalSched'),
      'departSched' => $this->input->post('departSched'),
      'pdosStat' => $this->input->post('pdosStat'),
      'pdspStat' => $this->input->post('pdspStat'),
      'docSend' => $this->input->post('docSend'),
      'remarks' => $this->input->post('remarks'),
      'coeAppPh' => $this->input->post('coeAppPh'),
      'coeReleasePh' => $this->input->post('coeReleasePh'),
      'coeTranslationJp' => $this->input->post('coeTranslationJp'),
      'coeReleaseJp' => $this->input->post('coeReleaseJp'),
      'coeForward' => $this->input->post('coeForward'),
      'coeReceive' => $this->input->post('coeReceive'),
      'poeaApp' => $this->input->post('poeaApp'),
      'poeaRelease' => $this->input->post('poeaRelease')
    );

    $this->db->insert('talents',$data);

    return $this->db->insert_id();

  }

  function upload_talent_images_where($image1, $image2, $image3, $image4, $talentId){
    $data = array(
      'pic1' => $image1,
      'pic2' => $image2,
      'pic3' => $image3,
      'pic4' => $image4
    );
    $this->db->where('talentId', $talentId);
    $query = $this->db->update('talents', $data);
  }

  function add_logs($action){
    $data = array(
      'action' => $action,
      'accountName' => $this->session->userdata('accountName')
    );
    $this->db->insert('system_logs',$data);
  }

  private function _get_logs_query()
  {
    $this->db->from($this->logs);

    $i = 0;

    foreach ($this->column_search_logs as $item) // loop column
    {
      if($_POST['search']['value']) // if datatable send POST for search
      {

        if($i===0) // first loop
        {
          $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
          $this->db->like($item, $_POST['search']['value']);
        }
        else
        {
          $this->db->or_like($item, $_POST['search']['value']);
        }

        if(count($this->column_search_logs) - 1 == $i) //last loop
          $this->db->group_end(); //close bracket
      }
      $i++;
    }

    if(isset($_POST['order'])) // here order processing
    {
      $this->db->order_by($this->column_order_logs[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }
    else if(isset($this->order_logs))
    {
      $order = $this->order_logs;
      $this->db->order_by(key($order), $order[key($order)]);
    }
  }

  function get_logs(){
    $this->_get_logs_query();
    if($_POST['length'] != -1)
    $this->db->limit($_POST['length'], $_POST['start']);
    $query = $this->db->get();
    return $query->result();
  }

  function count_filtered_logs()
  {
    $this->_get_logs_query();
    $query = $this->db->get();
    return $query->num_rows();
  }

  function count_all_logs()
  {
    $this->db->from($this->logs);
    return $this->db->count_all_results();
  }

  function get_prod_where($prodId){
    $this->db->where('productionId',$prodId);
    $q = $this->db->get('production');
    return $q->row();
  }

  function get_club_where($clubid){
    $this->db->where('clubId',$clubid);
    $q = $this->db->get('clubs');
    return $q->row();
  }

  function get_user_where($userid){
    $this->db->where('userId',$userid);
    $q = $this->db->get('accounts');
    return $q->row();
  }

  function add_salary($id){
    $this->db->where('talentId',$id);
    $q1 = $this->db->get('talents');

    $talcom = (double)$this->input->post('netSalary') + (double)$this->input->post('commission');
    $total = (double)$this->input->post('grossSalary') - $talcom;

    $data = array(
      'productionId' => $q1->row('productionId'),
      'talentId' => $id,
      'gross' => $this->input->post('grossSalary'),
      'talentFee' => $this->input->post('netSalary'),
      'managerCom' => $this->input->post('commission'),
      'total' => $total
    );
    $this->db->insert('sales',$data);
  }



  function edit_talent_salary($id){
    $this->db->where('talentId',$id);
    $q1 = $this->db->get('talents');

    $talcom = (double)$this->input->post('netSalary') + (double)$this->input->post('commission');
    $total = (double)$this->input->post('grossSalary') - $talcom;

    $this->db->where('talentId',$id);
    $sal = $this->db->get('sales');

    if($sal->num_rows() != 0){
      $data = array(
        'productionId' => $q1->row('productionId'),
        'gross' => $this->input->post('grossSalary'),
        'talentFee' => $this->input->post('netSalary'),
        'managerCom' => $this->input->post('commission'),
        'total' => $total
      );
      $this->db->where('talentId',$id);
      $query = $this->db->update('sales', $data);
    }else{
      $data = array(
        'productionId' => $q1->row('productionId'),
        'talentId' => $id,
        'gross' => $this->input->post('grossSalary'),
        'talentFee' => $this->input->post('netSalary'),
        'managerCom' => $this->input->post('commission'),
        'total' => $total
      );
      $this->db->insert('sales',$data);
    }

  }

  function addStatus($id){

    $this->db->where('talentId',$id);
    $q1 = $this->db->get('talents');

    $this->db->where('talentId',$id);
    $status = $this->db->get('talents_status');



    if($status->num_rows() != 0){
      if($q1->row('arrivalSched') != '0000-00-00'){
        $data = array(
          'status'    =>  'outjapan',
          'date'      =>  $q1->row('arrivalSched')
        );
        $this->db->where('talentId',$status->row('talentId'));
        $this->db->update('talents_status',$data);
      }else if($q1->row('departSched') != '0000-00-00'){
        $data = array(
          'status'    =>  'injapan',
          'date'      =>  $q1->row('departSched')
        );
        $this->db->where('talentId',$status->row('talentId'));
        $this->db->update('talents_status',$data);
      }else if($q1->row('coeReleasePh') != '0000-00-00'){
        $data = array(
          'status'    =>  'coerelease',
          'date'      =>  $q1->row('coeReleasePh')
        );
        $this->db->where('talentId',$status->row('talentId'));
        $this->db->update('talents_status',$data);
      }else if($q1->row('coeAppPh') != '0000-00-00'){
        $data = array(
          'status'    =>  'coeapp',
          'date'      =>  $q1->row('coeAppPh')
        );
        $this->db->where('talentId',$status->row('talentId'));
        $this->db->update('talents_status',$data);
      }else if($q1->row('docSend') != '0000-00-00'){
        $data = array(
          'status'    =>  'docsend',
          'date'      =>  $q1->row('docSend')
        );
        $this->db->where('talentId',$status->row('talentId'));
        $this->db->update('talents_status',$data);
      }else if($q1->row('agencyDate') != '0000-00-00'){
        $data = array(
          'status'    =>  'hired',
          'date'      =>  $q1->row('agencyDate')
        );
        $this->db->where('talentId',$status->row('talentId'));
        $this->db->update('talents_status',$data);
      }
    }else if($status->num_rows() == 0){
      if($q1->row('arrivalSched') != '0000-00-00'){
        $data = array(
          'talentId'  =>  $q1->row('talentId'),
          'status'    =>  'outjapan',
          'date'      =>  $q1->row('arrivalSched')
        );
        $this->db->insert('talents_status',$data);
      }else if($q1->row('departSched') != '0000-00-00'){
        $data = array(
          'talentId'  =>  $q1->row('talentId'),
          'status'    =>  'injapan',
          'date'      =>  $q1->row('departSched')
        );
        $this->db->insert('talents_status',$data);
      }else if($q1->row('coeReleaseJp') != '0000-00-00'){
        $data = array(
          'talentId'  =>  $q1->row('talentId'),
          'status'    =>  'injapan',
          'date'      =>  $q1->row('coeReleaseJp')
        );
        $this->db->insert('talents_status',$data);
      }else if($q1->row('coeAppPh') != '0000-00-00'){
        $data = array(
          'talentId'  =>  $q1->row('talentId'),
          'status'    =>  'coeapp',
          'date'      =>  $q1->row('coeAppPh')
        );
        $this->db->insert('talents_status',$data);
      }else if($q1->row('docSend') != '0000-00-00'){
        $data = array(
          'talentId'  =>  $q1->row('talentId'),
          'status'    =>  'docsend',
          'date'      =>  $q1->row('docSend')
        );
        $this->db->insert('talents_status',$data);
      }else if($q1->row('agencyDate') != '0000-00-00'){
        $data = array(
          'talentId'  =>  $q1->row('talentId'),
          'status'    =>  'hired',
          'date'      =>  $q1->row('agencyDate')
        );
        $this->db->insert('talents_status',$data);
      }

    }
  }

  function count_talents(){
    return  $this->db->count_all_results('talents');
  }

  function count_cancel(){
    $this->db->where('statusTalent','inactive');
		$this->db->select('count(*) as count');
		$query = $this->db->get('talents');
		return $query->row();
  }

  function get_agency(){
    $q = $this->db->get('agency');
    return $q->result();
  }

  function add_agency(){
    $data = array(
      'agencyName' => $this->input->post('agencyName'),
      'address' => $this->input->post('address')
    );
    $this->db->insert('agency',$data);
    $this->session->set_flashdata('success', 'Agency have been succesfully added!');
  }

  function delete_agency($id){
    $action = "Deleted agency id ".$id;
    $this->add_logs($action);
    $this->db->where('agencyId',$id);
    $this->db->delete('agency');
  }

}
