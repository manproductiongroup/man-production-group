$(document).on("click", "#viewTalentModal", function () {
      $('#viewTalent').openModal();
      var talentId = $(this).data('id');
      $(".modal-content #view-talent-id").val( talentId );

      $.ajax({
        type: "POST",
        url: "<?php echo site_url();?>administrator/view_talent/"+talentId,
        dataType: "JSON",
        success:function(data){
          $('#viewTalent').openModal();
          $(".modal-content #talent-name").val(data[0].talentName);
          $(".modal-content #category").val(data[0].category);
        },
      });
});
