<?php if($this->session->flashdata('login')){?>
    <script type="text/javascript">
      $(document).ready(function(){
          swal({
            title: "Welcome!",
            text:  '<?php echo $this->session->flashdata('login');?>',
            imageUrl: '<?php echo site_url('imgs/admin.png')?>',
            timer: 2000,
            showConfirmButton: false
          });
      });
    </script>
<?php }?>

<br>

<div class="right-align" id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'en', includedLanguages: 'en,ja,tl', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>


<div class="section no-pad-bot" id="index-banner">
  <div class="container">
    <br><br>
    <h3 class="header center cyan-text text-darken-3">Global Status Count</h3>
    <br><br>
  </div>
</div>

<div class="row">

  <div class="col s12 m6 l3">
    <a href="<?php echo site_url('administrator/get_all_stat/hired')?>">
      <div class="card hoverable ">
          <div class="card-content blue white-text">
              <h5 class="card-stats-title center"><i class="mdi-action-trending-up"></i>NEWLY HIRED</h5>
              <h4 class="card-stats-number center"><?php echo $hired->count?></h4>
              <p class="card-stats-compare center"><span class="blue-grey-text text-lighten-5"></span>
              </p>
          </div>
          <div class="card-action blue darken-2">
              <div id="profit-tristate" class="center-align"><canvas width="227" height="25" style="display: inline-block; width: 227px; height: 25px; vertical-align: top;"></canvas></div>
          </div>
      </div>
    </a>
  </div>

  <div class="col s12 m6 l3">
    <a href="<?php echo site_url('administrator/get_all_stat/docsend')?>">
      <div class="card hoverable ">
          <div class="card-content blue-grey white-text">
              <h5 class="card-stats-title center"><i class="mdi-action-trending-up"></i>DOCUMENTS FORWARDED</h5>
              <h4 class="card-stats-number center"><?php echo $docsend->count?></h4>
              <p class="card-stats-compare center"><span class="blue-grey-text text-lighten-5"></span>
              </p>
          </div>
          <div class="card-action blue-grey darken-2">
              <div id="profit-tristate" class="center-align"><canvas width="227" height="25" style="display: inline-block; width: 227px; height: 25px; vertical-align: top;"></canvas></div>
          </div>
      </div>
    </a>
  </div>

  <div class="col s12 m6 l3">
    <a href="<?php echo site_url('administrator/get_all_stat/coeapp')?>">
      <div class="card hoverable ">
          <div class="card-content purple white-text">
              <h5 class="card-stats-title center"><i class="mdi-editor-attach-money"></i>COE APPLIED</h5>
              <h4 class="card-stats-number center"><?php echo $coeapp->count?></h4>
              <p class="card-stats-compare center"><span class="purple-text text-lighten-5"></span>
              </p>
          </div>
          <div class="card-action purple darken-2">
              <div id="sales-compositebar" class="center-align"><canvas width="214" height="25" style="display: inline-block; width: 214px; height: 25px; vertical-align: top;"></canvas></div>

          </div>
      </div>
    </a>
  </div>

  <div class="col s12 m6 l3">
    <a href="<?php echo site_url('administrator/get_all_stat/coerelease')?>">
      <div class="card hoverable ">
          <div class="card-content deep-purple white-text">
              <h5 class="card-stats-title center"><i class="mdi-editor-insert-drive-file"></i>COE RELEASED</h5>
              <h4 class="card-stats-number center"><?php echo $coerelease->count?></h4>
              <p class="card-stats-compare center"><span class="deep-purple-text text-lighten-5"></span>
              </p>
          </div>
          <div class="card-action  deep-purple darken-2">
              <div id="invoice-line" class="center-align"><canvas style="display: inline-block; width: 241px; height: 25px; vertical-align: top;" width="241" height="25"></canvas></div>
          </div>
      </div>
    </a>
  </div>

</div>

<div class="row">

  <div class="col s12 m6 l3">
    <a href="<?php echo site_url('administrator/get_all_stat/injapan')?>">
      <div class="card hoverable ">
          <div class="card-content  green white-text">
              <h5 class="card-stats-title center"><i class="mdi-social-group-add"></i>IN JAPAN</h5>
              <h4 class="card-stats-number center"><?php echo $injapan->count?></h4>
              <p class="card-stats-compare center"><span class="green-text text-lighten-5"></span>
              </p>
          </div>
          <div class="card-action  green darken-2">
              <div id="clients-bar" class="center-align"><canvas width="227" height="25" style="display: inline-block; width: 227px; height: 25px; vertical-align: top;"></canvas></div>
          </div>
      </div>
    </a>
  </div>

  <div class="col s12 m6 l3">
    <a href="<?php echo site_url('administrator/get_all_stat/outjapan')?>">
      <div class="card hoverable ">
          <div class="card-content orange white-text">
              <h5 class="card-stats-title center"><i class="mdi-action-trending-up"></i>OUT JAPAN</h5>
              <h4 class="card-stats-number center"><?php echo $outjapan->count?></h4>
              <p class="card-stats-compare center"><span class="blue-grey-text text-lighten-5"></span>
              </p>
          </div>
          <div class="card-action orange darken-2">
              <div id="profit-tristate" class="center-align"><canvas width="227" height="25" style="display: inline-block; width: 227px; height: 25px; vertical-align: top;"></canvas></div>
          </div>
      </div>
    </a>
  </div>

  <div class="col s12 m6 l3">
    <a href="<?php echo site_url('administrator/get_all_stat/cancelled')?>">
      <div class="card hoverable ">
          <div class="card-content red white-text">
              <h5 class="card-stats-title center"><i class="mdi-editor-attach-money"></i>CANCELLED</h5>
              <h4 class="card-stats-number center"><?php echo $cancel->count?></h4>
              <p class="card-stats-compare center"><span class="purple-text text-lighten-5"></span>
              </p>
          </div>
          <div class="card-action red darken-2">
              <div id="sales-compositebar" class="center-align"><canvas width="214" height="25" style="display: inline-block; width: 214px; height: 25px; vertical-align: top;"></canvas></div>

          </div>
      </div>
    </a>
  </div>

  <div class="col s12 m6 l3">
    <a href="<?php echo site_url('administrator/get_all_stat/talents')?>">
      <div class="card hoverable ">
          <div class="card-content cyan white-text">
              <h5 class="card-stats-title center"><i class="mdi-editor-insert-drive-file"></i>TALENTS</h5>
              <h4 class="card-stats-number center"><?php echo $talentsCount?></h4>
              <p class="card-stats-compare center"><span class="deep-purple-text text-lighten-5"></span>
              </p>
          </div>
          <div class="card-action  cyan darken-2">
              <div id="invoice-line" class="center-align"><canvas style="display: inline-block; width: 241px; height: 25px; vertical-align: top;" width="241" height="25"></canvas></div>
          </div>
      </div>
    </a>
  </div>

</div>

<div class="section no-pad-bot" id="index-banner">
  <div class="container">
    <br><br>
    <h3 class="header center cyan-text text-darken-3">Production Status Count</h3>
    <br><br>
  </div>
</div>

<div class="container">
  <?php foreach($prod as $p) {?>
    <h5 class="header center"><a href="<?= site_url('administrator/get_all_prod/'.$p->productionName.'/'.$p->productionId)?>"><?php echo $p->productionName?></a></h5>
  <table class="striped table responsive-table" style="margin-bottom:100px">
    <thead>
      <tr>
          <th class="center" data-field="">Club Name</th>
          <th class="center" data-field="">Newly Hired</th>
          <th class="center" data-field="">Documents Forwarded</th>
          <th class="center" data-field="">COE Apply</th>
          <th class="center" data-field="">COE Release</th>
          <th class="center" data-field="">In Japan</th>
      </tr>
    </thead>

    <tbody>
    <?php foreach($clubs as $r){?>
      <?php if($r->productionId == $p->productionId):?>
      <tr>
        <td class="center"><a href="<?= site_url('administrator/get_all_club/'.$r->clubName.'/'.$r->clubId)?>"><?php echo $r -> clubName; ?></a></td>

        <td class="center"><a href="<?php echo site_url('administrator/get_all_stat_club/hired/'.$r->clubId)?>">
        <?php foreach($hiredClub as $jap){?>
        <?php if($r->clubId == $jap->clubId):?>
        <?php echo $jap->count?>
        <?php endif ?>
        <?php }?>
        </a></td>

        <td class="center"><a href="<?php echo site_url('administrator/get_all_stat_club/docsend/'.$r->clubId)?>">
        <?php foreach($docsendClub as $jap){?>
        <?php if($r->clubId == $jap->clubId):?>
        <?php echo $jap->count?>
        <?php endif ?>
        <?php }?>
        </a></td>

        <td class="center"><a href="<?php echo site_url('administrator/get_all_stat_club/coeapp/'.$r->clubId)?>">
        <?php foreach($coeappClub as $jap){?>
        <?php if($r->clubId == $jap->clubId):?>
        <?php echo $jap->count?>
        <?php endif ?>
        <?php }?>
        </a></td>

        <td class="center"><a href="<?php echo site_url('administrator/get_all_stat_club/coerelease/'.$r->clubId)?>">
        <?php foreach($coereleaseClub as $jap){?>
        <?php if($r->clubId == $jap->clubId):?>
        <?php echo $jap->count?>
        <?php endif ?>
        <?php }?>
        </a></td>

        <td class="center"><a href="<?php echo site_url('administrator/get_all_stat_club/injapan/'.$r->clubId)?>">
        <?php foreach($injapanClub as $jap){?>
        <?php if($r->clubId == $jap->clubId):?>
        <?php echo $jap->count?>
        <?php endif ?>
        <?php }?>
        </a></td>

      </tr>
      <?php else: ?>
      <?php endif ?>
    <?php }?>
    </tbody>
  </table>
  <?php }?>
</div>
