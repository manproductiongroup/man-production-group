<?php
	$head = $this->uri->segment('1');
  $head2 = $this->uri->segment('2');
	if($head == 'third' && $head2 == '')
	{
?>

<!-- Dropdown Structure -->
<ul id="navbar_dropdown" class="dropdown-content">
  <li><a href="<?php echo site_url('third/admin_profile');?>"><i class="material-icons left">perm_identity</i>My Profile</a></li>
  <li class="divider"></li>
  <li><a href="<?php echo site_url('third/settings');?>"><i class="material-icons left">settings</i>Account Settings</a></li>
  <li class="divider"></li>
  <li><a href="<?php echo site_url('third/logout');?>"><i class="material-icons left">power_settings_new</i>Logout</a></li>
</ul>
<nav>
  <div class="nav-wrapper indigo darken-4">
    <a href="#!" class="brand-logo" style="margin-left:20px;">Man Group Production</a>
    <a href="#" data-activates="mobile-demo" class="button-collapse" style="margin-left:10px;"><i class="material-icons">menu</i></a>
    <ul class="right hide-on-med-and-down">
      <li class="active"><a href="<?php echo site_url('third')?>">DASHBOARD</a></li>
      <li class=""><a href="<?php echo site_url('third/clubs')?>">CLUBS</a></li>
      <li>
        <!-- Dropdown Trigger -->
        <a class="dropdown-button center-align" href="#" data-activates="navbar_dropdown" style="width: 220px;">
          <?php echo $accountName; ?>
          <i class="material-icons right">keyboard_arrow_down</i>
        </a>
      </li>
    </ul>
    <ul class="side-nav collapsible collapsible-accordion" id="mobile-demo">
      <li class="active"><a href="<?php echo site_url('third')?>">DASHBOARD</a></li>
      <li class=""><a href="<?php echo site_url('third/clubs')?>">CLUBS</a></li>
      <li class="bold active">
        <a class="collapsible-header waves-effect waes-teal">
          <?php echo $accountName; ?>
          <i class="material-icons right">keyboard_arrow_down</i>
        </a>
        <div class="collapsible-body">
          <ul>
            <li><a href="<?php echo site_url('third/admin_profile')?>"><i class="material-icons left">perm_identity</i>My Profile</a></li>
            <li><a href="<?php echo site_url('third/settings')?>"><i class="material-icons left">settings</i>Account Settings</a></li>
            <li><a href="<?php echo site_url('third/logout')?>"><i class="material-icons left">power_settings_new</i>Logout</a></li>
          </ul>
        </div>
      </li>
    </ul>
  </div>
</nav>
<?php } ?>

<?php
	$head = $this->uri->segment('1');
  $clubs = $this->uri->segment('2');
	if($head == 'third' && $clubs == 'clubs')
	{
?>

<!-- Dropdown Structure -->
<ul id="navbar_dropdown" class="dropdown-content">
  <li><a href="<?php echo site_url('third/admin_profile');?>"><i class="material-icons left">perm_identity</i>My Profile</a></li>
  <li class="divider"></li>
  <li><a href="<?php echo site_url('third/settings');?>"><i class="material-icons left">settings</i>Account Settings</a></li>
  <li class="divider"></li>
  <li><a href="<?php echo site_url('third/logout');?>"><i class="material-icons left">power_settings_new</i>Logout</a></li>
</ul>
<nav>
  <div class="nav-wrapper indigo darken-4">
    <a href="#!" class="brand-logo" style="margin-left:20px;">Man Group Production</a>
    <a href="#" data-activates="mobile-demo" class="button-collapse" style="margin-left:10px;"><i class="material-icons">menu</i></a>
    <ul class="right hide-on-med-and-down">
      <li class=""><a href="<?php echo site_url('third')?>">DASHBOARD</a></li>
      <li class="active"><a href="<?php echo site_url('third/clubs')?>">CLUBS</a></li>
      <li>
        <!-- Dropdown Trigger -->
        <a class="dropdown-button center-align" href="#" data-activates="navbar_dropdown" style="width: 220px;">
          <?php echo $accountName; ?>
          <i class="material-icons right">keyboard_arrow_down</i>
        </a>
      </li>
    </ul>
    <ul class="side-nav collapsible collapsible-accordion" id="mobile-demo">
      <li class=""><a href="<?php echo site_url('third')?>">DASHBOARD</a></li>
      <li class="active"><a href="<?php echo site_url('third/clubs')?>">CLUBS</a></li>
      <li class="bold active">
        <a class="collapsible-header waves-effect waes-teal">
          <?php echo $accountName; ?>
          <i class="material-icons right">keyboard_arrow_down</i>
        </a>
        <div class="collapsible-body">
          <ul>
            <li><a href="<?php echo site_url('third/admin_profile')?>"><i class="material-icons left">perm_identity</i>My Profile</a></li>
            <li><a href="<?php echo site_url('third/settings')?>"><i class="material-icons left">settings</i>Account Settings</a></li>
            <li><a href="<?php echo site_url('third/logout')?>"><i class="material-icons left">power_settings_new</i>Logout</a></li>
          </ul>
        </div>
      </li>
    </ul>
  </div>
</nav>

<?php }?>


<?php
	$head = $this->uri->segment('1');
  $charts = $this->uri->segment('2');
	if($head == 'third' && $charts == 'get_status')
	{
?>

<!-- Dropdown Structure -->
<ul id="navbar_dropdown" class="dropdown-content">
  <li><a href="<?php echo site_url('third/admin_profile');?>"><i class="material-icons left">perm_identity</i>My Profile</a></li>
  <li class="divider"></li>
  <li><a href="<?php echo site_url('third/settings');?>"><i class="material-icons left">settings</i>Account Settings</a></li>
  <li class="divider"></li>
  <li><a href="<?php echo site_url('third/logout');?>"><i class="material-icons left">power_settings_new</i>Logout</a></li>
</ul>
<nav>
  <div class="nav-wrapper indigo darken-4">
    <a href="#!" class="brand-logo" style="margin-left:20px;">Man Group Production</a>
    <a href="#" data-activates="mobile-demo" class="button-collapse" style="margin-left:10px;"><i class="material-icons">menu</i></a>
    <ul class="right hide-on-med-and-down">
      <li class=""><a href="<?php echo site_url('third')?>">DASHBOARD</a></li>
      <li class="active"><a href="<?php echo site_url('third/clubs')?>">CLUBS</a></li>
      <li>
        <!-- Dropdown Trigger -->
        <a class="dropdown-button center-align" href="#" data-activates="navbar_dropdown" style="width: 220px;">
          <?php echo $accountName; ?>
          <i class="material-icons right">keyboard_arrow_down</i>
        </a>
      </li>
    </ul>
    <ul class="side-nav collapsible collapsible-accordion" id="mobile-demo">
      <li class=""><a href="<?php echo site_url('third')?>">DASHBOARD</a></li>
      <li class="active"><a href="<?php echo site_url('third/clubs')?>">CLUBS</a></li>
      <li class="bold active">
        <a class="collapsible-header waves-effect waes-teal">
          <?php echo $accountName; ?>
          <i class="material-icons right">keyboard_arrow_down</i>
        </a>
        <div class="collapsible-body">
          <ul>
            <li><a href="<?php echo site_url('third/admin_profile')?>"><i class="material-icons left">perm_identity</i>My Profile</a></li>
            <li><a href="<?php echo site_url('third/settings')?>"><i class="material-icons left">settings</i>Account Settings</a></li>
            <li><a href="<?php echo site_url('third/logout')?>"><i class="material-icons left">power_settings_new</i>Logout</a></li>
          </ul>
        </div>
      </li>
    </ul>
  </div>
</nav>

<?php }?>

<?php
	$head = $this->uri->segment('1');
  $charts = $this->uri->segment('2');
	if($head == 'third' && $charts == 'get_all_stat_club')
	{
?>

<!-- Dropdown Structure -->
<ul id="navbar_dropdown" class="dropdown-content">
  <li><a href="<?php echo site_url('third/admin_profile');?>"><i class="material-icons left">perm_identity</i>My Profile</a></li>
  <li class="divider"></li>
  <li><a href="<?php echo site_url('third/settings');?>"><i class="material-icons left">settings</i>Account Settings</a></li>
  <li class="divider"></li>
  <li><a href="<?php echo site_url('third/logout');?>"><i class="material-icons left">power_settings_new</i>Logout</a></li>
</ul>
<nav>
  <div class="nav-wrapper indigo darken-4">
    <a href="#!" class="brand-logo" style="margin-left:20px;">Man Group Production</a>
    <a href="#" data-activates="mobile-demo" class="button-collapse" style="margin-left:10px;"><i class="material-icons">menu</i></a>
    <ul class="right hide-on-med-and-down">
      <li class="active"><a href="<?php echo site_url('third')?>">DASHBOARD</a></li>
      <li class=""><a href="<?php echo site_url('third/clubs')?>">CLUBS</a></li>
      <li>
        <!-- Dropdown Trigger -->
        <a class="dropdown-button center-align" href="#" data-activates="navbar_dropdown" style="width: 220px;">
          <?php echo $accountName; ?>
          <i class="material-icons right">keyboard_arrow_down</i>
        </a>
      </li>
    </ul>
    <ul class="side-nav collapsible collapsible-accordion" id="mobile-demo">
      <li class="active"><a href="<?php echo site_url('third')?>">DASHBOARD</a></li>
      <li class=""><a href="<?php echo site_url('third/clubs')?>">CLUBS</a></li>
      <li class="bold active">
        <a class="collapsible-header waves-effect waes-teal">
          <?php echo $accountName; ?>
          <i class="material-icons right">keyboard_arrow_down</i>
        </a>
        <div class="collapsible-body">
          <ul>
            <li><a href="<?php echo site_url('third/admin_profile')?>"><i class="material-icons left">perm_identity</i>My Profile</a></li>
            <li><a href="<?php echo site_url('third/settings')?>"><i class="material-icons left">settings</i>Account Settings</a></li>
            <li><a href="<?php echo site_url('third/logout')?>"><i class="material-icons left">power_settings_new</i>Logout</a></li>
          </ul>
        </div>
      </li>
    </ul>
  </div>
</nav>

<?php }?>

<?php
	$head = $this->uri->segment('1');
  $charts = $this->uri->segment('2');
	if($head == 'third' && $charts == 'get_all_club')
	{
?>

<!-- Dropdown Structure -->
<ul id="navbar_dropdown" class="dropdown-content">
  <li><a href="<?php echo site_url('third/admin_profile');?>"><i class="material-icons left">perm_identity</i>My Profile</a></li>
  <li class="divider"></li>
  <li><a href="<?php echo site_url('third/settings');?>"><i class="material-icons left">settings</i>Account Settings</a></li>
  <li class="divider"></li>
  <li><a href="<?php echo site_url('third/logout');?>"><i class="material-icons left">power_settings_new</i>Logout</a></li>
</ul>
<nav>
  <div class="nav-wrapper indigo darken-4">
    <a href="#!" class="brand-logo" style="margin-left:20px;">Man Group Production</a>
    <a href="#" data-activates="mobile-demo" class="button-collapse" style="margin-left:10px;"><i class="material-icons">menu</i></a>
    <ul class="right hide-on-med-and-down">
      <li class="active"><a href="<?php echo site_url('third')?>">DASHBOARD</a></li>
      <li class=""><a href="<?php echo site_url('third/clubs')?>">CLUBS</a></li>
      <li>
        <!-- Dropdown Trigger -->
        <a class="dropdown-button center-align" href="#" data-activates="navbar_dropdown" style="width: 220px;">
          <?php echo $accountName; ?>
          <i class="material-icons right">keyboard_arrow_down</i>
        </a>
      </li>
    </ul>
    <ul class="side-nav collapsible collapsible-accordion" id="mobile-demo">
      <li class="active"><a href="<?php echo site_url('third')?>">DASHBOARD</a></li>
      <li class=""><a href="<?php echo site_url('third/clubs')?>">CLUBS</a></li>
      <li class="bold active">
        <a class="collapsible-header waves-effect waes-teal">
          <?php echo $accountName; ?>
          <i class="material-icons right">keyboard_arrow_down</i>
        </a>
        <div class="collapsible-body">
          <ul>
            <li><a href="<?php echo site_url('third/admin_profile')?>"><i class="material-icons left">perm_identity</i>My Profile</a></li>
            <li><a href="<?php echo site_url('third/settings')?>"><i class="material-icons left">settings</i>Account Settings</a></li>
            <li><a href="<?php echo site_url('third/logout')?>"><i class="material-icons left">power_settings_new</i>Logout</a></li>
          </ul>
        </div>
      </li>
    </ul>
  </div>
</nav>

<?php }?>
