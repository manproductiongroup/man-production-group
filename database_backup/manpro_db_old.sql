-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 01, 2016 at 05:52 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `manpro_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE `accounts` (
  `userId` int(11) NOT NULL,
  `accountName` varchar(50) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `productionId` int(11) NOT NULL DEFAULT '0',
  `type` varchar(1) NOT NULL,
  `active` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`userId`, `accountName`, `username`, `password`, `productionId`, `type`, `active`) VALUES
(1, 'Rom', 'try', '5431', 1, 'p', 1),
(2, 'Admin', 'admin', 'admin', 0, 'a', 1),
(3, 'Herb', 'hebi', '5431', 2, 'p', 1),
(4, 'Green', 'green', '1234', 3, 'p', 1),
(5, 'test', 'test', 'test', 0, 'a', 1);

-- --------------------------------------------------------

--
-- Table structure for table `clubs`
--

CREATE TABLE `clubs` (
  `clubId` int(11) NOT NULL,
  `clubName` varchar(50) NOT NULL,
  `capacity` int(11) NOT NULL,
  `productionId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `clubs`
--

INSERT INTO `clubs` (`clubId`, `clubName`, `capacity`, `productionId`) VALUES
(1, 'romantiko', 10, 1),
(2, 'stella', 10, 2),
(3, 'porno graffiti', 10, 1),
(4, 'Rezero', 15, 2),
(6, 'Kristal', 20, 1),
(7, 'Green', 10, 3),
(8, 'New Club', 10, 3);

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs` (
  `logId` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `action` varchar(100) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pricing`
--

CREATE TABLE `pricing` (
  `priceId` int(11) NOT NULL,
  `talentId` int(11) NOT NULL,
  `origPrice` double NOT NULL,
  `sellingPrice` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `production`
--

CREATE TABLE `production` (
  `productionId` int(11) NOT NULL,
  `productionName` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `production`
--

INSERT INTO `production` (`productionId`, `productionName`) VALUES
(1, 'JP'),
(2, 'Hebi'),
(3, 'Green Cross');

-- --------------------------------------------------------

--
-- Table structure for table `status_logs`
--

CREATE TABLE `status_logs` (
  `statusId` int(11) NOT NULL,
  `talentId` int(11) NOT NULL,
  `status` enum('applying','processing','preparation','immigaration') NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `status_logs`
--

INSERT INTO `status_logs` (`statusId`, `talentId`, `status`, `date`) VALUES
(1, 1, 'applying', '2016-07-03'),
(2, 2, 'applying', '2016-07-06'),
(3, 3, 'applying', '2016-07-19'),
(4, 1, 'processing', '2016-08-10'),
(5, 3, '', '2016-08-31');

-- --------------------------------------------------------

--
-- Table structure for table `system_logs`
--

CREATE TABLE `system_logs` (
  `logID` int(11) NOT NULL,
  `action` varchar(50) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `accountName` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `system_logs`
--

INSERT INTO `system_logs` (`logID`, `action`, `date`, `accountName`) VALUES
(1, 'Admin logged in.', '2016-09-01 12:43:57', 'Admin'),
(2, 'Rom logged in.', '2016-09-01 12:46:29', 'Rom'),
(3, 'Rom logged out.', '2016-09-01 12:48:13', 'Rom'),
(4, 'Admin logged in.', '2016-09-01 12:49:18', 'Admin'),
(5, 'Admin logged out.', '2016-09-01 12:49:22', 'Admin'),
(6, 'Admin logged in.', '2016-09-01 13:11:28', 'Admin'),
(7, 'Admin logged out.', '2016-09-01 14:39:24', 'Admin'),
(8, 'Admin logged in.', '2016-09-01 14:39:27', 'Admin'),
(9, 'Admin logged in.', '2016-09-01 14:41:58', 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `talents`
--

CREATE TABLE `talents` (
  `talentId` int(11) NOT NULL,
  `lastName` varchar(30) NOT NULL,
  `firstName` varchar(50) NOT NULL,
  `midName` varchar(30) NOT NULL,
  `bdate` date NOT NULL,
  `productionId` int(11) NOT NULL,
  `clubId` int(11) NOT NULL,
  `processing` varchar(50) NOT NULL,
  `manager` varchar(100) NOT NULL,
  `docSend` date NOT NULL,
  `apply` date NOT NULL,
  `visaLicense` date NOT NULL,
  `inJapan` date NOT NULL,
  `extension` date NOT NULL,
  `finishVisa` date NOT NULL,
  `talentName` varchar(50) NOT NULL,
  `category` varchar(20) NOT NULL,
  `pob` varchar(50) NOT NULL,
  `passportNo` varchar(30) NOT NULL,
  `issu` varchar(30) NOT NULL,
  `passExp` date NOT NULL,
  `pic1` varchar(50) NOT NULL,
  `pic2` varchar(50) NOT NULL,
  `pic3` varchar(50) NOT NULL,
  `pic4` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `talents`
--

INSERT INTO `talents` (`talentId`, `lastName`, `firstName`, `midName`, `bdate`, `productionId`, `clubId`, `processing`, `manager`, `docSend`, `apply`, `visaLicense`, `inJapan`, `extension`, `finishVisa`, `talentName`, `category`, `pob`, `passportNo`, `issu`, `passExp`, `pic1`, `pic2`, `pic3`, `pic4`) VALUES
(1, 'sanchez', 'erica', 'santos', '2016-06-02', 1, 1, '', '', '2016-06-16', '2016-06-23', '2016-06-08', '2016-06-22', '2016-06-27', '2016-06-17', 'silver', '', '', '0123456789', '', '2016-06-07', '', '', '', ''),
(2, 'Santos', 'Maria', 'Lopez', '0000-00-00', 2, 2, '', '', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '', '', '', '', '', '0000-00-00', '', '', '', ''),
(3, 'Malvar', 'Barbara', 'De Guzman', '1989-07-11', 2, 2, '2016/07/06', 'Bart', '2016-07-21', '2016-07-11', '2016-07-06', '2016-07-26', '2016-07-21', '2016-07-26', 'Scarlet', 'dancer-PRO', 'Manila', '123456789', '1111111111', '2016-07-27', 'C:/xampp/htdocs/manpro/uploads/talents/Scarlet/ima', 'C:/xampp/htdocs/manpro/uploads/talents/Scarlet/b9f', 'C:/xampp/htdocs/manpro/uploads/talents/Scarlet/136', 'C:/xampp/htdocs/manpro/uploads/talents/Scarlet/131'),
(4, 'test3', 'test1 ', 'test2 ', '2004-02-01', 2, 1, '', 'twete', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'haha', 'Dancer PRO', 'manila', '123654456', '121123132321', '0000-00-00', 'http://localhost/manpro/uploads/talents/haha/63708', 'http://localhost/manpro/uploads/talents/haha/image', 'http://localhost/manpro/uploads/talents/haha/image', 'http://localhost/manpro/uploads/talents/haha/13923');

-- --------------------------------------------------------

--
-- Table structure for table `talents_status`
--

CREATE TABLE `talents_status` (
  `statusId` int(11) NOT NULL,
  `talentId` int(11) NOT NULL,
  `status` enum('applying','processing','preparation','immigration') NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `talents_status`
--

INSERT INTO `talents_status` (`statusId`, `talentId`, `status`, `date`) VALUES
(1, 1, 'processing', '2016-08-10'),
(2, 2, 'applying', '2016-07-06'),
(3, 3, 'immigration', '2016-08-31');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`userId`);

--
-- Indexes for table `clubs`
--
ALTER TABLE `clubs`
  ADD PRIMARY KEY (`clubId`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`logId`);

--
-- Indexes for table `pricing`
--
ALTER TABLE `pricing`
  ADD PRIMARY KEY (`priceId`);

--
-- Indexes for table `production`
--
ALTER TABLE `production`
  ADD PRIMARY KEY (`productionId`);

--
-- Indexes for table `status_logs`
--
ALTER TABLE `status_logs`
  ADD PRIMARY KEY (`statusId`);

--
-- Indexes for table `system_logs`
--
ALTER TABLE `system_logs`
  ADD PRIMARY KEY (`logID`);

--
-- Indexes for table `talents`
--
ALTER TABLE `talents`
  ADD PRIMARY KEY (`talentId`);

--
-- Indexes for table `talents_status`
--
ALTER TABLE `talents_status`
  ADD PRIMARY KEY (`statusId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accounts`
--
ALTER TABLE `accounts`
  MODIFY `userId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `clubs`
--
ALTER TABLE `clubs`
  MODIFY `clubId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
  MODIFY `logId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pricing`
--
ALTER TABLE `pricing`
  MODIFY `priceId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `production`
--
ALTER TABLE `production`
  MODIFY `productionId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `status_logs`
--
ALTER TABLE `status_logs`
  MODIFY `statusId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `system_logs`
--
ALTER TABLE `system_logs`
  MODIFY `logID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `talents`
--
ALTER TABLE `talents`
  MODIFY `talentId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `talents_status`
--
ALTER TABLE `talents_status`
  MODIFY `statusId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
