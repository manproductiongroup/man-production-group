<br>

<div class="right-align" id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'en', includedLanguages: 'en,ja,tl', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>

<div class="container">
  <?php if($this->session->flashdata('success')){?>
    <script type="text/javascript">
      $(document).ready(function(){
          swal({
            title: "Done!",
            text:  '<?php echo $this->session->flashdata('success');?>',
            type: "success",
            timer: 3000,
            showConfirmButton: false
          });
      });
    </script>
  <?php }?>
  <div id="menu_button" class="fixed-action-btn" style="bottom: 45px; right: 24px;">
    <a class="btn-floating btn-large red">
      <i class="large material-icons">menu</i>
    </a>
    <ul>
      <li><a class="btn-floating blue tooltipped" data-position="left" data-delay="50" data-tooltip="Edit Talent" onclick="edit_mode()"><i class="material-icons">mode_edit</i></a></li>
      <li><a class="btn-floating yellow darken-1 tooltipped" data-position="left" data-delay="50" data-tooltip="Print" onclick="printBio()"><i class="material-icons">print</i></a></li>
    </ul>
  </div>

  <div id="edit_actions" class="fixed-action-btn" style="bottom: 45px; right: 24px; display: none;">
    <a class="btn-floating btn-large waves-effect waves-light green tooltipped" data-type="<?php echo $user_type;?>" onclick="update_bio(<?php echo $talent[0]->talentId?>)" data-position="top" data-delay="50" data-tooltip="Save"><i class="material-icons">check_circle</i></a>
    <a class="btn-floating btn-large waves-effect waves-light red tooltipped" onclick="cancel_edit()" data-position="top" data-delay="50" data-tooltip="Cancel"><i class="material-icons">cancel</i></a>
  </div>
  <br>
  <!-- <a class="waves-effect waves-light btn" onclick="javascript:printBio('section-to-print')">Print</a> -->
  <div class="section no-pad-bot">
    <div class="card">
      <div id="section-to-print" class="card-content">
        <div class="row">
          <br/><br/>
          <div class="col s12 m12 l12">
            <div class"row">
              <!-- IMAGES -->
              <div class="col s12 m4 l4">
                <img class="materialboxed" data-caption="Image 1" src="<?php echo $talent[0]->pic1?>" height="100%" width="100%">
                <br>
                <div class="row">
                  <div class="col s12 m12 l12">
                    <div class="col s4 m4">
                      <img class="materialboxed" data-caption="Image 2" src="<?php echo $talent[0]->pic2?>" height="100%" width="100%">
                    </div>
                    <div class="col s4 m4 center-align">
                      <img class="materialboxed" data-caption="Image 3" src="<?php echo $talent[0]->pic3?>" height="100%" width="100%">
                    </div>
                    <div class="col s4 m4 right-align">
                      <img class="materialboxed" data-caption="Image 4" src="<?php echo $talent[0]->pic4?>" height="100%" width="100%">
                    </div>
                  </div>
                </div>
                <div id="upload" class="row" style="display:none;">
                  <div class="col s12 m12 l12">
                    <a class="waves-effect waves-light btn-large modal-trigger" href="#uploadImages"><i class="material-icons left">file_upload</i>Upload new photos</a>
                  </div>
                </div>
              </div>
              <form action="#" id="formBio" class="form-horizontal">
                <div class="col s12 m8 l8">
                  <?php if($talent[0]->statusTalent == 'active'){ ?>
                          <span class="badge green">ACTIVE</span>
                  <?php }else{  ?>
                          <span class="badge yellow">INACTIVE</span>
                  <?php }
                  ?>
                  <div class="row">
                    <div class="input-field col s12 m12">
                      <input class="teal-text text-darken-4" placeholder="" id="talentName" name="talentName" length="50" type="text" value="<?php echo $talent[0]->talentName;?>" readonly/>
                      <label class="teal-text text-darken-4" for="talent-name">Talent Name</label>
                    </div>
                    <div class="input-field col s12 m12">
                      <textarea placeholder="" name="stat_remarks" class="materialize-textarea teal-text text-darken-4" readonly><?php echo (empty($talent[0]->stat_remarks)) ? 'No remarks provided.' : $talent[0]->stat_remarks;?></textarea>
                      <label class="teal-text text-darken-4" for="stat_remarks">Status Remarks</label>
                    </div>
                  </div>
                  <div class="row center">
                    <h5 class="header col s12 light">PERSONAL</h5>
                  </div>
                  <!-- PERSONAL -->
                  <div class="row">
                    <div class="input-field col s12 m4">
                      <input class="teal-text text-darken-4" placeholder="" name="firstName" type="text" value="<?php echo $talent[0]->firstName;?>" readonly/>
                      <label class="teal-text text-darken-4" for="firstName">First Name</label>
                    </div>
                    <div class="input-field col s12 m4">
                      <input class="teal-text text-darken-4" placeholder="" name="midName" type="text" value="<?php echo $talent[0]->midName;?>" readonly/>
                      <label class="teal-text text-darken-4" for="midName">Middle Name</label>
                    </div>
                    <div class="input-field col s12 m4">
                      <input class="teal-text text-darken-4" placeholder="" name="lastName" type="text" value="<?php echo $talent[0]->lastName;?>" readonly/>
                      <label class="teal-text text-darken-4" for="lastName">Last Name</label>
                    </div>
                  </div>
                  <div class="row">
                    <div class="input-field col s12 m6">
                      <input class="teal-text text-darken-4 datepicker" id="birthdate" name="birthdate" value="<?php echo ($talent[0]->bdate == '0000-00-00') ? 'No date provided.' : $talent[0]->bdate;?>" disabled/>
                      <label class="teal-text text-darken-4" for="birthdate">Birthdate</label>
                    </div>
                    <div class="input-field col s12 m6">
                      <textarea placeholder="" id="bplace" name="birthplace" class="materialize-textarea teal-text text-darken-4" readonly><?php echo (empty($talent[0]->pob)) ? 'No data provided.' : $talent[0]->pob;?></textarea>
                      <label class="teal-text text-darken-4" for="birthplace">Birthplace</label>
                    </div>
                  </div>
                </div>
                <div class="col s12 m12 l12">
                  <!-- EMPLOYMENT -->
                  <div class="row center">
                    <h5 class="header col s12 light">EMPLOYMENT</h5>
                  </div>
                  <div class="row">
                    <div class="input-field col s12 m6 l6">
                      <?php
                      $options = array(
                                    'Dancer PRO'  => 'Dancer PRO',
                                    'Dancer NON-PRO'    => 'Dancer NON-PRO',
                                    'Singer PRO'   => 'Singer PRO',
                                    'Singer NON-PRO' => 'Singer NON-PRO',
                                  );
                      ?>
                      <?php echo form_dropdown('category', $options, $talent[0]->category, 'class="teal-text text-darken-4 browser-default" id="category" disabled');?>
                      <label class="teal-text text-darken-4" for="category">Category</label>
                    </div>
                    <div class="input-field col s12 m6 l6">
                      <select class="teal-text text-darken-4 browser-default" id="booking" name="booking" disabled>
                          <?php if(isset($clubs)) : foreach($clubs as $club):?>
                            <?php if($club->clubName == $talent[0]->clubName): ?>
                              <option value="<?php echo $club->clubId; ?>" selected="selected"><?php echo $club->clubName; ?></option>
                            <?php else: ?>
                              <option value="<?php echo $club->clubId; ?>"><?php echo $club->clubName; ?></option>
                            <?php endif; ?>
                          <?php endforeach; ?>
                          <input type="hidden" value="<?php echo $club->productionId; ?>" name="production"/>
                        <?php endif; ?>
                      </select>
                      <label class="teal-text text-darken-4" for="booking">Booking</label>
                    </div>
                  </div>
                  <div class="row">
                    <div class="input-field col s12 m6 l6">
                      <select class="teal-text text-darken-4 browser-default" id="agency" name="agency" disabled>
                          <?php if(isset($agencies)) : foreach($agencies as $agency):?>
                            <?php if($agency->agencyName == $talent[0]->agencyName): ?>
                              <option value="<?php echo $agency->agencyId; ?>" selected="selected"><?php echo $agency->agencyName; ?></option>
                            <?php else: ?>
                              <option value="<?php echo $agency->agencyId; ?>"><?php echo $agency->agencyName; ?></option>
                            <?php endif; ?>
                          <?php endforeach; ?>
                        <?php endif; ?>
                      </select>
                      <label class="teal-text text-darken-4" for="agency">Agency</label>
                    </div>

                    <div class="input-field col s12 m6 l6">
                      <input class="teal-text text-darken-4 datepicker" id="agencyDate" name="agencyDate" value="<?php echo ($talent[0]->agencyDate == '0000-00-00') ? 'No date provided.' : $talent[0]->agencyDate;?>" disabled/>
                      <label class="teal-text text-darken-4" for="doc-send">Date Hired</label>
                    </div>
                  </div>
                  <div class="row">
                    <div class="input-field col s12 m4 l4">
                      <input class="teal-text text-darken-4" placeholder="" name="grossSalary" type="number" value="<?php echo (empty($salary[0]->gross)) ? '0' : $salary[0]->gross;?>" readonly/>
                      <label class="teal-text text-darken-4" for="grossSalary">Gross Salary</label>
                    </div>
                    <div class="input-field col s12 m4 l4">
                      <input class="teal-text text-darken-4" placeholder="" name="netSalary" type="number" value="<?php echo (empty($salary[0]->talentFee)) ? '0' : $salary[0]->talentFee;?>" readonly/>
                      <label class="teal-text text-darken-4" for="netSalary">Net Salary</label>
                    </div>
                    <div class="input-field col s12 m4 l4">
                      <input class="teal-text text-darken-4" name="commission" type="number" value="<?php echo (empty($salary[0]->managerCom)) ? '0' : $salary[0]->managerCom;?>" readonly/>
                      <label class="teal-text text-darken-4" for="commission">Commission</label>
                    </div>
                  </div>
                  <div class="row">
                    <div class="input-field col s12 m4 l4">
                      <input class="teal-text text-darken-4" placeholder="" name="passNumber" type="text" value="<?php echo (empty($talent[0]->passportNo)) ? 'No data provided.' : $talent[0]->passportNo;?>" readonly/>
                      <label class="teal-text text-darken-4" for="passNumber">Passport Number</label>
                    </div>
                    <div class="input-field col s12 m4 l4">
                      <input class="teal-text text-darken-4" placeholder="" name="visaNumber" type="text" value="<?php echo (empty($talent[0]->visaNo)) ? 'No data provided.' : $talent[0]->visaNo;?>" readonly/>
                      <label class="teal-text text-darken-4" for="visaNumber">VISA Number</label>
                    </div>
                    <div class="input-field col s12 m4 l4">
                      <input class="teal-text text-darken-4 datepicker" id="visaFin" name="visaFin" value="<?php echo ($talent[0]->finishVisa == '0000-00-00') ? 'No date provided.' : $talent[0]->finishVisa;?>" disabled/>
                      <label class="teal-text text-darken-4" for="visaFin">VISA Finish</label>
                    </div>
                  </div>
                  <div class="row">
                    <div class="input-field col s12 m6 l6">
                      <input class="teal-text text-darken-4" id="coeNum" name="coeNum" type="text" value="<?php echo (empty($talent[0]->coeNum)) ? 'No data provided.' : $talent[0]->coeNum;?>" readonly/>
                      <label class="teal-text text-darken-4" for="coeNum">COE number</label>
                    </div>
                    <div class="input-field col s12 m6 l6">
                      <input class="teal-text text-darken-4" id="grossPrice" name="grossPrice" type="text" value="<?php echo ($talent[0]->grossPrice == '0') ? 'No price provided.' : $talent[0]->grossPrice;?>" readonly/>
                      <label class="teal-text text-darken-4" for="grossPrice">Gross Price</label>
                    </div>
                  </div>
                  <div class="row">
                    <div class="input-field col s12 m6 l6">
                      <input class="teal-text text-darken-4 datepicker" id="arrivalSched" name="arrivalSched" value="<?php echo ($talent[0]->arrivalSched == '0000-00-00') ? 'No date provided.' : $talent[0]->arrivalSched;?>" disabled/>
                      <label class="teal-text text-darken-4" for="arrivalSched">Arrival Schedule</label>
                    </div>
                    <div class="input-field col s12 m6 l6">
                      <input class="teal-text text-darken-4 datepicker" id="departSched" name="departSched" value="<?php echo ($talent[0]->departSched == '0000-00-00') ? 'No date provided.' : $talent[0]->departSched;?>" disabled/>
                      <label class="teal-text text-darken-4" for="departSched">Depart Schedule</label>
                    </div>
                  </div>
                  <div class="row">
                    <div class="input-field col s12 m6 l6">
                      <?php
                      $options = array(
                                    'OK'  => 'OK',
                                    'PENDING'    => 'PENDING'
                                  );
                      ?>
                      <?php echo form_dropdown('pdosStat', $options, $talent[0]->pdosStat, 'class="teal-text text-darken-4 browser-default" id="pdosStat" disabled');?>
                      <label class="teal-text text-darken-4" for="pdosStat">PDOS Status</label>
                    </div>
                    <div class="input-field col s12 m6 l6">
                      <?php
                      $options = array(
                                    'OK'  => 'OK',
                                    'PENDING'    => 'PENDING'
                                  );
                      ?>
                      <?php echo form_dropdown('pdspStat', $options, $talent[0]->pdspStat, 'class="teal-text text-darken-4 browser-default" id="pdspStat" disabled');?>
                      <label class="teal-text text-darken-4" for="pdspStat">PDSP Status</label>
                  </div>
                  <!-- DATES -->
                  <div class="row center">
                    <h5 class="header col s12 light">DATES</h5>
                  </div>
                  <div class="row">
                    <div class="input-field col s12 m6 l6">
                      <input class="teal-text text-darken-4 datepicker" id="docSend" name="docSend" value="<?php echo ($talent[0]->docSend == '0000-00-00') ? 'No date provided.' : $talent[0]->docSend;?>" disabled/>
                      <label class="teal-text text-darken-4" for="doc-send">Document Send</label>
                    </div>
                    <div class="input-field col s12 m6 l6">
                      <textarea placeholder="" name="remarks" class="materialize-textarea teal-text text-darken-4" readonly><?php echo (empty($talent[0]->remarks)) ? 'No remarks provided.' : $talent[0]->remarks;?></textarea>
                      <label class="teal-text text-darken-4" for="remarks">Remarks</label>
                    </div>
                  </div>
                  <div class="row">
                    <div class="input-field col s6 m6">
                      <input class="teal-text text-darken-4 datepicker" id="coeAppPh" name="coeAppPh" value="<?php echo ($talent[0]->coeAppPh == '0000-00-00') ? 'No date provided.' : $talent[0]->coeAppPh;?>" disabled/>
                      <label class="teal-text text-darken-4" for="coeAppPh">COE Application (Philippines)</label>
                    </div>
                    <div class="input-field col s6 m6">
                      <input class="teal-text text-darken-4 datepicker" id="coeReleasePh" name="coeReleasePh" value="<?php echo ($talent[0]->coeReleasePh == '0000-00-00') ? 'No date provided.' : $talent[0]->coeReleasePh;?>" disabled/>
                      <label class="teal-text text-darken-4" for="coeReleasePh">COE Release (Philippines)</label>
                    </div>
                  </div>
                  <div class="row">
                    <div class="input-field col s6 m6">
                      <input class="teal-text text-darken-4 datepicker" id="coeTranslationJp" name="coeTranslationJp" value="<?php echo ($talent[0]->coeTranslationJp == '0000-00-00') ? 'No date provided.' : $talent[0]->coeTranslationJp;?>" disabled/>
                      <label class="teal-text text-darken-4" for="coeTranslationJp">COE Translation (Japan)</label>
                    </div>
                    <div class="input-field col s6 m6">
                      <input class="teal-text text-darken-4 datepicker" id="coeReleaseJp" name="coeReleaseJp" value="<?php echo ($talent[0]->coeReleaseJp == '0000-00-00') ? 'No date provided.' : $talent[0]->coeReleaseJp;?>" disabled/>
                      <label class="teal-text text-darken-4" for="coeReleaseJp">COE Release (Japan)</label>
                    </div>
                  </div>
                  <div class="row">
                    <div class="input-field col s6 m6">
                      <input class="teal-text text-darken-4 datepicker" id="coeForward" name="coeForward" value="<?php echo ($talent[0]->coeForward == '0000-00-00') ? 'No date provided.' : $talent[0]->coeForward;?>" disabled/>
                      <label class="teal-text text-darken-4" for="coeForward">COE Forward to Manila</label>
                    </div>
                    <div class="input-field col s6 m6">
                      <input class="teal-text text-darken-4 datepicker" id="coeReceive" name="coeReceive" value="<?php echo ($talent[0]->coeReceive == '0000-00-00') ? 'No date provided.' : $talent[0]->coeReceive;?>" disabled/>
                      <label class="teal-text text-darken-4" for="coeReceive">COE Receive from Manila</label>
                    </div>
                  </div>
                  <div class="row">
                    <div class="input-field col s6 m6">
                      <input class="teal-text text-darken-4 datepicker" id="poeaApp" name="poeaApp" value="<?php echo ($talent[0]->poeaApp == '0000-00-00') ? 'No date provided.' : $talent[0]->poeaApp;?>" disabled/>
                      <label class="teal-text text-darken-4" for="poeaApp">POEA Application</label>
                    </div>
                    <div class="input-field col s6 m6">
                      <input class="teal-text text-darken-4 datepicker" id="poeaRelease" name="poeaRelease" value="<?php echo ($talent[0]->poeaRelease == '0000-00-00') ? 'No date provided.' : $talent[0]->poeaRelease;?>" disabled/>
                      <label class="teal-text text-darken-4" for="poeaRelease">POEA Release</label>
                    </div>
                  </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php $this->load->view('modals/upload_images_modal');?>
<script>
$( document ).ready(function() {
      $('.modal-trigger').leanModal();

      $('.datepicker').pickadate({
           container: 'body',
           selectMonths: true,
           selectYears: 60,
           today: '',
           formatSubmit: 'yyyy/mm/dd',
           format: 'yyyy/mm/dd',
           min: new Date(2009,1,1)
      });

      //birthdate
      var $birthdate = $('#bdate').pickadate({
          editable: true,
          onClose: function() {
              $('#bdate').focus();
          }
      });
      var picker = $birthdate.pickadate('picker');
      picker.set('min', new Date(1955,1,1));
      picker.set('max', new Date(2004,1,1));
      $birthdate.on('click', function(event) {
        if (picker.get('open')) {
            picker.close();
        } else {
            picker.open();
        }
        event.stopPropagation();
      });
});

function cancel_edit(){
  location.reload();
}

function update_bio(id){
  var user_type = $(this).data("type");
  var method;
   //if userdata type = a then administrator/edit_talent_bio/ else bio/edit_talent_bio
   if(user_type == 'a' ){
     method = 'administrator/edit_talent_bio/';
   }else{
     method = 'biodata/edit_talent_bio/';
   }
   $.ajax({
     type: "POST",
     url: "<?php echo site_url();?>"+method+id,
     data: $('#formBio').serialize(),
     success: function(data){
       location.reload();
     }
   });
}

function edit_mode(){
   $("#menu_button").toggle();
   $("#upload").toggle();
   $("#edit_actions").toggle();

   $("input[type='text']").attr("readonly", false);
   $("input[type='number']").attr("readonly", false);
   $("#bplace").attr("readonly", false);
   $("input").removeAttr("disabled");
   $("#talentName").focus();
   $('#category').prop('disabled', false);
   $('#production').prop('disabled', false);
   $('#booking').prop('disabled', false);
   $('#agency').prop('disabled', false);
   $('#pdosStat').prop('disabled', false);
   $('#pdspStat').prop('disabled', false);
}

function printBio(divID) {
    //Get the HTML of div
    var divElements = document.getElementById(divID).innerHTML;
    //Get the HTML of whole page
    var oldPage = document.body.innerHTML;

    //Reset the page's HTML with div's HTML only
    document.body.innerHTML =
      "<html><head><title></title></head><body>" +
      divElements + "</body>";

    //Print Page
    window.print();

    //Restore orignal HTML
    document.body.innerHTML = oldPage;
}

var formBio = $("#formBio");

formBio.validate({
    rules: {
        talentName: {
            required: true,
            maxlength: 20
        }
    },
    //For custom messages
    messages: {
        talentName: {
            maxlength: "Reached maximum length!"
        }
    },
    errorElement : 'div',
    errorPlacement: function(error, element) {
      var placement = $(element).data('error');
      if (placement) {
        $(placement).append(error)
      } else {
        error.insertAfter(element);
      }
    }
});


//UPLOAD
var formUpload = $("#uploadImagesForm");
formUpload.children("div").steps({
    headerTag: "h3",
    bodyTag: "section",
    transitionEffect: "slideLeft",
    onInit: function (event, current) {
         $('.wizard > .actions > ul > li.disabled ').attr('style', 'display:none');
    },
    onFinished: function (e, currentIndex)
    {
        form.submit();
    }
});

formUpload.validate({
    rules: {
        image1: {
            extension: "jpg|jpeg|png"
        },
        image2: {
            extension: "jpg|jpeg|png"
        },
        image3: {
            extension: "jpg|jpeg|png"
        },
        image4: {
            extension: "jpg|jpeg|png"
        }
    },
    //For custom messages
    messages: {
        image1: {
            extension: 'Please upload a VALID image!'
        },
        image2: {
            extension: 'Please upload a VALID image!'
        },
        image3: {
            extension: 'Please upload a VALID image!'
        },
        image4: {
            extension: 'Please upload a VALID image!'
        }
    },
    errorElement : 'div',
    errorPlacement: function(error, element) {
      var placement = $(element).data('error');
      if (placement) {
        $(placement).append(error)
      } else {
        error.insertAfter(element);
      }
    }
});
</script>
