<!-- updateTalentStat MODAL -->
<?php
  $att = array(
            'id' => 'activeInactiveForm',
            'novalidate' => 'novalidate'
        );
  echo form_open('administrator/update_talent_status/', $att);
?>
<div id="activeInactive" class="modal modal-fixed-footer">
  <div class="modal-content">
    <h4 class="center-align">Update Talent Status</h4>
    <br><br>
    <div class="row">
      <div class="input-field col s6">
          <select id="status" name="status" required="" aria-required="true">
            <option value="" disabled selected>Choose one</option>
            <option value="applying">Applying</option>
            <option value="processing">Processing</option>
            <option value="preparation">Preparation</option>
            <option value="immigration">Immigration</option>
          </select>
          <label>Status</label>
      </div>
      <div class="input-field col s6">
        <input placeholder="" id="date-status" name="date-status" type="text" class="datepicker" required="" aria-required="true">
        <label for="date-status">Date</label>
      </div>
    </div>
  </div>
  <div class="modal-footer">
      <a class="modal-action modal-close waves-effect btn-flat">Cancel</a>
      <button type="submit" id="submit-update" disabled="disabled" id="submit" class="modal-action modal-close waves-effect btn-flat">Save</button>
  </div>
</div>
</form>
<!-- /. updateTalentStat MODAL -->

<!-- updateTalentStatModal -->
<script>
$(document).on("click", "#activeInactiveModal", function () {
      $('#activeInactive').openModal();
      var talentId = $(this).data('id');
      $("#talentId").val( talentId );
      $('#activeInactiveForm').attr('action','<?php echo site_url();?>administrator/update_talent_status/'+talentId);
});
</script>
<!-- ./ updateTalentStatModal -->
