$(document).on("click", "#editTalentModal", function () {
      $('#editTalent').openModal();
      var talentId = $(this).data('id');
      $(".modal-content #talentId").val( talentId );
      $('#editTalentForm').attr('action','<?php echo site_url();?>administrator/edit_talent/'+talentId);

      $.ajax({
        type: "POST",
        url: "<?php echo site_url();?>administrator/view_talent/"+talentId,
        dataType: "JSON",
        success:function(data){
          $('#editTalent').openModal();
          $(".modal-content #talent-name").val(data[0].talentName);
          $(".modal-content #first-name").val(data[0].firstName);
          $(".modal-content #middle-name").val(data[0].midName);
          $(".modal-content #last-name").val(data[0].lastName);
        },
      });
});
