<br>

<div class="right-align" id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'en', includedLanguages: 'en,ja,tl', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>

<?php if($this->session->flashdata('login')){?>
    <script type="text/javascript">
      $(document).ready(function(){
          swal({
            title: "Welcome!",
            text:  '<?php echo $this->session->flashdata('login');?>',
            imageUrl: '<?php echo site_url('imgs/prod.png')?>',
            timer: 2000,
            showConfirmButton: false
          });
      });
    </script>
<?php }?>

  <div class="section no-pad-bot" id="index-banner">
      <div class="container">
        <br><br>
        <h3 class="header center indigo-text text-darken-3"><?php echo $prodName ?></h3>
        <br><br>
      </div>
    </div>

    <div class="container">
      <!-- get talent list -->
      <table class="bordered" style="margin-bottom:100px">
        <thead>
          <tr>
            <th class="center" data-field="">Club Name</th>
            <th class="center" data-field="">Newly Hired</th>
            <th class="center" data-field="">Documents Forwarded</th>
            <th class="center" data-field="">COE Apply</th>
            <th class="center" data-field="">COE Release</th>
            <th class="center" data-field="">In Japan</th>
          </tr>
        </thead>

        <tbody>
        <?php foreach($clubs as $r){?>
          <tr>
              <td><a href="<?= site_url('third/get_all_club/'.$r->clubName.'/'.$r->clubId)?>"><?php echo $r -> clubName; ?></a></td>
            <td class="center"><a href="<?php echo site_url('third/get_all_stat_club/hired/'.$r->clubId)?>">
            <?php foreach($hired as $jap){?>
            <?php if($r->clubId == $jap->clubId):?>
            <?php echo $jap->count?>
            <?php endif ?>
            <?php }?>
            </a></td>

            <td class="center"><a href="<?php echo site_url('third/get_all_stat_club/docsend/'.$r->clubId)?>">
            <?php foreach($docsend as $jap){?>
            <?php if($r->clubId == $jap->clubId):?>
            <?php echo $jap->count?>
            <?php endif ?>
            <?php }?>
            </a></td>

            <td class="center"><a href="<?php echo site_url('third/get_all_stat_club/coeapp/'.$r->clubId)?>">
            <?php foreach($coeapp as $jap){?>
            <?php if($r->clubId == $jap->clubId):?>
            <?php echo $jap->count?>
            <?php endif ?>
            <?php }?>
            </a></td>

            <td class="center"><a href="<?php echo site_url('third/get_all_stat_club/coerelease/'.$r->clubId)?>">
            <?php foreach($coerelease as $jap){?>
            <?php if($r->clubId == $jap->clubId):?>
            <?php echo $jap->count?>
            <?php endif ?>
            <?php }?>
            </a></td>

            <td class="center"><a href="<?php echo site_url('third/get_all_stat_club/injapan/'.$r->clubId)?>">
            <?php foreach($injapan as $jap){?>
            <?php if($r->clubId == $jap->clubId):?>
            <?php echo $jap->count?>
            <?php endif ?>
            <?php }?>
            </a></td>

          </tr>
        <?php }?>
        </tbody>
      </table>
    </div>
