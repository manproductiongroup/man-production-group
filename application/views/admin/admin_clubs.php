<?php if($this->session->flashdata('success')){?>
  <script type="text/javascript">
    $(document).ready(function(){
        swal({
          title: "Done!",
          text:  '<?php echo $this->session->flashdata('success');?>',
          type: "success",
          timer: 3000,
          showConfirmButton: false
        });
    });
  </script>
<?php }?>

<?php if($this->session->flashdata('club')){?>
  <script type="text/javascript">
    $(document).ready(function(){
        swal({
          title: "Done!",
          text:  '<?php echo $this->session->flashdata('club');?>',
          type: "error",
          timer: 3000,
          showConfirmButton: false
        });
    });
  </script>
<?php }?>

<br>

<div class="right-align" id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'en', includedLanguages: 'en,ja,tl', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>

<div class="section no-pad-bot" id="index-banner">
  <div class="container">
    <br><br>
    <h3 class="header center cyan-text text-darken-3">Clubs</h3>
    <br><br>
  </div>
</div>

<div class="container">
  <!-- get talent list -->
    <table class="bordered" style="margin-bottom:100px">
      <thead>
        <tr>
            <th>Club Name</th>
            <th>Production Name</th>
            <th>Color Code</th>
            <th>Options</th>

        </tr>
      </thead>

      <tbody>
        <?php foreach($clubs as $r){?>
        <tr>
            <td><?php echo $r->clubName ?></td>
            <td><?php echo $r->productionName ?></td>
            <td><input type="color" value="<?php echo $r->color ?>" disabled></td>
            <td>
              <a href="<? echo site_url('administrator')?>" class="btn-floating btn-small waves-effect waves-light blue tooltipped edit_club"  data-clubid = "<?php echo $r->clubId ?>" data-position="right" data-delay="50" data-tooltip="Edit Club"><i class="material-icons">mode_edit</i></a>
              <a class="btn-floating btn-small waves-effect waves-light red darken-3 tooltipped" data-position="right" data-delay="50" data-tooltip="Delete Club" onclick="delete_club(<?php echo $r->clubId ?>)"><i class="material-icons">delete</i></a>
            </td>
        </tr>
        <?php } ?>
      </tbody>
    </table>
</div>

<!-- Add new clubs button -->
<div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
  <a class="btn-floating btn-large waves-effect waves-light green-dark modal-trigger" href="#addClub">
    <i class="material-icons">add</i>
  </a>
</div>
<!--  -->

<!-- Add clubs modal -->
<div id="addClub" class="modal modal-fixed-footer">
  <div class="modal-content">
    <h4 class="center-align">新しいクラブを追加</h4>
    <h5 class="center-align">Add New Club</h5>
    <br><br>
      <form action="<?= site_url('administrator/add_clubs'); ?>" method="post">
        <div class="input-field col s12">
          <input id="clubName" name="clubName" type="text" class="validate">
          <label for="clubName">Club Name [クラブ名]</label>
        </div>
        <br>
        <div class="input-field col s12">
          <select id="production" name="production">
            <option value="" disabled selected>Choose one</option>
            <?php if(isset($production)) : foreach($production as $prod):?>
            <option value="<?php echo $prod->productionId; ?>"><?php echo $prod->productionName; ?></option>
            <?php endforeach; ?>
          </select>
          <?php else: ?>
          <input disabled value="No production" id="production" type="text" class="validate">
          <?php endif; ?>
          <label for="production">Production [日本プロダクション]</label>
        </div>
        <br>
        <div class="input-field col s12">
          <label for="clubColor">Color Code</label>
          <br>
          <input style="margin-left:10px;width:10%" id="uclubColor" name="clubColor" type="color" class="validate">
        </div>
    </div>
    <div class="modal-footer">
      <a  class="modal-close waves-effect waves-red btn-flat ">Cancel [キャンセル]</a>
      <button  type="submit" class="modal-action modal-close waves-effect waves-green btn-flat ">Save [セーブ]</button>
    </div>
    </form>
  </div>
<!--  -->

<!-- edit clubs modal -->
<div id="editClub" class="modal modal-fixed-footer">
  <div class="modal-content">
    <h4 class="center-align">新しいクラブを追加</h4>
    <h5 class="center-align">Edit Club</h5>
    <br><br>
      <?php echo form_open('administrator/update_club'); ?>
        <div class="input-field col s12">
          <input id="uclubId" name="clubId" type="hidden">
          <input id="uclubName" name="clubName" type="text" class="validate">
          <label for="clubName">Club Name [クラブ名]</label>
        </div>
        <br>
        <div class="input-field col s12">
          <select id="uproduction" name="production">
            <option value="" disabled selected>Choose one</option>
            <?php if(isset($production)) : foreach($production as $prod):?>
            <option value="<?php echo $prod->productionId; ?>"><?php echo $prod->productionName; ?></option>
            <?php endforeach; ?>
          </select>
          <?php else: ?>
          <input disabled value="No production" id="uproduction" type="text" class="validate">
          <?php endif; ?>
          <label for="production">Production [日本プロダクション]</label>
        </div>
        <br>
        <div class="input-field col s12">
          <label for="clubColor">Color Code</label>
          <br>
          <input style="margin-left:10px;width:10%" id="uclubColor" name="clubColor" type="color" class="validate">
        </div>
    </div>
    <div class="modal-footer">
      <a  class="modal-close waves-effect waves-red btn-flat ">Cancel [キャンセル]</a>
      <button  type="submit" class="modal-action modal-close waves-effect waves-green btn-flat ">Save [セーブ]</button>
    </div>
    <?php echo form_close()?>
  </div>
<!--  -->


<script>
$(document).ready(function() {
  // the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
  $('.modal-trigger').leanModal();
});
</script>

<script>
$(document).ready(function() {
   $('select').material_select();
 });
</script>

<script>
$(document).ready(function(){
    $('.tooltipped').tooltip({delay: 50});
  });
</script>

<script>
function delete_club(id){
  swal({    title: "Confirmation",
              text: "Are you sure you want to delete club "+id+"?",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#f44336",
              confirmButtonText: "Yes, delete it!",
              closeOnConfirm: false },
              function(isConfirm){
                if (isConfirm) {
                    $.ajax({
                        url : "<?php echo site_url('administrator/delete_club')?>/"+id,
                        type: "POST",
                        dataType: "JSON",
                        success: function(data)
                        {
                          swal("Deleted!", "Club "+id+" successfully deleted!", "success");
                          location.reload(true);
                        },
                        error: function (jqXHR, textStatus, errorThrown)
                        {
                          alert('An error occured while deleting data');
                        }
                    });
                }
              });

}
</script>
