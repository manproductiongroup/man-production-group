<!-- addTalent MODAL -->
<div id="addTalent" class="modal modal-fixed-footer">
  <div class="modal-content">
    <h4 class="center-align">Add New Talent</h4>
    <div class="divider"></div>
    <br>
    <div class="row">
      <div class="col s12">
          <?php
            $att = array(
                      'id' => 'addTalentForm',
                      'novalidate' => 'novalidate'
                   );
            echo form_open_multipart('administrator/add_talents', $att);
          ?>
          <div>
          <h3>Personal</h3>
          <section>
          <br/>
            <div class="row">
              <div class="input-field col s4">
                <input id="firstName" name="firstName" type="text"/>
                <label for="firstName">First Name</label>
              </div>
              <div class="input-field col s4">
                <input id="middleName" name="middleName" type="text"/>
                <label for="middleName">Middle Name</label>
              </div>
              <div class="input-field col s4">
                <input id="lastName" name="lastName" type="text"/>
                <label for="lastName">Last Name</label>
              </div>
            </div>
            <div class="row">
              <div class="input-field col s12">
                <input id="birthdate" name="birthdate" type="text" class="datepicker" />
                <label for="birthdate">Birthdate</label>
              </div>
            </div>
            <div class="row">
              <div class="input-field col s12">
                <textarea id="birthplace" name="birthplace" class="materialize-textarea" length="150"></textarea>
                <label for="birthplace">Birthplace</label>
              </div>
            </div>
          </section>
          <h3>Employment</h3>
          <section>
          <br>
            <div class="row">
              <br>
              <div class="input-field col s4 m4">
                <input id="talentName" name="talentName" type="text" length="50">
                <label for="talentName">Talent Name</label>
              </div>
              <div class="input-field col s4 m4">
                  <select class="browser-default" id="category" name="category" data-error=".categoryError">
                    <option value="" disabled selected>Choose one</option>
                    <option value="Dancer PRO">Dancer PRO</option>
                    <option value="Dancer NON-PRO">Dancer NON-PRO</option>
                    <option value="Singer PRO">Singer PRO</option>
                    <option value="Singer NON-PRO">Singer NON-PRO</option>
                  </select>
                  <label>Category</label>
                  <div class="input-field">
                      <div class="categoryError"></div>
                  </div>
              </div>
              <div class="input-field col s4 m4">
                <select class="browser-default" id="booking" name="booking" data-error=".bookingError">
                  <option value="" disabled selected>Choose one</option>
                  <?php if(isset($clubs)) : foreach($clubs as $club):?>
                  <option value="<?php echo $club->clubId; ?>:<?php echo $club->productionId; ?>"><?php echo $club->clubName; ?></option>
                  <?php endforeach; ?>
                </select>
                <input type="hidden" value="" id="production" name="production" />
                <?php else: ?>
                <input disabled value="" id="booking" name="booking" type="text">
                <?php endif; ?>
                <label for="booking">Booking</label>
                <div class="input-field">
                    <div class="bookingError"></div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="input-field col s8 m8">
                <select class="browser-default" id="agency" name="agency" data-error=".agencyError">
                  <option value="" disabled selected>Choose one</option>
                  <?php if(isset($agencies)) : foreach($agencies as $agency):?>
                  <option value="<?php echo $agency->agencyId; ?>:<?php echo $agency->agencyId; ?>"><?php echo $agency->agencyName; ?></option>
                  <?php endforeach; ?>
                </select>
                <input type="hidden" value="" id="agencyHidden" name="agencyHidden" />
                <?php else: ?>
                <input disabled value="" id="agency" name="agency" type="text">
                <?php endif; ?>
                <label for="agency">Agency</label>
                <div class="input-field">
                    <div class="agencyError"></div>
                </div>
              </div>
              <div class="input-field col s4 m4">
                <input id="agencyDate" name="agencyDate" type="date" class="datepicker">
                <label for="agencyDate">Agency Date</label>
              </div>
            </div>
            <br>
            <div class="row">
              <div class="input-field col s4 m4">
                <input id="grossSalary" name="grossSalary" type="number">
                <label for="grossSalary">Gross Salary</label>
              </div>
              <div class="input-field col s4 m4">
                <input id="netSalary" name="netSalary" type="number">
                <label for="netSalary">Net Salary</label>
              </div>
              <div class="input-field col s4 m4">
                <input id="commission" name="commission" type="number">
                <label for="commission">Commission</label>
              </div>
            </div>
            <br>
            <div class="row">
              <div class="input-field col s4 m4">
                <input id="passNumber" name="passNumber" type="text">
                <label for="passNumber">Passport Number</label>
              </div>
              <div class="input-field col s4 m4">
                <input id="visaNumber" name="visaNumber" type="text">
                <label for="visaNumber">VISA Number</label>
              </div>
              <div class="input-field col s4 m4">
                <input id="visaFin" name="visaFin" type="date" class="datepicker">
                <label for="visaFin">VISA Finish</label>
              </div>
            </div>
            <div class="row">
              <div class="input-field col s6 m6">
                <input id="coeNum" name="coeNum" type="text">
                <label for="coeNum">COE number</label>
              </div>
              <div class="input-field col s6 m6">
                <input id="grossPrice" name="grossPrice" type="text">
                <label for="grossPrice">Gross Price</label>
              </div>
            </div>
            <div class="row">
              <div class="input-field col s6 m6">
                <input id="arrivalSched" name="arrivalSched" type="date" class="datepicker">
                <label for="arrivalSched">Arrival Schedule</label>
              </div>
              <div class="input-field col s6 m6">
                <input id="departSched" name="departSched" type="date" class="datepicker">
                <label for="departSched">Departure Schedule</label>
              </div>
            </div>
            <div class="row">
              <label>PDOS Status</label>
              <input type="hidden" name="pdosStat" value="PENDING">
              <div class="switch"><label>Pending<input type="checkbox" id="pdosStat" name="pdosStat"><span class="lever"></span>OK</label></div>
            </div>
            <div class="row">
              <label>PDSP Status</label>
              <input type="hidden" name="pdspStat" value="PENDING">
              <div class="switch"><label>Pending<input type="checkbox" id="pdspStat" name="pdspStat"><span class="lever"></span>OK</label></div>
            </div>
          </section>
          <h3>Dates</h3>
          <section>
            <br>
            <div class="row">
              <div class="input-field col s6 m6">
                <input id="docSend" name="docSend" type="date" class="datepicker">
                <label for="docSend">Document Send</label>
              </div>
              <div class="input-field col s6 m6">
                <textarea id="remarks" name="remarks" class="materialize-textarea" length="150"></textarea>
                <label for="remarks">Remarks</label>
              </div>
            </div>
            <div class="row">
              <div class="input-field col s6 m6">
                <input id="coeAppPh" name="coeAppPh" type="date" class="datepicker">
                <label for="coeAppPh">COE Application (Philippines)</label>
              </div>
              <div class="input-field col s6 m6">
                <input id="coeReleasePh" name="coeReleasePh" type="date" class="datepicker">
                <label for="coeReleasePh">COE Release (Philippines)</label>
              </div>
            </div>
            <div class="row">
              <div class="input-field col s6 m6">
                <input id="coeTranslationJp" name="coeTranslationJp" type="date" class="datepicker">
                <label for="coeTranslationJp">COE Translation (Japan)</label>
              </div>
              <div class="input-field col s6 m6">
                <input id="coeReleaseJp" name="coeReleaseJp" type="date" class="datepicker">
                <label for="coeReleaseJp">COE Release (Japan)</label>
              </div>
            </div>
            <div class="row">
              <div class="input-field col s6 m6">
                <input id="coeForward" name="coeForward" type="date" class="datepicker">
                <label for="coeForward">COE Forward to Manila</label>
              </div>
              <div class="input-field col s6 m6">
                <input id="coeReceive" name="coeReceive" type="date" class="datepicker">
                <label for="coeReceive">COE Receive from Manila</label>
              </div>
            </div>
            <div class="row">
              <div class="input-field col s6 m6">
                <input id="poeaApp" name="poeaApp" type="date" class="datepicker">
                <label for="poeaApp">POEA Application</label>
              </div>
              <div class="input-field col s6 m6">
                <input id="poeaRelease" name="poeaRelease" type="date" class="datepicker">
                <label for="poeaRelease">POEA Release</label>
              </div>
            </div>
          </section>
          <h3>Images</h3>
          <br>
          <section>
            <!-- IMAGES -->
            <!-- IMAGE 1 -->
            <div class="row">
             <div class="file-field input-field">
                <div class="btn">
                   <span>File</span>
                   <input type="file" name="image1" id="image1" data-error=".image1Error">
                 </div>
                 <div class="file-path-wrapper">
                    <input class="file-path validate" type="text" placeholder="Picture 1">
                 </div>
                 <div class="input-field">
                     <div class="image1Error"></div>
                 </div>
              </div>
            </div>
            <!-- IMAGE 2 -->
            <div class="row">
             <div class="file-field input-field">
              <div class="btn">
                 <span>File</span>
                 <input type="file" name="image2" id="image2" data-error=".image2Error">
               </div>
               <div class="file-path-wrapper">
                  <input class="file-path validate" type="text" placeholder="Picture 2">
               </div>
               <div class="input-field">
                   <div class="image2Error"></div>
               </div>
              </div>
            </div>
            <!-- IMAGE 3 -->
            <div class="row">
             <div class="file-field input-field">
              <div class="btn">
                 <span>File</span>
                 <input type="file" name="image3" id="image3" data-error=".image3Error">
               </div>
               <div class="file-path-wrapper">
                  <input class="file-path validate" type="text" placeholder="Picture 3">
               </div>
               <div class="input-field">
                   <div class="image3Error"></div>
               </div>
              </div>
            </div>
            <!-- IMAGE 4 -->
            <div class="row">
             <div class="file-field input-field">
              <div class="btn">
                 <span>File</span>
                 <input type="file" name="image4" id="image4" data-error=".image4Error">
               </div>
               <div class="file-path-wrapper">
                  <input class="file-path validate" type="text" placeholder="Picture 4">
               </div>
               <div class="input-field">
                   <div class="image4Error"></div>
               </div>
              </div>
            </div>
          </section>
          </div>
        </form>
      </div>
    </div>
  </div>
  <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-teal btn-flat ">Go Back</a>
  </div>
</div>
<!-- /. addTalent MODAL -->


<script>
//DATEPICKER
$( document ).ready(function() {
  $('.datepicker').pickadate({
       container: 'body',
       selectMonths: true,
       selectYears: 60,
       today: '',
       formatSubmit: 'yyyy/mm/dd',
       format: 'yyyy/mm/dd',
       min: new Date(2009,1,1)
  });

  //birthdate
  var $birthdate = $('#birthdate').pickadate({
      editable: true,
      onClose: function() {
          $('#birthdate').focus();
      }
  });
  var picker = $birthdate.pickadate('picker');
  picker.set('min', new Date(1955,1,1));
  picker.set('max', new Date(2004,1,1));
  $birthdate.on('click', function(event) {
    if (picker.get('open')) {
        picker.close();
    } else {
        picker.open();
    }
    event.stopPropagation();
  });
});

var form = $("#addTalentForm");
form.children("div").steps({
    headerTag: "h3",
    bodyTag: "section",
    transitionEffect: "slideLeft",
    enableKeyNavigation: true,
    /* Labels */
    labels: {
        cancel: "Cancel?",
        current: "current step:",
        pagination: "Pagination",
        loading: "Loading ...",
        finish: 'SAVE',
        next: 'NEXT',
        previous: 'PREVIOUS'
    },
    onInit: function (event, current) {
         $('.actions > ul > li:first-child').attr('style', 'display:none');
    },
    onStepChanged: function (event, current, next) {
         if (current > 0) {
             $('.actions > ul > li:first-child').attr('style', '');
         } else {
             $('.actions > ul > li:first-child').attr('style', 'display:none');
         }
    },
    onStepChanging: function (e, currentIndex, newIndex)
    {
        if (currentIndex > newIndex)
        {
            return true;
        }
        form.validate().settings.ignore = ":disabled,:hidden";
        return form.valid();
    },
    onFinishing: function (e, currentIndex)
    {
        form.validate().settings.ignore = ":disabled";
        return form.valid();
    },
    onFinished: function (e, currentIndex)
    {
        form.submit();
    }
});

form.validate({
    rules: {
        firstName: {
            maxlength: 20
        },
        middleName: {
            maxlength: 20
        },
        lastName: {
            maxlength: 20
        },
        talentName: {
            required: true,
            maxlength: 50
        },
        category: "required",
        production: "required",
        booking: "required",
        agency: "required",
        grossSalary:"required",
        netSalary:"required",
        commission:"required",

        image1: {
            extension: "jpg|jpeg|png"
        },
        image2: {
            extension: "jpg|jpeg|png"
        },
        image3: {
            extension: "jpg|jpeg|png"
        },
        image4: {
            extension: "jpg|jpeg|png"
        }
    },
    //For custom messages
    messages: {
        firstName: {
            maxlength: "Reached maximum length!"
        },
        middleName: {
            maxlength: "Reached maximum length!"
        },
        lastName: {
            maxlength: "Reached maximum length!"
        },
        talentName: {
            maxlength: "You have reached the maximum length!"
        },
        image1: {
            extension: 'Please upload a VALID image!'
        },
        image2: {
            extension: 'Please upload a VALID image!'
        },
        image3: {
            extension: 'Please upload a VALID image!'
        },
        image4: {
            extension: 'Please upload a VALID image!'
        }
    },
    errorElement : 'div',
    errorPlacement: function(error, element) {
      var placement = $(element).data('error');
      if (placement) {
        $(placement).append(error)
      } else {
        error.insertAfter(element);
      }
    }
});

//SWITCH VALUES
$('#pdosStat').change(function(){
  pdos = $('#pdosStat');
  pdos.val(pdos.prop('checked') ? "OK":"PENDING");
});

$('#pdspStat').change(function(){
  pdsp = $('#pdspStat');
  pdsp.val(pdsp.prop('checked') ? "OK":"PENDING");
});

$('#booking').on('change', function(){
  var value = $('#booking').val();
  var ids = value.split(':');
  $('#production').val(ids[1]);
});

$('#agency').on('change', function(){
  var value = $('#agency').val();
  var ids = value.split(':');
  $('#agencyHidden').val(ids[1]);
});
</script>
