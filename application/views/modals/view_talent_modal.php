<!-- viewTalent MODAL -->
<div id="viewTalent" class="modal modal-fixed-footer">
  <div class="modal-content">
    <h4 class="center-align">ビュー芸能人データ</h4>
    <h5 class="center-align">View Talent</h5>
    <div class="divider"></div>
    <br>
    <form id="viewTalentForm" novalidate="novalidate" method="GET" accept-charset="utf-8">
    <div>
      <h3>Personal</h3>
      <section>
          <br/>
          <div class="row">
            <div class="input-field col s4">
              <input class="teal-text text-darken-4" placeholder="" id="view-first-name" name="view-first-name" type="text" value="" disabled/>
              <label class="teal-text text-darken-4" for="first-name">First Name</label>
            </div>
            <div class="input-field col s4">
              <input class="teal-text text-darken-4" placeholder="" id="view-middle-name" name="view-middle-name" type="text" value="" disabled/>
              <label class="teal-text text-darken-4" for="middle-name">Middle Name</label>
            </div>
            <div class="input-field col s4">
              <input class="teal-text text-darken-4" placeholder="" id="view-last-name" name="view-last-name" type="text" value="" disabled/>
              <label class="teal-text text-darken-4" for="last-name">Last Name</label>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s12">
              <input class="teal-text text-darken-4" placeholder="" id="view-birthdate" name="view-view-birthdate" type="text" value="" disabled/>
              <label class="teal-text text-darken-4" for="birthdate">Birthdate [生年月日]</label>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s12">
              <textarea placeholder="" id="view-birthplace" name="view-birthplace" class="materialize-textarea teal-text text-darken-4" value="" disabled></textarea>
              <label class="teal-text text-darken-4" for="birthplace">Birthplace [出生地]</label>
            </div>
          </div>
      </section>
      <h3>Employment</h3>
      <section>
          <br/>
          <div class="row">
            <br>
            <div class="input-field col s8">
              <input class="teal-text text-darken-4" placeholder="" id="view-talent-name" name="view-talent-name" type="text" value="" disabled/>
              <label class="teal-text text-darken-4" for="talent-name">Talent Name [タレント名]</label>
            </div>
            <div class="input-field col s4">
              <input class="teal-text text-darken-4" placeholder="" id="view-category" name="view-category" type="text" value="" disabled/>
              <label class="teal-text text-darken-4" for="category">Category</label>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s12">
              <input class="teal-text text-darken-4" placeholder="" id="view-manager" name="view-manager" type="text" value="" disabled/>
              <label class="teal-text text-darken-4" for="manager">Manager [マネージャ]</label>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="input-field col s6">
              <input class="teal-text text-darken-4" placeholder="" id="view-production" name="view-production" type="text" value="" disabled/>
              <label class="teal-text text-darken-4" for="production">Production [日本プロダクション]</label>
            </div>
            <div class="input-field col s6">
              <input class="teal-text text-darken-4" placeholder="" id="view-club-name" name="view-club-name" type="text" value="" disabled/>
              <label class="teal-text text-darken-4" for="club-name">Club Name [経歴出演店]</label>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s6">
              <input class="teal-text text-darken-4" placeholder="" id="view-pass-number" name="view-pass-number" type="text" value="" disabled/>
              <label class="teal-text text-darken-4" for="pass-number">Passport Number [パスポートNo]</label>
            </div>
            <div class="input-field col s6">
              <input class="teal-text text-darken-4" placeholder="" id="view-pass-exp" name="view-pass-exp" type="text" value="" disabled/>
              <label class="teal-text text-darken-4" for="pass-exp">Expiry Date of Passport [期限]</label>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s12">
              <input class="teal-text text-darken-4" placeholder="" id="view-issu" name="view-issu" type="text" value="" disabled/>
              <label class="teal-text text-darken-4" for="issu">ISSU [発行日]</label>
            </div>
          </div>
        </section>
        <h3>Dates</h3>
        <section>
          <br/>
          <div class="row">
            <div class="input-field col s12">
              <input class="teal-text text-darken-4" placeholder="" id="view-apply" name="view-apply" type="text" value="" disabled/>
              <label class="teal-text text-darken-4" for="apply">Apply [申請日]</label>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s12">
              <input class="teal-text text-darken-4" placeholder="" id="view-doc-send" name="view-doc-send" type="text" value="" disabled/>
              <label class="teal-text text-darken-4" for="doc-send">Document Send [ドキュメント入手日]</label>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s12">
              <input class="teal-text text-darken-4" placeholder="" id="view-processing" name="view-processing" type="text" value="" disabled/>
              <label class="teal-text text-darken-4" for="processing">Processing [比国エージェント]</label>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s12">
              <input class="teal-text text-darken-4" placeholder="" id="view-vis-lic" name="view-vis-lic" type="text" value="" disabled/>
              <label class="teal-text text-darken-4" for="visa-lic">VISA License [VISA交付日]</label>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s12">
              <input class="teal-text text-darken-4" placeholder="" id="view-fin-visa" name="view-fin-visa" type="text" value="" disabled/>
              <label class="teal-text text-darken-4" for="fin-visa">Finish VISA [帰国日]</label>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s12">
              <input class="teal-text text-darken-4" placeholder="" id="view-in-japan" name="view-in-japan" type="text" value="" disabled/>
              <label class="teal-text text-darken-4" for="in-japan">In Japan [入国日]</label>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s12">
              <input class="teal-text text-darken-4" placeholder="" id="view-extension" name="view-extension" type="text" value="" disabled/>
              <label class="teal-text text-darken-4" for="extension">Extension [更新日]</label>
            </div>
          </div>
        </section>
        <h3>Images</h3>
        <br>
        <section>
          <br/>
            <div class="row">
               <div class="gallery-item col s3 m3">
                 <img id="view-image1" class="materialboxed" src="" width="650">
               </div>
               <div class="gallery-item col s3 m3">
                 <img id="view-image2" class="materialboxed" src="" width="650">
               </div>
               <div class="gallery-item col s3 m3">
                <img id="view-image3" class="materialboxed" src="" width="650">
               </div>
               <div class="gallery-item col s3 m3">
                 <img id="view-image4" class="materialboxed" src="" width="650">
               </div>
            </div>
        </section>
    </div>
    </form>
  </div>
  <div class="modal-footer">
    <a class="modal-action modal-close waves-effect btn-flat">Back [バック]</a>
  </div>
</div>
<!-- /. viewTalent MODAL -->

<!-- viewTalentModal -->
<script>
$(document).on("click", "#viewTalentModal", function () {
      var talentId = $(this).data('id');

      $.ajax({
        type: "GET",
        url: "<?php echo site_url();?>administrator/view_talent/"+talentId,
        dataType: "JSON",
        success:function(data){
          $('#viewTalent').openModal();
          $(".modal-content #view-talent-name").val(data[0].talentName);
          $(".modal-content #view-category").val(data[0].category);
          $(".modal-content #view-first-name").val(data[0].firstName);
          $(".modal-content #view-middle-name").val(data[0].midName);
          $(".modal-content #view-last-name").val(data[0].lastName);
          $(".modal-content #view-club-name").val(data[0].clubName);
          $(".modal-content #view-birthdate").val(data[0].bdate);
          $(".modal-content #view-birthplace").val(data[0].pob);
          $(".modal-content #view-manager").val(data[0].manager);
          $(".modal-content #view-production").val(data[0].productionName);
          $(".modal-content #view-processing").val(data[0].processing);
          $(".modal-content #view-doc-send").val(data[0].docSend);
          $(".modal-content #view-apply").val(data[0].apply);
          $(".modal-content #view-vis-lic").val(data[0].visaLicense);
          $(".modal-content #view-in-japan").val(data[0].inJapan);
          $(".modal-content #view-extension").val(data[0].extension);
          $(".modal-content #view-fin-visa").val(data[0].finishVisa);
          $(".modal-content #view-pass-number").val(data[0].passportNo);
          $(".modal-content #view-pass-exp").val(data[0].passExp);
          $(".modal-content #view-issu").val(data[0].issu);
          $(".modal-content #view-image1").attr('src', data[0].pic1);
          $(".modal-content #view-image2").attr('src', data[0].pic2);
          $(".modal-content #view-image3").attr('src', data[0].pic3);
          $(".modal-content #view-image4").attr('src', data[0].pic4);
        },
    });
});

var form = $("#viewTalentForm");
form.children("div").steps({
    headerTag: "h3",
    bodyTag: "section",
    transitionEffect: "slideLeft",
    enableKeyNavigation: true,
    enableFinishButton: false,
    /* Labels */
    labels: {
        cancel: "Cancel?",
        current: "current step:",
        loading: "Loading ...",
        next: 'NEXT',
        previous: 'PREVIOUS'
    },
    onInit: function (event, current) {
         $('.actions > ul > li:first-child').attr('style', 'display:none');
    },
    onStepChanged: function (event, current, next) {
         if (current > 0) {
             $('.actions > ul > li:first-child').attr('style', '');
         } else if ( currentIndex == 1 ) {
             $("#wizard .actions a[href='#previous']").hide();
         } else {
             $('.actions > ul > li:first-child').attr('style', 'display:none');
         }
    }
});
</script>
<!-- ./ viewTalentModal -->
