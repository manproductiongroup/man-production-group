<style>
table {
border:none;
border-collapse: collapse;
}

table td {
border-left: 1px solid #000;
border-right: 1px solid #000;
}

table td:first-child {
border-left: none;
}

table td:last-child {
border-right: none;
}

table tr {
border-top: 1px solid #000;
border-bottom: 1px solid #000;
}

table tr:first-child {
border-top: none;
}

table tr:last-child {
border-bottom: none;
}
</style>
<br>

<div class="right-align" id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'en', includedLanguages: 'en,ja,tl', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
<div class="section no-pad-bot" id="index-banner">
  <div class="container">
    <br><br>
    <h3 class="header center indigo-text text-darken-4"><?php echo $prodName ?></h3>
    <br><br>
  </div>
</div>

<div class="">
  <br><br>
  <!-- get club list -->
  <?php foreach($clubs as $row){?>
      <h5 class="center"><?php echo $row -> clubName; ?></h5>
      <br>
      <!-- <h6 class="right" style="margin-left:5px"><a href="#deleteClub-<?php echo $row->clubId ?>" class="btn-floating red modal-trigger"><i class="material-icons">delete</i></a></h6> -->
      <br>
      <div class="">
        <!-- get talent list -->
          <table class="bordered responsive-table" style="margin-bottom:100px">
            <thead>
              <tr>
                <th class="center">Full Name</th>
                <th class="center">Talent Name</th>
                <th class="center">Production</th>
                <th class="center">Club</th>
                <th class="center">Agency</th>
                <th class="center blue-text">Date Hired</th>
                <th class="center blue-grey-text">Documents Forwarded</th>
                <th class="center purple-text">COE Applied</th>
                <th class="center deep-purple-text">COE Released</th>
                <th class="center ">COE Number</th>
                <th class="center green-text">In Japan</th>
                <th class="center orange-text">Out Japan</th>
                <th class="center">Status</th>
                <th class="center">Gross Salary</th>
                <th class="center">Net Salary</th>
                <th class="center">Commission</th>
                <th class="center">Jan</th>
                <th class="center">Feb</th>
                <th class="center">Mar</th>
                <th class="center">Apr</th>
                <th class="center">May</th>
                <th class="center">Jun</th>
                <th class="center">Jul</th>
                <th class="center">Aug</th>
                <th class="center">Sept</th>
                <th class="center">Oct</th>
                <th class="center">Nov</th>
                <th class="center">Dec</th>
              </tr>
            </thead>

            <tbody>
            <?php foreach($talents as $r){?>
              <?php if($r->clubId == $row->clubId){?>
              <tr>
                <td class="center" bgcolor="<?= $r->color ?>">
                  <a class="black-text" link="black" href="<?php
                    $key = "1234MaNpr0";
                    $talent_id = base64_encode($key. $r->talentId);
                    $result = rtrim($talent_id,'=');
                  echo site_url('administrator/talent_biodata/'.$result);?>" target="_blank"><u><b><?php echo $r->firstName." ".$r->midName." ".$r->lastName ?></a></td>
                <td class="center" bgcolor="<?= $r->color ?>"><b><?php echo $r->talentName ?></td>
                <td class="center" bgcolor="<?= $r->color ?>" ><b><?php echo $r->productionName ?></td>
                <td class="center" bgcolor="<?= $r->color ?>"><b><?php echo $r->clubName ?></td>
                <td class="center" bgcolor="<?= $r->color ?>"><b><?php echo $r->agencyName ?></td>
                <td class="center"><b><?php echo ($r->agencyDate == '0000-00-00') ? '' : date_format(date_create($r->agencyDate),"m/d/Y") ; ?></td>
                <td class="center"><b><?php echo ($r->docSend == '0000-00-00') ? '' : date_format(date_create($r->docSend),"m/d/Y") ; ?></td>
                <td class="center"><b><?php echo ($r->coeAppPh == '0000-00-00') ? '' : date_format(date_create($r->coeAppPh),"m/d/Y") ; ?></td>
                <td class="center"><b><?php echo ($r->coeReleasePh == '0000-00-00') ? '' : date_format(date_create($r->coeReleasePh),"m/d/Y") ; ?></td>
                <td class="center"><b><?php echo ( $r->coeNum == 'No data provided.') ? '' :  $r->coeNum ?></td>
                <td class="center"><b><?php echo ($r->departSched == '0000-00-00') ? '' : date_format(date_create($r->departSched),"m/d/Y") ; ?></td>
                <td class="center"><b><?php echo ($r->arrivalSched == '0000-00-00') ? '' : date_format(date_create($r->arrivalSched),"m/d/Y") ; ?></td>
                  <td class="center"><b>
                    <?php
                      switch($r->status){
                        case 'hired':
                          echo 'Hired';
                          break;
                        case 'coeapp':
                          echo 'COE Applied';
                          break;
                        case 'coerelease':
                          echo 'COE Released';
                          break;
                        case 'injapan':
                          echo 'In Japan';
                          break;
                        case 'inactive':
                          echo 'Cancelled';
                          break;
                          case 'docsend':
                            echo 'Documents Forwarded';
                            break;
                            case 'outjapan':
                              echo 'Out Japan';
                              break;
                      }

                      ?>
                  </td>
                <td class="center"><b><?php echo (empty($r->gross)) ? '' : '¥ '.number_format($r->gross,2);?></td>
                <td class="center"><b><?php echo (empty($r->talentFee)) ? '' : '¥ '.number_format($r->talentFee,2);?></td>
                <td class="center"><b><?php echo (empty($r->managerCom)) ? '' : '¥ '.number_format($r->managerCom,2)?></td>
                <!-- jan -->
                <?php
                  if(!$r->date){
                    $mon = '0000-00-00';
                  }else{
                    $mon = explode("-",$r->date);
                  }

                  $color = "";
                  $date = "";
                  switch($r->status){
                    case 'injapan':
                      $mon1 = $mon[1];
                      $mon2 = $mon1+1;
                      $mon3 = $mon1+2;
                      $mon4 = $mon1+3;

                      // $color = 'green';
                      $effectiveDate = strtotime("+3 months", strtotime($r->date));
                      $effectiveDate = strftime ( '%Y-%m-%d' , $effectiveDate );
                      $dep = date_format(date_create($r->departSched),"m/d/Y");
                      $arr = date_format(date_create($effectiveDate),"m/d/Y");
                      $date = "In Japan ".$dep." - ".$arr;

                      switch($mon2){
                        case '13':
                          $mon2 = 1;
                          break;
                        case '14':
                          $mon2 = 2;
                          break;
                        case '15':
                          $mon2 = 3;
                          break;
                      }

                      switch($mon3){
                        case '13':
                          $mon3 = 1;
                          break;
                        case '14':
                          $mon3 = 2;
                          break;
                        case '15':
                          $mon3 = 3;
                          break;
                      }

                      switch($mon4){
                        case '13':
                          $mon4 = 1;
                          break;
                        case '14':
                          $mon4 = 2;
                          break;
                        case '15':
                          $mon4 = 3;
                          break;
                      }

                      break;

                      case 'outjapan':
                        $mon1 = $mon[1];

                        // $color = 'green';
                        $dep = date_format(date_create($r->departSched),"m/d/Y");
                        $date = "Out Japan ".$dep;

                        break;
                  }

                ?>
                <?php for($x = 1; $x <= 12 ;$x++){?>

                  <td
                  <?php if($r->status == 'injapan'){?>
                  bgcolor="<?php echo ($mon1 == $x || $mon2 == $x || $mon3 == $x || $mon4 == $x ) ? $r->color : ''?>"
                  class="tooltipped"
                  data-position="left" data-delay="50" data-tooltip="<?= $date ?>"
                  <?php }elseif($r->status == 'outjapan'){?>
                    bgcolor="<?php echo ($mon1 == $x) ? $r->color : ''?>"
                    <?php }?>
                    >
                    <?php
                    if($r->status == 'injapan'){
                      switch($x){
                        case $mon1:
                          echo (date_format(date_create($r->departSched),"m/d/Y") == '11/30/-0001') ? '' : date_format(date_create($r->departSched),"m/d/Y");
                          break;
                        case $mon2:
                          $nextMo = strtotime("+1 months", strtotime($r->date));
                          $nextMo = strftime ( '%Y-%m-%d' , $nextMo );
                          $nextMo = date_format(date_create($nextMo),"m/d/Y");
                          echo (date_format(date_create($r->departSched),"m/d/Y") == '11/30/-0001') ? '' : $nextMo;
                          break;
                        case $mon3:
                          $nextMo = strtotime("+2 months", strtotime($r->date));
                          $nextMo = strftime ( '%Y-%m-%d' , $nextMo );
                          $nextMo = date_format(date_create($nextMo),"m/d/Y");
                          echo (date_format(date_create($r->departSched),"m/d/Y") == '11/30/-0001') ? '' : $nextMo;
                          break;
                        case $mon4:
                          $nextMo = strtotime("+3 months", strtotime($r->date));
                          $nextMo = strftime ( '%Y-%m-%d' , $nextMo );
                          $nextMo = date_format(date_create($nextMo),"m/d/Y");
                          echo (date_format(date_create($r->departSched),"m/d/Y") == '11/30/-0001') ? '' : $nextMo;
                          break;
                        }
                      }elseif($r->status == 'outjapan'){
                        switch($x){
                          case $mon1:
                            echo (date_format(date_create($r->arrivalSched),"m/d/Y") == '11/30/-0001') ? '' : date_format(date_create($r->arrivalSched),"m/d/Y");
                            break;
                          }
                      }
                     ?>
                  </td>

                <?php }?>

              </tr>
              <?php }?>
            <?php }?>
            </tbody>
          </table>
      </div>
  <?php }?>

  <!-- <div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
    <a class="btn-floating btn-large waves-effect waves-light green-dark modal-trigger" href="#addClub"><i class="material-icons">add</i></a>
  </div> -->
</div>


<!-- add club modal -->
<!-- <div id="addClub" class="modal modal-fixed-footer">
<div class="modal-content">
  <h4 class="center-align">新しいクラブを追加</h4>
  <h5 class="center-align">Add New Club</h5>

  <br><br>
  <?php echo form_open('production/add_clubs'); ?>
    <div class="input-field col s12">
      <input id="clubName" name="clubName" type="text" class="validate">
      <label for="clubName">クラブ名 / Club Name</label>
    </div>
    <div class="input-field col s12">
      <input id="capacity" name="capacity" type="text" class="validate">
      <label for="capacity">容量 / Capacity</label>
    </div>
</div>
<div class="modal-footer">
  <button  class="modal-action modal-close waves-effect waves-red btn-flat ">同意しません</button>
  <button  type="submit" class="modal-action modal-close waves-effect waves-green btn-flat ">同意します</button>
</div>
</div> -->
<!-- end add club modal -->

<!-- delete club dialogue -->
<!-- <?php foreach($clubs as $row){?>
<div id="deleteClub-<?php echo $row->clubId?>" class="modal">
  <div class="modal-content">
    <h4 class="center-align">クラブを削除</h4>
    <h5 class="center-align">Delete Club</h5>

    <br><br>
    <?php echo form_open('production/delete_clubs/'.$row->clubId ); ?>
      <input type="hidden" id="clubId" value="<?php echo $row->clubId?>">
      <p>Are you sure you want to delete it?</p>
  </div>
  <div class="modal-footer">
    <button  class="modal-action modal-close waves-effect waves-red btn-flat ">同意しません</button>
    <button  type="submit" class="modal-action modal-close waves-effect waves-green btn-flat ">同意します</button>
  </div>
  <?php echo form_close(); ?>
</div>
<?php }?> -->
<!-- end delete club modal -->

<script>
$(document).ready(function() {
// the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
$('.modal-trigger').leanModal();
});
</script>
