<!-- uploadImages MODAL -->
<div id="uploadImages" class="modal modal-fixed-footer">
  <div class="modal-content">
    <h4 class="center-align">Upload Images</h4>
    <div class="divider"></div>
    <br>
    <div class="row">
      <div class="col s12">
          <?php
            $att = array(
                      'id' => 'uploadImagesForm',
                      'novalidate' => 'novalidate'
                   );
            echo form_open_multipart('administrator/upload_talent_images/'.$talent[0]->talentId, $att);
          ?>
          <div>
            <input type="hidden" name="talentName" value="<?php echo $talent[0]->talentName;?>">
            <!-- IMAGES -->
            <!-- IMAGE 1 -->
            <div class="row">
             <div class="file-field input-field">
                <div class="btn">
                   <span>File</span>
                   <input type="file" name="image1" id="image1" data-error=".image1Error">
                 </div>
                 <div class="file-path-wrapper">
                    <input class="file-path validate" type="text" placeholder="Picture 1" name="def_img1_val" value="<?php echo $talent[0]->pic1?>">
                 </div>
                 <div class="input-field">
                     <div class="image1Error"></div>
                 </div>
              </div>
            </div>
            <!-- IMAGE 2 -->
            <div class="row">
             <div class="file-field input-field">
              <div class="btn">
                 <span>File</span>
                 <input type="file" name="image2" id="image2" data-error=".image2Error">
               </div>
               <div class="file-path-wrapper">
                  <input class="file-path validate" type="text" placeholder="Picture 2" name="def_img2_val" value="<?php echo $talent[0]->pic2?>">
               </div>
               <div class="input-field">
                   <div class="image2Error"></div>
               </div>
              </div>
            </div>
            <!-- IMAGE 3 -->
            <div class="row">
             <div class="file-field input-field">
              <div class="btn">
                 <span>File</span>
                 <input type="file" name="image3" id="image3" data-error=".image3Error">
               </div>
               <div class="file-path-wrapper">
                  <input class="file-path validate" type="text" placeholder="Picture 3" name="def_img3_val" value="<?php echo $talent[0]->pic3?>">
               </div>
               <div class="input-field">
                   <div class="image3Error"></div>
               </div>
              </div>
            </div>
            <!-- IMAGE 4 -->
            <div class="row">
             <div class="file-field input-field">
              <div class="btn">
                 <span>File</span>
                 <input type="file" name="image4" id="image4" data-error=".image4Error">
               </div>
               <div class="file-path-wrapper">
                  <input class="file-path validate" type="text" placeholder="Picture 4" name="def_img4_val" value="<?php echo $talent[0]->pic4?>">
               </div>
               <div class="input-field">
                   <div class="image4Error"></div>
               </div>
              </div>
            </div>
          </div>
      </div>
    </div>
  </div>
  <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-teal btn-flat ">Go Back</a>
      <button type="submit" id="submit-upload-images" id="submit" class="modal-action modal-close waves-effect btn-flat">Upload</button>
  </div>
  <?php echo form_close();?>
</div>
<!-- /. addTalent MODAL -->
