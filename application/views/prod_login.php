
  <style>
    body {
      display: flex;
      min-height: 100vh;
      flex-direction: column;
    }

    main {
      flex: 1 0 auto;
    }

    body {
      background: #fff;
    }

    .input-field input[type=date]:focus + label,
    .input-field input[type=text]:focus + label,
    .input-field input[type=email]:focus + label,
    .input-field input[type=password]:focus + label {
      color: #e91e63;
    }

    .input-field input[type=date]:focus,
    .input-field input[type=text]:focus,
    .input-field input[type=email]:focus,
    .input-field input[type=password]:focus {
      border-bottom: 2px solid #e91e63;
      box-shadow: none;
    }
  </style>

  <?php if($this->session->flashdata('password')){?>
      <script type="text/javascript">
        $(document).ready(function(){
            swal({
              title: "Oops!",
              text:  '<?php echo $this->session->flashdata('password');?>',
              type: "error",
              timer: 2000,
              showConfirmButton: false
            });
        });
      </script>
  <?php }?>

  <nav class="grey darken-3" role="navigation">
    <div class="nav-wrapper container"><a id="logo-container" href="#" class="brand-logo center">Man Group Production</a>
    </div>
  </nav>

  <br>

  <div class="right-align" id="google_translate_element"></div><script type="text/javascript">
  function googleTranslateElementInit() {
    new google.translate.TranslateElement({pageLanguage: 'en', includedLanguages: 'en,ja,tl', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
  }
  </script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
  <div class="section"></div>
  <main>
    <center>
      <!--<img class="responsive-img" style="width: 250px;" src="<?php echo site_url();?>imgs/manpro_logo.gif" />-->
      <div class="section"></div>
      <h5 class="indigo-text">Welcome to Production!</h5>
      <h5 class="indigo-text">Please login into your account</h5>
      <div class="section"></div>

      <div class="container">
        <div class="z-depth-1 grey lighten-4 row" style="display: inline-block; padding: 32px 48px 0px 48px; border: 1px solid #EEE;">
          <div class="col s12">
            <?php echo form_open('prod_login/validate_account'); ?>
            <div class='row'>
              <div class='col s12'>
              </div>
            </div>

            <div class='row'>
              <div class='input-field col s12'>
                <input class='validate' type='text' name='username' id='username' />
                <label for='username'>Enter your username</label>
              </div>
            </div>

            <div class='row'>
              <div class='input-field col s12'>
                <input class='validate' type='password' name='password' id='password' />
                <label for='password'>Enter your password</label>
              </div>
              <label style='float: right;'>
								<a class='pink-text' href='#!'><b>Forgot Password?</b></a>
							</label>
            </div>

            <br />

            <center>
              <!--
              <div class='row'>
                <button type='submit' name='btn_login' class='col s12 btn btn-large waves-effect indigo'>Login</button>
              </div>-->
              <div class='row'>
                <button type="submit" class='col s12 btn btn-large waves-effect indigo'>Login</button>
              </div>
            </center>
            <?php echo form_close(); ?>
          </div>
        </div>
      </div>
      <a href="<?php echo site_url('home')?>">Return home</a>
    </center>

    <div class="section"></div>
    <div class="section"></div>
  </main>
  <footer class="page-footer grey darken-3">
    <div class="footer-copyright center">
      <div class="container">
      Powered by <a class="orange-text" href="#">Tweebs</a>
      </div>
    </div>
  </footer>
