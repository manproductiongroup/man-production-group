  <!--  Scripts-->
  <script type="text/javascript" src="<?php echo base_url('js/materialize.js');?>"></script>
  <script type="text/javascript" src="<?php echo base_url('js/init.js');?>"></script>
  <script type="text/javascript" src="<?php echo base_url('js/datacalls.js');?>"></script>
  <script type="text/javascript" src="<?php echo base_url('js/fullcalendar/gcal.js');?>"></script>
  <script type="text/javascript" src="<?php echo base_url('js/fullcalendar/lang-all.js');?>"></script>
  <script type="text/javascript" src="<?php echo base_url('js/highcharts/js/highcharts.js');?>"></script>
  <script type="text/javascript" src="<?php echo base_url('js/swa/sweetalert-master/dist/sweetalert.min.js');?>"></script>
  <script type="text/javascript" src="<?php echo base_url('js/jsgrid/dist/jsgrid.min.js');?>"></script>
  <script type="text/javascript" src="<?php echo base_url('js/data-tables/js/jquery.dataTables.min.js');?>"></script>
  </body>
</html>
